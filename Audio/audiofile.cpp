
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "audiofile.h"

AudioData::AudioData(SF_INFO f_info, int peak_frames) : time(peak_frames), data(f_info.channels)
{
    finfo = f_info;
    for(auto & it : data) {
        it.resize(peak_frames);
    }
}

AudioData::AudioData(SndfileHandle *handle, int peak_frames) : time(peak_frames), data(handle->channels()) {
    finfo.format = handle->format();
    finfo.frames = handle->frames();
    finfo.channels = handle->channels();
    finfo.samplerate = handle->samplerate();

    for(auto & it : data) {
        it.resize(peak_frames);
    }


}

AudioData::~AudioData() {}

AudioFile::AudioFile(std::string filepath) : path(filepath), peakpath(filepath + ".peak"), valid(false)
{
    size_t found=path.find_last_of("/");
    std::string filename=path.substr(found+1,path.size());
    found=filename.find_last_of(".");
    std::string extension=filename.substr(found,filename.size());

    const std::vector<std::string> possible_extensions={".wav",".WAV",".aif"};
    for(std::size_t i=0;i<possible_extensions.size();i++) {
        if(std::regex_search(extension,std::regex(possible_extensions[i]))) {
            valid=true;
            break;
        }
    }
}

AudioFile::~AudioFile()
{
}

// I should add some other soundfile types, or check real Audio file with sndfile
bool AudioFile::IsAudioFile(std::string path)
{
    size_t found = path.find_last_of("/");
    std::string filename =path.substr(found+1, path.size());
    found=filename.find_last_of(".");
    std::string extension = filename.substr(found,filename.size());

   // std::array<std::string,4> audio_extensions = {".wav",".WAV",".aif",".aiff"};
    std::vector<std::string> audio_extensions={".wav",".WAV",".aif"};
    for(std::size_t i = 0; i< audio_extensions.size() ; i++) {
        if(std::regex_search(extension, std::regex(audio_extensions[i]))) {
            return true;
        }
    }
    return false;
}

bool AudioFile::isValid() {return valid;}


SF_INFO AudioFile::GetFileInfo(std::string path)
{
    SF_INFO finfo;
    finfo.format=0;
    SNDFILE *infile = sf_open(path.c_str(), SFM_READ, &finfo);
    sf_close(infile);
    return finfo;
}


SF_INFO AudioFile::getFileInfo()
{
    return AudioFile::GetFileInfo(this->path);
}

/*

void AudioFile::generatePeakFile(std::string path, std::string peakpath)
{
    FILE *peakstream=fopen(peakpath.c_str(),"w");
    fprintf(peakstream,"#PEAKFILE#  DATA \n");

    SF_INFO finfo;
    finfo.format=0;
    SNDFILE *infile=sf_open(path.c_str(),SFM_READ,&finfo);
    sf_count_t bsize=512;

    //float buf[bsize*finfo.channels];
    std::vector<float> buf;
    buf.resize(bsize * finfo.channels);
    sf_count_t rcnt;
    int ncnt=0; // rest_coun2t
    int total_count = 0;

    do {
        rcnt=sf_readf_float(infile,buf.data(),bsize);
            for(int k=0;k<rcnt;k+=32) {
                for(int m=0;m<finfo.channels;m++) {
                    fprintf(peakstream,"%f\t%12.10f\n", (double)(double)(( (ncnt) /(double)finfo.samplerate)) * 16.0 ,buf[k*finfo.channels+m]);
                    //fprintf(peakstream, "%d\t%12.10f\n", total_count, buf[k*finfo.channels+m]);
                    ncnt++;
                    total_count += 32;
                }
            }
    } while(rcnt>0);

    sf_close(infile);
    fclose(peakstream);


}

void AudioFile::generatePeakFile() {
    FILE *peakstream=fopen(peakpath.c_str(),"w");
    fprintf(peakstream,"#PEAKFILE#  DATA \n");

    SF_INFO finfo;
    finfo.format=0;
    SNDFILE *infile=sf_open(path.c_str(),SFM_READ,&finfo);
    sf_count_t bsize=512;

    //float buf[bsize*finfo.channels];
    std::vector<float> buf;
    buf.resize(bsize * finfo.channels);
    sf_count_t rcnt;
    int ncnt=0; // rest_coun2t
    int total_count = 0;

    do {
        rcnt=sf_readf_float(infile,buf.data(),bsize);
            for(int k=0;k<rcnt;k+=32) {
                for(int m=0;m<finfo.channels;m++) {
                    fprintf(peakstream,"%f\t%12.10f\n", (double)(double)(( (ncnt) /(double)finfo.samplerate)) * 16.0 ,buf[k*finfo.channels+m]);
                    //fprintf(peakstream, "%d\t%12.10f\n", total_count, buf[k*finfo.channels+m]);
                    ncnt++;
                    total_count += 32;
                }
            }
    } while(rcnt>0);

    sf_close(infile);
    fclose(peakstream);

    finishPeakFileTask(peakpath);
}

*/

void AudioFile::generatePeakFile()
{
    AudioFile::generatePeakFile(path, peakpath);
}


void AudioFile::generatePeakFile(std::string p, std::string peakp)
{
    SndfileHandle sfile(p, SFM_READ);
    std::ofstream ofs(peakp);
    ofs << "#PEAKFILE#  DATA \n";
    sf_count_t bsize = 512;

    std::vector<float> buf;
    buf.resize(bsize * sfile.channels());
    sf_count_t rcnt;
    int ncnt = 0;
    int total_count = 0;


    do {
        rcnt = sfile.readf(buf.data(), bsize);
        for(int k = 0; k < rcnt; k += 32) {
            for(int ch = 0; ch < sfile.channels(); ch++) {
                ofs << (double)(double)(( (ncnt) /(double)sfile.samplerate())) * 16.0 << "\t" << buf[k * sfile.channels()  + ch] << "\n";
            }
                ncnt++;
                total_count += 32;
        }
    } while(rcnt > 0);
    ofs.close();
}

AudioData *AudioFile::getPeakSamples(std::string soundfile, int number_of_peaks)
{
    SndfileHandle sfile(soundfile, SFM_READ);
    std::vector<double> buf(sfile.channels());
    buf.resize(sfile.channels());
    double inc = (double)sfile.frames() / number_of_peaks;
    AudioData *ad = new AudioData(&sfile, number_of_peaks);

    int frame_count = 0;
    int ncnt = 0;
    for(double i = 0.0; i <sfile.frames(); i+= inc) {
        sfile.seek(int(i),SF_SEEK_SET);
        sf_count_t readf = sfile.readf(buf.data(), 1);
        std::cout << "read : " << readf << std::endl;

        ad->time[frame_count] = ( (double)(double) (( (ncnt) / (double) sfile.samplerate())) * (double)inc/sfile.channels());
        for(int ch = 0; ch < sfile.channels(); ch++) {
            ad->data[ch][frame_count] = buf[ch];
            std::cout << "buf : " << buf[ch] << std::endl;
        }

        ncnt += sfile.channels();
        frame_count++;
    }

    return ad;


}

void AudioFile::write(double *data, int size)
{
    SF_INFO sfinfo;
    sfinfo.channels = 1;
    sfinfo.samplerate = size;
    sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_24;

    SNDFILE *outfile = sf_open(path.c_str(), SFM_WRITE, &sfinfo);

    sf_count_t count = sf_write_double(outfile, &data[0], size);
    sf_write_sync(outfile);
    sf_close(outfile);

    this->generatePeakFile();

}
