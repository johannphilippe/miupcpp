/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef AUDIOFILE_H
#define AUDIOFILE_H

#include<iostream>
#include<regex>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>

#include"sndfile.h"
#include"sndfile.hh"

#include"Dependencies/lsignal/lsignal.h"
#include<math.h>



// Data and description of an audiofile or an audio content
struct AudioData
{
    AudioData(SF_INFO f_info, int peak_frames);
    AudioData(SndfileHandle *handle, int peak_frames);
    ~AudioData();

    SF_INFO finfo;
    std::vector<double> time;
    std::vector<std::vector<double>> data;
};

// Audio file class, allowing to read/write and generate peak files
class AudioFile
{
public:
    AudioFile(std::string filepath);
    ~AudioFile();

    lsignal::signal<void(std::string)> finishPeakFileTask;

    // is it a valid soundfile ?
    bool isValid();

    // Generates peak data, discrete downsampling of the soundfile
    //void generatePeakFile();
    //static void generatePeakFile(std::string path, std::string peakpath);

    void generatePeakFile();
    static void generatePeakFile(std::string path, std::string peakpath);



    static AudioData *getPeakSamples(std::string soundfile, int number_of_peaks);

    // write to audiofile
    void write(double *data, int size);


    SF_INFO getFileInfo();

    static bool IsAudioFile(std::string path);
    static SF_INFO GetFileInfo(std::string path);

private:
    std::string path,peakpath;
    bool valid;


    //if reading driven from outside
    FILE *pstream;
    SF_INFO finfo_;
    SNDFILE *infile_;
    sf_count_t blocksize, rcnt_;
    int ncnt_;
    int total_count_;
    //float *buff;
    //std::vector<float>buff;

    int peak_downsamp;

protected:

};

#endif // AUDIOFILE_H
