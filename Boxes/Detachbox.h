#ifndef DETACHBOX_H
#define DETACHBOX_H
#include"Boxes/vbox.h"
#include"Dialogs/dialog.h"

// This is a homemade Detachbox, since the original one is crashing the app.
class Detachbox : public Vbox {

public:

    Detachbox() : Vbox() {}
    Detachbox(Ihandle *ih) : Vbox(ih) {
        is_mapped = true;
        child = ih;
    }

    template<typename T> Detachbox(T *o): Vbox(o) {
        is_mapped = true;
        child=  o->getObj();
    }
    virtual ~Detachbox() {}
    lsignal::signal<void(bool)>detachBoxSig;


    void detach_box() {
        this->detach(child);
        parent = new Dialog(child);
        parent->setAttr("EXPAND","BOTH");
        parent->show();
        fill = new Fill();
        this->push(fill);
    }

    void restore_box() {
        this->detach(getChild(0));
        parent->hide();
        IupDetach(child);
        this->push(child);
        delete parent;
        delete fill;
    }

protected:

private:

    Fill *fill;
    Ihandle * child;
    Dialog *parent;
};

#endif // DETACHBOX_H
