/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef BOX
#define BOX

//Base class for Hbox,Vbox, Zbox,Menu

#include"Core/miupobj.h"
#include<cstdarg>
#include"utils/miuputil.h"
#include"utils/miupdatatype.h"
#include<typeinfo>



/** Base container class **/

class Box : public MiupObj {
public:

    //ctor
    Box() : is_mapped(false) {}

    //dtor
    ~Box() {
        IupDestroy(obj);
    }


    /** Basic methods for management **/
    int getChildCount() {
        return IupGetChildCount(obj);
    }

    int getChildPos(MiupObj *child) { return IupGetChildPos(obj,child->getObj());}
    template<typename T>int getChildPos(std::shared_ptr<T> child) { return IupGetChildPos(obj, child.get()->getObj());}

   Ihandle* getChild(int pos) {return IupGetChild(obj, pos);}

   //overrides of miupobj methods to get children with it.
   void update() override {IupUpdateChildren(obj); }
   void redraw() override {IupRedraw(obj,1);}
   void refresh() override {IupRefreshChildren(obj);}

   Ihandle * getObj() override {
       if(!is_mapped) construct();
       return obj;
   }

   //special container methods

    /** Equivalent to Ihandle * type - MiupObj only contains a Ihandle ptr  **/
    //methods

   void push(Ihandle*ih) {
       if(!is_mapped) {
           construct();
       }
       append_child(ih);
   }

   void push(Ihandle * ih, Ihandle *objs...) {
       if(!is_mapped) {
           construct();
       }
       append_child(ih);
       this->push(objs);
   }



    /** Main method(s) to insert into Boxes. This is to be used when you really handle objects that you pass here by reference. **/


   void push(MiupObj *o) {
       if(!is_mapped) {
           construct();
       }
       append_child(o->getObj());
       //IupMap(o->getObj());
       //IupRefresh(o->getObj());
   }

  void push(MiupObj *o, MiupObj *objs...) {
      if(!is_mapped) {
          construct();
      }
      append_child(o->getObj());
      this->push(objs);
  }

    template<typename T>
    void push(T *o) {
        if(!is_mapped) {
            construct();
        }
        append_child(o->getObj());
    }

    template<typename T, typename ...Args>
    void push(T *o, Args*...objs) {
        if(!is_mapped) {
            construct();
        }
        append_child(o->getObj());
        this->push(objs...);
    }


    /** Useful for dynamic allocation without ownership management. For example, it can be used to create items in a loop **/

    template<typename T>
    void push(std::shared_ptr<T> o) {
        if(!is_mapped) {
            construct();
        }
        listPtr.push_back(o);
        append_child(o.get()->getObj());
        //IupMap(o.get()->getObj());
        //IupRefresh(o.get()->getObj());
    }

    template<typename T, typename ... Args>
    void push(std::shared_ptr<T> o, Args ... objs ) {
        if(!is_mapped) {
            construct();
        }
        listPtr.push_back(o);
        append_child(o.get()->getObj());
        this->push(objs...);
    }

    void clear() {
        listPtr.clear();
    }


    //More low level, doesn't map and refresh container after pushing.
    void append(MiupObj *o) {IupAppend(obj,o->getObj());}
    void insert(MiupObj* o, MiupObj *ref_o) {IupInsert(obj,ref_o->getObj(),o->getObj());}
    void detach(MiupObj *o) {IupDetach(o->getObj());}
    void detach(Ihandle *o) {IupDetach(o);}



    template<typename T> void append(std::shared_ptr<T> o) {
        listPtr.push_back(o);
        IupAppend(obj,listPtr.back().get()->getObj());
    }

    template<typename T> void insert(std::shared_ptr<T> o, MiupObj *ref_o) {
        int position = getChildPos(ref_o);
        if(position < getChildCount()) {
                listPtr.insert(listPtr.at(position), o);
                IupInsert(obj, ref_o->getObj(), o.get()->getObj());
        } else {
            std::cout  << "Cannot insert element at this position" << std::endl;
        }
    }


protected:
   std::vector< std::shared_ptr<MiupObj> > listPtr;
    bool is_mapped;
    virtual void construct() {}


private:

    void append_child(Ihandle *ih) {
        IupAppend(obj,ih);
        IupMap(ih);
        IupRefresh(ih);
    }

    void insert_child(Ihandle *ih,Ihandle *ref) {
        IupInsert(obj,ref,ih);
    }
};


class Fill : public MiupObj
{
public:
    Fill() {
        obj=IupFill();
    }

    Fill(int width, int height) {
        obj = IupFill();
        setSize(width, height);
    }

    virtual ~Fill() {
    }

private:
};

class Space : public MiupObj
{
public:
    Space() : MiupObj(IupSpace()) {

    }
    ~Space() {}
};

#endif // BOX_H
