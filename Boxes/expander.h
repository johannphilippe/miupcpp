
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef EXPANDER
#define EXPANDER
#include"Boxes/box.h"

class Expander : public Box
{
public:
    Expander() : Box() {}
    Expander(Ihandle *ih) : Box() {
        is_mapped=true;
        obj=IupExpander(ih);
        init();
    }

    template<typename T> Expander(T *o) {
        is_mapped=true;
        obj=IupExpander(o->getObj());
        init();
    }

    template<typename T> Expander(std::shared_ptr<T> o) {
    }


    virtual ~Expander() {}

    lsignal::signal<void(int)>emitStateChangedSig;

protected:
    virtual IUP_CLASS_DECLARECALLBACK_IFni(Expander,openclose_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(Expander,action_cb)

private:
    bool is_open;

    void init() {
        IUP_CLASS_INITCALLBACK(obj,Expander);
        IUP_CLASS_SETCALLBACK(obj,"OPENCLOSE_CB",openclose_cb);
        IUP_CLASS_SETCALLBACK(obj,"ACTION",action_cb);
    }
};

inline int Expander::openclose_cb(Ihandle *ih, int state)
{
    return IUP_CLOSE;
}

inline int Expander::action_cb(Ihandle *ih)
{
   emitStateChangedSig(getAttrStr("STATE")==std::string("OPEN") ? 1 : 0);
   return IUP_CLOSE;
}

#endif // EXPANDER

