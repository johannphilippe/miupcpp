
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef FRAME
#define FRAME

#include"box.h"
//Not the same implementation as Hbox, Vbox because IUP doesn't allow multiple child in constructor for frame, so a bit more tricky here
class Frame : public Box
{
public:
    Frame() : Box() {}
    Frame(Ihandle *ih) : Box() {construct();push(ih);}
    Frame(Ihandle *ih,Ihandle *objs... ) : Box() {construct();push(ih,objs);}
    template<typename T> Frame(T *o) : Box() {construct();push(o);}
    template<typename T,typename...Args> Frame(T *o,Args...objs) : Box() {construct();push(o,objs...);}

    template<typename T> Frame(std::shared_ptr<T> o) {construct(); push(o);}
    template<typename T, typename ... Args> Frame(std::shared_ptr<T> o, Args ... objs) {construct(); push(o, objs...);}

    ~Frame() {}

protected:
    void construct() override {
        if(!is_mapped)
        {
            is_mapped=true;
            obj=IupFrame(NULL);
        }
    }
private:
};

#endif // FRAME_H
