
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef MENU
#define MENU

#include"Boxes/box.h"

class Menu : public Box
{
public:
    Menu() : Box(), selected_item("NULL") {}
    Menu(Ihandle *ih) : Box(), selected_item("NULL") {construct();push(ih);}
    Menu(Ihandle *ih,Ihandle *objs...) : Box() , selected_item("NULL") {construct();push(ih,objs);}
    template<typename T> Menu(T *o) : Box() , selected_item("NULL") {construct();push(o);}
    template<typename T,typename...Args> Menu(T *o,Args*...objs) : Box(), selected_item("NULL")  {construct(),push(o,objs...);}
    ~Menu() {}

    void setSelectedItem(const char *value) {
        selected_item=std::string(value);
    }

    void popup(int x, int y) {IupPopup(obj, x,y);}

    lsignal::signal<void(std::string item)> menuCloseSig;

protected:
    void construct() override {
        if(!is_mapped)
        {
            is_mapped=true;
            obj=IupMenu(NULL);
             initCallbacks();
        }
    }
    virtual IUP_CLASS_DECLARECALLBACK_IFn(Menu, menuclose_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(Menu, destroy_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(Menu, open_cb)

private:

    std::string selected_item;

    void initCallbacks() {
       IUP_CLASS_INITCALLBACK(obj, Menu);
       IUP_CLASS_SETCALLBACK(obj, "MENUCLOSE_CB", menuclose_cb);
       IUP_CLASS_SETCALLBACK(obj,"DESTROY_CB",destroy_cb);
       IUP_CLASS_SETCALLBACK(obj,"OPEN_CB",open_cb);
    }
};

inline int Menu::menuclose_cb(Ihandle * /*ih*/)
{
    std::cout << " menu close !" << std::endl;
    menuCloseSig(selected_item);
    return IUP_CLOSE;
}

inline int Menu::destroy_cb(Ihandle * /*ih*/)
{
    std::cout << "menu destroyed" << std::endl;
    return IUP_CLOSE;
}

inline int Menu::open_cb(Ihandle * /*ih*/)
{
    std::cout << " open menu" << std::endl;
    //return IUP_DEFAULT;
    return IUP_CLOSE;
}


#endif // MENU




#ifndef SUBMENU
#define SUBMENU

class SubMenu : public Box
{
public:
    SubMenu() : Box() {}
    SubMenu(const char *name, Ihandle *ih)
    {
        is_mapped=true;
        obj=IupSubmenu(name,ih);
    }

    template<typename T>
    SubMenu(const char *name,T *o) {
        is_mapped=true;
        obj=IupSubmenu(name,o->getObj());
    }


    ~SubMenu() {}

protected:
private:
    std::string title;
};

#endif // SUBMENU








#ifndef SEPARATOR
#define SEPARATOR

class Separator : public MiupObj {
public:
    Separator() {
        obj=IupSeparator();
    }
    ~Separator() {}
};

#endif // SEPARATOR








#ifndef ITEM
#define ITEM

class Item : public MiupObj {
public:
    Item(const char *name="Item") : item_name(name) {
        obj=IupItem(name,NULL);
        initCallback();
    }
    ~Item() {}

        lsignal::signal<void(const char *)> itemSelectedSig;
        lsignal::signal<void()> clickItemSig;
protected:
        virtual IUP_CLASS_DECLARECALLBACK_IFn(Item,action)
private:
        void initCallback() {
                IUP_CLASS_INITCALLBACK(obj, Item);
                IUP_CLASS_SETCALLBACK(obj,"ACTION",action);
        }

        std::string item_name;
};

inline int Item::action(Ihandle *ih)
{
    itemSelectedSig(item_name.c_str());
    clickItemSig();
    std::cout << " action " << std::endl;
    return IUP_CLOSE;
    //return IUP_IGNORE;
}


#endif // ITEM





