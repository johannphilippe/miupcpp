#include "radio.h"

Ihandle *Radio::getObj() {
    if(! is_mapped) {
        is_mapped=true;
        switch(orientation) {
        case Miup::Orientation::Horizontal :
        {
            Hbox *hbx=new Hbox();
            for(int i=0;i<listObj.size();i++) {
                hbx->push(listObj.at(i));
            }
            obj=IupRadio(hbx->getObj());
        }
            break;
        case Miup::Orientation::Vertical :
        {
            Vbox *vbx=new Vbox();
            for (int i=0;i<listObj.size();i++) {
                vbx->push(listObj.at(i));
            }
            obj=IupRadio(vbx->getObj());
        }
            break;
        }
    }

    return obj;
}
