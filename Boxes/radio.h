
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef RADIO
#define RADIO

#include"box.h"
#include"vbox.h"
#include"hbox.h"
#include"Core/miup.h"

class Radio : public Box
{
public:
    Radio() = default; //: Box (), orientation(Miup::Orientation::Horizontal) {}
    Radio(Ihandle *o) : Box(),orientation(Miup::Orientation::Horizontal) {construct(); push(o);}
    Radio(MiupObj *o) : Box(), orientation(Miup::Orientation::Horizontal) {construct();push(o);}
    template<typename T> Radio(T *o) : Box(),orientation(Miup::Orientation::Horizontal) {construct(); push(o);}

    ~Radio() {}
protected:
    void construct() override {
        if(!is_mapped)
        {
            is_mapped=true;
            obj=IupRadio(NULL);
            setAttr("ORIENTATION", (orientation == Miup::Orientation::Horizontal) ? "HORIZONTAL"  : "VERTICAL" );
        }
    }
private:
    Miup::Orientation  orientation;
};

#endif // RADIO_H
