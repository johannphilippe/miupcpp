#include "scrollbox.h"

ScrollBox::~ScrollBox()
{

}

void ScrollBox::construct()
{
    if(!is_mapped && listObj.size()>0) {
        is_mapped=true;
        obj=IupScrollBox(listObj.at(0));
        for(int i=1;i<listObj.size();i++)
            push(listObj.at(i));
    }
}
