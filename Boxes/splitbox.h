
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef SPLITBOX
#define SPLITBOX
#include"Boxes/box.h"
//not really in the same design diagram as other containers
class SplitBox  : public Box
{
public:
    SplitBox(Ihandle *ih1,Ihandle *ih2) {
        is_mapped=true;
        obj=IupSplit(ih1,ih2);
    }

    template<typename T,typename T2>
    SplitBox(T obj1,T2 obj2) {
        is_mapped=true;
        obj=IupSplit(obj1->getObj(),obj2->getObj());
    }



    virtual ~SplitBox() {}


protected:
    IUP_CLASS_DECLARECALLBACK_IFn(SplitBox,map_cb)
private:
    void init() {
        IUP_CLASS_INITCALLBACK(obj,SplitBox);
        IUP_CLASS_SETCALLBACK(obj,"MAP_CB",map_cb);
    }
};

int SplitBox::map_cb(Ihandle *ih)
{
    setAttr("EXPANDCHILDREN","YES");
    return IUP_DEFAULT;
}

#endif // SPLITBOX

