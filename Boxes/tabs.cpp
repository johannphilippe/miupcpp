#include "tabs.h"

Tabs::~Tabs() {}

void Tabs::construct()
{
    if(! is_mapped && listObj.size()>0)
    {
        is_mapped=true;
        obj=IupTabs(listObj.at(0));
        for(int i=1;i<listObj.size();i++)
            push(listObj.at(i));
    }
}
