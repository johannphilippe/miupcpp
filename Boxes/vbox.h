
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef VBOX
#define VBOX

#include"box.h"
#include"stdio.h"
#include"stdlib.h"

class Vbox : public Box
{
public:

    Vbox() = default;
    Vbox(Ihandle *ih) : Box() {construct();push(ih);}
    Vbox(Ihandle *ih,Ihandle *objs...) : Box() {construct();push(ih,objs);}
    template<typename T> Vbox(T *o) : Box() {construct();push(o);}
    template<typename T,typename...Args> Vbox(T *o,Args*...objs) : Box() {construct(); push(o,objs...);}
    ~Vbox() {}
protected:
    void construct() override {
        if(!is_mapped)
        {
            is_mapped=true;
            obj=IupVbox(NULL);
        }
    }

private:


};

#endif // VBOX_H
