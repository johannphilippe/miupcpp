
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef CANVAS
#define CANVAS

#include"iup.h"
#include"cd.h"
#include"cdiup.h"
#include<iostream>
#include<forward_list>
class Canvas
{
public:
    Canvas(Ihandle *ih) {
        canvas=cdCreateCanvas(CD_IUP,ih);
    }
    
    template<typename T>
    Canvas(T obj) {
        canvas=cdCreateCanvas(CD_IUP,obj->getObj());
    }

    virtual ~Canvas() {
       // cdReleaseState(state);
        cdKillCanvas(canvas);
    }

    //base
    int activate() {return cdCanvasActivate(canvas);}
    void deactivate() {cdCanvasDeactivate(canvas);}
    void clear() {cdCanvasClear(canvas);}
    void flush() {cdCanvasFlush(canvas);}

    //state
    void saveState() {state=cdCanvasSaveState(canvas);}
    void restoreState() {cdCanvasRestoreState(canvas,state);}
    void releaseState() {cdReleaseState(state);}

    //attributes
    void setAttr(const char *name, char *data) {cdCanvasSetAttribute(canvas,name,data);}
    const char *getAttr(const char *name) {return cdCanvasGetAttribute(canvas,name);}

    //colors
    int setForegroundColor(int r,int g, int b) { return cdCanvasForeground(canvas,cdEncodeColor(r,g,b));}
    int setForegroundColor(long color) {return cdCanvasForeground(canvas,color);}
    int setBackgroundColor(int r, int g, int b) {return cdCanvasBackground(canvas,cdEncodeColor(r,g,b));}
    int setBackgroundColor(long color) {return cdCanvasBackground(canvas,color);}

    //complex -- region
    void begin(int mode) {cdCanvasBegin(canvas,mode);}
    void pathSet(int action) {cdCanvasPathSet(canvas,action);}
    void vertex(int x, int y) {cdCanvasVertex(canvas,x,y);}
    void end() {cdCanvasEnd(canvas);}

    //region
    int regionCombineMode(int mode) {cdCanvasRegionCombineMode(canvas,mode);}
    bool isPointInRegion(int x,int y) {
        if(cdCanvasIsPointInRegion(canvas,x,y)!=0) return true;
        return false;
    }

    int setInteriorStyle(int style) {return cdCanvasInteriorStyle(canvas, style);}

    void regionOffset(int dx,int dy) {cdCanvasOffsetRegion(canvas,dx,dy);}
    void getRegionBox(int *xmin,int *xmax,int *ymin,int *ymax) {cdCanvasGetRegionBox(canvas,xmin,xmax,ymin,ymax);}

    std::pair<int, int> getOrigin() {
        std::pair<int, int> coord;
        cdCanvasGetOrigin(canvas, &coord.first, &coord.second);
        return coord;
    }



    //marks
    void mark(int x, int y) {cdCanvasMark(canvas,x,y);}
    void pixel(int x,int y,long color) {cdCanvasPixel(canvas,x,y,color);}

    void markType(int type) {cdCanvasMarkType(canvas,type);}
    void markSize(int sizeInPixel) {cdCanvasMarkSize(canvas,sizeInPixel);}

    //lines
    void line(int x1,int y1,int x2,int y2) {cdCanvasLine(canvas,x1,y1,x2,y2);}
    void lineStyle(int style) {cdCanvasLineStyle(canvas,style);}
    //C way and C++ way
    void lineStyleDashes(const int *dashes, int count) {cdCanvasLineStyleDashes(canvas,dashes,count);}
    void lineStyleDashes(std::forward_list<int> dashes) {
        int count=std::distance(dashes.begin(),dashes.end());
        //int arr[count];

        std::vector<int> arr;
        arr.resize(count);

        int cnt=0;
        for(std::forward_list<int>::iterator it=dashes.begin();it!=dashes.end();++it) {
            arr[cnt]=*it;
            cnt++;
        }
        cdCanvasLineStyleDashes(canvas,arr.data(),count);
    }
    int lineWidth(int w) {cdCanvasLineWidth(canvas,w);}
    int lineJoin(int style) {cdCanvasLineJoin(canvas,style);}
    int lineCap(int style) {cdCanvasLineCap(canvas,style);}

    //empty areas
    void rect(int xmin,int xmax,int ymin,int ymax) {cdCanvasRect(canvas,xmin,xmax,ymin,ymax);}
    void arc(int xc,int yc,int w,int h, double angle1, double angle2) {cdCanvasArc(canvas,xc,yc,w,h,angle1,angle2);}

    //filled areas
    void box(int xmin,int xmax,int ymin,int ymax) {cdCanvasBox(canvas,xmin,xmax,ymin,ymax);}
    void sector(int xc,int yc,int w, int h, double angle1, double angle2) {cdCanvasSector(canvas,xc,yc,w,h,angle1, angle2);}
    void chord(int xc, int yc,int w, int h, double angle1, double angle2) {cdCanvasChord(canvas,xc,yc,w,h,angle1,angle2);}

    void fbox(double xmin, double xmax, double ymin, double ymax) {cdfCanvasBox(canvas, xmin, xmax, ymin ,ymax);}

    //attributes of filled areas
    int backgroundOpacity(int opacity) {return cdCanvasBackOpacity(canvas,opacity);}
    int fillMode(int mode) {return cdCanvasFillMode(canvas,mode);}
    int interiorStyle(int style) {return cdCanvasInteriorStyle(canvas,style);}
    int hatch(int style) {return cdCanvasHatch(canvas,style);}
    //pattern
    void pattern(int w, int h, const long int *color) {cdCanvasPattern(canvas,w,h,color);}
    long int *getPattern(int *w, int *h) {return cdCanvasGetPattern(canvas,w,h);}
    //stipple - pointillés
    void stipple(int w, int h,const unsigned char *fgbg) {cdCanvasStipple(canvas,w,h,fgbg);}
    unsigned char *getStipple(int *w, int *h) {return cdCanvasGetStipple(canvas,w,h);}


    //size
    std::pair<int,int> getSizeInPixel() {
        cdCanvasGetSize(canvas,&width,&height,&mwidth,&mheight);
        return std::make_pair(width,height);
    }

    int getWidthInPixel() {
        cdCanvasGetSize(canvas,&width,&height,&mwidth,&mheight);
        return width;
    }
    int getHeightInPixel() {
        cdCanvasGetSize(canvas,&width,&height,&mwidth,&mheight);
        return height;
    }

    cdCanvas *getCanvas() {return canvas;}

    Canvas operator=(const Canvas *&c) {
    }

protected:
    cdCanvas *canvas;
    cdState *state;
private:
    int width,height, xpos,ypos;
    double mwidth,mheight;
};


#endif // CANVAS

