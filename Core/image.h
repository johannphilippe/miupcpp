
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef IMAGE_H
#define IMAGE_H

#include"iup.h"
#include"im.h"
#include"im_image.h"
#include"iupim.h"
#include"miupobj.h"

#include<iostream>

class Image : public MiupObj {
public:
    Image() {
    }

    Image(std::string path) : MiupObj(IupLoadImage(path.c_str())){}
    Image(std::string path, std::string handleName) : MiupObj(IupLoadImage(path.c_str())) {
        setHandle(handleName.c_str());
    }

    //load image from source file C or Header
    Image(int width, int height, const unsigned char *pixels) {
        IupImage(width,height, pixels);
    }

    Image(int width, int height, const unsigned char *pixels, std::string handleName) : Image(width, height, pixels) {
        setHandle(handleName.c_str());
    }

    Image(Ihandle *o) : MiupObj(o) {}
    Image(Ihandle *o, std::string handleName) : MiupObj(o) {setHandle(handleName.c_str());}

    ~Image() {
    }

    int save(std::string path, std::string format){
        return IupSaveImage(obj, path.c_str(), format.c_str());
    }

    void load(std::string path) {
        IupLoadImage(path.c_str());
    }

    static Image Load(std::string path) {
        return Image(path);
    }

private:
};

#endif // IMAGE_H
