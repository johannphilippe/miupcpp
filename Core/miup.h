
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef MIUP
#define MIUP

#include"iup.h"
#include"iupcontrols.h"
#include"iup_plot.h"
#include"iupgl.h"
#include"iup_scintilla.h"
#include"iupweb.h"

#include<iostream>
#include"utils/miuputil.h"
#include"utils/miupdatatype.h"
#include<functional>
#include"utils/miuptimer.h"

namespace {
    static lsignal::signal<void()> eventLoopSig;
class Miup {
public:
    //Global Enums
    enum ModeList {
        native,
        flat,
        opengl
    };

    enum Theme {
        basic,
        dark,
        red
    };

    enum Orientation {
        Vertical,
        Horizontal
    };

    //Global Variables
    constexpr static const char *dark_bg="10 10 10";
    constexpr static const char *dark_red="100 0 0";
    constexpr static const char *light_blue="154 185 215";
    constexpr static const char *light_green="148 228 152";

    constexpr static int EVENT_Q_MAX_SIZE = 256;
    constexpr static int EVENT_Q_BLOCK = 64;

   //Global Methods
    static void Init(ModeList mode=native, int argc=NULL, char *argv[]=NULL)
    {
           //Create a resource directory
           //std::cout << "Create directory " << MiupUtil::filesystem::createDirectory("res") << std::endl;

           //Open IUP libs
            IupOpen(&argc,&argv);
            IupControlsOpen();
            IupPlotOpen();
            IupImageLibOpen();
            IupScintillaOpen();
            IupWebBrowserOpen();
            Mode=mode;
            isOpen=true;

    }

    static void Close()
    {
            isOpen=false;
            IupClose();
    }


    static void StartIdle() {

        /*
        delete timer;
        timer = new MiupTimer(30);
        //timer->timerSig.connect(&Miup::processQueue);
        timer->timerSig.connect([&]() {
            int idx = 0;
       while(cbk_q.try_pop(current_cbk)    ) {
           current_cbk->method(current_cbk->value);
           idx ++;
       }
       for(auto &it : timer_cbk) {
           it();
       }
    });
        timer->start();

        */
        regular_timer->setDuration(30);

    }


    static void StopIdle() {
        regular_timer->setDuration(100);
        /*
        timer->stop();
        timer->timerSig.disconnect_all();
        timer_cbk.clear();
        */

    }

    static void SetIdleRefreshTime(int ms) {
        if(timer->isRunning()) {timer->setDuration(ms);}
    }

    static void processQueue() {
       //std::cout << "Process queue - size : " << cbk_q.size()  << std::endl;
       while(cbk_q.try_pop(current_cbk)) {
          // std::cout << "TRY POP SUCCEDED" << std::endl;
           current_cbk->method(current_cbk->value);
           delete current_cbk;
       }
    }

    static int action_cb(Ihandle *ih) {
       while(cbk_q.try_pop(current_cbk)) {
           current_cbk->method(current_cbk->value);
           delete current_cbk;
       }
        return IUP_DEFAULT;
    }

    static void clearQueue() {
        while(cbk_q.try_pop(current_cbk)) {
            delete current_cbk;
        }
    }


    static void processCallbackMap()
    {
        callback_map.callAll();
    }

    static void MainLoop()
    {

       regular_timer = new MiupTimer(100);
       //regular_timer->timerSig.connect(&Miup::processQueue);
       regular_timer->timerSig.connect(&Miup::processCallbackMap);
       regular_timer->start();

        while(isOpen) {
            IupLoopStepWait() ;
                //std::cout << "Process queue - size : " << cbk_q.size()  << std::endl;
                if(cbk_q.try_pop(current_cbk)) {
                   current_cbk->method(current_cbk->value);
                   delete current_cbk;
               }
        }
    }

    static void MainLoopTimerBased(int ms) {
        delete regular_timer;
        regular_timer = new MiupTimer(ms);
        regular_timer->timerSig.connect([&](){
           IupLoopStep();
           if(cbk_q.try_pop(current_cbk)) {
               current_cbk->method(current_cbk->value);
               delete current_cbk;
           }
        });
        regular_timer->start();
    }


    static void MainLoopNoWait() {
        while(isOpen) {
            //IupLoopStep();
            IupFlush();
            if(cbk_q.try_pop(current_cbk)) {
                current_cbk->method(current_cbk->value);
                delete current_cbk;
            }
        }

    }

    static void pushCallbackMethod(double val,std::function<void(double)> f) {
        //if(cbk_q.size() >= EVENT_Q_MAX_SIZE) return;
        cbk_q.push(new callback_method_queue_data(val,f));
       // if (cbk_q.size() > EVENT_Q_MAX_SIZE) cbk_q.try_pop(current_cbk);
       // std::cout << "queue size " << cbk_q.size() << std::endl;
    }


    static void pushTimerCallback(std::function<void(void)>method)
    {
        timer_cbk.push_back(method);
    }

    static void StopLoop()
    {
        isOpen=false;
    }

    static void flush() {IupFlush();}
    static void setFocus(MiupObj *o) {IupSetFocus(o->getObj());}
    static MiupObj getFocus() {return IupGetFocus();}



    static void registerCallback(CallbackData data)
    {
        callback_map.registerCallback(data);
    }

    static void unregisterCallback(std::string name)
    {
        callback_map.unregisterCallback(name);
    }



protected:

private:
    static Ihandle *tim;
    static MiupTimer *timer;

    static MiupTimer *regular_timer;

    static ModeList Mode;
    static bool isOpen;
    static concurrent_queue<callback_method_queue_data *> cbk_q;
    static callback_method_queue_data *current_cbk;

    static std::vector<std::function<void(void)> > timer_cbk;


    // new timer based callback
    static ConcurrentCallbackMap callback_map;



};

ConcurrentCallbackMap Miup::callback_map;

Ihandle *Miup::tim = IupTimer();

MiupTimer *Miup::regular_timer = new MiupTimer();

MiupTimer *Miup::timer = new MiupTimer();

Miup::ModeList Miup::Mode=Miup::ModeList::native;

bool Miup::isOpen=false;

concurrent_queue<callback_method_queue_data *> Miup::cbk_q;

callback_method_queue_data *Miup::current_cbk=new callback_method_queue_data(0,NULL);

std::vector<std::function<void(void)> >  Miup::timer_cbk;
}

#endif // MIUP

