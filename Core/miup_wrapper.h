
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef MIUP_WRAPPER
#define MIUP_WRAPPER


#include"Core/miup.h"

//graphical parent class
#include"Core/miupobj.h"
#include"Core/canvas.h"
#include"Core/image.h"


//graphical child

//Boxes
#include"Boxes/box.h"
#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Boxes/radio.h"
#include"Boxes/frame.h"
#include"Boxes/scrollbox.h"
#include"Boxes/tabs.h"
#include"Boxes/menu.h"
#include"Boxes/splitbox.h"
#include"Boxes/expander.h"
#include"Boxes/Detachbox.h"

//Dialogs
#include"Dialogs/dialog.h"
#include"Dialogs/filedialog.h"
#include"Dialogs/progressiondialog.h"

//Plots
#include"Plots/plot.h"
#include"Plots/curveplot.h"
#include"Plots/waveformplot.h"
#include"Plots/scopeplot.h"
#include"Plots/multichannelwaveform.h"
#include"Plots/scriptablecurve.h"
#include"Plots/fftplot.h"

//child controls
#include"Widgets/matrix.h"

//child base gui
#include"Widgets/spinbox.h"
#include"Widgets/button.h"
#include"Widgets/slider.h"
#include"Widgets/levelmeter.h"
#include"Widgets/toggle.h"
#include"Widgets/list.h"
#include"Widgets/gainmeter.h"
#include"Widgets/webbrowser.h"
#include"Widgets/cells.h"

//homemade Widgets
#include"Widgets/miupslider.h"

//Text Widgets
#include"Text/codeeditor.h"
#include"Text/text.h"
#include"Text/label.h"

//audio classes
#include "Audio/audiofile.h"
#include"Audio/csoundthreaded.h"

//Time Class
#include"utils/timer.h"

//else -- utilities
#include"utils/miuputil.h"
#include"utils/miupdatatype.h"
#include"utils/miuputil.h"
#include"utils/value.h"

//lib
#include"Dependencies/lsignal/lsignal.h"



#endif // MIUP_WRAPPER

