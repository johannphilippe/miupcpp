
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "miupobj.h"

MiupObj::MiupObj(){
}
MiupObj::MiupObj(Ihandle *ih)
{
    obj=ih;
}

MiupObj::~MiupObj() {
    //check which one is best ?
    //IupDestroy(obj);
    //destroy();
    //delete obj;
}

void MiupObj::destroy(){IupDestroy(obj);}
void MiupObj::refresh() { IupRefresh(obj);}
void MiupObj::map(){IupMap(obj);}
void MiupObj::unmap(){IupUnmap(obj);}
void MiupObj::detach(){IupDetach(obj);}
void MiupObj::update(){IupUpdate(obj);}
void MiupObj::redraw(){IupRedraw(obj,0);}

bool MiupObj::isMapped() {
    if(getAttrInt("WID")==-1) return true;
    return false;
}

char *MiupObj::getAttr(const char *name) {
    return IupGetAttribute(obj,name);
}

int MiupObj::getAttrInt(const char *name) {
    return IupGetInt(obj,name);
}

float MiupObj::getAttrFloat(const char *name) {
    return IupGetFloat(obj,name);
}

double MiupObj::getAttrDouble(const char *name) {
    return IupGetDouble(obj,name);
}

std::string MiupObj::getAttrStr(const char *name) {
    return std::string(IupGetAttribute(obj,name));
}

std::pair<const char *, const char *> MiupObj::getAttrPair(const char *name) // not working (const char * is unsafe in pairs)
{
    std::string str(getAttr(name));
    std::size_t separator = str.find("x");
    if((int)separator==-1) {
        separator=str.find(",");
    }
    return std::make_pair<const char *, const char *>(str.substr(0,separator).c_str(), str.substr(separator+1).c_str());
}

std::pair<std::string, std::string> MiupObj::getAttrPairStr(const char *name)
{
    std::string attr(getAttr(name));
    std::size_t separator = attr.find("x");
    if((int)separator==-1) {
        separator=attr.find(",");
    }

    return std::make_pair<std::string, std::string>(attr.substr(0,separator), attr.substr(separator+1));
}

std::pair<int,int> MiupObj::getAttrPairInt(const char *name)
{
    std::pair<std::string, std::string> pair=getAttrPairStr(name);
    return {MiupUtil::string::getNumber<int>(pair.first.c_str()), MiupUtil::string::getNumber<int>(pair.second.c_str())};
}

Ihandle *MiupObj::getHandle(const char *name) {
    return IupGetHandle(name);
}

char *MiupObj::getName() {
    return IupGetName(obj);
}

void MiupObj::setHandle(const char *name) {
    IupSetHandle(name,obj);
}

void MiupObj::setAttr(const char *name,const char *val) {
    IupSetAttribute(obj,name,val);
}

void MiupObj::setAttrs(const std::string str)
{
    IupSetAttributes(obj, std::move(str.c_str()));
}

void MiupObj::setAttrs(const char *val)
{
    IupSetAttributes(obj,val);
}

void MiupObj::setAttrInt(const char *name,int val) {
    IupSetInt(obj,name,val);
}

void MiupObj::setAttrFloat(const char *name, float val) {
    IupSetFloat(obj,name,val);
}

void MiupObj::setAttrDouble(const char *name,double val) {
    IupSetDouble(obj,name,val);
}


void MiupObj::setAttrDoubleCbk(const char *name,const  char *attrName, double val) {
    if (name==getName()) {
        setAttrDouble(attrName,val);
    }
}

void MiupObj::setAttrStrf(const char *name, const char *format...)
{
    IupSetStrf(obj,name,format);
}

void MiupObj::setAttrRGB(const char *name, unsigned char red, unsigned char green, unsigned char blue)
{
    IupSetRGB(obj,name,red,green,blue);
}

void MiupObj::setStrAttr(const char *name, const char *val)
{
    IupSetStrAttribute(obj,name,val);
}

void MiupObj::setAttrStr(const char *name, std::string value)
{
    setAttr(name,value.c_str());
}

void MiupObj::setAttrId(const char *name, int id, const char *val)
{
    IupSetAttributeId(obj,name,id,val);
}

Ihandle *MiupObj::getObj() {
    return obj;
}

void MiupObj::setSize(int width, int height) {
    std::string size=std::to_string(width) + "x" + std::to_string(height);
    setAttr("SIZE",size.c_str());
    //setAttr("SIZE",size.c_str());
}

void MiupObj::setMaxSize(int width,int height)
{
    std::string size=std::to_string(width) + "x" + std::to_string(height);
    setAttr("MAXSIZE",size.c_str());
}

void MiupObj::setMaxHeight(int height)
{
    setAttr("MAXSIZE",std::string(std::to_string(height)+"x").c_str());
}

void MiupObj::setMaxWidth(int width)
{
    setAttr("MAXSIZE",std::string("x"+std::to_string(width)).c_str());
}

void MiupObj::setHeight(int height)
{
    setAttr("SIZE",std::string("x"+std::to_string(height)).c_str());
}

void MiupObj::setWidth(int width)
{
    setAttr("SIZE",std::string(std::to_string(width)+"x").c_str());
}

void MiupObj::setRasterWidth(int width)
{
   setAttr("RASTERSIZE",std::string(std::to_string(width) + "x" ).c_str());
}

void MiupObj::setRasterHeight(int height)
{
    setAttr("RASTERSIZE",std::string("x" + std::to_string(height)).c_str());
}

void MiupObj::setRasterSize(int width, int height)
{
    setAttr("RASTERSIZE",std::string(std::to_string(width) + "x" + std::to_string(height)).c_str());
}


std::pair<int, int> MiupObj::getRasterSize()
{
    std::pair<int, int> sz;
    IupGetIntInt(obj, "RASTERSIZE", &sz.first, &sz.second);
    return sz;
}

int MiupObj::getWidth()
{
    std::string size(getAttr("SIZE"));
    std::size_t pos=size.find("x");
    std::string width=size.substr(0,pos-1);
    int res=std::stoi(width);
    return res;
}

int MiupObj::getHeight()
{
    std::string size(getAttr("SIZE"));
    std::size_t pos=size.find("x");
    std::string height=size.substr(pos+1,size.length());
    int res=std::stoi(height);
    return res;
}

std::pair<int, int> MiupObj::getCoordinates()
{
    std::pair<int, int>coor;
    IupGetIntInt(obj, "SCREENPOSITION", &coor.first, &coor.second);

    return coor;

}

MiupObj MiupObj::getParent() {return IupGetParent(obj);}

void MiupObj::setVisible(bool visible) {setAttr("VISIBLE", (visible) ? "YES" : "NO");}

