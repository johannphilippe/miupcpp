
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef MIUPOBJ_H
#define MIUPOBJ_H

#include<iup.h>
#include<iupcontrols.h>

#include<iostream>
#include<stdio.h>
#include<iup_class_cbs.hpp>
#include<iupcbs.h>

#include"utils/miuputil.h"
#include"Dependencies/lsignal/lsignal.h"

class MiupObj
{
public:
    MiupObj();
    MiupObj(Ihandle *ih);
    virtual ~MiupObj();

    virtual void setHandle(const char *name);

    virtual bool isMapped();

    void destroy();
    void map();
    void unmap();
    void detach();


    //Set Attributes
    virtual void setAttr(const char *name,const char *val);
    virtual void setAttrs(const char *val);
    virtual void setAttrs(const std::string str);
    virtual void setAttrInt(const char *name,int val);
    virtual void setAttrFloat(const char *name,float val);
    virtual void setAttrDouble(const char *name,double val);

    virtual void setAttrDoubleCbk(const char *name,const  char *attrName, double val);

    virtual void setAttrStrf(const char *name, const char *format ...);
    virtual void setAttrRGB(const char *name, unsigned char red, unsigned char green, unsigned char blue);
    virtual void setStrAttr(const char *name, const char *val);

    virtual void setAttrStr(const char *name, std::string value);

    virtual void setAttrId(const char *name, int id, const char *val);

    virtual void setSize(int width,int height);
    virtual void setMaxSize(int width,int height);

    virtual void setMaxHeight(int height);
    virtual void setMaxWidth(int width);

    virtual void setHeight(int height);
    virtual void setWidth(int width);

    virtual void setRasterWidth(int width);
    virtual void setRasterHeight(int height);

    virtual void setRasterSize(int width, int height);

    virtual std::pair<int, int> getRasterSize();

    //Get Attributes
    virtual Ihandle *getHandle(const char *name);
    virtual char *getName();

    virtual char *getAttr(const char *name);
    virtual int getAttrInt(const char *name);
    virtual float getAttrFloat(const char *name);
    virtual double getAttrDouble(const char *name);
    virtual std::string getAttrStr(const char *name);

    virtual std::pair<const char *, const char *> getAttrPair(const char *name); // not working (const char * unsafe in pairs)
    virtual std::pair<std::string, std::string> getAttrPairStr(const char *name);
    virtual std::pair<int,int> getAttrPairInt(const char *name);

    virtual Ihandle *getObj();

    virtual MiupObj getParent();

    virtual int getHeight();
    virtual int getWidth();

    virtual void refresh();
    virtual void update();
    virtual void redraw();

    virtual std::pair<int, int> getCoordinates();

    //additionnal methods facilities
    virtual void setVisible(bool visible);

protected:
    Ihandle *obj;

};

#endif // MIUPOBJ_H
