#ifndef JSONUTIL_H
#define JSONUTIL_H

#include"Dependencies/nlohmann/json.hpp"
#include"utils/miuputil.h"
using json = nlohmann::json;

static json getJsonFromFile(const std::string path)
{
    json data;
    if(MiupUtil::filesystem::isValidNonEmptyFile(path))
    {
        std::ifstream ifs(path);
        ifs >> data;
        ifs.close();
        return data;
    }
    return nullptr;
}


static void removeJSEntries(json *data, std::vector<json::object_t::key_type> &entries)
{
    for(auto key : entries)
    {
        data->erase(key);
    }
}

#endif // JSONUTIL_H
