#ifndef TERRA_HPP
#define TERRA_HPP

#ifdef __cplusplus
extern "C" {
#endif
#include"terra/lua.h"
#include"terra/lauxlib.h"
#include"terra/lualib.h"
#include"terra/terra.h"
#ifdef __cplusplus
}
#endif

#include<iostream>

class Terra {
public:
    Terra() {
        lua = luaL_newstate();
        luaL_openlibs(lua);
        terra_state = terra_init(lua);
    }

    ~Terra() {
        lua_close(lua);
    }

    //execute something
    int loadString(std::string script) {
        int error = terra_dostring(lua, script.c_str());
        isError(error);
        return error;
    }

    int doFile(std::string path) {
        int error = terra_dofile(lua, path.c_str());
        isError(error);
        return error;
    }

    //put on top of stack
    void getGlobal(std::string global) {lua_getglobal(lua, global.c_str());}
    void call(int input_nbr, int output_nbr) {lua_call(lua,input_nbr, output_nbr);}

    void pushString(std::string val) {lua_pushstring(lua, val.c_str());}
    void pushInt(int val) {lua_pushinteger(lua, val);}
    void pushNumber(double val) {lua_pushnumber(lua,val);}
    void pushBool(bool val) {lua_pushboolean(lua,val);}
    void pushNil() {lua_pushnil(lua);}

    std::string toString(int index) {return std::string(lua_tostring(lua, index));}
    double toNumber(int index) {return (double)lua_tonumber(lua,index);}
    int toInt(int index) {return (int)lua_tointeger(lua,index);}
    bool toBool(int index) {return (bool)lua_toboolean(lua,index);}


private:

    //private methods
    void isError(int error) {if(error) std::cout << "Terra error loading script" << std::endl;}

   //members
    lua_State *lua;
    int terra_state;
};

#endif // TERRA_HPP
