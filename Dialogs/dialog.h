
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef DIALOG
#define DIALOG

#include"Core/miupobj.h"

class Dialog : public MiupObj
{
public:
    //Constructor -- possible both with Ihandle => Vbox.getObj() -- or with Vbox directly (with MiupObj type that has the getObj() method)
    Dialog() = default;

    Dialog(Ihandle *child) : is_visible(false) {
        obj=IupDialog(child);
        initCallback();
    }

    template<typename T>
    Dialog(T *arg) : is_visible(false ) {
        obj=IupDialog(arg->getObj());
        initCallback();
    }

    virtual ~Dialog() {
        IupDestroy(obj);
    }

    virtual void show() { //show as non modal dialog, modal can be simulated with simulatemodal attribute
        IupShow(obj);
        is_visible = true;
        openDialogSig();
    }
    virtual void showXY(int x = IUP_CENTER, int y = IUP_CENTER) { // idem
        IupShowXY(obj,x,y);
        is_visible = true;
        openDialogSig();
    }

    virtual void popup(int x = IUP_CENTER, int y = IUP_CENTER) { // show as modal dialog
        IupPopup(obj, x,y);
        is_visible = true;
        openDialogSig();
    }

    virtual void hide() {
        IupHide(obj);
        is_visible = false;
    }

    virtual void construct(MiupObj *o) {
        obj =IupDialog(o->getObj());
        initCallback();
    }

    virtual bool isVisible()
    {
        return is_visible;
    }


    static void ShowPopup(MiupObj *o,int x, int y) {IupPopup(o->getObj(), x,y);}  // Modal - works for menu and dialog
    static void ShowDialog(MiupObj *o) {IupShow(o->getObj());} // non modal - only dialog
    static void ShowDialogXY(MiupObj *o,int x, int y) {IupShowXY(o->getObj(), x,y);} // non modal with position - only dialog

    lsignal::signal<int(void)> closeDialogSig;
    lsignal::signal<void(void)> openDialogSig;
    lsignal::signal<void(int x,int y)> resizeDialogSig;
    lsignal::signal<void()> mapSig;
protected:
        bool is_visible;
        //callback to be called when close button is clicked (CLOSE_CB in IUP)
        virtual IUP_CLASS_DECLARECALLBACK_IFn(Dialog,close_cb)
        virtual IUP_CLASS_DECLARECALLBACK_IFnii(Dialog,resize_cb)
        virtual IUP_CLASS_DECLARECALLBACK_IFn(Dialog, map_cb)
private:

        void initCallback() {
            IUP_CLASS_INITCALLBACK(obj,Dialog);
            IUP_CLASS_SETCALLBACK(obj,"CLOSE_CB",close_cb);
            IUP_CLASS_SETCALLBACK(obj,"RESIZE_CB",resize_cb);
            IUP_CLASS_SETCALLBACK(obj, "MAP_CB",map_cb);
        }
};


inline int Dialog::close_cb( Ihandle *) {

    is_visible = false;
    return closeDialogSig();
    //return IUP_CLOSE;
}

inline int Dialog::resize_cb(Ihandle *, int w, int h)
{
    resizeDialogSig(w,h);
    return IUP_CLOSE;
}

inline int Dialog::map_cb(Ihandle *)
{
    mapSig();
    return IUP_DEFAULT;
}

#endif // DIALOG

