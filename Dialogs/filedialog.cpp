
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "filedialog.h"

FileDialog::FileDialog()
{
    this->obj=IupFileDlg();
    initCallbacks();
}

FileDialog::FileDialog(const char *mode)
{
    this->obj=IupFileDlg();
    this->setAttr("DIALOGTYPE",mode);
    initCallbacks();
}

FileDialog::FileDialog(const char *mode, const char *dir)
{
    this->obj=IupFileDlg();
    this->setAttr("DIALOGTYPE",mode);
    this->setAttr("DIRECTORY",dir);
    initCallbacks();
}


FileDialog::~FileDialog() {}

void FileDialog::initCallbacks()
{
    IUP_CLASS_INITCALLBACK(obj,FileDialog);
    IUP_CLASS_SETCALLBACK(obj,"FILE_CB",file_cb);
}

int FileDialog::popup()
{
   return IupPopup(obj,IUP_CENTER,IUP_CENTER);
}

int FileDialog::popup(int x,int y) {
    return IupPopup(obj,x,y);
}

void FileDialog::hide()
{
    IupHide(obj);
}

const char *FileDialog::getPath()
{
    return getAttr("VALUE");
}


bool FileDialog::isValid(const char *filename)
{
    return (access(filename,0) !=-1);
}


int FileDialog::file_cb(Ihandle * /*ih*/,char *filename,char *status) {

    std::cout << "File browser callback = > " << status <<  std::endl;
    std::cout << std::string(status).size() << std::endl;

    std::string stat(status);

    if(stat=="INIT") { // when the dialog starts

    } else if (stat=="FINISH") { // when the dialog is closed

    } else if(stat=="SELECT") { // a file has been selected
    } else if(stat=="OTHER") { // when invalid file or a directory is selected
            std::cout << "Invalid file or directory" << std::endl;
            //std::string err="Invalid filename or directory name : " + std::string(filename);
            //IupAlarm("Error in filepath",err.c_str(),"Ok",NULL,NULL);
    } else if(stat=="OK") { // when user clicks ok
        std::cout << "STATUS OK " << std::endl;
        if(isValid( std::string(filename).c_str())) {
            path=std::string(filename);
            emitPath(path);
        } else {
            path = std::string(filename);
            //std::string err="Invalid filename or directory name : " + std::string(filename);
            //IupAlarm("Error in filepath",err.c_str(),"Ok",NULL,NULL);
            //return IUP_IGNORE;
            emitPath(path);
        }

    } else if(stat=="PAINT") { // the preview area must be redrawn

    } else if(stat=="FILTER") { // when a filter is changed (WINDOWS ONLY)

    }
    return IUP_DEFAULT;
}
