
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef FILADIALOG_H
#define FILADIALOG_H

#include"Core/miupobj.h"
#include<sys/stat.h>
#include<string>
#include<fstream>

#ifdef _WIN32
#include"io.h"
#elif __linux__
#include<unistd.h>
#endif

class FileDialog : public MiupObj
{
public:
    FileDialog();
    FileDialog(const char *mode); // OPEN, SAVE, DIR
    FileDialog(const char *mode, const char *dir);

    virtual ~FileDialog();

    virtual int popup();
    virtual int popup(int x,int y);
    virtual const char *getPath();

    void hide();

    virtual bool isValid(const char *filename);

    lsignal::signal<void(std::string)>emitPath;

    // File Dialog Modes
    constexpr static const char *OPEN = "OPEN";
    constexpr static const char *SAVE = "SAVE";
    constexpr static const char *DIR = "DIR";

protected:
  virtual IUP_CLASS_DECLARECALLBACK_IFnss(FileDialog,file_cb)

private:
        void initCallbacks();



    //attr
    std::string path;

    //private methods

};

#endif // FILADIALOG_H
