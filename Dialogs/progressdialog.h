#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include"Dialogs/dialog.h"
#include"Widgets/levelmeter.h"
#include"Boxes/hbox.h"


class ProgressDialog : public Dialog {
public:
    ProgressDialog(int max_val = 100)  : max(max), progress(0,0,100), hbx(), Dialog(&hbx) {
        hbx.push(&progress);
    }

    ~ProgressDialog() {

    }

    void setValue(int val) {
        progress.setAttrInt("VALUE",  ((val / max) *100));
    }

private:
    int max;
    LevelMeter progress;
    Hbox hbx;
};
#endif // PROGRESSDIALOG_H
