/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef PROGRESSIONDIALOG_H
#define PROGRESSIONDIALOG_H
#include"Core/miupobj.h"
#include"Dialogs/dialog.h"
#include"Widgets/levelmeter.h"
#include"Text/label.h"
#include"Boxes/vbox.h"
#include"utils/miuptimer.h"
class ProgressionDialog : public Dialog
{
public:
    ProgressionDialog(std::string txt = "Progression = ", int max_ = 100) :  mmax(max_), display(txt), pb(0,0,100), lab("Progression"), timer(1000)
    {
        this->construct(&box);

        box.push(&lab);
        box.push(&pb);
        this->setAttr("SIZE","200x50");

        this->mapSig.connect([&](){this->setProgression(0);});
    }
    ~ProgressionDialog() {

    }

    void setProgression(int val) {
        int scaled = (double)(val / mmax) * 100;
        std::string current = display + " " + std::to_string(scaled) + " / 100" + " %";
        pb.setAttrInt("VALUE",scaled);
        lab.setAttr("TITLE", current.c_str());

        if(scaled == 100 && timer.isRunning()) {
            timer.stop();
        }
    }


    void startPointerFollowing(int *p)
    {
        ptr = p;
        timer.timerSig.connect([&](){
           this->setProgression(*ptr);
        });
    }

private:
    int mmax;
    LevelMeter pb;
    Label lab;
    std::string display;
    Vbox box;

    MiupTimer timer;
    int *ptr;
};


#endif // PROGRESSIONDIALOG_H
