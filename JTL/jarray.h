
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef JARRAY_H
#define JARRAY_H

#include<iostream>
#include<algorithm>
#include<memory>
#include<cstring>



// Array without allocation - faster than std::array for small data types
//even faster than C array sometimes (for filling) with complex data
template<typename T, const std::size_t S>
class JArray
{
public:
    typedef T *iterator;
    typedef const T *const_iterator;

    JArray(int n = 0) : size_(S)
    {
        std::memset(arr, n, size_);
    }


    JArray(const JArray &that)
    {
       std::memcpy(&this->arr, &that.arr, size_);
    }

    JArray(JArray&& that)
    {
        arr = std::move(that.arr);
    }


    JArray(T src[])
    {
        std::memcpy(&arr,src, size_);
    }



    T &operator[] (int index)
    {
        return arr[index];
    }

    std::size_t size(){return size_;}


    iterator begin() {return &arr[0];}
    iterator end() {return &arr[size_];}

    const_iterator cbegin() const {return &arr[0];}
    const_iterator cend() const {return &arr[size_];}




private:
    T arr[S];
    const std::size_t size_;

};


#endif // JARRAY_H
