
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef JLINKEDLIST_H
#define JLINKEDLIST_H

#include<iostream>

template<typename T>
struct JLinkedListElement
{

    // operators not working for iteration

    JLinkedListElement<T> operator++(int)
    {
        JLinkedListElement<T> tmp = *this;
        ++(*this);
        return tmp;
    }

    JLinkedListElement<T> &operator++()
    {

        return *this->next;
    }


    JLinkedListElement<T> *next;
    T value;
};

// Simple linked list, from http://carl.seleborg.free.fr/cpp/cours/chap2/linked_list.html
// it is a LIFO (list member always points to last element
//to implement : clear, iterator, copy constructor


template<typename T>
class JLinkedList
{

public:

    typedef JLinkedListElement<T> *iterator;
    typedef const JLinkedListElement<T> *const_iterator;

    JLinkedList() : list(nullptr)
    {

    }

    JLinkedList(const JLinkedList<T> &that)
    {
        JLinkedListElement<T> * prev = nullptr;
        bool front = true;
        for(const_iterator it = that.cbegin(); it != that.end(); it = it->next)
        {
            JLinkedListElement<T> *toadd = new JLinkedListElement<T>;
            toadd->value = it->value;
            if(front)
            {
                list = toadd;
                front = false;
            }
            prev->next = toadd;
            prev = prev->next;
        }

    }

    JLinkedList(JLinkedList<T> &&that)
    {
        this->list = that.list;
        that.list = nullptr;
    }

    ~JLinkedList()
    {
        this->clear();
    }


    void push_front(const T &value)
    {
        JLinkedListElement<T> *element = new JLinkedListElement<T>;
        element->value = value;
        element->next = list;
        list = element;
    }

    JLinkedListElement<T> *find(const T& value)
    {
        JLinkedListElement<T> *element = list;

        while(element != nullptr && element->value != value)
            element = element->next;

        return element;
    }

    // erase from element
    void erase(JLinkedListElement<T> *elem)
    {
        if(elem == list)
        {
            list = nullptr;
            delete elem;
            return;
        }
        JLinkedListElement<T> *previous = list;
        while(previous != nullptr && previous->next != elem)
            previous = previous->next;
        if(previous == nullptr) return;
        previous->next = elem->next;
        delete elem;
    }

    // erase with index, not tested
    void erase(std::size_t at)
    {
        std::size_t idx = 0;
        JLinkedListElement<T> *elem = list;
        JLinkedListElement<T> *prev = nullptr;
        while(idx != at && elem != nullptr)
        {
            prev = elem;
            elem = elem->next;
            idx++;
        }

        if(elem != nullptr) {
            prev->next = elem->next;
            delete elem;
        }

    }

    void display()
    {
        JLinkedListElement<T> *elem = list;
        while(elem != nullptr) {
            std::cout << elem->value << "\t";
            elem = elem->next;

        }
        std::cout << std::endl;
    }


    // clear deletes elements
    void clear()
    {
        JLinkedListElement<T> *elem = list;
        while(elem != nullptr)
        {
            list = elem->next;
            delete elem;
            elem = list;
        }
        list = nullptr;
    }

    JLinkedListElement<T> *next()
    {
        list = list->next;
        return list;
    }

    iterator begin() {return list;}
    iterator end() {
        return nullptr;
    }

    const_iterator cbegin() const {return list;}
    const_iterator cend() const { return nullptr;}

private:

    JLinkedListElement<T> *list;
};

#endif // JLINKEDLIST_H
