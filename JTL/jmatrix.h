
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef JMATRIX_H
#define JMATRIX_H

#include<iostream>
#include"jvector.h"

//bidimensional vector




template<typename T>
class JMatrix
{

public:

    JMatrix(std::size_t nlin_, std::size_t ncol_) : nlin(nlin_), ncol(ncol_),
        data(JVector<JVector<T>>(nlin, nlin, nlin))
    {
        for(int i = 0; i < nlin; i++) {

            data[i].resize(ncol);
        }
    }

    ~JMatrix() {}


    void resize(std::size_t lin, std::size_t col)
    {
        if( (lin != nlin ) || (col != ncol) )
        {
            data.resize(lin);
            for(auto it = data.begin(); it != data.end(); ++it)
            {
                it->resize(col);
            }
        }
    }

    T& operator[] (int index) {
        return this->data[index];
    }

    T at(const std::size_t lin,const  std::size_t col)
    {
        if( (lin >= nlin || lin < 0) || (col >= ncol || col < 0)) {
            std::cout << "Matrix index out of bound" << std::endl;
            exit(0);
        };

        return this->data[lin][col];
    }

    void setValue(const std::size_t lin, const std::size_t col, T &val)
    {
        if( (lin >= nlin || lin < 0) || (col >= ncol || col < 0)) {
            std::cout << "Matrix index out of bound" << std::endl;
            exit(0);
        };

        this->data[lin][col] = val;
    }

    void printAll()
    {
        for(auto rit = this->begin(); rit != this->end(); ++rit)
        {
            for(auto cit = rit->begin(); cit != rit->end(); ++cit)
            {
                std::cout << *cit << " ";
            }
            std::cout << std::endl;
        }
    }

private:
   std::size_t nlin, ncol;
   JVector<JVector<T>> data;
};


#endif // JMATRIX_H
