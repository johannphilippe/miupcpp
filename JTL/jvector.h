﻿
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef CONTAINERS_H
#define CONTAINERS_H
#include<iostream>
#include<cstring>
#include<array>
#include<memory>
#include<algorithm>
#include<iomanip>
#include<utility>
#include<string>


//simple vector class
// block size is reallocation block when push back
// initial size is initial capacity

// seems faster than std::vector for simple data types, not so much for complex/composite.
template<typename T>
class JVector
{
public:

    typedef T *iterator;
    typedef const T *const_iterator;

    JVector(size_t initial_capacity = 10, size_t block_size = 10, std::size_t init_size = 0) :
        size_(init_size),
        capacity_(initial_capacity),
        block_size_(block_size),
        data(new T[capacity_])
    {
    }

    //copy semantics
    JVector(const JVector& that) :
        size_(that.size()),
        capacity_(that.capacity()),
        block_size_(that.block_size())
    {
        data = new T[capacity_];
        std::memcpy(data, that.data, size_);
    }


    //move semantics
    JVector(JVector&& that)
    {
        data = std::move(that.data);
        size_ = that.size();
        block_size_ = that.block_size();
        capacity_ = that.capacity();

        that.reset();
    }

    ~JVector()
    {
        delete[] data;
    }


    void reserve(std::size_t nsize)
    {
        if(nsize > capacity_) {
            capacity_ = nsize;
            T *tmp =new T[capacity_];
            std::memcpy(tmp, data, size_);
            delete[] data;
            data = tmp;
        }
    }

    void resize(std::size_t nsize)
    {
        if(nsize != size_) {
            if(nsize <= capacity_)
            {
                size_ = nsize;
                return;
            }

            T *tmp = new T[nsize];
            if(size_ > 0) std::memcpy(tmp, data, size_);
            delete[] data;
            data = tmp;
            size_ = nsize;
            capacity_ = nsize;
        }
    }

    void push_back(const T& val)
    {
        if(size_ +1 > capacity_) { // realloc block
            capacity_+=block_size_;
            T * tmp = new T[capacity_];
            std::memcpy(tmp, data, size_);
            delete[] data;
            data = tmp;
        }
        size_++;
        data[size_ - 1] = val;
    }


    T &operator[] (int index)
    {
        return data[index];
    }

    JVector& operator=(const JVector &that)
    {
        this->size_ = that.size_;
        this->capacity_ = that.capacity_;
        this->block_size_ = that.block_size_;
        this->data = new T[capacity_];
        std::memcpy(this->data, that.data, size_);
        //std::swap(data, that.data());
        return *this;
    }

    JVector& operator=(JVector &&that)
    {
        return *that;
    }

    T &at(std::size_t index) {
        if(index >= size_ || index <0) {
            std::cout << "Array index out of bound" << std::endl;
            exit(0);
        }
        return data[index];
    }

    std::size_t size() const
    {
        return size_;
    }

    std::size_t capacity() const
    {
        return capacity_;
    }

    std::size_t block_size() const
    {
        return block_size_;
    }

    /*
    T *begin() {return &arr[0];}
    T *end() {return &arr[size_];}

    */

    T operator++(int){
        return data++;
    }

    /*
    T *data() const {
        return arr;
    }
    */

    void setBlockSize(size_t n) {
        block_size_ = n;
    }

    //actually clears values, except if values are pointers (only clears pointers)
    void clear()
    {
        delete[] data;
        this->reset();
    }

    //does not clear values -- used in move semantics
    void reset()
    {
        data = nullptr;
        block_size_ = 0;
        size_ = 0;
        capacity_ = 0;
    }

    iterator begin() {return &data[0];}
    iterator end() {return &data[size_];}

    const_iterator cbegin() const {return &data[0];}
    const_iterator cend() const {return &data[size_];}

protected:
    std::size_t block_size_;
    std::size_t size_;
    std::size_t capacity_;
    T *data;

private:
};


#endif // CONTAINERS_H
