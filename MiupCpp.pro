unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += gtk+-3.0
unix: PKGCONFIG -= gtk+-2.0

CONFIG += c++11
CONFIG -= QT
QT -= gui core

INCLUDEPATH = $$PWD
DEPENDPATH = $$PWD

unix { #linux specific - Tested with Clang++ and g++
        INCLUDEPATH += $$PWD /usr/local/include /usr/include/glib-2.0 /usr/share/jo_tracker
        INCLUDEPATH += /usr/include/iup /usr/include/cd /usr/include/im /usr/include/luajit-2.1 /usr/include/lua5.1
        LIBS += -L/usr/lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib64
        LIBS += -liup -liup_plot -liupcontrols -liupcd -lcd -lcdgl -lcdcontextplus -lcdim \
        -liupgl -liupimglib -liup_scintilla -liupweb -lim -liupim -lftgl
        #LIBS += -lwebkit2gtk-4.0
        LIBS += -lwebkit
        LIBS += -lsndfile
        LIBS += -L/usr/local/lib -lcsound64
        LIBS += -L/usr/local/lib -lcsnd6
        #LIBS += -L/usr/lib -lterra
        LIBS += -lluajit-5.1 # replaced terra by luajit on linux (terra doesn't support llvm 10)
        LIBS += -ldl -pthread -lz -ltinfo

        LIBS += -lfftw3

        # GCC optimization flags
        #first remove possible optimization flags
        QMAKE_CXXFLAGS_RELEASE -= -O
        QMAKE_CXXFLAGS_RELEASE -= -O1
        QMAKE_CXXFLAGS_RELEASE -= -O2

        # Then add aggressive optimization
        QMAKE_CXXFLAGS_RELEASE += -O3
        QMAKE_CXXFLAGS += -Os

} else { #windows specific - must be used with MSVC compiler (MSVC 17 64bit recommanded)
        INCLUDEPATH += "C:/Program Files/jo_tracker/include"
        #INCLUDEPATH += "C:/Program Files/Csound6_x64/csound/include/" "C:/Program Files/libsndfile/include" "C:/Program Files/Terra/include"
        #INCLUDEPATH += "C:/Program Files/IUP/iup/include" "C:/Program Files/IUP/cd/include" "C:/Program Files/IUP/im/include"
        LIBS += -L"C:/Program Files/jo_tracker/bin" -llibsndfile-1 -lcsound64
        #LIBS += -L"C:/Program Files/libsndfile/lib" -llibsndfile-1
        #LIBS += -L"C:/Program Files/Csound6_x64/lib" -lcsound64
        #LIBS += -L"C:/Program Files/IUP/iup"
        #LIBS += -L"C:/Program Files/IUP/im"
        #LIBS += -L"C:/Program Files/IUP/cd"
        LIBS += -liup -liup_plot -liupcontrols -liupcd -lcd -lcdgl -lcdcontextplus -liupgl -liupimglib -liup_scintilla -liupole -liupweb -lcdim
        #LIBS += -L"C:/Program Files/Terra/lib" -llua51 -lterra
        LIBS +=  -llua51 -lterra
        CONFIG(release, debug|release) {
            CONFIG += optimize_full
        }
}

macx { #mac osx specific

}

VPATH += $$PWD
SOURCES += Core/miupobj.cpp \
    Audio/audiofile.cpp \
    Plots/fftplot.cpp \
    Plots/scriptablecurve.cpp \
    Widgets/matrix.cpp \
    Widgets/spinbox.cpp \
    Widgets/button.cpp \
    Widgets/slider.cpp \
    Widgets/levelmeter.cpp \
    Widgets/toggle.cpp \
    Widgets/list.cpp \
    Widgets/miupslider.cpp \
    Dialogs/filedialog.cpp \
    Plots/plot.cpp \
    Plots/curveplot.cpp \
    Plots/waveformplot.cpp \
    Plots/scopeplot.cpp \
    jo_tracker/global_data.cpp \
    jo_tracker/jtrack.cpp \
    jo_tracker/csoundeditor.cpp \
    jo_tracker/jtrackset.cpp \
    jo_tracker/jtransport.cpp \
    jo_tracker/audiodevicelist.cpp \
    Text/codeeditor.cpp \
    Text/label.cpp \
    Text/text.cpp \
    Widgets/gainmeter.cpp \
    jo_tracker/geneditortab.cpp \
    Widgets/flatbutton.cpp \
    utils/luaeditor.cpp \
    utils/miuptimer.cpp \
    jo_tracker/jscripterview.cpp \
    jo_tracker/res/icon_light.c \
    jo_tracker/res/icon_lua_32.c \
    Widgets/webbrowser.cpp \
    Widgets/cells.cpp \
    Plots/multichannelwaveform.cpp \


HEADERS += \
    Boxes/Detachbox.h \
    Core/miupobj.h \
    Core/miup_wrapper.h \
    Core/miup.h \
    Core/canvas.h \
    Audio/audiofile.h \
    Audio/csoundthreaded.h \
    Dependencies/simple_fft/check_fft.hpp \
    Dependencies/simple_fft/copy_array.hpp \
    Dependencies/simple_fft/error_handling.hpp \
    Dependencies/simple_fft/fft.h \
    Dependencies/simple_fft/fft.hpp \
    Dependencies/simple_fft/fft_impl.hpp \
    Dependencies/simple_fft/fft_settings.h \
    Plots/fftplot.h \
    Plots/scriptablecurve.h \
    Scripting/Lua/lua.hpp \
    Scripting/Lua/lua_fft_api.h \
    Scripting/Lua/lua_math_utils.h \
    Scripting/Lua/lua_sndfile_api.h \
    Widgets/matrix.h \
    Widgets/spinbox.h \
    Widgets/button.h \
    Widgets/slider.h \
    Widgets/levelmeter.h \
    Widgets/toggle.h \
    Widgets/list.h \
    Widgets/miupslider.h \
    Dependencies/curvelib/spline.h \
    Dependencies/lsignal/lsignal.h \
    Dependencies/nlohmann/json.hpp \
    jo_tracker/JToolBox.h \
    jo_tracker/aboutdialog.h \
    jo_tracker/global_data.h \
    jo_tracker/jcolor.h \
    utils/MathUtils.h \
    utils/luaeditor.h \
    utils/miupdatatype.h \
    utils/timer.h \
    utils/miuputil.h \
    utils/value.h \
    Dialogs/dialog.h \
    Dialogs/filedialog.h \
    Boxes/box.h \
    Boxes/vbox.h \
    Boxes/hbox.h \
    Boxes/radio.h \
    Boxes/frame.h \
    Boxes/scrollbox.h \
    Boxes/tabs.h \
    Boxes/menu.h \
    Boxes/splitbox.h \
    Plots/plot.h \
    Plots/curveplot.h \
    Plots/waveformplot.h \
    Plots/scopeplot.h \
    jo_tracker/jo_tracker_wrapper.h \
    jo_tracker/jtrack.h \
    jo_tracker/csoundeditor.h \
    jo_tracker/audiodevicelist.h \
    jo_tracker/jtrackset.h \
    jo_tracker/jtransport.h \
    Text/codeeditor.h \
    Text/label.h \
    Text/text.h \
    Widgets/gainmeter.h \
    Boxes/expander.h \
    utils/notificationbase.h \
    jo_tracker/csdformatter.h \
    Dialogs/popup.h \
    Widgets/matrixlist.h \
    Widgets/flatseparator.h \
    jo_tracker/geneditortab.h \
    jo_tracker/jdef.h \
    jo_tracker/exportparametersdialog.h \
    Widgets/flatbutton.h \
    Core/image.h \
    jo_tracker/jdef.h \
    utils/miuptimer.h \
    jo_tracker/jscripterview.h \
    res/languagekeywords.h \
    jo_tracker/helpdialog.h \
    jo_tracker/jseqorganizer.h \
    Widgets/webbrowser.h \
    utils/cubicspline.h \
    jo_tracker/macrodialog.h \
    Dialogs/progressiondialog.h \
    Widgets/cells.h \
    jo_tracker/jimportorchestradialog.h \
    jo_tracker/lua_api.h \
    jo_tracker/jotrackerutilities.h \
    jo_tracker/generalparameterdialog.h \
    Plots/multichannelwaveform.h \
    jo_tracker/parametersdialog.h \
    Dependencies/nlohmann/jsonutil.h \
    JTL/jvector.h \
    JTL/jmatrix.h \
    JTL/jlinkedlist.h \
    JTL/jgranularvector.h \
    JTL/jarray.h \
    jo_tracker/joscilloscopedialog.h

DISTFILES += \
    README.md \
    TODO.md \
    LICENCE \
    res/scope_meter.csd \
    res/csound_words.txt \
    examples/jo_tracker/Changelog.md


