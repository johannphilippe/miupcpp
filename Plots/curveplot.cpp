
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "curveplot.h"

CurvePlot::CurvePlot(std::string name,plot_t vxmin,plot_t vxmax,plot_t vymin,plot_t vymax,plot_t vxstep,plot_t vystep)
{
    plotName=name;

    curveMode = new Toggle(1, "Curve");
    splineMode = new Toggle(0, "Spline");
    bezierMode = new Toggle(0, "Bezier");

    radioBox = new Hbox(curveMode, splineMode, bezierMode);
    radio = new Radio(radioBox);

    mode="curveMode";

    curveMode->setHandle(CURVE_GEN16);
    splineMode->setHandle(CURVE_GEN08);
    bezierMode->setHandle(CURVE_GENBEZIER);

    radio->setAttr("VALUE", CURVE_GEN16);


    //IupPlotOpen();
    position=1;
    domain=16384;
    //obj=IupPlot();

    xmin=vxmin;
    xmax=vxmax;
    ymin=vymin;
    ymax=vymax;
    ystep=vystep;
    xstep=vxstep;

    setAttr("HIGHLIGHTMODE","SAMPLE");
    setAttr("MARGINBOTTOM","30");
    setAttr("MENUCONTEXT","YES");
    setAttr("READONLY","NO");
    setAttr("SCREENTOLERENCE","10");
    setAttr("MENUITEMPROPERTIES","YES");
    setAttr("PLOT_CURRENT","0");
    setAttr("EDITABLEVALUES","YES");
    setAttr("AXS_XAUTOMIN","NO");
    setAttr("AXS_YAUTOMIN","NO");
    setAttr("AXS_XAUTOMAX","NO");
    setAttr("AXS_YAUTOMAX","NO");

    //constructor paramsy
    setAttrDouble("AXS_XMAX",xmax);
    setAttrDouble("AXS_XMIN",xmin);
    setAttrDouble("AXS_YMIN",ymin);
    setAttrDouble("AXS_YMAX",ymax);

    setAttr("AXS_XTICK","YES");
    setAttr("AXS_YTICK","YES");
    setAttr("BOX","YES");
    setAttr("BOXLINEWIDTH","2");
    setAttr("GRID","YES");
    setAttr("DS_COLOR","255 0 255");
    setAttr("DS_MARKSTYLE","CIRCLE");
    setAttr("AXS_XTICKAUTO","NO");
    setAttr("AXS_YTICKAUTO","NO");

    setAttrDouble("AXS_XTICKMAJORSPAN",xstep);
    setAttrDouble("AXS_YTICKMAJORSPAN",ystep);
    setAttr("AXS_XTICKMAJORSIZE","8");
    setAttr("AXS_YTICKMAJORSIZE","8");
    setAttr("AXS_XTICKFORMATAUTO","NO");
    setAttr("AXS_YTICKFORMATAUTO","NO");

    setAttr("AXS_XTICKFORMAT","%.01f");
    setAttr("AXS_YTICKFORMAT","%.01f");

    setAttr("AXS_XLABEL","Duration");
    setAttr("AXS_YLABEL","Y");
    setAttr("AXS_XTICKNUMBER","YES");
    setAttr("AXS_YTICKNUMBER","YES");



    setAttr("DATASETCLIPPING","NONE");
    setAttr("FORMULA_PARAMETRIC","YES");
    //setAttr("SIZE","64x20");
    setAttr("BGCOLOR","45 26 28");

    //Change this according to OS (OPENGL on windows ?)
//    setAttr("GRAPHICSMODE","NATIVEPLUS");

    setAttr("GRAPHICSMODE","NATIVEPLUS");

    IupPlotBegin(obj,0);
    IupPlotEnd(obj);

    setAttr("CURRENT","0");
    setAttr("DS_MODE","MARK");
    setAttr("DS_MARKSTYLE","DIAMOND");


    IUP_CLASS_INITCALLBACK(obj,CurvePlot);
    IUP_CLASS_INITCALLBACK(curveMode->getObj(),CurvePlot);
    IUP_CLASS_INITCALLBACK(splineMode->getObj(),CurvePlot);
    IUP_CLASS_INITCALLBACK(bezierMode->getObj(),CurvePlot);

    IUP_CLASS_SETCALLBACK(obj,"PLOTBUTTON_CB",m_button);
    IUP_CLASS_SETCALLBACK(obj,"PLOTMOTION_CB",m_motion);
    IUP_CLASS_SETCALLBACK(obj,"PREDRAW_CB",m_predraw);

    IUP_CLASS_SETCALLBACK(curveMode->getObj(),"ACTION",radio_cb);
    IUP_CLASS_SETCALLBACK(splineMode->getObj(),"ACTION",radio_cb);
    IUP_CLASS_SETCALLBACK(bezierMode->getObj(),"ACTION",radio_cb);


}

CurvePlot::~CurvePlot(){
    delete curveMode;
    delete splineMode;
    delete bezierMode;
    delete radioBox;
    delete radio;
}

void CurvePlot::setDomain(int val) {
    domain=val;
    redraw();
}

void CurvePlot::setMode(const char *new_mode)
{
    radio->setAttr("VALUE",new_mode);
    mode=new_mode;
    redraw();
}

Radio *CurvePlot::getRadio() {
    return radio;
}


json CurvePlot::getData()
{
    json data;
    data["valid"]=false;
   // std::cout << "Compte des points : " << getAttr("DS_COUNT") << std::endl;
    if(getAttrInt("DS_COUNT") > 0) data["valid"]=true;

    int count = getAttrInt("DS_COUNT");

    data["count"]=getAttrStr("DS_COUNT");
    data["mode"]=mode;
    data["type"]=GEN_CURVE;
    int cnt =0;
    for(int i = 0; i< count; i++) {
        std::string index = std::to_string(cnt);
        double x = 0, y = 0;

        //std::cout << "GET DATA : " << index << " " << x << " " << y << std::endl;

        IupPlotGetSample(obj, 0, i, &x, &y);
        double crv = (curvetab.size() > size_t(i)) ? curvetab[i] : 0.0;

        data["points"][index]["x"] = x;
        data["points"][index]["y"] = y;
        data["points"][index]["crv"] = crv;
                //(curvetab.size() > std::size_t(i) ) ? curvetab.at(i) : 0.0;
        cnt++;
    }
    return data;
}

int CurvePlot::radio_cb(Ihandle *,int state) {
    if(state==1) {
        const char *cur_mode = radio->getAttr("VALUE");
        mode=cur_mode;
        redraw();
    }
    return IUP_CLOSE;
}


void CurvePlot::insertPoint(double x, double y, double crv)
{

    //std::cout << "insert point " << getAttr("DS_COUNT") << " "  << x << " " << y << " " << crv << std::endl;
    this->insertSample(0,getAttrInt("DS_COUNT"), x, y);
    curvetab.push_back(crv);
}


void CurvePlot::clear()
{
    curvetab.clear();
    for (int i=getAttrInt("DS_COUNT")-1; i>=0 ;i--) {
        setAttrInt("DS_REMOVE",i);
    }
    redraw();
}

//Check parameters type (not sure)
plot_t CurvePlot::getCurve(plot_t beg, plot_t ending, int dur, int idx, plot_t typ) {
    //cout << "curve getvalue = " << beg << " " << ending << "  " << dur << " " << idx << " "<< typ << endl;
    plot_t res;
    plot_t type;

    if (typ==0) {
        res=beg+(idx*((ending-beg)/dur));
    } else {
        if(typ>10) type=10;
        else if(typ<-10) type=-10;
        else type=typ;

        res=beg+(ending-beg)*(1-exp(idx*type/(dur-1)))/(1-exp(type));
    }

    return res;
}


void CurvePlot::getBezier(plot_t idx, plot_t vals[], plot_t *bx,plot_t *by) {
    plot_t 	cX= 3 * (vals[2]-vals[0]),
            bX= cX*-1,
            aX= vals[4]-vals[0]-cX-bX;

    plot_t 	cY= 3 *(vals[3]-vals[1]),
            bY=cY*-1,
            aY=vals[5]-vals[1]-cY-bY;

   *bx = (aX*pow(idx,3)) + (bX * pow(idx,2)) + (cX * idx) + vals[0];
   *by= (aY * pow(idx,3)) + (bY * pow(idx,2)) + (cY * idx) + vals[1];
}



//Callbacks of the plot:

int CurvePlot::m_motion(Ihandle *,plot_t xm, plot_t ym, char *rm) {

    plot_t x=xm;
    plot_t y=ym;
    std::string r=rm;

    if(x>xmax) x=xmax;
    if(x<xmin) x=xmin;
    if(y>ymax) y=ymax;
    if(y<ymin) y=ymin;

    //if alt  or shift click on a segment, change curve factor
    if(std::regex_search(r,std::regex("1\\s+A"))  || std::regex_search(r, std::regex("S\\s+1"))) {
        int cnt=IupGetInt(obj,"DS_COUNT");
        int prevsamp=-1;
        for(int a=0;a<cnt;a++) {
            plot_t xd,yd;
            IupPlotGetSample(obj,0,a,&xd,&yd);
            if(xd>x) {
                prevsamp=a-1;
                break;
            }
        }
        if(prevsamp!=-1 && prevsamp<cnt-1) {
            //if(curvetab.size()-1>=prevsamp) {curvetab[prevsamp]=0;}

            //the "*2" is an multiplicator of the curve factor - more makes the curve curving quicker, less does the opposite
            curvetab[prevsamp]+=((y/ymax)-(yclick/xmax))*2;
            if(curvetab[prevsamp]>10) curvetab[prevsamp]=10;
            else if(curvetab[prevsamp]<-10) curvetab[prevsamp]=-10;
           // cout << "curvetab : " << curvetab[prevsamp] << " prevsamp : " << prevsamp << endl;
           // cout << "yclick = " << yclick << endl;
        }
    redraw();
    }

    //cout << "==first sampfound" << sampfound << " " << dsfound << endl;
    if(z==1 && dsfound==0) {
        plot_t prevx=xmin,prevy=ymin,aftx=xmax,afty=ymax;
        if(sampfound>0) IupPlotGetSample(obj,0,sampfound-1,&prevx,&prevy);
        if(sampfound<IupGetInt(obj,"DS_COUNT")-1) IupPlotGetSample(obj,0,sampfound+1,&aftx,&afty);

        if(IupPlotGetSampleSelection(obj,0,sampfound)==1) {
            if(x>prevx && x<aftx) {
                //cout << "=>prevx prevy : " << prevx << " " << prevy << endl;
                IupPlotSetSample(obj,0,sampfound,x,y);
                redraw();
            } else if(sampfound>0 && x<prevx) {
                IupPlotSetSampleSelection(obj,0,sampfound,0);
                IupPlotSetSample(obj,0,sampfound,prevx,prevy);
                plot_t prevcurve=curvetab[sampfound-1],thiscurve=curvetab[sampfound];
                curvetab[sampfound-1]=thiscurve;
                curvetab[sampfound]=prevcurve;
                sampfound-=1;
                IupPlotSetSampleSelection(obj,0,sampfound,1);
            } else if(sampfound<IupGetInt(obj,"DS_COUNT")-1 && x>aftx) {
                IupPlotSetSampleSelection(obj,0,sampfound,0);
                IupPlotSetSample(obj,0,sampfound,aftx,afty);
                plot_t thiscurve=curvetab[sampfound],nextcurve=curvetab[sampfound+1];
                curvetab[sampfound]=nextcurve;
                curvetab[sampfound+1]=thiscurve;
                sampfound+=1;
                IupPlotSetSampleSelection(obj,0,sampfound,1);
            }
        }

    }

    if(std::regex_search(r,std::regex("1\\s+A"))  || std::regex_search(r, std::regex("S\\s+1"))) return IUP_IGNORE;
    return IUP_DEFAULT;

}



int CurvePlot::m_button(Ihandle *,int but, int press, plot_t xm, plot_t ym, char *stat) {
   std::cout << "button pressed " << but << " press= "  << press <<  " xm = " << xm << " ym= " << ym << " stat = " << stat << std::endl;
    plot_t x=xm;
    plot_t y=ym;

    if(x>xmax) x=xmax;
    if(x<xmin) x=xmin;
    if(y>ymax) y=ymax;
    if(y<ymin) y=ymin;

    if(press==1) yclick=y;
    IupPlotTransform(obj,x,y,&a,&b);
    z=IupPlotFindSample(obj,a,b,&dsfound,&sampfound);
    seg=IupPlotFindSegment(obj,a,b,&segds,&segsamp1,&segsamp2);

    //if double click on a point -- > looking for the nearest snap to grid point
    if(press==1 && z==1 && but==49 && std::regex_search(stat,std::regex("1\\s+D")) && dsfound==0) {
        plot_t nextx,nexty;

        for(plot_t m=xmin;m<=xmax+xstep;m+=xstep) {
            if(m>x) {
                plot_t upx=m-x;
                plot_t dnx=x-(m-xstep);
                if(dnx<upx) {
                    nextx=m-xstep;
                } else if(upx<dnx) {
                    nextx=m;
                    break;
                }
                break;
            }
        }
        for(plot_t n=ymin;n<=ymax+ystep;n+=ystep) {
            if(n>y) {
                plot_t upy=n-y;
                plot_t dny=y-(n-ystep);
                if(dny<upy) {
                    nexty=n-ystep;
                } else if(upy<dny) {
                    nexty=n;
                    break;
                }
                break;
            }
        }

        int dscnt=IupGetInt(obj,"DS_COUNT")-1;
        bool flag=false;

        //if(nextx && nexty) {
            if(dscnt>sampfound) {
                plot_t tox,toy;
                IupPlotGetSample(obj,dsfound,sampfound+1,&tox,&toy);
                if(tox<nextx) {
                    plot_t this_crv=curvetab[sampfound];
                    plot_t to_crv=curvetab[sampfound+1];
                    IupPlotSetSample(obj,dsfound,sampfound+1,nextx,nexty);
                    IupPlotSetSample(obj,dsfound,sampfound,tox,toy);
                    curvetab[sampfound]=to_crv;
                    curvetab[sampfound+1]=this_crv;
                    flag=true;
                }
            }
            if(sampfound>0) {
                plot_t tox,toy;
                IupPlotGetSample(obj,dsfound,sampfound-1,&tox,&toy);
                    if(tox>nextx) {
                        plot_t this_crv=curvetab[sampfound];
                        plot_t to_crv=curvetab[sampfound-1];
                        IupPlotSetSample(obj,dsfound,sampfound-1,nextx,nexty);
                        IupPlotSetSample(obj,dsfound,sampfound,tox,toy);
                        curvetab[sampfound]=to_crv;
                        curvetab[sampfound-1]=this_crv;
                        flag=true;
                    }
            }
            if(flag==false) IupPlotSetSample(obj,dsfound,sampfound,nextx,nexty);
            flag=false;
        //}
       redraw();

       //if shift click, then delete point
    } else if(( std::regex_search(stat,std::regex("1\\s+A"))  ||   std::regex_search(stat, std::regex("S\\s+1"))) && z==1) {
        if(press == 1 && but == 49) {
                int ct=IupGetInt(obj,"DS_COUNT");
                setAttrInt("DS_REMOVE",sampfound);
                if(ct>sampfound) {
                    for(int a=sampfound+1;a<=ct;a++) {
                        curvetab[a-1]=curvetab[a];
                    }
                }
                redraw();
        }
    }


    if(press==1 && z!=1 && !std::regex_search(stat,std::regex("C1")) && !std::regex_search(stat,std::regex("S")) && !std::regex_search(stat,std::regex("1\\s+A"))) {
        //cout << "Simple click r= " << stat << endl;
        //if more than 0 sample on the dataset
        if(IupGetInt(obj,"DS_COUNT")>0) {
            for(int cnt=0;cnt<IupGetInt(obj,"DS_COUNT");cnt++) {
                plot_t tx,ty;
                IupPlotGetSample(obj,0,cnt,&tx,&ty);
                if(tx>x) {
                    position=cnt;
                    break;
                } else if(tx<=x && cnt==IupGetInt(obj,"DS_COUNT")-1) {
                    position=IupGetInt(obj,"DS_COUNT");
                }
            }
        } else if(IupGetInt(obj,"DS_COUNT")<1) {
            position=0;
        }
        //cout << "position found : " <<position << endl;
        IupPlotInsert(obj,0,position,x,y);
        curvetab.insert(curvetab.begin()+position,0);
        //curvetab[position]=0;
        redraw();
        IupPlotSetSampleSelection(obj,0,position,1);
        z=IupPlotFindSample(obj,a,b,&dsfound,&sampfound);
        //cout << " --> is sample selected : " << IupPlotGetSampleSelection(obj,0,position) << " and sampfound is ? : " << sampfound << endl;
    } else if(press==1 && z==1 && dsfound==0) {
        //cout << "Select sample " << sampfound << endl;
        IupPlotSetSampleSelection(obj,dsfound,sampfound,1);
        //cout << " --> noCreation is sample selected : " << IupPlotGetSampleSelection(obj,0,position) << " and sampfound is ? : " << sampfound << endl;
    } else if (press==0) {
        IupPlotSetSampleSelection(obj,0,sampfound,0);
        std::cout << "---->unselect sample " << sampfound << std::endl;

    }
    return IUP_DEFAULT;

}






int CurvePlot::m_predraw(Ihandle *, cdCanvas *canvas) {
    setAttrInt("CURRENT",0);
    int compte=getAttrInt("DS_COUNT");
  //  std::cout << "count : " << compte << std::endl;
    std::vector<std::pair<plot_t,plot_t>> taby;


    if (mode=="curveMode") {
        if(compte>1) {
            //format points to calculate each segments

            for(int dr=0;dr<compte-1;dr++) {
                //if(dr<compte-1) {

                    plot_t begx,begy,fbx,fby,crv;
                    IupPlotGetSample(obj,0,dr,&begx,&begy);
                    IupPlotGetSample(obj,0,dr+1,&fbx,&fby);

                    //if(begy == ymin) begy = ymin+0.00001;
                    //if(fby == ymin) fby=ymin+0.00001;

                   // std::cout << "values : " << begy << " " << fby << std::endl;

                    crv=curvetab[dr];
                    plot_t base1_begx=begx/xmax;
                    plot_t base1_fbx=fbx/xmax;

                    int basedom_begx=floor(base1_begx*domain);
                    int basedom_fbx=floor(base1_fbx*domain);


                    plot_t ampx=abs(basedom_begx-basedom_fbx) ;
                    //plot_t ampy=floor(abs(begy-fby));

                            if(curvetab[dr] != 0) {
                            //calculate segment
                                    for(int zeidx=0;zeidx<ampx;zeidx++) {
                                        plot_t cr;
                                        if(begy<fby) cr=crv*(-1);
                                        else cr=crv;
                                        plot_t yy=getCurve(begy,fby,ampx,zeidx,cr);
                                        std::pair<plot_t,plot_t> cur(zeidx+basedom_begx,yy);
                                        taby.push_back(cur);
                                    }
                            } else { // straight line
                               std::pair<plot_t, plot_t> cur(basedom_begx, begy);
                               std::pair<plot_t, plot_t> to(basedom_fbx, fby);
                               taby.push_back(cur);
                               taby.push_back(to);
                            }
                //}
            }
        }
    } else if(mode=="bezierMode") {
        if(compte>=3) {
                for(int dr=0;dr<compte;dr+=2) {
                    if(dr<compte-2) {

                    plot_t arr[6];
                    IupPlotGetSample(obj,0,dr,&arr[0],&arr[1]);
                    IupPlotGetSample(obj,0,dr+1,&arr[2],&arr[3]);
                    IupPlotGetSample(obj,0,dr+2,&arr[4],&arr[5]);


                    plot_t base1_begx=arr[0]/xmax;
                    plot_t base1_fbx=arr[4]/xmax;


                    int basedom_begx=round(base1_begx*domain);
                    int basedom_fbx=round(base1_fbx*domain);


                    plot_t ampx=abs(basedom_begx-basedom_fbx);
                    plot_t miniamp=base1_begx-base1_fbx;
                    plot_t scaleFactor = abs(xmax/miniamp);


                    arr[0]=(arr[0]-base1_begx)*scaleFactor;
                    arr[2]=(arr[2]-base1_begx)*scaleFactor;
                    arr[4]=(arr[4]-base1_begx)*scaleFactor;
                    for(int zeidx=0;zeidx<=ampx-1;zeidx++) {
                        plot_t bx,by;
                        getBezier(zeidx/ampx,arr,&bx,&by);
                        std::pair<plot_t,plot_t> cur((bx/scaleFactor) * domain + basedom_begx
                                                ,by);
                        taby.push_back(cur);
                    }
                }

            }
        }

        //cout << "size : " << taby.size() << endl;

    } else if(mode=="splineMode") {
        if (compte>=3) {
            std::vector<std::pair< plot_t, plot_t> > valsMap;



            plot_t tempx,tempy;
            for(int i=0;i<compte;i++) {
                IupPlotGetSample(obj,0,i,&tempx,&tempy);
                valsMap.push_back({tempx, tempy});
            }
            tk::spline spl;


            spl.set_points(valsMap);

            for(int i=0;i<compte - 1 ;i++) {
                if(i<compte-1) {
                    plot_t begx,begy,fbx,fby;

                    IupPlotGetSample(obj,0,i,&begx,&begy);
                    IupPlotGetSample(obj,0,i+1,&fbx,&fby);

                    plot_t base1_begx=begx/xmax;
                    plot_t base1_fbx=fbx/xmax;

                    int basedom_begx=round(base1_begx*domain);
                    int basedom_fbx=round(base1_fbx*domain);

                    plot_t ampx=abs(basedom_begx-basedom_fbx);

                    std::cout << "domain " << domain << " ampx : " << ampx << " basedom : " << basedom_begx << std::endl;
                    for(int zeidx = 0; zeidx <= ampx-1; zeidx++) {
                        double px = (double) ((double)zeidx + (double)basedom_begx) / (double)domain;
                        plot_t splVal = spl(px);

                        if(splVal>ymax) splVal=ymax;
                        if(splVal<ymin) splVal=ymin;

                        if(px >= 1.0) continue;

                        std::pair<plot_t,plot_t> cur(zeidx+basedom_begx,splVal);
                        //std::cout << "CURRENT POINT " << cur.first << " "<< cur.second << std::endl;
                        taby.push_back(cur);
                    }
                }
            }
        }
    }


               // std::cout << "taby size : " << taby.size() << std::endl;
            cdCanvasForeground(canvas,cdEncodeColor(0,0,255));
            cdCanvasLineWidth(canvas, 3);
            for(std::size_t it=0;it< taby.size() ;it++) {
                if(it == taby.size() -1) break;
                plot_t ax,ay;
                IupPlotTransform(obj,taby[it].first/domain,taby[it].second,&ax,&ay);

                if(taby.size() > it) {
                    plot_t ax2,ay2;
                    IupPlotTransform(obj,taby[it+1].first/domain,taby[it+1].second,&ax2,&ay2);

                    if(ax2 == ax) ax -= 0.001;


                    cdCanvasLine(canvas,ax,ay,ax2,ay2);
                } else { // useless
                    break;
                    plot_t ax2, ay2;
                    IupPlotGetSample(obj,0,getAttrInt("COUNT"), &ax2, &ay2);
                    cdCanvasLine(canvas,ax,ay,ax2,ay2);


                }
            }
    return IUP_DEFAULT;
}
