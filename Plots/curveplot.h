
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef CURVEPLOT_H
#define CURVEPLOT_H

#include"Plots/plot.h"
#include"Boxes/radio.h"
#include"Boxes/hbox.h"
#include"Widgets/toggle.h"

#include<regex>
#include<utility>

#include"cd.h"
#include"cdiup.h"

#include"math.h"
#include"Dependencies/curvelib/spline.h"
#include"utils/miuputil.h"

#include"Dependencies/nlohmann/json.hpp"

#include"utils/cubicspline.h"

#define GEN_CURVE  "CURVE"

#define CURVE_GEN08 "splineMode" // CUBIC SPLINE CURVE
#define CURVE_GENBEZIER "bezierMode" // BEZIER CURVE
#define CURVE_GEN16 "curveMode" // EXPONENTIAL AND LOGARITHMIC CURVE

using json = nlohmann::json;

typedef double plot_t;

class CurvePlot : public Plot
{
public:

    CurvePlot(std::string name = "curveplot",plot_t vxmin = 0, plot_t vxmax = 1, plot_t vymin=0, plot_t vymax=1, plot_t vxstep=0.1, plot_t vystep=0.1);
    virtual ~CurvePlot();

    void setDomain(int val);
    void setMode(const char *new_mode);
    json getData();

    Radio *getRadio();

    void insertPoint(double x, double y, double crv = 0.0);
    //debug

    void clear() override;

private:
    //debut
    std::string plotName,mode;

    Toggle *curveMode, *splineMode, *bezierMode;
    Hbox *radioBox;
    Radio *radio;

    plot_t getCurve(plot_t beg,plot_t ending,int dur,int idx,plot_t typ);
    void getBezier(plot_t idx,plot_t vals[],plot_t *bx, plot_t *by);

    plot_t xmin,xmax,ymin,ymax,ystep,xstep,yclick,a,b;
    //to verify
    int sampfound,dsfound,seg,segds,segsamp1,segsamp2;
    int position,domain,z;

    std::vector<plot_t> curvetab;

protected:
    IUP_CLASS_DECLARECALLBACK_IFniidds(CurvePlot,m_button)
    IUP_CLASS_DECLARECALLBACK_IFndds(CurvePlot,m_motion)
    IUP_CLASS_DECLARECALLBACK_IFnC(CurvePlot,m_predraw)

    IUP_CLASS_DECLARECALLBACK_IFni(CurvePlot,radio_cb)
};

#endif // CURVEPLOT_H
