
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/


#include "fftplot.h"


template<typename T>
T filter(T val, T min, T max)
{
    if(val > max) return max;
    if(val < min) return min;
    return val;
}


//-----------------------------------------------------------------------------------
// FFT PLOT OBJECT (HIGH LEVEL OBJECT, MULTITRACK FFT VISUALIZATION)
//-----------------------------------------------------------------------------------


FFTPlot::FFTPlot() :  MiupObj(IupCanvas(NULL)), is_mapped(false),  scale(FFTScale::LINEAR), luminostity(1.0), hop_factor(1)
{
    IUP_CLASS_INITCALLBACK(obj,FFTPlot);
    IUP_CLASS_SETCALLBACK(obj,"MAP_CB",map_cb);
    IUP_CLASS_SETCALLBACK(obj,"ACTION_CB",action_cb);
}

FFTPlot::FFTPlot(float lum) : FFTPlot()
{
    luminostity = lum;
}

FFTPlot::~FFTPlot()
{

}

void FFTPlot::setScale(FFTScale sc)
{
    scale = sc;
    this->drawWith(soundfile, fft_length, hop_factor);
}

void FFTPlot::setLuminosity(float lum)
{
    luminostity = lum;
    drawWith(soundfile, fft_length, hop_factor);
}

void FFTPlot::setFFTLength(int len)
{
    this->drawWith(soundfile, len, hop_factor);
}

void FFTPlot::setHopFactor(int hop)
{
    drawWith(soundfile, fft_length, hop);
}

void FFTPlot::drawWith(std::string soundfile_, std::size_t fft_len, int hop)
{
    if(MiupUtil::filesystem::isValidNonEmptyFile(soundfile_) && AudioFile::IsAudioFile(soundfile_)) {
        soundfile = soundfile_;
        fft_length = fft_len;
    }
    else {
        std::cout << "Invalid soundfile" << std::endl;
        return;
    }
    hop_factor = hop;

    int idx = 0;
    SF_INFO finfo;
    finfo.format = 0;
    SNDFILE *infile = sf_open(soundfile.c_str(), SFM_READ, &finfo);

    // Set the canvas list to the number of channels, and create things
    // Find coordinates for each channel, draw blackbox, and white lines

    std::pair<int,int> csize = this->getRasterSize(); //canvas->getSizeInPixel();
    std::pair<int, int> ccoord = this->getCoordinates(); //canvas->getOrigin();

    int channel_height = csize.second / finfo.channels;

    canvas->activate();
    canvas->clear();
    canvas->setForegroundColor(CD_BLACK);
    canvas->setInteriorStyle(CD_SOLID);
    canvas->box(ccoord.first,  ccoord.first + csize.first,  0 , csize.second);

    canvas->lineStyle(CD_CONTINUOUS);

    canvas->setForegroundColor(CD_WHITE);

    if(finfo.channels > 1) {
        for(int i = 0; i < finfo.channels; i++) {
            //int y = ( csize.second) / (i + 1);
            int y = channel_height * (i + 1);
            canvas->line(ccoord.first, y, ccoord.first + csize.first, y);
        }
    }

    sf_count_t bsize = fft_len;
    sf_count_t rcnt;
    double buf[fft_len * finfo.channels];

    fftw_complex *out;
    double *in;
    in =  (double *) fftw_malloc(sizeof(double) * fft_len);
    out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * fft_len);
    fftw_plan p = fftw_plan_dft_r2c_1d(fft_len, in, out, FFTW_ESTIMATE);


    int max = 0;
    int min = 9999;
    int minphs = 9999;
    int maxphs = 0;
    double maxbark = 0;

    double xval, yval;
    //double y_ratio = (double)((fft_len/2) / csize.second) / finfo.channels;
    double y_ratio = (double)(fft_len / 2) /  (csize.second / finfo.channels); // hop_factor;
    double x_ratio = (double) (finfo.frames / (fft_len / hop_factor)) / csize.first;


    int maxy = 0;


    double bark_multiplier = (double)channel_height / 24.0;
    double multiplier = double( (finfo.samplerate/ 2) / (fft_len/ 2) );

    int passed = 0;

    double nextx, nexty, lasty;

    // calculate the y height of each band

    int line_height = 1;
    if(scale == LINEAR) {

    } else if(scale == BARK) {
    } else if(scale == LOGARITHMIC)
    {

    }
    //std::cout << "yratio : " << y_ratio << std::endl;
    do {
    sf_seek(infile, passed, SF_SEEK_SET);
    rcnt = sf_readf_double(infile, buf, bsize);
    passed += fft_len / hop_factor;


        for(int ch = 0; ch < finfo.channels ; ch++ )
        {
            lasty = -1;
            for(std::size_t s = 0; s < fft_len; s++) {
                in[s] = buf[(s * finfo.channels) + ch];
                in[s] *= MathUtils::Hamming(s, fft_length);
            }


            fftw_execute(p);
            bool b = true;
            //bool b = simple_fft::FFT<std::vector<real_type>, std::vector<complex_type> > (x, f, fft_len, error);
            if(b) {
            // display
                for(std::size_t i = 0; i < fft_len / 2; i++) {
                    double v = sqrt(pow(out[i][0], 2) + pow(out[i][1], 2));
                    double phs = atan2(out[i][1], out[i][0]);

                    if(v > max) max = v;
                    if(v < min) min = v;
                    if(phs > maxphs) maxphs = phs;
                    if(phs < minphs) minphs = phs;

                    // filter non important data (wherer there is nothing). Important for performance.
                    if(v < 1) continue;

                    int val = (int) v  * luminostity;

                    xval = (double(idx)  / x_ratio) + ccoord.first;

                    if(scale == FFTScale::LINEAR) {
                        yval = (i/y_ratio);
                    } else if(scale == FFTScale::LOGARITHMIC) {
                        yval = MathUtils::ScaledLog<double> (i/y_ratio + 1, 1, channel_height) - 1;
                    } else if(scale == FFTScale::BARK) {
                        double b_band = MathUtils::BarkBand( i * multiplier ) - 1;
                        if(b_band > maxbark) maxbark = b_band;
                        yval = b_band * bark_multiplier;
                    } else if(scale == FFTScale::MEL) {
                        return;
                    }

                    yval += (channel_height * ch);

                    if(yval > maxy) maxy = yval;

                    yval =filter<int>(yval, (channel_height * ch) + 1, ccoord.second + (channel_height * (ch + 1)));
                    nextx = double((idx + 1)) / x_ratio + ccoord.first;
                    if(i == (fft_len - 1) ) nexty = yval;

                    //double ival = std::abs(val - 255) ;
                    int red = (int)MathUtils::ScaledLog<double>(val + 1, 1, 256) - 1;
                    int green = (val > 150) ? red : 0;
                    int blue = val / 4 + ((red > 180) ?  0 : red);

                    //red = val;


                    if(idx == 1) {
                        std::cout << "X, nextX : " << xval << "  " << nextx << std::endl;
                    }

                    canvas->setForegroundColor(red,green,blue );
                    canvas->line(xval, yval, nextx, yval);



                }

            } else {
                std::cout << "FFT ERROR" << std::endl;
            }
        }

    //    break;
    //}

    idx++;
    } while(rcnt > 0);

    std::cout << "Min : " << min << std::endl;
    std::cout << "Max : " << max << std::endl;
    std::cout << "Minphs : " << minphs << std::endl;
    std::cout << "Maxphs : " << maxphs << std::endl;
    std::cout << "Max y : " << maxy << std::endl;
    std::cout << "Channel height : " << channel_height << std::endl;
    std::cout << "Y SIZE : " << csize.second << std::endl;
    std::cout << "Y ratio : " << y_ratio << std::endl;
    std::cout << "MAX BARK : " << maxbark << std::endl;

    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);

    canvas->deactivate();
    sf_close(infile);
}

void FFTPlot::processAsync(std::string file, int fft_len, int lum, FFTScale sc)
{
    SndfileHandle sfile(file, SFM_READ);



}


int FFTPlot::map_cb(Ihandle *)
{
    std::cout << " MAP FFT PLOT " << std::endl;

    canvas = new Canvas(obj);
    canvas->setForegroundColor(CD_BLACK);
    is_mapped = true;
    return IUP_DEFAULT;
}

int FFTPlot::action_cb(Ihandle *, float , float )
{
    if(!is_mapped) {
        is_mapped = true;

    }

    return IUP_CLOSE;
}
