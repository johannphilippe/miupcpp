/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/


#ifndef FFTPLOT_H
#define FFTPLOT_H
#include"Plots/plot.h"
#include"Core/canvas.h"
#include"sndfile.h"
#include"sndfile.hh"
#include"utils/MathUtils.h"

#include"Dependencies/simple_fft/fft.h"
#include"Dependencies/simple_fft/fft_settings.h"
#include<array>
#include<iostream>
#include"Audio/audiofile.h"

#include"Boxes/vbox.h"
#include"Core/image.h"
#include"cd/cdim.h"
#include"Scripting/Lua/lua.hpp"

#include<fftw3.h>

#include<thread>

typedef std::vector<real_type> Real1D;
typedef std::vector<complex_type> Complex1D;

template<typename T> static T filter(T val, T min, T max);




class FFTPlot : public MiupObj
{
public:
    enum FFTScale {
      LINEAR = 0,
      LOGARITHMIC = 1,
      MEL = 2,
      BARK = 3
    };

    FFTPlot();
    FFTPlot(float lum);
    ~FFTPlot();

    void drawWith(std::string soundfile, std::size_t fft_len, int hop_factor = 1);

    void setLuminosity(float lum);
    void setFFTLength(int len);
    void setScale(FFTScale sc);
    void setHopFactor(int hop);

    void processAsync(std::string file, int fft_len, int lum, FFTScale sc);

    //void drawWindow(int idx, int size, double *data);
    //void drawSomething();
protected:
    IUP_CLASS_DECLARECALLBACK_IFn(FFTPlot,map_cb)
    IUP_CLASS_DECLARECALLBACK_IFnff(FFTPlot,action_cb)
private:
        bool is_mapped;
        Canvas *canvas;
        FFTPlot::FFTScale scale;
        float luminostity;
        int fft_length;
        std::string soundfile;
        int hop_factor;
};



#endif // FFTPLOT_H
