
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "multichannelwaveform.h"

MultichannelWaveform::MultichannelWaveform() : Vbox()
{
    plotList.push_back(new WaveformPlot);
    this->push(plotList.back());
}

MultichannelWaveform::~MultichannelWaveform(){
    for(int i = 0; i < (int)plotList.size(); i++) {
        delete plotList[i];
    }
}

void MultichannelWaveform::setPlotNumber(int nbr)
{
    if(nbr > (int)plotList.size()) {
        for(int i =plotList.size(); i < nbr; i++) {
            plotList.push_back(new WaveformPlot);
            this->push(plotList.back());
        }
    } else if(nbr < (int)plotList.size() && nbr >=0) {
        for(int i = nbr; i <= (int)plotList.size(); i++) {
            this->detach(plotList.back());
            delete plotList.back();
            plotList.pop_back();
        }
    }

    plotList.resize(nbr);
    redraw();
}


void MultichannelWaveform::setSoundFile(std::string path)
{
    this->path = path;

    if(AudioFile::IsAudioFile(this->path)) {
            file_info = AudioFile::GetFileInfo(this->path);
            setPlotNumber(file_info.channels);
            setAxis();
             AudioData * data = AudioFile::getPeakSamples(this->path, 4096);

             for(std::size_t i =0; i < plotList.size(); i++) {
                 plotList[i]->clear();
                 plotList[i]->addSamples(0, data->time.data(), data->data[i].data(), data->time.size());
                 plotList[i]->redraw();
             }

             delete data;

             this->refresh();

    } else {

    }
}


json MultichannelWaveform::getFileInfo()
{
   json j;
   j["valid"] = false;
   j["type"] = GEN_WAVEFORM;

   if( MiupUtil::filesystem::isValidFilePath(this->path) &&  AudioFile::IsAudioFile(path)) {
        j["valid"]=true;
        j["path"] = path;
    }
   return j;
}

void MultichannelWaveform::setAxis()
{
    double max = (double)file_info.frames / (double)file_info.samplerate;
    for(auto & it : plotList) {
        it->setAxis(max);
    }
}


void MultichannelWaveform::clear()
{
    for(auto & it : plotList) {
        it->clear();
    }
}
