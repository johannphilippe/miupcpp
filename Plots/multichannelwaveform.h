
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef MULTICHANNELWAVEFORM_H
#define MULTICHANNELWAVEFORM_H

#include"Plots/waveformplot.h"
#include"Boxes/vbox.h"

#include<iostream>



class MultichannelWaveform  : public Vbox
{
public:
    MultichannelWaveform();
    ~MultichannelWaveform();


    void setSoundFile(std::string path);
    json getFileInfo();

    void setPlotNumber(int nbr);

    void clear();
private:
    void setAxis();



    std::string path;
    SF_INFO file_info;


    std::vector<WaveformPlot *> plotList;

};

#endif // MULTICHANNELWAVEFORM_H
