
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "plot.h"
Plot::Plot()
{
    obj = IupPlot();
}

Plot::~Plot() {

}

void Plot::clear() {
    for (int i=getAttrInt("DS_COUNT")-1; i>=0 ;i--) {
        setAttrInt("DS_REMOVE",i);
    }
    redraw();
}

void Plot::redraw() {
    setAttr("REDRAW","YES");
}

void Plot::setSample(int dataset,int idx,double xval,double yval) {
    IupPlotSetSample(obj,dataset,idx,xval,yval);
}

void Plot::insertSample(int dataset,int idx,double xval,double yval) {
    IupPlotInsert(obj,dataset,idx,xval,yval);
}


void Plot::insertSamples(int dataset, int index, double *x, double *y, int count)
{
    IupPlotInsertSamples(obj, dataset, index, x, y, count);
}

void Plot::addSamples(int dataset, double *x, double *y, int count)
{
    IupPlotAddSamples(obj, dataset, x, y, count);
}

void Plot::begin(int strXdata)
{
    IupPlotBegin(obj, strXdata);
}

void Plot::end()
{
    IupPlotEnd(obj);
}
