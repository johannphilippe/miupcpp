
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "scopeplot.h"

ScopePlot::ScopePlot(int s) : size(s), n_samps(size)
{
    //obj=IupPlot();
    setAttr("AXS_YAUTOMIN","NO");
    setAttr("AXS_YAUTOMAX","NO");
    setAttr("AXS_YMIN","-1");
    setAttr("AXS_YMAX","1");
    setAttr("AXS_XMIN","0");
    setAttr("AXS_XMAX","1");
    setAttr("AXS_X","NO");
    setAttr("AXS_Y","NO");
    setAttr("AXS_XTICK","NO");
    setAttr("AXS_YTICK","NO");
    setAttr("HIGHLIGHTMODE","SAMPLE");
    setAttr("BOX","YES");

    setAttr("GRID","NO");
    setAttr("AXS_XTICKAUTO","NO");
    setAttr("AXS_XTICKMAJORSPAN","16");
    setAttr("AXS_YTICKAUTO", "NO");
    setAttr("AXS_YTICKMAJORSPAN","0.5");

    setAttr("SIZE","60x20");
    setAttr("BGCOLOR","45 26 28");
    setAttr("GRAPHICSMODE","NATIVEPLUS");
    setAttr("CURRENT","0");
    setAttr("DS_COLOR","255 0 255");

    setAttr("DS_MODE","LINE");
    setAttr("DATASETCLIPPING","NONE");
    setAttr("READONLY","YES");
    setAttr("MENUCONTEXT","YES");
    setAttr("LEGEND","NO");

    setAttr("LEGEND","YES");
    setAttr("LEGENDBOX", "YES");
    setAttr("LEGENDPOS","BOTTOMRIGHT");


    initPlot();
    std::cout << "Current dataset : " << getAttr("CURRENT") << std::endl;
}

ScopePlot::~ScopePlot() {}

void ScopePlot::push(double yval) {
    q.push_back(yval);
    q.pop_front();
    drawScope();
}

//public method to set size -- forces the plot to remove dataset and create a new one,
//fill it with 0, and clears de queue to fille with 0
void ScopePlot::setDomain(int s) {
    size=s;
    n_samps = s;
    std::cout << "resize : " << s << std::endl;
}

int ScopePlot::getDomain()
{
    return this->size;
}

void ScopePlot::initPlot() {
    setAttr("CLEAR","ATTR_IGNORED");
    IupPlotBegin(obj,0);
    IupPlotEnd(obj);
    q.clear();

    for(int i=0;i<size;i++) {
        insertSample(0,i,(double)i/size,0);
        q.push_back(0);
    }
    drawScope();
    setAttr("DS_LINEWIDTH","3");
    setAttr("DS_MODE","LINE");
}

void ScopePlot::initDrawing(CsoundThreaded *cs)
{
    std::cout << "init plot of size : " << n_samps << std::endl;

    setAttr("CLEAR", "YES");

    indexes.clear();
    indexes.resize(n_samps);
    for(int i = 0; i < n_samps; i++)
    {
        indexes[i] = i;
    }
    for(int ch = 0; ch< cs->GetNchnls(); ch++) {
        IupPlotBegin(obj, 0);
            setAttr("DS_COLOR", MiupUtil::color::getRandomColorString().c_str());

            // resize vector

            for(int i = 0; i < n_samps; i++) {
                IupPlotAdd(obj, i, 0.0);
            }
        IupPlotEnd(obj);
        std::string ds_name = "channel " + std::to_string(ch);
        setAttr("DS_NAME", ds_name.c_str());
    }
}

void ScopePlot::drawWith(CsoundThreaded * cs)
{
    // first add samples to vectors
    std::lock_guard<std::mutex> guard(JoTracker::GlobalData::mtx);

    for(int ch = 0; ch < cs->GetNchnls(); ch++) {

        for(size_t i  = 0; i < JoTracker::GlobalData::samples[ch].size(); i++) {
            this->setSample(ch, i, (double)i  / cs->GetKsmps(), JoTracker::GlobalData::samples[ch][i]);
        }
    }
    redraw();
}

void ScopePlot::drawScope() {
    int cnt=0;
    for(std::list<double>::iterator it=q.begin();it!=q.end();++it) {
        setSample(0,cnt,(double)cnt/size,*it);
        cnt++;
    }
    redraw();
}

void ScopePlot::setXY(double min, double max) {
    setAttrDouble("AXS_YMIN",min);
    setAttrDouble("AXS_YMAX",max);
}

void ScopePlot::reset()
{
    int cnt = 0;
    for(std::list<double>::iterator it=q.begin(); it != q.end(); ++it) {
        *it=0.0f;
        setSample(0,cnt,(double)cnt/size,*it);
        cnt++;
    }
    redraw();
}

void ScopePlot::setSampleAt(int index, double sample)
{
    setSample(0, index, (double)index/size, sample);
}
