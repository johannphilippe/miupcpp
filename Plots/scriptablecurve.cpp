
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/


#include "scriptablecurve.h"

ScriptableCurve::ScriptableCurve() : Vbox(), okButton("Execute", "IUP_ActionOk")
{
    buttonBox.push(&okButton);
    this->push(&plot, &editor, &buttonBox);
    //this->push(&plot, &editor);

    plot.setAttr("HIGHLIGHTMODE","SAMPLE");
    plot.setAttr("MARGINBOTTOM","30");
    plot.setAttr("MENUCONTEXT","YES");
    plot.setAttr("READONLY","NO");
    plot.setAttr("SCREENTOLERENCE","10");
    plot.setAttr("MENUITEMPROPERTIES","YES");
    plot.setAttr("PLOT_CURRENT","0");
    plot.setAttr("EDITABLEVALUES","YES");
    plot.setAttr("AXS_XAUTOMIN","NO");
    plot.setAttr("AXS_YAUTOMIN","YES");
    plot.setAttr("AXS_XAUTOMAX","NO");
    plot.setAttr("AXS_YAUTOMAX","YES");

    //constructor paramsy
    plot.setAttrDouble("AXS_XMAX",1.0);
    plot.setAttrDouble("AXS_XMIN",0.0);
    plot.setAttrDouble("AXS_YMIN",-1.0);
    plot.setAttrDouble("AXS_YMAX",1.0);

    plot.setAttr("AXS_XTICK","YES");
    plot.setAttr("AXS_YTICK","YES");
    plot.setAttr("BOX","YES");
    plot.setAttr("BOXLINEWIDTH","2");
    plot.setAttr("GRID","YES");
    plot.setAttr("DS_COLOR","255 0 255");
    plot.setAttr("DS_MARKSTYLE","CIRCLE");
    plot.setAttr("AXS_XTICKAUTO","NO");
    plot.setAttr("AXS_YTICKAUTO","YES");

    plot.setAttrDouble("AXS_XTICKMAJORSPAN",0.1);
    plot.setAttrDouble("AXS_YTICKMAJORSPAN",0.1);
    plot.setAttr("AXS_XTICKMAJORSIZE","8");
    plot.setAttr("AXS_YTICKMAJORSIZE","8");
    plot.setAttr("AXS_XTICKFORMATAUTO","NO");
    plot.setAttr("AXS_YTICKFORMATAUTO","NO");

    plot.setAttr("AXS_XTICKFORMAT","%.01f");
    plot.setAttr("AXS_YTICKFORMAT","%.01f");

    plot.setAttr("AXS_XLABEL","Duration");
    plot.setAttr("AXS_YLABEL","Y");
    plot.setAttr("AXS_XTICKNUMBER","YES");
    plot.setAttr("AXS_YTICKNUMBER","YES");



    plot.setAttr("DATASETCLIPPING","NONE");
    plot.setAttr("FORMULA_PARAMETRIC","YES");
    //plot.setAttr("SIZE","64x20");
    plot.setAttr("BGCOLOR","45 26 28");

    setAttr("GRAPHICSMODE","NATIVEPLUS");


    IupPlotBegin(plot.getObj(), 0);
    IupPlotEnd(plot.getObj());

    setAttr("CURRENT","0");
    setAttr("DS_MODE","MARK");
    setAttr("DS_MARKSTYLE","DIAMOND");

    okButton.buttonClickOffSig.connect(this, &ScriptableCurve::execute);

    editor.setAttr("EXPAND","HORIZONTAL");
    initEditor();

    IUP_CLASS_INITCALLBACK(editor.getObj(), ScriptableCurve);
    IUP_CLASS_SETCALLBACK(editor.getObj(), "MAP_CB", map_cb);
}

ScriptableCurve::~ScriptableCurve()
{

}

int ScriptableCurve::map_cb(Ihandle *ih)
{
    this->initEditor();
    return IUP_DEFAULT;
}

Plot *ScriptableCurve::scriptable_plot = nullptr;

void ScriptableCurve::initEditor()
{

    editor.setAttr("LEXERLANGUAGE","lua");

    editor.setAttr("KEYWORDS0",lua_keywords.c_str());
    editor.setAttr("KEYWORDS1", lua_secondary_keywords.c_str());


    editor.setAttr("STYLEFONT32","Consolas");
    editor.setAttr("STYLEFONTSIZE32","11");
    editor.setAttr("STYLECLEARALL","YES");

    editor.setAttr("STYLEFGCOLOR1","0 128 0");
    editor.setAttr("STYLEFGCOLOR2","0 128 0");
    editor.setAttr("STYLEFGCOLOR3","128 0 0");
    editor.setAttr("STYLEFGCOLOR5","0 0 255");
    editor.setAttr("STYLEFGCOLOR6","160 20 20");
    editor.setAttr("STYLEFGCOLOR7","128 0 0");
    editor.setAttr("STYLEFGCOLOR9","0 0 255");
    editor.setAttr("STYLEFGCOLOR10","255 0 255");

    editor.setAttr("STYLEFGCOLOR13","255 0 0");
    editor.setAttr("STYLEFGCOLOR14","128 0 128");
    editor.setAttr("STYLEFGCOLOR15", "0 128 128");
    editor.setAttr("STYLEFGCOLOR16", "128 128 0");

    editor.setAttr("STYLEITALIC5","YES");
    editor.setAttr("STYLEBOLD10","YES");
    editor.setAttr("STYLEHOTSPOT6","YES");
    editor.setAttr("MARGINWIDTH0","50");

    editor.setAttr("PROPERTY","fold=1");
    editor.setAttr("PROPERTY","fold.compact=0");
    editor.setAttr("PROPERTY","fold.comment=1");
    editor.setAttr("PROPERTY","fold.preprocessor=1");

    editor.setAttr("MARGINWIDTH1","20");
    editor.setAttr("MARGINTYPE1","SYMBOL");
    editor.setAttr("MARGINMASKFOLDERS1","Yes");
    editor.setAttr("MARKDEFINE","FOLDER=PLUS");
    editor.setAttr("MARKDEFINE","FOLDEROPEN=MINUS");
    editor.setAttr("MARKDEFINE","FOLDEREND=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDERMIDTAIL=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDEROPENMID=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDERSUB=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDERTAIL=EMPTY");
    editor.setAttr("FOLDFLAGS","LINEAFTER_CONTRACTED");
    editor.setAttr("MARGINSENSITIVE1","YES");

    editor.setAttr("FONT","DejaVu, 10");

    lua = nullptr;

}


void ScriptableCurve::execute()
{    //if (lua != nullptr) delete lua;
    std::cout << "Execute Lua" << std::endl;

    plot.clear();
    ScriptableCurve::scriptable_plot = &plot;

    delete lua;
    lua = nullptr;

    lua = new Lua();

    lua->registerFunction("setCurve", setCurve);
    lua->Lregister("mathutils", mathutils_lua_api );

    if(lua->loadString(editor.getAttr("VALUE"))) {
        return;
    }

}

std::string ScriptableCurve::getScript()
{
    return std::string(editor.getAttr("VALUE"));
}

void ScriptableCurve::setContent(std::string content)
{
    temp_content = std::string(content);
    editor.setAttr("VALUE", temp_content.c_str());
    this->execute();
}

void ScriptableCurve::storeRaw(std::string path)
{
    std::cout << "sample path " << getSamplePath() << std::endl;
    if(!MiupUtil::filesystem::isValidFilePath(getSamplePath()))
        MiupUtil::filesystem::createDirectory(getSamplePath());

    int size = plot.getAttrInt("DS_COUNT");
    double *data = new double[size];

    for(int i = 0; i < size; i++)
    {
        double x;
        IupPlotGetSample(plot.getObj(), 0, i, &x, &data[i]);
    }

    std::thread t(&ScriptableCurve::storeAsync, path, data, size);
    t.detach();
}


void ScriptableCurve::storeAsync(std::string path, double *data, int size)
{
    std::ofstream ofs(path);

    for(int i = 0; i < size; i ++) {
        ofs << data[i] << "\n";
    }

    delete[] data;
}


std::size_t ScriptableCurve::getSize()
{
    return plot.getAttrInt("DS_COUNT");
}

void ScriptableCurve::clear()
{
    plot.clear();
    editor.clear();
}

extern "C" {
int setCurve(lua_State *l)
{
    luaL_checktype(l, 1, LUA_TTABLE);
   int arr_len = lua_objlen(l, 1);

#ifdef _WIN32
   double *arr = new double[arr_len];
   double *time = new double[arr_len];
#else
   double arr[arr_len];
   double time[arr_len];
#endif

   for (int i = 1; i <= arr_len; i++)
   {
       lua_rawgeti(l, 1, i);
       arr[i - 1] = lua_tonumber(l, -1);
       time[i - 1] =(double) (i - 1) / arr_len;
   }

   ScriptableCurve::scriptable_plot->addSamples(0, time, arr, arr_len );
   ScriptableCurve::scriptable_plot->redraw();

#ifdef _WIN32
   delete[] arr;
   delete[] time;
#endif
    return 0;
}
}
