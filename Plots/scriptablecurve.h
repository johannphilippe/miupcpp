/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/


#ifndef SCRIPTABLECURVE_H
#define SCRIPTABLECURVE_H

#include"plot.h"
#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Text/codeeditor.h"
#include"Widgets/button.h"

#include"Scripting/Lua/lua.hpp"
#include"Scripting/Lua/lua_math_utils.h"
#include"Scripting/Lua/lua_fft_api.h"

#include"res/languagekeywords.h"

#include"Audio/audiofile.h"

#include"jo_tracker/jdef.h"

#include<thread>

#include"iup.h"
#include"iup_class_cbs.hpp"


extern "C" {
static int setCurve(lua_State *l);
}

class ScriptableCurve : public Vbox
{
public:
    ScriptableCurve();
    ~ScriptableCurve();


    void execute();

    static Plot *scriptable_plot;

    std::string getScript();

    void setContent(std::string content);
    void storeRaw(std::string path);

    std::size_t getSize();

    void clear();
    void initEditor();

protected:
    IUP_CLASS_DECLARECALLBACK_IFn(ScriptableCurve, map_cb)

private:

    static void storeAsync(std::string path, double *data, int size);


    Plot plot;
    CodeEditor editor;
    Button okButton;
    Hbox buttonBox;

    Lua *lua;

    std::string temp_content;

};

#endif // SCRIPTABLECURVE_H
