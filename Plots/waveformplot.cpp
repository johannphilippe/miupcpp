
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "waveformplot.h"

WaveformPlot::WaveformPlot()
{

    //obj=IupPlot();

    setAttr("AXS_YMIN","-1");
    setAttr("AXS_YMAX","1");

    //setAttr("AXS_XAUTOMIN","NO");
    setAttr("DS_MODE","AREA");


 //   setAttr("AXS_XTICKFORMATAUTO","YES");
    setAttr("AXS_YAUTOMIN","NO");
    setAttr("AXS_YAUTOMAX","NO");
    setAttr("AXS_XAUTOMAX","NO");
   // setAttr("AXS_XAUTOTICK","NO");
    //setAttr("AXS_XAUTOTICKSIZE","NO");
    setAttr("AXS_XTICKFORMAT","%.2f");

    setAttr("AS_XMIN","0");
    setAttr("AXS_XMAX","1");
    setAttr("AXS_XTICKSIZEAUTO","NO");

    setAttrInt("AXS_XTICKMINORDIVISION", 10);
    //setAttr("AXS_XTICKAUTO","NO");


    begin(0);
    end();
}


WaveformPlot::~WaveformPlot() {
}


void WaveformPlot::setSoundFile(std::string path) {
    this->path=path;

    //std::cout << "Audiofile generated" << std::endl;
    if(AudioFile::IsAudioFile(path)) {
        std::cout << "FILE VALID" << std::endl;


        file_info = AudioFile::GetFileInfo(path);
        std::cout << "file length " << file_info.frames  << " " << file_info.samplerate << " " << file_info.frames / file_info.samplerate << std::endl;
        setAxis();
        this->clear();


        int seconds = int(file_info.frames / file_info.samplerate);
        int ds_factor = (int(seconds / 15) * 16 );

        if(ds_factor < 16) ds_factor = 16;
        if(ds_factor > 512) ds_factor = 512;


        std::cout << "Seconds :: " << seconds << "   ds_factor    " << ds_factor << std::endl;

        //AudioData *data = AudioFile::getPeaks(path, 512, ds_factor);
        AudioData *data = AudioFile::getPeakSamples(path, 4096);

        this->addSamples(0 , data->time.data(), data->data[0].data(), data->time.size());
        this->redraw();

        delete data;

    } else {
        std::cout << "Audiofile not valid" << std::endl;
    }
}

void WaveformPlot::setAxis() {
    setAttrDouble("AXS_XMAX", (double)file_info.frames / (double)file_info.samplerate);
    setAttr("DS_MODE","LINE");
}


void WaveformPlot::setAxis(double max)
{
    setAttrDouble("AXS_XMAX", max);
    setAttr("DS_MODE", "LINE");
}

void WaveformPlot::loadData(std::string /*path*/) {
   // clear();
   // IupPlotLoadData(obj,path.c_str(),0);
   // redraw();
    //delete af;
}


void WaveformPlot::loadDataAsync(std::string path)
{
    AudioFile::generatePeakFile(path, peakpath);

    Miup::pushCallbackMethod((double)0.0, [=](double v) {
        this->loadData(path);
    });
}

json WaveformPlot::getFileInfo()
{
    json j;
    j["valid"]=false;
    j["type"]=GEN_WAVEFORM;
    if( MiupUtil::filesystem::isValidFilePath(this->path) &&  AudioFile::IsAudioFile(path)) {
        j["valid"]=true;
        j["path"] = path;
        j["peakpath"] = peakpath;
    }

    return j;
}
