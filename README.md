# MIUP

MIUP stands as Portable User Interface for Music.
It is based on IUP - Portable User Interface.

MIUP is copyright (c) 2019-2020

MIUP is free software; you can redistribute them and/or modify them
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

MIUP is distributed in the hope that they will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this software; if not, write to the Free Software Foundation,
Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

MIUP is only distributed as source code.


## Requirements
This library requires a compiler with C++11 support - g++ or clang++ are recommanded.     
The following requirements can be downloaded with the provided script "install_dependencies.sh".     

* [IUP](https://webserver2.tecgraf.puc-rio.br/iup) - MIT license
* [Csound](https://csound.com) - LGPL license 
* [Terra](http://terralang.org) - MIT license 
* [sndfile](http://www.mega-nerd.com/libsndfile/) - LGPL license

# Getting started

To use MIUP, user must have the previous dependencies installed. You should be able to get everything installed by running the `setup.rb` script.
It must be run as administrator on Windows, and sudo on Linux.
`sudo ruby setup.rb`

This project is managed with the Qmake build system (though it doesn't uses any qt library).
Though, you should be able to produce your own makefile if needed.
The ".pro" files contains all the informations about compilations.

# Included libraries
This project is distributed with source files from external libraries

- Dependencies/lsignal.h : MIT license - from : https://github.com/cpp11nullptr/lsignal.
- Dependencies/nlohmann/json.hpp : MIT license - from : https://github.com/nlohmann/json.
- Dependencies/spline.h : LGPL license - from : https://github.com/ttk592/spline/
- Audio/csoundthreaded.h : LGPL license - from https://github.com/csound/csound
- Dependencies/simple_fft : MIT license - from https://github.com/d1vanov/Simple-FFT

Both spline.h and csoundthreaded.h have been modified, for the use of MIUP.

## Support

This project is using IUP library. IUP has support for Windows,
Linux, SunOs. MacOs is not supported now, but might be in the future.

# Examples folder     
The project folder contains a folder named "examples". It is actually more like a project folder containing MIUP
examples and jo\_tracker software. Inside, you will find a bash script called
create_new_example.sh. It must be called with the new project name as argument, for example :     
```
        ./create_new_example.sh my_example_name
```    
Once the project folder is generated, you can to open the .pro file with QtCreator.



## Usage Requirements
On Linux :
`sudo apt-get install build-essential libgtk-3-dev libluajit-5.1-dev libtinfo-dev libwebkit2gtk-4.0-dev`
Create a symlink from libwebkit.so to libwebkit2gtk-4.0.so in /usr/lib/x86_64-linux-gnu
IUP, IM, and CD must be installed with install_dev script, and ftgl.so must be installed in /usr/lib
I need to remove the lua5.1 prefix to lua #include and add it to includepath
Then compile Csound from source, and install it.
