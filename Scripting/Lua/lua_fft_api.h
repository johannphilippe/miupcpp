/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/


/** TODO : in fftclass
 * Add choice for HAMMING, HANN, BLACKMAN (windowing)
 * Add display selection (Bark, Linear, Logarithmic)
 *
**/

#ifndef LUA_FFT_API_H
#define LUA_FFT_API_H

#include"Scripting/Lua/lua.hpp"

#include"Dependencies/simple_fft/fft.h"
#include"Dependencies/simple_fft/fft_settings.h"

#include"Audio/audiofile.h"

#include"utils/miuputil.h"
#include"utils/MathUtils.h"

#include<math.h>
#include<iostream>

#include<fftw3.h>
#include"lua_sndfile_api.h"

struct lua_fft_data
{
    double *real_buf;
    fftw_complex *complex_buf;
    fftw_plan p;
    std::size_t fft_len;
};


struct lua_fir_data
{
    double *real_buf;
    fftw_complex *complex_buf;
    std::vector<fftw_complex *> ir;
    fftw_plan p;
    std::size_t fft_len;
    SndfileHandle sfile;

    std::vector<double> buf;
    std::string ir_path;
    std::size_t seg_count;

    // used for convolution
    fftw_complex *tmp_buffer;
    int current;
};

/**
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  OO IR Class for Lua.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**/

static int firlua_new(lua_State *lua)
{

    lua_fir_data *data = new lua_fir_data;

    data->ir_path = std::string(lua_tostring(lua, 1));
    data->fft_len = lua_tointeger(lua, 2);


    data->sfile = SndfileHandle(data->ir_path, SFM_READ);
    data->buf.clear();
    data->buf.resize(data->sfile.channels() * data->fft_len);
    data->current = 0;

    data->tmp_buffer = fftw_alloc_complex(data->fft_len / 2 + 1);
    data->real_buf = fftw_alloc_real(data->fft_len);
    data->complex_buf = fftw_alloc_complex(data->fft_len / 2 + 1);
    data->seg_count = data->sfile.frames() / data->fft_len + 1;
    data->ir.resize(data->seg_count);
    for(std::size_t i = 0; i < data->seg_count; i++) {
        data->ir[i] = fftw_alloc_complex(data->fft_len / 2 + 1);
    }

    data->p = fftw_plan_dft_r2c_1d(data->fft_len, data->real_buf, data->complex_buf, FFTW_ESTIMATE);

    lua_fir_data **ptr = (lua_fir_data **) lua_newuserdata(lua, sizeof(lua_fir_data *));
    *ptr = data;
    luaL_getmetatable(lua, "LuaFFT.fir");
    lua_setmetatable(lua, -2);

    return 1;
}


static int firlua_process(lua_State *lua)
{
    int argc = lua_gettop(lua);
    lua_fir_data * data = *(lua_fir_data **)luaL_checkudata(lua, 1, "LuaFFT.fir");

    int ch = 0;
    if(argc > 1) {
        ch = lua_tointeger(lua, 2);
    }

    int idx = 0;
    while(data->sfile.readf(data->buf.data(), data->fft_len ) > 0) {
        // copy desired channel to fft buffer
        for(std::size_t i = 0; i < data->fft_len / 2 + 1; i++) {
            data->real_buf[i] = data->buf[i * data->sfile.channels() + ch];
        }
        //pad with 0
        memset(&data->real_buf[data->fft_len / 2], 0, (data->fft_len - (data->fft_len / 2 + 1)) * sizeof(double));
        // process fft
        fftw_execute(data->p);
        memcpy(data->ir[idx], data->complex_buf, (data->fft_len / 2 + 1) * sizeof(fftw_complex));

        idx++;
    }

    fftw_free(data->complex_buf);
    fftw_free(data->real_buf);
    data->buf.clear();
    fftw_destroy_plan(data->p);
    return 0;
}

static int firlua_clean(lua_State *lua)
{
    lua_fir_data * data = *(lua_fir_data **)luaL_checkudata(lua, 1, "LuaFFT.fir");
    for(auto & it : data->ir) {
        fftw_free(it);
    }
    fftw_free(data->tmp_buffer);
    return 0;
}

static const struct luaL_Reg firlua_m [] ={
  {"process", firlua_process},
  {"clean", firlua_clean},
  {NULL, NULL}
};

static const struct luaL_Reg firlua_f [] = {
  {"new", firlua_new},
  {NULL, NULL}
};

static void firlua_open(lua_State *lua)
{
    luaL_newmetatable(lua, "LuaFFT.fir");
    lua_pushstring(lua, "__index");
    lua_pushvalue(lua, -2);
    lua_settable(lua, -3);

    luaL_openlib(lua, NULL, firlua_m, 0);
    luaL_openlib(lua, "fir", firlua_f, 0);
}
/**
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  OO FFT Class for Lua.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**/

static int fftclass_new(lua_State *lua)
{
    lua_fft_data *data = new lua_fft_data;

    data->fft_len = lua_tointeger(lua, 1);


    //data->real_buf =(double *) fftw_malloc(sizeof(double) * data->fft_len);
    //data->complex_buf  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (data->fft_len / 2 + 1));
    data->real_buf = fftw_alloc_real(data->fft_len);
    data->complex_buf = fftw_alloc_complex(data->fft_len/2 + 1);
    data->p = fftw_plan_dft_r2c_1d(data->fft_len, data->real_buf, data->complex_buf, FFTW_ESTIMATE);

    lua_fft_data **ptr = (lua_fft_data **) lua_newuserdata(lua, sizeof(lua_fft_data *));
    *ptr = data;
    luaL_getmetatable(lua, "LuaFFT.fft");
    lua_setmetatable(lua, -2);
    return 1;
}

static int fftclass_setInput(lua_State *lua)
{
    int argc = lua_gettop(lua);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");

    bool zero_padding = false;
    if(argc > 2) zero_padding = lua_toboolean(lua, 3);

    luaL_checktype(lua, 2, LUA_TTABLE);
    std::size_t arr_len = lua_objlen(lua, 2);

    //getting real only
    for(std::size_t i = 1; i <= arr_len; i++) {
        if( (i-1) >= data->fft_len) {
            luaL_argerror(lua, 2, "Table too large, must be maximum fft_len");
        }
        lua_rawgeti(lua, 2, i);
        data->real_buf[i - 1] = lua_tonumber(lua, -1);
        lua_pop(lua,1);
    }

    if(zero_padding)
        memset(&data->real_buf[arr_len], 0.0, sizeof(double) * (data->fft_len - arr_len));

    return 0;
}

// Make another set input from an sndfile object (quicker, as C arrays instead of lua_table)
static int fftclass_setSndfileInput(lua_State *lua)
{
    int argc = lua_gettop(lua);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    lua_sndfile_data *snd = *(lua_sndfile_data **) luaL_checkudata(lua, 2, "LuaSndfile.sndfile");

    int ch = lua_tointeger(lua, 3);

    bool zero_padding = false;
    if(argc > 3) zero_padding = lua_toboolean(lua, 4);

    for(int i = 0; i < snd->block_size; i++) {
        data->real_buf[i] = snd->buf[i * snd->sfile.channels() + ch];
    }
    if(zero_padding)
        memset(&data->real_buf[snd->block_size], 0.0, sizeof(double) * (data->fft_len - snd->block_size));

    return 0;
}



static int fftclass_FFT(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    fftw_execute(data->p);
    return 0;
}


static int fftclass_getOutput(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    lua_newtable(lua);

    for(std::size_t i = 0; i < (data->fft_len /2 + 1); i++) {
        lua_newtable(lua);
        lua_pushnumber(lua, data->complex_buf[i][0]);
        lua_rawseti(lua, -2, 1);
        lua_pushnumber(lua, data->complex_buf[i][1]);
        lua_rawseti(lua, -2, 2);

        lua_rawseti(lua, -2, i+1);
    }

    return 1;
}


static int fftclass_convolveWithFIR(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    lua_fir_data * ir = *(lua_fir_data **)luaL_checkudata(lua, 2, "LuaFFT.fir");

    memset(ir->tmp_buffer, 0, (data->fft_len / 2 + 1) * sizeof(fftw_complex));

    // need the current value to decrement

    for(std::size_t i = 1; i < ir->seg_count; ++i)
    {
        const std::size_t indexAudio = (ir->current + i) % ir->seg_count;

        for(std::size_t idx = 0; idx < data->fft_len / 2 + 1; idx++) {
            std::complex<double> n1(data->complex_buf[indexAudio][0], data->complex_buf[indexAudio][1]);
            std::complex<double> n2(ir->ir[i][idx][0], ir->ir[i][idx][1]);
            std::complex<double> res = MathUtils::ComplexCrossProduct<double>(n1, n2);
            ir->tmp_buffer[idx][0] += res.real();
            ir->tmp_buffer[idx][1] += res.imag();
        }
    }

    for(std::size_t idx = 0; idx < data->fft_len / 2 + 1; idx++) {
        std::complex<double> n1(data->complex_buf[ir->current][0], data->complex_buf[ir->current][1]);
        std::complex<double> n2(ir->ir[0][idx][0], ir->ir[0][idx][1]);
        std::complex<double> res = MathUtils::ComplexCrossProduct<double>(n1, n2);
        ir->tmp_buffer[idx][0] += res.real();
        ir->tmp_buffer[idx][1] += res.imag();
    }

    memcpy(data->complex_buf, ir->tmp_buffer, (data->fft_len  / 2 + 1) * sizeof(fftw_complex));

    ir->current = (ir->current > 0) ? (ir->current - 1) : (ir->seg_count - 1);

    return 0;
}

static int fftclass_convolveWith(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    //lua_fir_data * ir = *(lua_fir_data **)luaL_checkudata(lua, 2, "LuaFFT.fir");
    lua_fft_data * ir = *(lua_fft_data **)luaL_checkudata(lua, 2, "LuaFFT.fft");

    // get current seg of data
    /*
    std::size_t current = lua_tointeger(lua, 3);

    for(std::size_t i = 1; i < ir->seg_count; ++i)
    {
        const std::size_t indexIr = i;
        const std::size_t indexAudio = (current + i) % ir->seg_count;
    }
    */



    for(std::size_t i = 1; i < data->fft_len / 2 + 1; i++)
    {
        std::complex<double> n1(data->complex_buf[i][0], data->complex_buf[i][1]);
        std::complex<double> n2(ir->complex_buf[i][0], ir->complex_buf[i][1]);

        std::complex<double> res = MathUtils::ComplexCrossProduct<double>(n1, n2);
        data->complex_buf[i][0] = res.real();
        data->complex_buf[i][1] = res.imag();
    }


    return 0;
}

static int fftclass_clean(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");

    fftw_destroy_plan(data->p);
    fftw_free(data->real_buf);
    fftw_free(data->complex_buf);

    delete data;

    return 0;

}


static const struct luaL_Reg fftclass_lualib_f [] = {
  {"new", fftclass_new},
  {NULL, NULL}
};

static const struct luaL_Reg fftclass_lualib_m [] = {
  {"setInput", fftclass_setInput},
  {"setSndfileInput", fftclass_setSndfileInput},
  {"FFT", fftclass_FFT},
  {"getOutput", fftclass_getOutput},
  {"convolveWith", fftclass_convolveWith},
  {"convolveWithFIR", fftclass_convolveWithFIR},
  {"clean", fftclass_clean},
  {NULL,NULL}
};

static void fftclass_open(lua_State *lua)
{
    luaL_newmetatable(lua, "LuaFFT.fft");
    lua_pushstring(lua, "__index");
    lua_pushvalue(lua, -2);
    lua_settable(lua, -3);

    luaL_openlib(lua, NULL, fftclass_lualib_m, 0);
    luaL_openlib(lua, "fft", fftclass_lualib_f, 0);


}
/**
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  OO IFFT Class for Lua.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**/


static int ifftclass_new(lua_State *lua)
{
    lua_fft_data *data = new lua_fft_data;

    data->fft_len = lua_tointeger(lua, 1);

    data->complex_buf =fftw_alloc_complex(data->fft_len/2 + 1); // (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (data->fft_len / 2 + 1));
    //data->real_buf = (double *) fftw_malloc(sizeof(double) * data->fft_len);
    data->real_buf = fftw_alloc_real(data->fft_len); //  (double *) fftw_malloc(sizeof(double) * data->fft_len);
    data->p = fftw_plan_dft_c2r_1d(data->fft_len, data->complex_buf, data->real_buf, FFTW_ESTIMATE);

    lua_fft_data **ptr = (lua_fft_data **) lua_newuserdata(lua, sizeof(lua_fft_data *));
    *ptr = data;
    luaL_getmetatable(lua, "LuaFFT.ifft");
    lua_setmetatable(lua, -2);
    return 1;
}

static int ifftclass_setInput(lua_State *lua)
{
    int argc = lua_gettop(lua);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.ifft");

    bool zero_padding = false;
    if(argc > 2) zero_padding = lua_toboolean(lua, 3);

    luaL_checktype(lua, 2, LUA_TTABLE);
    std::size_t arr_len = lua_objlen(lua, 2);

    //getting real only
    for(std::size_t i = 1; i <= arr_len; i++) {
        if( (i-1) >= data->fft_len) {
            luaL_argerror(lua, 2, "Table too large, must be maximum fft_len");
        }
        lua_rawgeti(lua, 2, i);
        lua_rawgeti(lua,-1, 1);
        data->complex_buf[i - 1][0] =(double) lua_tonumber(lua, -1);
        lua_pop(lua, 1);
        lua_rawgeti(lua, -1, 2);
        data->complex_buf[i - 1][1] =(double) lua_tonumber(lua, -1);
        lua_pop(lua, 1);
        //std::cout << "Data input : " << data->complex_buf[i-1][0] << "  & " << data->complex_buf[i-1][1] << std::endl;
    }

    if(zero_padding)
        memset(&data->complex_buf[arr_len], 0.0, sizeof(fftw_complex) * (data->fft_len - arr_len));

    return 0;
}


static int ifftclass_setFFTInput(lua_State *lua)
{
    int argc = lua_gettop(lua);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.ifft");
    lua_fft_data * fft = *(lua_fft_data **)luaL_checkudata(lua, 2, "LuaFFT.fft");

    bool zero_padding = false;
    if(argc > 2) zero_padding = lua_toboolean(lua,3);

    std::size_t size = data->fft_len / 2 + 1;
    memcpy(data->complex_buf, fft->complex_buf, sizeof(fftw_complex) * size);
    if(zero_padding)
        memset(&data->complex_buf[size], 0, sizeof(fftw_complex) * size);

    return 0;
}

static int ifftclass_IFFT(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.ifft");
    fftw_execute(data->p);

    for(std::size_t i= 0; i < data->fft_len; i++) {
        data->real_buf[i] /= (double)data->fft_len;
    }
    return 0;
}

static int ifftclass_getOutput(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.ifft");

    lua_newtable(lua);

    for(std::size_t i = 0; i < data->fft_len; i++) {
        lua_pushnumber(lua, (double)data->real_buf[i]);
        lua_rawseti(lua, -2, i+1);
    }

    return 1;
}

static int ifftclass_clean(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.ifft");

    fftw_destroy_plan(data->p);


    fftw_free(data->real_buf);
    fftw_free(data->complex_buf);
    delete data;

    return 0;

}


static const struct luaL_Reg ifftclass_lualib_f [] = {
  {"new", ifftclass_new},
  {NULL, NULL}
};


static const struct luaL_Reg ifftclass_lualib_m [] = {
  {"setInput", ifftclass_setInput},
  {"setFFTInput", ifftclass_setFFTInput},
  {"IFFT", ifftclass_IFFT},
  {"getOutput", ifftclass_getOutput},
  {"clean", ifftclass_clean},
  {NULL, NULL}
};

static void ifftclass_open(lua_State *lua)
{
    luaL_newmetatable(lua, "LuaFFT.ifft");
    lua_pushstring(lua, "__index");
    lua_pushvalue(lua, -2);
    lua_settable(lua, -3);

    luaL_openlib(lua, NULL, ifftclass_lualib_m, 0);
    luaL_openlib(lua, "ifft", ifftclass_lualib_f, 0);
}




/**
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  OO FFT Class for Lua.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**/

/**
static int fftclass_new(lua_State *lua)
{

    int arg_nbr = lua_gettop(lua);
    lua_fft_data *data = new lua_fft_data;
    data->is_ready = false;
    data->soundfile_from = std::string(lua_tostring(lua, 1 ));


    if(!lua_isnil(lua, 2))
        data->soundfile_to = std::string(lua_tostring(lua, 2 ));

    data->fft_len = lua_tointeger(lua, 3);
    data->block_size = data->fft_len;

    data->read_idx = 0;

    data->padded_len = data->fft_len * 2 - 1;


    if(!MiupUtil::filesystem::isValidFilePath(data->soundfile_from)) {
        std::cout << "Invalid file" << std::endl;
        luaL_argerror(lua, 1, "Invalid soundfile");
        //lua_pushboolean(lua, false);
        //return 1;
    }
    if(!AudioFile::IsAudioFile(data->soundfile_from)) {
        std::cout << "Not an audio file " << std::endl;
        luaL_argerror(lua, 1, "Not an audio file");
        //lua_pushboolean(lua, false);
        //return 1;
    }



    if(!lua_isnil(lua, 2)) {
        std::string sf_to = data->soundfile_to;
        int inc = 1;
        while(MiupUtil::filesystem::isValidNonEmptyFile(data->soundfile_to)) {
            std::string path = MiupUtil::filesystem::getFolderFromPath(sf_to);
            std::string name = MiupUtil::filesystem::getFilenameFromPath(sf_to);
            std::pair<std::string, std::string> n_ext = MiupUtil::filesystem::splitNameAndExtension(name);
            n_ext.first += std::to_string(inc);
            //data->soundfile_to = path + n_ext.first + n_ext.second;
            data->soundfile_to = MiupUtil::filesystem::join({path, n_ext.first + n_ext.second});
            std::cout << "datapath " << data->soundfile_to << std::endl;
            inc++;
        }
    }

    data->hop_factor = 1;

    if(arg_nbr == 4) {
        data->hop_factor = lua_tointeger(lua,4);
    }

    data->passed = 0;

    data->finfo.format = 0;
    data->file = sf_open(data->soundfile_from.c_str(), SFM_READ, &data->finfo);

    if(!lua_isnil(lua, 2))
        data->outfile = sf_open(data->soundfile_to.c_str(), SFM_WRITE, &data->finfo);
    else
        data->outfile = NULL;

    data->buf.clear();
    data->buf.resize(data->fft_len * data->finfo.channels);
    data->overlap_buf.clear();
    data->overlap_buf.resize(data->fft_len * data->finfo.channels);
    std::fill(data->overlap_buf.begin(), data->overlap_buf.end(), 0.0);


    data->in = (double *) fftw_malloc( sizeof(double) * data->fft_len);
    data->out = (double *) fftw_malloc( sizeof(double) * data->fft_len);
    data->offt = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (data->fft_len / 2 + 1));
    data->ifft = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (data->fft_len / 2 + 1));
    data->pforward = fftw_plan_dft_r2c_1d(data->fft_len, data->in, data->offt, FFTW_ESTIMATE);
    data->pbackward = fftw_plan_dft_c2r_1d(data->fft_len, data->ifft, data->out, FFTW_ESTIMATE);

    data->is_ready = true;
    data->rcnt = 1;

    //lua_pushlightuserdata(lua, (void *)data);


    lua_fft_data **ptr = (lua_fft_data **) lua_newuserdata(lua, sizeof(lua_fft_data *)); // new lua_fft_data;
    *ptr = data;
    luaL_getmetatable(lua, "LuaFFT.fft");
    lua_setmetatable(lua, -2);

    return 1;
}


static int fftclass_convolve(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");

    //convolution data  as second parameter
    lua_fft_data * c_data = *(lua_fft_data **)luaL_checkudata(lua, 2, "LuaFFT.fft");

    // in this case, resize first FFT input to *2, and pad it with 0. The same for ifft input.



}


static int fftclass_readBlock(lua_State *lua)
{

    int argc =lua_gettop(lua);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    if(!data->is_ready) {
        std::cout << "SNDFILE not ready " << std::endl;
        lua_pushnumber(lua, -1);
        return 1;
    }

    bool loop = false;
    if(argc > 1) {
        loop = lua_toboolean(lua, 2);
    }

    if(data->rcnt <= 0) {
        lua_pushnumber(lua, data->rcnt);
        return 1;
    }


    sf_seek(data->file, data->passed, SF_SEEK_SET);
    data->rcnt = sf_readf_double(data->file, data->buf.data(), data->block_size);
    data->read_idx++;
    data->passed += data->fft_len / data->hop_factor;
    lua_pushnumber(lua, data->rcnt);

    if(loop && data->passed >= data->finfo.frames) {
        data->passed = 0;
    }

    return 1;
}

static int fftclass_writeBlock(lua_State *lua) {

    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    if(!data->is_ready) {
        lua_pushstring(lua, "SNDFILE not ready, please init first");
        return 1;
    }

    if(data->hop_factor > 1) {
        int overlap_size = (data->fft_len * data->finfo.channels) / data->hop_factor;
        int overlap_index = data->overlap_buf.size() - overlap_size - 1;
        for(int i = 0; i < overlap_size; i++) {
            data->buf[i] += data->overlap_buf[overlap_index + i];
        }
        std::copy(data->buf.begin(), data->buf.end(), data->overlap_buf.data());
    }

    sf_seek(data->outfile, data->passed, SF_SEEK_SET);
    sf_write_double(data->outfile, data->buf.data(), data->block_size * data->finfo.channels);

    lua_pushboolean(lua, true);
    return 1;

}

static int fftclass_getChannelCount(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    if(!data->is_ready) {
        std::cout << "SNDFILE not initialized, please call fft.init first" << std::endl;
        lua_pushnil(lua);
        return 1;
    }
    lua_pushnumber(lua, data->finfo.channels);
    return 1;
}

static int fftclass_getFrameCount(lua_State *lua)
{
    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    if(!data->is_ready) {
        std::cout << "SNDFILE not initialized, please call fft.init first" << std::endl;
        lua_pushnil(lua);
        return 1;
    }
    lua_pushnumber(lua, data->finfo.frames);

    return 1;
}

static int fftclass_getSampleRate(lua_State *lua)
{

    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    if(!data->is_ready) {
        std::cout << "SNDFILE not initialized, please call fft.init first" << std::endl;
        lua_pushnil(lua);
        return 1;
    }
    lua_pushnumber(lua, data->finfo.samplerate);

    return 1;
}


// Returning array of complex numbers in form { {real, imaginary}, {real, imaginary} }
static int fftclass_FFT(lua_State *lua)
{
    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    int chn_nbr = lua_tointeger(lua, 2);
    if(chn_nbr >= data->finfo.channels) {
        std::cout << "Wrong channel number " << std::endl;
        lua_pushboolean(lua, false);
        lua_pushnil(lua);
        return 2;
    }

    // copy channel
    int idx;
    for(int i = 0; i < data->fft_len; i++) {
        idx = (i * data->finfo.channels) + chn_nbr;
        data->in[i] = data->buf[idx];
        data->in[i] *= MathUtils::Hamming(i, data->fft_len);
    }
    fftw_execute(data->pforward);

    lua_pushboolean(lua, true);
    lua_newtable(lua);
    for(int i = 0; i < data->fft_len / 2 + 1 ; i++) {
        lua_newtable(lua);
        lua_pushnumber(lua, data->offt[i][0]);
        lua_rawseti(lua, -2, 1);
        lua_pushnumber(lua, data->offt[i][1]);
        lua_rawseti(lua, -2, 2);

        lua_rawseti(lua, -2, i + 1);
    }

    return 2;
}


static int fftclass_IFFT(lua_State * lua)
{
    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    int chn_nbr = lua_tointeger(lua, 2);
    if(chn_nbr >= data->finfo.channels) {
        std::cout << "Channel requested is above sndfile channel number" << std::endl;
        lua_pushboolean(lua, false);
        return 1;
    }

    // reorder data in output buffer
    fftw_execute(data->pbackward);
    double val;
    for(int i = 0; i < data->fft_len; i++) {
        //Amplitude signal reconstruction (experimental)
        val = data->out[i] / data->fft_len;
        data->buf[(i * data->finfo.channels) + chn_nbr] = val ; // / MathUtils::Hamming(i, data->fft_len);
    }

    lua_pushboolean(lua, true);

    return 1;
}


static int fftclass_clean(lua_State *lua) {

    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    sf_close(data->file);

    if(data->outfile != NULL) sf_close(data->outfile);

    fftw_destroy_plan(data->pforward);
    fftw_destroy_plan(data->pbackward);
    fftw_free(data->in);
    fftw_free(data->offt);
    fftw_free(data->out);
    fftw_free(data->ifft);

    data->is_ready = false;
    delete data;

    lua_pushboolean(lua, true);
    return 1;
}


static int fftclass_setData(lua_State *lua)
{
    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");

    luaL_checktype(lua, 2, LUA_TTABLE);
    std::size_t arr_len = lua_objlen(lua, 2);

    for( std::size_t i = 1 ; i <= arr_len; i++) {
        if(i > std::size_t(data->fft_len) ) {
            std::cout << "Overflow"  << std::endl;
            lua_pushboolean(lua, false);
            return 1;
        }

        lua_rawgeti(lua, 2, i);
        lua_rawgeti(lua, -1, 1);
        double real = lua_tonumber(lua, -1);
        lua_pop(lua, 1);
        lua_rawgeti(lua, -1, 2);
        double imag = lua_tonumber(lua, -1);
        lua_pop(lua, 1);


        data->ifft[i - 1][0] = real;
        data->ifft[i - 1][1] = imag;
    }

    lua_pushboolean(lua, true);

    return 1;
}

static int fftclass_getFFTValue(lua_State *lua)
{
    //lua_fft_data *data = (lua_fft_data *) lua_touserdata(lua, 1);
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    int idx = lua_tointeger(lua, 2);

    if(idx >= data->fft_len) {
        luaL_argerror(lua, 2, "Wrong index");
    }

    lua_newtable(lua);
    lua_pushnumber(lua, data->offt[idx][0]);
    lua_rawseti(lua, -2, 1);
    lua_pushnumber(lua, data->offt[idx][1]);
    lua_rawseti(lua, -2, 1);


    return 1;

}


static int fftclass_getSamplesValue(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    int ch = lua_tointeger(lua, 2);
    int index = lua_tointeger(lua, 3);

    if(ch >= data->finfo.channels) {
        luaL_argerror(lua, 2, "Wrong channel");
    }

    if(index >= data->block_size) {
        luaL_argerror(lua, 2, "Wrong index");
    }

    lua_pushnumber(lua, data->buf[ index * data->finfo.channels + ch]);
    return 1;
}

static int fftclass_setSampleValue(lua_State * lua)
{

    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    int ch = lua_tointeger(lua, 2);
    int index = lua_tointeger(lua, 3);

    if(ch >= data->finfo.channels) {
        luaL_argerror(lua, 2, "Wrong channel");
    }

    if(index >= data->block_size) {
        luaL_argerror(lua, 2, "Wrong index");
    }
    double value = lua_tonumber(lua, 4);
    data->buf[ index * data->finfo.channels + ch] = value;

    return 1;
}


static int fftclass_getChannelSamples(lua_State *lua)
{
    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    int ch = lua_tointeger(lua, 2);
    if(ch >= data->finfo.channels) {
        luaL_argerror(lua, 2, "Wrong channel");
    }

    lua_newtable(lua);
    for(int i = 0; i < data->block_size; i++) {
        lua_pushnumber(lua, data->buf[ i * data->finfo.channels + ch]);
        lua_rawseti(lua, -2, 1);
    }

    return 1;
}


static int fftclass_setChannelSamples(lua_State *lua)
{

    lua_fft_data * data = *(lua_fft_data **)luaL_checkudata(lua, 1, "LuaFFT.fft");
    int ch = lua_tointeger(lua, 2);
    if(ch >= data->finfo.channels) {
        luaL_argerror(lua, 2, "Wrong channel");
    }


    luaL_checktype(lua, 3, LUA_TTABLE);
    std::size_t arr_len = lua_objlen(lua, 3);

    for( std::size_t i = 1 ; i <= arr_len; i++) {
        if(i > std::size_t(data->fft_len) ) {
            break;
        }

        lua_rawgeti(lua, 3, i);
        double val = lua_tonumber(lua, -1);
        lua_pop(lua, 1);
        data->buf[(i- 1) * data->finfo.channels + ch] = val;
    }

    lua_pushboolean(lua, true);

    return 1;

}


static const struct luaL_Reg fftclass_lualib_f [] = {
    {"new", fftclass_new},
    {NULL, NULL}
};

static const struct luaL_Reg fftclass_lualib_m [] = {
    {"readBlock", fftclass_readBlock},
    {"writeBlock", fftclass_writeBlock},
    {"FFT", fftclass_FFT},
    {"IFFT", fftclass_IFFT},
    {"setData", fftclass_setData},
    {"clean", fftclass_clean},
    {"getFrameCount", fftclass_getFrameCount},
    {"getChannelCount", fftclass_getChannelCount},
    {"getSampleRate", fftclass_getSampleRate},
    {"getFFTValue", fftclass_getFFTValue},
    {"getSampleValue", fftclass_getSamplesValue},
    {"setSampleValue", fftclass_setSampleValue},
    {"getChannelSamples", fftclass_getChannelSamples},
    {"setChannelSamples", fftclass_setChannelSamples},

    {NULL,NULL}
};

static int fftclass_open(lua_State *lua) {
    luaL_newmetatable(lua, "LuaFFT.fft");
    lua_pushstring(lua, "__index");
    lua_pushvalue(lua, -2);
    lua_settable(lua, -3);

    luaL_openlib(lua, NULL, fftclass_lualib_m, 0);
    luaL_openlib(lua, "fft", fftclass_lualib_f, 0);
    return 0;
}
*/



#endif // LUA_FFT_API_H
