/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/



#ifndef LUA_MATH_UTILS_H
#define LUA_MATH_UTILS_H
#include"Scripting/Lua/lua.hpp"
#include"utils/MathUtils.h"
#include<complex>

static int mathutils_hamming(lua_State *lua)
{
    int index = lua_tointeger(lua, 1);
    int length = lua_tointeger(lua, 2);
    lua_pushnumber(lua, MathUtils::Hamming(index, length));
    return 1;
}

static int mathutils_hann(lua_State *lua)
{
    int index = lua_tointeger(lua, 1);
    int length = lua_tointeger(lua, 2);
    lua_pushnumber(lua, MathUtils::Hanning(index, length));
    return 1;
}

static int mathutils_blackman(lua_State *lua)
{
    int index = lua_tointeger(lua, 1);
    int length = lua_tointeger(lua, 2);
    lua_pushnumber(lua, MathUtils::Blackman(index, length));
    return 1;
}

static int mathutils_scaledLog(lua_State *lua)
{
    double val = lua_tonumber(lua, 1);
    double min = lua_tonumber(lua, 2);
    double max = lua_tonumber(lua, 2);
    lua_pushnumber(lua, MathUtils::ScaledLog<double>(val, min, max));
    return 1;
}

static int mathutils_complexMultiplication(lua_State *lua)
{
    std::complex<double> n1(lua_tonumber(lua, 1), lua_tonumber(lua, 2));
    std::complex<double> n2(lua_tonumber(lua, 3), lua_tonumber(lua, 4));

    std::complex<double> res = n1 * n2;
    lua_newtable(lua);
    lua_pushnumber(lua, res.real());
    lua_rawseti(lua, -2, 1);
    lua_pushnumber(lua, res.imag());
    lua_rawseti(lua, -2, 1);
    return 1;
}

static int mathutils_complexCrossProduct(lua_State *lua)
{
    std::complex<double> n1(lua_tonumber(lua, 1), lua_tonumber(lua, 2));
    std::complex<double> n2(lua_tonumber(lua, 3), lua_tonumber(lua, 4));

    std::complex<double> res = MathUtils::ComplexCrossProduct<double>(n1, n2);
    lua_newtable(lua);
    lua_pushnumber(lua, res.real());
    lua_rawseti(lua, -2, 1);
    lua_pushnumber(lua, res.imag());
    lua_rawseti(lua, -2, 1);

    return 1;
}


static const luaL_Reg mathutils_lua_api[] = {
    {"hamming", mathutils_hamming},
    {"hann", mathutils_hann},
    {"blackman", mathutils_blackman},
    {"scaledLog", mathutils_scaledLog},
    {"complexMultiplication", mathutils_complexMultiplication},
    {"complexCrossProduct", mathutils_complexCrossProduct},
    {NULL, NULL}
};


#endif // LUA_MATH_UTILS_H
