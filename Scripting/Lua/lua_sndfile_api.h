#ifndef LUA_SNDFILE_API_H
#define LUA_SNDFILE_API_H

#include"Scripting/Lua/lua.hpp"

#include"Dependencies/simple_fft/fft.h"
#include"Dependencies/simple_fft/fft_settings.h"

#include"Audio/audiofile.h"
#include"sndfile.hh"

#include<math.h>
#include<iostream>

#include"utils/miuputil.h"
#include"utils/MathUtils.h"

#define WINDOW_NULL 0b00000000
#define WINDOW_HANNING 0b00000010
#define WINDOW_HAMMING 0b00000100
#define WINDOW_BLACKMAN 0b00001000


struct lua_sndfile_data
{
    SndfileHandle sfile;
    sf_count_t block_size;
    sf_count_t rcnt;

    int mode;
    std::vector<double> buf;
    std::vector<double> overlapped;

    // optionnal hop when using with FFT
    int hop_factor;
    int passed;
};




static int sndlua_new(lua_State *lua)
{
   int argc = lua_gettop(lua);
   lua_sndfile_data *data = new lua_sndfile_data;
   std::string file_path = std::string(lua_tostring(lua, 1));

   data->hop_factor = 1;
   int mode = lua_tointeger(lua, 2);
   data->block_size = lua_tointeger(lua, 3);
   if(mode == SFM_WRITE) {
       data->mode = SFM_WRITE;

       if(argc > 6) data->hop_factor = lua_tointeger(lua, 7);
       int fmt = lua_tointeger(lua, 4);
       int chn = lua_tointeger(lua, 5);
       int sr = lua_tointeger(lua, 6);

       std::cout << "mode : " << mode  << "  --> SFM_WRITE " <<  std::endl;
       std::cout << "format "<< fmt << std::endl;
       std::cout << " chns : " << chn << std::endl;
       std::cout << "sr : " << sr << std::endl;

        std::string sf_to = file_path;
        int inc = 1;
        while(MiupUtil::filesystem::isValidNonEmptyFile(file_path)) {
            std::string path = MiupUtil::filesystem::getFolderFromPath(sf_to);
            std::string name = MiupUtil::filesystem::getFilenameFromPath(sf_to);
            std::pair<std::string, std::string> n_ext = MiupUtil::filesystem::splitNameAndExtension(name);
            n_ext.first += std::to_string(inc);
            //data->soundfile_to = path + n_ext.first + n_ext.second;
            file_path = MiupUtil::filesystem::join({path, n_ext.first + n_ext.second});
            std::cout << "datapath " << file_path << std::endl;
            inc++;
        }

       data->sfile = SndfileHandle(file_path, data->mode, fmt, chn, sr);
   } else if(mode == SFM_READ){
       if(!MiupUtil::filesystem::isValidNonEmptyFile(file_path)) luaL_argerror(lua, 1, "Invalid file");
       if(!AudioFile::IsAudioFile(file_path)) luaL_argerror(lua, 1, "Not an audio file");

       data->mode = SFM_READ;
       if(argc > 3) data->hop_factor = lua_tointeger(lua, 4);
       data->sfile = SndfileHandle(file_path, data->mode);
   }

   data->passed = 0;
   data->overlapped.clear();
   if(data->hop_factor > 1 && data->mode == SFM_WRITE) {
       data->overlapped.resize(data->block_size * data->sfile.channels() / data->hop_factor);
   }

   data->buf.clear();
   data->buf.resize(data->block_size * data->sfile.channels());

   lua_sndfile_data **ptr = (lua_sndfile_data **) lua_newuserdata(lua, sizeof(lua_sndfile_data *)); // new lua_fft_data;
   *ptr = data;
   luaL_getmetatable(lua, "LuaSndfile.sndfile");
   lua_setmetatable(lua, -2);


   return 1;
}

static int sndlua_getChannelCount(lua_State *lua)
{
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");
    lua_pushinteger(lua, data->sfile.channels());
    return 1;
}

static int sndlua_getFrameCount(lua_State *lua)
{
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");
    lua_pushinteger(lua, data->sfile.frames());
    return 1;
}

static int sndlua_getSampleRate(lua_State *lua)
{
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");
    lua_pushinteger(lua, data->sfile.samplerate());
    return 1;
}

static int sndlua_getFormat(lua_State *lua)
{
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");
    lua_pushinteger(lua, data->sfile.format());
    return 1;
}

static int sndlua_readBlock(lua_State *lua)
{
    int argc =lua_gettop(lua);
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");

    if(data->mode == SFM_WRITE) {
        luaL_error(lua, "%s\n", "Error, this sndfile instance is in SFM_WRITE mode");
    }
    bool loop = false;
    if(argc > 1) {
        loop = lua_toboolean(lua, 2);
    }

    if(data->rcnt <= 0) {
        lua_pushnumber(lua, data->rcnt);
        return 1;
    }

    data->sfile.seek(data->passed, SF_SEEK_SET);
    data->rcnt = data->sfile.readf(data->buf.data(), data->block_size);
    data->passed += data->block_size / data->hop_factor;
    if(loop && data->passed >= data->sfile.frames()) {
        data->passed = 0;
    }

    lua_pushnumber(lua, data->rcnt);
    return 1;
}


static int sndlua_getSamples(lua_State *lua)
{
    int argc =lua_gettop(lua);
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");

    int ch = lua_tointeger(lua, 2);
    if(ch >= data->sfile.channels()) {
        luaL_argerror(lua, 2, "Invalid channel");
    }

    int window = 0;
    if(argc > 2) {
        window = lua_tointeger(lua, 3);
    }

    lua_newtable(lua);;

    for(int i = 0; i < data->block_size; i++) {
        if(window != 0) {
            switch(window) {
            case WINDOW_HANNING:
            {
                lua_pushnumber(lua, data->buf[i * data->sfile.channels() + ch] * MathUtils::Hanning(i, data->block_size));
                break;
            }
            case WINDOW_HAMMING:
            {
                lua_pushnumber(lua, data->buf[i * data->sfile.channels() + ch] * MathUtils::Hamming(i, data->block_size));
                break;
            }
            case WINDOW_BLACKMAN:
            {
                lua_pushnumber(lua, data->buf[i * data->sfile.channels() + ch] * MathUtils::Blackman(i, data->block_size));
                break;
            }
            }
        } else {
            lua_pushnumber(lua, data->buf[i * data->sfile.channels() + ch]);

        }
        lua_rawseti(lua, -2, i + 1);
    }

    return 1;
}


static int sndlua_writeBlock(lua_State *lua)
{

    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");

    if(data->mode == SFM_READ) {
        luaL_error(lua, "%s\n", "Error, this sndfile instance is in SFM_READ mode");
    }

    data->sfile.seek(data->passed, SF_SEEK_SET);

    if(data->hop_factor > 1) {
        for(std::size_t i = 0; i < data->overlapped.size(); i++) {
            data->buf[i] += data->overlapped[i];
        }
    }

    sf_count_t written = data->sfile.writef(data->buf.data(), data->block_size  /*   *data->sfile.channels()   */);

    if(data->hop_factor > 1) {
        int overlap_size = data->block_size * data->sfile.channels() / data->hop_factor;
        int overlap_begin = data->buf.size() - overlap_size - 1;
        std::copy(data->buf.begin() + overlap_begin, data->buf.end(), data->overlapped.data());
    }
    data->passed += (data->block_size) / data->hop_factor;

    std::cout << "Data passed = " << data->passed << std::endl;
    std::cout << "Data written = " << written << std::endl;


    return 0;
}

static int sndlua_setSamples(lua_State *lua)
{
    int argc =lua_gettop(lua);
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");
    int ch = lua_tointeger(lua, 2);

    if(ch >= data->sfile.channels()) {
        luaL_argerror(lua, 2, "Invalid channel");
    }

    int window = 0;
    if(argc > 3) {
        window = lua_tointeger(lua, 4);
    }

    luaL_checktype(lua, 3, LUA_TTABLE);
    std::size_t arr_len = lua_objlen(lua, 3);

    if(arr_len > std::size_t(data->block_size) ) {
        luaL_argerror(lua, 3, "Sample array too big for one channel. It must be inferior or equal to block_size");
    }

    for(std::size_t i = 1; i <= arr_len; i++) {
        lua_rawgeti(lua, 3, i);
        double val = lua_tonumber(lua, -1);
        //std::cout << "Val = > " << val << std::endl;
        switch(window) {
        case WINDOW_HAMMING:
            data->buf[(i - 1) * data->sfile.channels() + ch] = val / MathUtils::Hamming(i - 1, data->block_size);
            break;
        case WINDOW_HANNING:
            data->buf[(i - 1) * data->sfile.channels() + ch] = val / MathUtils::Hanning(i - 1, data->block_size);
            break;
        case WINDOW_BLACKMAN:
            data->buf[(i - 1) * data->sfile.channels() + ch] = val / MathUtils::Blackman(i - 1, data->block_size);
            break;
        case WINDOW_NULL:
            data->buf[(i-1) * data->sfile.channels() + ch] = val;
            break;
        }

        lua_pop(lua,1);;
    }

    return 0;
}


static int sndlua_clean(lua_State *lua) {
    lua_sndfile_data * data = *(lua_sndfile_data **)luaL_checkudata(lua, 1, "LuaSndfile.sndfile");
    data->buf.clear();
    data->overlapped.clear();

    return 0;
}

static const struct luaL_Reg sndlua_f [] = {
  {"new", sndlua_new},
  {NULL,NULL}
};

static const struct luaL_Reg sndlua_m [] = {
  {"readBlock", sndlua_readBlock},
  {"writeBlock", sndlua_writeBlock},
  {"getSamples", sndlua_getSamples},
  {"setSamples", sndlua_setSamples},
  {"getChannelCount", sndlua_getChannelCount},
  {"getFrameCount", sndlua_getFrameCount},
  {"getSampleRate", sndlua_getSampleRate},
  {"getFormat", sndlua_getFormat},
  {"clean", sndlua_clean},
  {NULL,NULL}
};

static void sndlua_open(lua_State *lua)
{
    luaL_newmetatable(lua, "LuaSndfile.sndfile");
    lua_pushstring(lua, "__index");
    lua_pushvalue(lua, -2);
    lua_settable(lua, -3);


    luaL_openlib(lua, NULL, sndlua_m, 0);
    luaL_openlib(lua, "sndfile", sndlua_f, 0);

    lua_pushinteger(lua, SFM_READ);
    lua_setglobal(lua, "SFM_READ");
    lua_pushinteger(lua, SFM_WRITE);
    lua_setglobal(lua, "SFM_WRITE");

    lua_pushinteger(lua, SF_FORMAT_WAV);
    lua_setglobal(lua, "SF_FORMAT_WAV");
    lua_pushinteger(lua, SF_FORMAT_AIFF);
    lua_setglobal(lua, "SF_FORMAT_AIFF");

    lua_pushinteger(lua, SF_FORMAT_PCM_S8);
    lua_setglobal(lua, "SF_FORMAT_PCM_S8");
    lua_pushinteger(lua, SF_FORMAT_PCM_16);
    lua_setglobal(lua, "SF_FORMAT_PCM_16");
    lua_pushinteger(lua, SF_FORMAT_PCM_24);
    lua_setglobal(lua, "SF_FORMAT_PCM_24");
    lua_pushinteger(lua, SF_FORMAT_PCM_32);
    lua_setglobal(lua, "SF_FORMAT_PCM_32");

    lua_pushinteger(lua, WINDOW_HANNING);
    lua_setglobal(lua, "WINDOW_HANNING");
    lua_pushinteger(lua, WINDOW_HAMMING);
    lua_setglobal(lua, "WINDOW_HAMMING");
    lua_pushinteger(lua, WINDOW_BLACKMAN);
    lua_setglobal(lua, "WINDOW_BLACKMAN");

}

#endif // LUA_SNDFILE_API_H
