### TODO

### BIG IMPROVEMENTS TO DO
-MacOS - implement CD for OSX
-VST support ?
!DONE-URGENT : Resource Files strategy
-URGENT : Replace Ihandle with MiupObj in every container in constructors

### GUI
!DONE-Graphic Scope to visualise realtime waveforms  --> almost done, or done ?
-Make LevelMeter dB etalonnage (and add a mark for the 0db value and other values like -3 -6 etc)
!DONE-IUP Base object (for IupOpen, IupPlotOpen etc... and also for global variables like main colors, themes etc...)
-BOX : implement detach and clear for an already mapped BOX (if is mapped, then iterate childs to detach). And for detach(if !is_mapped, find index
and remove element from vector).
-RADIO - make like other boxes (create Radio at construct time, and push/append ? )
-GainMeter - redraw when resize.
-MiupSlider - idem

# To fix since mint 19
-ScopePlot - find why it is faster at startup of csound play
-AudioDeviceList - not working ?

# NEW GUI :
-SplitBox : Seems to work only when childrens are expandable. So expandchildren = yes ?

# New Widgets :
!DONE-Containers : Tab, Menu
!DONE-Basic : Label
!DONE-Scintilla text editor
-Config (subclass from IupConfig) - Json might be great for it
!DONE-Resource files (permanent files to load / write but remaining inside the binary?)
-Plot with a kind of multislider representing frequencies (vertical bars)
-Homemade Slider & LevelMeter - Partially done
-IupCells
-FFT plots (non realtime and realtime)
-Frequency plot (like Eq)

# To Fix :
!DONE-SpinBox : infinite max when max=NULL (or something)
!DONE-Frame - (no multiple child in constructor)
?-Text Base Class (for Text, Scintilla, Label) containint setFont Method etc...
!DONE-A bit messy in MiupPerformanceCallback.h -> only keep what's necessary
!-idem for MiupDataType
-Segfaults when quit : CurvePlot - hard to find why Radio buttons are segfaulting when quiting CurvePlot
!DONE-put SystemUtils to MiupUtil and add a namespace (or several, string, system etc...)
-MiupUtil::print -- blocks the console output when many calls are done
-CurvePlot : spline to fix (rewrite)
!DONE-CurvePlot : aliasing - and number of point (unexpected bar sometimes)
-WaveformPlot : Find an async solution (generate peakfile async, and refresh UI when done, currently not working with miup callback queue
-WaveformPlot : format time ticks - currently can write 36.8 seconds (which doesn't exists)


### AUDIO
-Check whether iuploopstep could work with VST3 implementation
-Implement Chuck audio language
-Implement Faust (or be glad with csound faust implementation ? )
-Implement Soul (once Soul exists)
-->In order to implement many audio languages, why not implement a AudioEngine class that lets the user push whatever he wants as audio engine
(even homemade implementation, or VST3...).

# To Fix :
-Csound Segfault when quit button clicked (implement proper csound destuctor) ~ seems to depend on MIUP memory leaks --> perhaps IupDestroy(obj)
in MiupObj base class fixed this.




# MAC OSX :
-Need to port CD (canvas draw) to probably Quartz or equivalent.


########################################
########################################
########### JO TRACKER
########################################
########################################



### JO_TRACKER
//DIRECTIONS : this version must give facilities to write quickly music, and efficiently. Also add facilitied to move blocks, and a tab to compose
the main form of a music, and script system to fill cells.

# To Add

## Big Features

- Editable waveforms
- Integration of spectral display (in waveform)
- Spectral Edit : in Lua API and, why not, in editable waveforms.
- A Csound Code editor for waveform edition ? (that would be great, the edit result is the output soundfile of Csound).


## Small Features

!DONE- Mute / Solo Button on tracks
- Improve project management (copy samples when saving, import samples...).
- Move samples data inside samples directory all the time (waveforms)
!DONE With expanders- Possibility to completely hide a track
!DONE- Gather scripted samples inside gathered folder
-New copy paste for blocks (or some shortcuts to select a line, several like in vim, then a shortcut to store the block)
!DONE-Play seq button
-Play shortcuts -- global callback - not working now (conflict with some widgets)
!DONE(Not tested)-Handwritten GEN - simple text editor to write GEN by hand (can be in matrix GEN tab)
-New Parameter Dialog for params like : enable scope, for what number of outputs,  ?? else (...) - started (empty yet)
-Improve transport with time informations ( line/nline - secs_mins/totalsecs_mins - totaldurofproject)


# To Fix :
BUGS :
!DONE - Big one. The resize issue on main tab seems to be fixed (with a Fill object, and the Staterefresh of tempo expander set to false).
!DONE- MultichannelWaveform Plot -- causing crash after 1 or two soundfiles loading (started to clean audiofile yet) - perheaps due to certains soundfiles (or mono)
!DONE- Improve callback system - pass from queue based callbacks to timer based (do not remove queue yet) --
an idea : timer should always run, but with a different timing (80-100 ms
when no audio, 30 ms when audio)

!DONE (added interpolation for situation when starting performance between two tempos) -Tempo value when not play from start
-Display instrument list sometimes not working - also to fix a way to handwrite without bugs (without instrlist)
!PARTIALLY DONE (With IupUpdate on main dialog, or IupRedraw) -Resize issue - sometimes expandable dosn't work properly in main tab : when clicking left expandable


!DONE (synchronously but faster - and with multitrack support) -WaveformPlot : async
-Orc evaluation with instr requirements check. -- to fix (in the case of an instrument calling another)
!DONE-Clean main.cpp by adding a static method file.

WEIRD BEHAVIOR :
-Performance with big projects (maybe matrix isn't really the good tool for this, see IupCells)  -
there might be a "redraw" somewhere it shouldn't be

TO MODIFY :
!REMOVED - Scopeplot out of main tab (a dialog) - and disabled by default
-!DONE - Lua API in C++ ? Not an emergency here



!DONE in JTRACK-track.cpp -> must inherit from Vbox rather than Matrix (and implement a way to access matrix ? Not sure about that)
!DONE-Jtrack.cpp -> resize when show/hide
!FIXED-JTrack - crash when show/hide.
!FIXED-JTrack - not exactly the good size at startup
-JTrack - GUI design (organise the buttons so it's more pretty)

!DONE-Track : give the number of lines when creating object
!DONE WITH FRAME-Track : inherits from Vbox rather than matrix so that setAttr (and all methods) can access the container rather than the matrix only
!DONE-JTrack : implement p1 selector ?
!DONE-JTrackSet : implement auto calculation for p3 (... owww), p1 selector according to instrument database.
!DONE-JTrackSet : only redraw matrixes when recall are done (so, remove redraw() from setValueAt()) // caused to slow the program
!DONE-JTrackSet : show/hide tempo
-TrackSet and Track : a way to remove track not only at the end
?ALMOST DONE-TrackSet and Track : bug when change colsize (data bug) -- Fixed with a condition ?

# NEW FEATURES
!DONE-Macro system to refer to values // with csound macro
?Maybe not useful => -Track system : Add new tracks : Plot (automation), MIDI or easy track (track assigned to one instrument with only three/four cols)
?Maybe soundfile track ?
!DONE-Database for Instruments -> and a csd wizard (or orc/sco wizard) to automatically generate orc or csd from database according
to instruments used in the score... //
!NO-?Database for GEN templates.
!DONE-Scintilla code editor
!DONE-New Tab to organize sequences (how many loops) that let the user put the sequences in any order, even repet it at some point.
Also some features like reverse sequence, subsequence ... etc. Inspired by Renoise.
-Import a sequence/instrument/udo/function from another project
!NO-Track data (sequences score etc..) stored into database. Then can be saved as Json or XML.
!DONE SYNCHRONOUSLY-Json async parser for live score data..
-?Replace Matrix in tracks by Cells
!FIXED-JTrackSet : how to write p1 ? Maybe write it as 1.1 or 2.4 like in v2, but create a table where included instruments are referenced. Then, if a
new instrument is written (like 3.1), it asks user what is that instrment, showing him a list of instruments) ? Or simply add indexes (1 2 3)
to instrument Json's database. Or simply add a number to each instrument...
!DONE-NEW FEATURE : Lua scriptable patch - a scintilla interface allowing to programmatically create and fill/modify a score data.
!DONE-Save scripts (optionnal) (in user project)

# Old features to implement :
!DONE-Save Load (XML, Json, Lua tables ?)
!DONE-Menus (export stereo, export multichannel, record realtime, record realtime multichannel, export CSD)...

# Urgent to make it work properly
- Clear track, seq, all...
!DONE-Render, Record
!DONE-Play ordered
- Render ordered ? could be great to generate different soundfiles from one project.



# DO A DOCUMENTATION
- Install documentation - with install scripts
- User guide
- MIUP notes and quick guide (no emergency)

