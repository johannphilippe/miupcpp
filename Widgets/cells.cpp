
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "cells.h"

Cells::Cells(int lines, int cols) : nlines(lines), ncols(cols), curcol(-1), curline(-1), matrix(nlines,ncols)
{
       obj = IupCells();

       IUP_CLASS_INITCALLBACK(obj, Cells);
       IUP_CLASS_SETCALLBACK(obj, "NCOLS_CB",ncols_cb);
       IUP_CLASS_SETCALLBACK(obj, "NLINES_CB", nlines_cb);
       IUP_CLASS_SETCALLBACK(obj,"DRAW_CB",draw_cb);
       IUP_CLASS_SETCALLBACK(obj, "HEIGTH_CB",height_cb);
       IUP_CLASS_SETCALLBACK(obj, "WIDTH_CB", width_cb);
       IUP_CLASS_SETCALLBACK(obj, "MOUSECLICK_CB",mouseclick_cb);
       IUP_CLASS_SETCALLBACK(obj, "K_ANY", k_any);
}

Cells::~Cells()
{
}

int Cells::ncols_cb(Ihandle * /*ih*/)
{
    return ncols;
}

int Cells::nlines_cb(Ihandle * /*ih*/)
{
    return nlines;
}

int Cells::draw_cb(Ihandle * /*ih*/, int line, int col, int xmin, int xmax, int ymin, int ymax, _cdCanvas *canvas)
{
    int xm = (xmax + xmin) / 2;
    int ym = (ymax + ymin) / 2;

    if(line == curline && col == curcol) {
        cdCanvasForeground(canvas, CD_RED);
        cdCanvasBox(canvas, xmin,xmax,ymin,ymax);
    }

    //std::string v = std::to_string(line) + ":" + std::to_string(col);
    std::string v = matrix.at(line - 1, col - 1);
    cdCanvasTextAlignment(canvas, CD_CENTER);
    cdCanvasForeground(canvas, CD_BLACK);
    cdCanvasText(canvas, xm,ym, v.c_str());

    return IUP_DEFAULT;
}

int Cells::height_cb(Ihandle *, int /*line*/)
{
    return 15;
}

int Cells::width_cb(Ihandle *, int /*column*/)
{
        return 70;
}

int Cells::mouseclick_cb(Ihandle *, int /*button*/, int /*pressed*/, int line, int column,
                         int /*x*/, int /*y*/, char * /*status*/)
{
    curline = line;
    curcol = column;
    setAttr("REPAINT","YES");
    return IUP_DEFAULT;
}

int Cells::k_any(Ihandle * /*ih*/, int key)
{

    std::cout  << "key " << key << std::endl;
    if(iup_isprint(key)) {
        std::string v = matrix.at(curline - 1, curcol - 1) + char(key);
        matrix.setValue(curline - 1, curcol - 1, v);
    } else {
        switch(key) {
        case K_BS:
        {
            std::string v = matrix.at(curline - 1, curcol - 1);
            std::string v2 = v.substr(0, v.size() - 1);
            matrix.setValue(curline - 1, curcol - 1, v2);
            break;
        }
        case K_UP:
        {
            if( curline > 1 ) curline--;
            break;
        }
        case K_DOWN:
        {
            if( curline < nlines ) curline++;
            break;
        }
        case K_LEFT:
        {
            if(curcol > 1) curcol--;
            break;
        }
        case K_RIGHT:
        {
            if(curcol < ncols ) curcol++;
            break;
        }
        }

    }
        setAttr("REPAINT","YES");
        return IUP_DEFAULT;
}
