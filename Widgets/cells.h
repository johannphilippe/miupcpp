
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef CELLS_H
#define CELLS_H

#include"Core/miupobj.h"
#include"Core/canvas.h"
#include"JTL/jmatrix.h"

class Cells : public MiupObj
{
public:
    Cells(int lines = 8, int cols = 8);
    ~Cells();

    void setValueAt(int lin, int col, std::string val);
    std::string getValueAt(int lin, int col);

protected:
    IUP_CLASS_DECLARECALLBACK_IFn(Cells, ncols_cb)
    IUP_CLASS_DECLARECALLBACK_IFn(Cells, nlines_cb)
    IUP_CLASS_DECLARECALLBACK_IFniiiiiiC(Cells,draw_cb)

    IUP_CLASS_DECLARECALLBACK_IFni(Cells,height_cb)
    IUP_CLASS_DECLARECALLBACK_IFni(Cells,width_cb)
    IUP_CLASS_DECLARECALLBACK_IFniiiiiis(Cells,mouseclick_cb)

    IUP_CLASS_DECLARECALLBACK_IFni(Cells, k_any)

private:
    int ncols, nlines, curcol, curline;

    JMatrix<std::string> matrix;

};

#endif // CELLS_H
