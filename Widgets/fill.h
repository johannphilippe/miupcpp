#ifndef FILL
#define FILL

#include"Core/miupobj.h"

class Fill : public MiupObj
{
public:
    Fill() {
        obj=IupFill();
    }

    Fill(int width, int height) {
        obj = IupFill();
        setSize(width, height);
    }

    virtual ~Fill() {
    }

private:
};

#endif // FILL

