
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "gainmeter.h"

GainMeter::GainMeter(const char *orientation) : MiupObj(IupCanvas(NULL)),
    is_mapped(false),
    mmin(-90.0),
    mmax(6.0),
    amb(96.0),
    value(-90.0),
    cval(-90.0),
    mode(std::string(orientation))
{
    setAttr("CURSOR","CROSS");
    setAttr("EXPAND","NO");

    if(mode=="VERTICAL") {
       setAttr("RASTERSIZE","20x200");
    } else if(mode=="HORIZONTAL") {
       setAttr("RASTERSIZE","200x20");
    }

    IUP_CLASS_INITCALLBACK(obj,GainMeter);
    IUP_CLASS_SETCALLBACK(obj,"MAP_CB",map_cb);
    IUP_CLASS_SETCALLBACK(obj,"REDRAW_CB",redraw_cb);
    IUP_CLASS_SETCALLBACK(obj,"BUTTON_CB",button_cb);
    IUP_CLASS_SETCALLBACK(obj,"MOTION_CB",motion_cb);
    IUP_CLASS_SETCALLBACK(obj,"ENTERWINDOW_CB",enter_cb);
    IUP_CLASS_SETCALLBACK(obj,"LEAVEWINDOW_CB",leave_cb);
    IUP_CLASS_SETCALLBACK(obj,"ACTION",action_cb);
}

GainMeter::~GainMeter()
{
    delete canvas;
}


void GainMeter::setValue(double val)
{
   // std::cout <<  val << std::endl;
        if(val<mmin){
            value=mmin;
            cval=mmin;
            }
        else if(val>mmax) {
            value=mmax;
            cval=mmin;
            }
        else {
            value=val;
            cval=val;
            }
        draw();
}

void GainMeter::draw()
{
    std::pair<int,int> size=canvas->getSizeInPixel();
    double base_1,normalized;
    base_1=(value+std::abs((int)mmin))/amb;

    canvas->activate();
    canvas->clear();

    if(mode=="HORIZONTAL") {
        normalized=base_1*size.first;
        canvas->setForegroundColor(CD_GREEN);
        canvas->box(0,normalized,0,size.second);
        if(value>-12) {
            canvas->setForegroundColor(CD_YELLOW);
            double ybase_1=(abs(mmin)-12)/amb;
            double ynormalized=(ybase_1*size.first);
            canvas->box(ynormalized,normalized,0,size.second);
        }
        if (value>0) {
            canvas->setForegroundColor(CD_RED);
            double rbase_1=abs(mmin)/amb;
            double rnormalized=(rbase_1*size.first);
            canvas->box(rnormalized,normalized,0,size.second);
        }
    } else if(mode=="VERTICAL") {
        normalized=(base_1*size.second); // rescale to height
        canvas->setForegroundColor(CD_GREEN);
        canvas->box(0,size.first,normalized,0);
        if(value>-12) {
            canvas->setForegroundColor(CD_YELLOW);
            double ybase_1=(abs(mmin)-12)/amb;
            double ynormalized=(ybase_1*size.second);
            canvas->box(0,size.first,ynormalized,normalized);
        }
        if(value>0) {
            canvas->setForegroundColor(CD_RED);
            double rbase_1=abs(mmin)/amb;
            double rnormalized=(rbase_1*size.second);
            canvas->box(0,size.first,rnormalized,normalized);
        }
    }
    canvas->deactivate();
}

//Callbacks
int GainMeter::redraw_cb(Ihandle * /*ih*/, float /*sx*/, float /*sy*/)
{
    return IUP_CLOSE;
}

int GainMeter::action_cb(Ihandle * /*ih*/, float /*x*/, float /*y*/)
{
    if(!is_mapped) { // init draw here
        is_mapped=true;
        canvas->activate();
        canvas->clear();
        canvas->deactivate();
    }
    return IUP_CLOSE;
}

int GainMeter::button_cb(Ihandle *, int /*but*/, int /*stat*/, int /*x*/, int /*y*/)
{
    return IUP_CLOSE;
}

int GainMeter::motion_cb(Ihandle *, int /*x*/, int /*y*/)
{
    return IUP_CLOSE;
}

int GainMeter::enter_cb(Ihandle *)
{
    return IUP_CLOSE;
}

int GainMeter::leave_cb(Ihandle *)
{
    return IUP_CLOSE;
}

int GainMeter::map_cb(Ihandle *)
{
    canvas=new Canvas(obj);
    canvas->setForegroundColor(CD_WHITE);
    return IUP_CLOSE;
}
