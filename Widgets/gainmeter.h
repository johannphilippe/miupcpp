
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef GAINMETER_H
#define GAINMETER_H

#include"Core/miupobj.h"
#include"Core/canvas.h"
#include"utils/timer.h"
#include"Core/miup.h"
#include<functional>

class GainMeter : public MiupObj
{
public:
    GainMeter(const char *orientation="VERTICAL");
    ~GainMeter();

    void setValue(double val);

    std::function<void(double)> fptr;
protected:
    virtual IUP_CLASS_DECLARECALLBACK_IFnff(GainMeter,redraw_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFniiii(GainMeter,button_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFnii(GainMeter,motion_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(GainMeter,leave_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(GainMeter,enter_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(GainMeter,map_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFnff(GainMeter,action_cb)

private:
    void draw();
    double mmin,mmax,amb;


    std::string mode;
    double value;
    double cval;
    Canvas *canvas;
    bool is_mapped;
};

#endif // GAINMETER_H
