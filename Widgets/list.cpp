
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "list.h"

List::List()
{
    obj=IupList(NULL);
    IUP_CLASS_INITCALLBACK(obj,List);
    IUP_CLASS_SETCALLBACK(obj,"VALUECHANGED_CB",valuechanged_cb);
    IUP_CLASS_SETCALLBACK(obj, "BUTTON_CB",button_cb);
    IUP_CLASS_SETCALLBACK(obj,"K_ANY",k_any);
    IUP_CLASS_SETCALLBACK(obj, "DBLCLICK_CB",dbclick_cb);
}

List::~List()
{

}

void List::push_back(const char *name)
{
    std::string idx=std::to_string(getAttrInt("COUNT")+1);
    //std::cout << " idx " << idx << " : & name : " << name <<  std::endl;
    setAttr(idx.c_str(),name);
}

void List::push_back(std::string name) {push_back(name.c_str());}

void List::push_list(std::list<std::string> &list_)
{
    std::string str("");
   std::size_t cnt = 1;
    for(auto & it : list_) {
        str += std::to_string(cnt) + "=" + it;
        if(cnt < list_.size() ) str += ", ";
        cnt++;
    }
   list_of_items= str;
   setAttrs(list_of_items.c_str());
}

void List::pop_back()
{
    setAttrInt("REMOVEITEM",getAttrInt("COUNT"));
}

void List::clear()
{
    setAttr("REMOVEITEM","ALL");
}

void List::removeSelectedItem()
{
    setAttr("REMOVEITEM",getAttr("VALUE"));
}


const char *List::getSelectedItem()
{
    int idx=getAttrInt("VALUE");
    const char *val=getAttr(std::to_string(idx).c_str());
    return val;
}

const char *List::getItem(int idx)
{
    const char *val=getAttr(std::to_string(idx).c_str());
    return val;
}

int List::getSelectedItemIndex()
{
    return getAttrInt("VALUE");
}

int List::count()
{
    return getAttrInt("COUNT");
}

void List::selectItemFromText(std::string str)
{
    for(int i = 1; i <= getAttrInt("COUNT") ; i++) {
        std::string cur = getAttrStr(std::to_string(i).c_str());
        if(cur == str)  {
            setAttrInt("VALUE",i);
            break;
        }
    }
}


bool List::itemExists(std::string itemname)
{
    bool exists = false;
    for(int i = 1; i<= getAttrInt("COUNT"); i++) {
        if(getAttrStr(std::to_string(i).c_str()) == itemname) {
            exists = true;
            break;
        }
    }
    return exists;
}


//Callbacks
int List::valuechanged_cb(Ihandle *)
{
    valueChangedSig(getSelectedItem());
    return IUP_CLOSE;
}

int List::button_cb(Ihandle *, int /*button*/, int /*pressed*/, int /*x*/, int /*y*/, char * /*status*/)
{

    return IUP_DEFAULT;
}

int List::k_any(Ihandle *, int val)
{
    keyPressedSig(val);
    return IUP_DEFAULT;
}

int List::dbclick_cb(Ihandle *, int item, char *text)
{
   // std::cout << " double clicked " << std::endl;
    doubleClickedSig(item, text);
    return IUP_DEFAULT;
}



