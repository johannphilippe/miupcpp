
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef LIST_H
#define LIST_H

#include"Core/miupobj.h"
class List : public MiupObj
{
public:
    List();
    virtual ~List();

    void push_back(const char *name);
    void push_back(std::string name);

    void push_list(std::list<std::string>&list_);

    void pop_back();
    void clear();

    void removeSelectedItem();

    const char *getSelectedItem();
    const char *getItem(int idx);

    int count();

    int getSelectedItemIndex();
    void selectItemFromText(std::string str);

    bool itemExists(std::string itemname);


    lsignal::signal<void(const char *value)> valueChangedSig;
    lsignal::signal<void(int item, const char *text)> doubleClickedSig;

    lsignal::signal<void(int key)> keyPressedSig;

protected:
    virtual IUP_CLASS_DECLARECALLBACK_IFn(List,valuechanged_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFniiiis(List, button_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFni(List, k_any)
    virtual IUP_CLASS_DECLARECALLBACK_IFnis(List, dbclick_cb)
private:
        //std::vector<std::string> list;
        std::string list_of_items;

};

#endif // LIST_H
