
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "matrix.h"
Matrix::Matrix( char *lin, char *col)
{
    obj=IupMatrixEx();
    setAttr("NUMLIN",lin);
    setAttr("NUMCOL",col);
    redraw();
}

Matrix::Matrix(int lin, int col) {
    obj=IupMatrixEx();
    setAttrInt("NUMLIN",lin);
    setAttrInt("NUMCOL",col);
    redraw();
}

Matrix::~Matrix() {
}

void Matrix::clear() {
    setAttr("CLEARVALUE","ALL");
}

void Matrix::redraw() {
    IupSetAttribute(obj,"REDRAW","ALL");
}

void Matrix::setNumcol(int val) {
    setAttrInt("NUMCOL",val);
}

void Matrix::setNumlin(int val) {
    setAttrInt("NUMLIN",val);
}

const char *Matrix::getValueAt(int lin, int col)
{
    std::string attr= std::to_string(lin) + ":" + std::to_string(col);
    const char *res=getAttr(attr.c_str());
    if(res==NULL) return "";
    return res;
}

void Matrix::setValueAt(int lin, int col, const char *val)
{
    std::string attr=std::to_string(lin) + ":" + std::to_string(col);
    setAttr(attr.c_str(),val);
}

