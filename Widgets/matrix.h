
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef MATRIX_H
#define MATRIX_H

#include"Core/miupobj.h"
#include"iupcbs.h"
#include<iostream>
#include<stdio.h>
#include"utils/miuputil.h"

class Matrix :  public MiupObj
{
public:
    Matrix(char *lin,char *col);
    Matrix(int lin,int col);
    
    virtual ~Matrix();


    void setNumcol(int val);
    void setNumlin(int val);

    const char *getValueAt(int lin,int col);
    void setValueAt(int lin,int col, const char *val);


    void clear();

    virtual void redraw();

private:

protected:

};

#endif // MATRIX_H
