
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "miupslider.h"

MiupSlider::MiupSlider(const char *orientation,double initval,double minval, double maxval) :
    init(initval),
    mmin(minval),
    mmax(maxval),
    mode(orientation),
    is_mapped(false)
{
    amb=mmax-mmin;
    obj=IupCanvas(NULL);
    setAttr("CURSOR","CROSS");
    setAttr("EXPAND","NO");

    if(mode=="VERTICAL") {
       setAttr("RASTERSIZE","20x200");
    } else if(mode=="HORIZONTAL") {
        setAttr("RASTERSIZE","200x20");
    }

    IUP_CLASS_INITCALLBACK(obj,MiupSlider);
    IUP_CLASS_SETCALLBACK(obj,"MAP_CB",map_cb);
    IUP_CLASS_SETCALLBACK(obj,"BUTTON_CB",button_cb);
    IUP_CLASS_SETCALLBACK(obj,"MOTION_CB",motion_cb);
    IUP_CLASS_SETCALLBACK(obj,"ENTERWINDOW_CB",enter_cb);
    IUP_CLASS_SETCALLBACK(obj,"LEAVEWINDOW_CB",leave_cb);
    IUP_CLASS_SETCALLBACK(obj,"ACTION",action_cb);
    IUP_CLASS_SETCALLBACK(obj,"RESIZE_CB",resize_cb);
}

MiupSlider::~MiupSlider()
{
    cdKillCanvas(canvas);
}

void MiupSlider::setOrientation(const char *orientation) {mode=std::string(orientation);setValue(value);}

void MiupSlider::calculateValue(int x,int y, int w,int h)
{
    if(mode=="HORIZONTAL") {
        double x_base1=(double)x/w;
        value=(x_base1 * amb)+mmin;
    } else if(mode=="VERTICAL") {
        double y_base1=(double)(h-y)/h;
        value=(y_base1*amb)+mmin;
    }
    if(value<mmin) value=mmin;
    if(value>mmax) value=mmax;
    emitValueSig(value);
    //std::cout << value << std::endl;
}

void MiupSlider::setRange(double minval, double maxval)
{
    mmin=minval;
    mmax=maxval;
    amb=mmax-mmin;
}

void MiupSlider::setValue(double val)
{
    int w,h;
    double mw,mh;
    cdCanvasActivate(canvas);
    cdCanvasGetSize(canvas,&w,&h,&mw,&mh);

    if(val<mmin) value=mmin;
    if(val>mmax) value=mmax;

    double base_1=(value+abs(mmin))/amb;
    cdCanvasClear(canvas);
    if(mode=="HORIZONTAL") {
       double x=base_1*w;
       draw(canvas,x,0,w,h);
    } else if(mode=="VERTICAL") {
       double y=h-(base_1*h);
       //std::cout << "slider val y  : " << y << std::endl;
       draw(canvas,0,y,w,h);
    }
    cdCanvasDeactivate(canvas);
    emitValueSig(value);
}


void MiupSlider::setColor(int r, int g , int b)
{
    color=cdEncodeColor(r,g,b);
    cdCanvasForeground(canvas,cdEncodeColor(r,g,b));
    int w,h;
    double mw,mh;
    cdCanvasGetSize(canvas,&w,&h,&mw,&mh);
    draw(canvas,b_x,b_y,w,h);
}

void MiupSlider::draw(cdCanvas *cv,int x,int y,int w,int h)
{
    int yval=h-y; // necessary to make a vertical from bottom to top
    int xval;

    if(x>w) xval=w;
    else if(x<0) xval=0;
    else xval=x;
    if(yval>h) yval=h;
    else if(yval<0) yval=0;

        cdCanvasForeground(cv,CD_WHITE);

        if(mode=="HORIZONTAL") {

            cdCanvasChord(cv,0+(w/10),h/2,w/5,h,90,270);
            cdCanvasBox(cv,0+(w/10),w-(w/10),0+(h/10),h);
            cdCanvasChord(cv,w-(w/10),h/2,w/5,h,270,360+90);

            cdCanvasForeground(canvas,color);

            if(xval<w/5) {
                cdCanvasChord(cv,xval/2,h/2,xval,h,90,270);
                cdCanvasChord(cv,xval/2,h/2,xval,h,270,360+90);
            } else {
                cdCanvasChord(cv,0+(w/10),h/2,w/5,h,90,270);
                cdCanvasBox(cv,0+(w/10),xval-(w/10),0+(h/10),h);
                cdCanvasChord(cv,xval-(w/10),h/2,w/5,h,270,360+90);
            }

        } else if(mode=="VERTICAL") {

            cdCanvasChord(cv,w/2,h-(h/10),w,h/5,0,180);
            cdCanvasBox(cv,0,w,h-(h/10),0+(h/10));
            cdCanvasChord(cv,w/2,(h/10),w,h/5,180,360);

            cdCanvasForeground(canvas,color);

            if(y>(h/5)*4) {
                cdCanvasChord(cv,w/2,yval/2,w,yval,0,180);
                cdCanvasChord(cv,w/2,yval/2,w,yval,180,360);
            } else {
                cdCanvasChord(cv,w/2,yval-(h/10),w,h/5,0,180);
                cdCanvasBox(cv,0,w,yval-(h/10),0+(h/10));
                cdCanvasChord(cv,w/2,(h/10),w,h/5,180,360);
            }
        }
}

void MiupSlider::resize(int w, int h)
{
    double base_1=(value+abs(mmin))/amb;
    cdCanvasActivate(canvas);
    cdCanvasClear(canvas);
    if(mode=="HORIZONTAL") {
       double x=base_1*w;
       draw(canvas,x,0,w,h);
    } else if(mode=="VERTICAL") {
       double y=h-(base_1*h);
       draw(canvas,0,y,w,h);
    }
    cdCanvasDeactivate(canvas);
}

//callbacks
int MiupSlider::action_cb(Ihandle *, float /*x*/, float /*y*/)
{
    //std::cout << "ACTION " << x << " " << y << std::endl;
    //after first map -- is_mapped should repass 0 when window is left/ and comes back
    if(!is_mapped) {
        is_mapped=true;
        cdCanvasActivate(canvas);
        cdCanvasClear(canvas);
        cdCanvasDeactivate(canvas);
        setValue(init);
    } else {
        int ww,hh;
        double mw,mh;
        cdCanvasActivate(canvas);
        cdCanvasGetSize(canvas,&ww,&hh,&mw,&mh);

        resize(ww,hh);
    }
    //std::cout << std::rand() % 100 << std::endl;
    return IUP_CLOSE;
}

int MiupSlider::button_cb(Ihandle *, int but, int stat, int x, int y)
{
    b_but=but;
    b_stat=stat;

    if(b_but==49 && b_stat==1) {
        b_x=x;
        b_y=y;
        int w,h;
        double mw,mh;
        cdCanvasActivate(canvas);
        cdCanvasGetSize(canvas,&w,&h,&mw,&mh);
        cdCanvasClear(canvas);
        draw(canvas,x,y,w,h);
        calculateValue(x,y,w,h);
    } else if(b_stat==0) {
        cdCanvasDeactivate(canvas);
    }

    return IUP_CLOSE;
    //std::cout << "Button : mode : " << mode  << " "  << but << " " << stat << " " << x << "  " << y << std::endl;
}

int MiupSlider::motion_cb(Ihandle *, int x, int y)
{
    if(b_but==49 && b_stat==1) {
        b_x=x;
        b_y=y;
        int w,h;
        double mw,mh;
        cdCanvasActivate(canvas);
        cdCanvasGetSize(canvas,&w,&h,&mw,&mh);
        cdCanvasClear(canvas);
        draw(canvas,x,y,w,h);
        calculateValue(x,y,w,h);
    } else if(b_stat==0) {
        cdCanvasDeactivate(canvas);
    }
    //std::cout << " MOTION : " << x << " " << y << std::endl;
    return IUP_CLOSE;
}

int MiupSlider::enter_cb(Ihandle *)
{
    //std::cout << "enter : " << mode << std::endl;
    return IUP_CLOSE;
}

int MiupSlider::leave_cb(Ihandle *)
{
    //std::cout << "leave : " << mode << std::endl;
    return IUP_CLOSE;
}

int MiupSlider::map_cb(Ihandle *)
{
    //std::cout << "Mapped OK " << std::endl;
    canvas=cdCreateCanvas(CD_IUP,obj);

    color=CD_BLUE;
    cdCanvasForeground(canvas,CD_BLUE);
    cdCanvasBackground(canvas,CD_TRANSPARENT);
    char res[]="0";
    cdCanvasSetAttribute(canvas,"ANTIALIAS",res);

    //std::cout << "color : " << IupGetGlobal("DLGBGCOLOR") << std::endl;
    cdCanvasBackground(canvas,cdEncodeColor(212,212,212));

    return IUP_CLOSE;
}

int MiupSlider::resize_cb(Ihandle *, int /*w*/, int /*h*/)
{
    return IUP_CLOSE;
}
