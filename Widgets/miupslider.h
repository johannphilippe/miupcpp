
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef MIUPSLIDER_H
#define MIUPSLIDER_H
#include"Core/miupobj.h"
#include"cd.h"
#include"cdiup.h"
#include"Dependencies/lsignal/lsignal.h"

class MiupSlider : public MiupObj
{
public:
    MiupSlider(const char *orientation="HORIZONTAL",double initval=0., double minval=0.0, double maxval=1.0);
    virtual ~MiupSlider();

    void setRange(double minval,double maxval);
    void setValue(double val);
    void setColor(int r,int g,int b);
    void setOrientation(const char *orientation);

    lsignal::signal<void(double)> emitValueSig;

protected:
    virtual IUP_CLASS_DECLARECALLBACK_IFniiii(MiupSlider,button_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFnii(MiupSlider,motion_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(MiupSlider,leave_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(MiupSlider,enter_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFn(MiupSlider,map_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFnff(MiupSlider,action_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFnii(MiupSlider,resize_cb)

private:
    void calculateValue(int x,int y,int w,int h);
    void draw(cdCanvas *cv,int x,int y, int w,int h);

    void resize(int w, int h);

    long color;
    double value;
    double init,mmin,mmax,amb;
    cdCanvas *canvas;
    int b_but,b_stat;
    int b_x,b_y;
    std::string mode;
    bool is_mapped;
};

#endif // MIUPSLIDER_H
