
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "slider.h"

template<typename T>
Slider<T>::Slider(T init,T min,T max,T step,const char *orientation)
{
    obj=IupVal(orientation);
    setAttr("MIN",MiupUtil::string::getCString(min,5));
    setAttr("MAX",MiupUtil::string::getCString(max,5));
    setAttr("STEP",MiupUtil::string::getCString(step,5));
    setAttr("VALUE",MiupUtil::string::getCString(init,5));

    IUP_CLASS_INITCALLBACK(obj,Slider);
    IUP_CLASS_SETCALLBACK(obj,"VALUECHANGED_CB",valuechanged_cb);
}

template<typename T>
Slider<T>::~Slider() {
}

template<typename T>
int Slider<T>::valuechanged_cb(Ihandle *)
{
    preValueChangedSig(value);
    value=getAttrFloat("VALUE");
    postValueChangedSig(value);
     return IUP_CLOSE;
}

template class Slider<int>;
template class Slider<float>;
template class Slider<double>;

