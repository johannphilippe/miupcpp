
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "spinbox.h"


template<typename T>
SpinBox<T>::SpinBox(T initval,T minval,T maxval,T stepval)
{
    obj=IupText(NULL);
    //nbx=IupText(NULL);
    if (maxval == NULL)
        max = (std::numeric_limits<T>::max)();
    else
        max=maxval;

    min=minval;
    value=initval;
    strVal = MiupUtil::string::getString(value);
   // std::cout << "value = " << strVal << std::endl;
    step=stepval;
    init=initval;

    T smallamb=max-min;
    T smallinit=init-min;
    ambitus=smallamb/step;
    int curinit=smallinit/step;

    IupSetAttribute(obj,"SPIN","YES");
    IupSetInt(obj,"SPINMIN",0);
    IupSetInt(obj,"SPINMAX",ambitus);
    IupSetInt(obj,"SPINVALUE",curinit);
    IupSetAttribute(obj,"SPINAUTO","NO");
    IupSetInt(obj,"SPININC",1);
    setAttr("VALUE",strVal.c_str());

    IUP_CLASS_INITCALLBACK(obj,SpinBox);
    IUP_CLASS_SETCALLBACK(obj,"SPIN_CB",spin_cb);
    IUP_CLASS_SETCALLBACK(obj,"K_ANY",k_any);
}

template<typename T>
SpinBox<T>::~SpinBox(){
}

template<typename T>
T SpinBox<T>::getValue() {
    //return (T)IupGetDouble(obj,"VALUE");
    return value;
}

template<typename T>
void SpinBox<T>::setValue(T val) { 
   T pos=(val-min)/step;
   strPos = std::to_string(pos);
   setAttr("SPINVALUE", strPos.c_str());
   spin_cb(obj,pos);
}

template<typename T>
void SpinBox<T>::setValueNoCallback(T val)
{
    T pos = (val-min) / step;
    strPos = std::to_string(pos);
    setAttr("SPINVALUE",strPos.c_str());
    value=(step*pos)+min;
    //strVal=std::to_string(value);
    strVal = MiupUtil::string::getString(value,5);
    setAttr("VALUE",strVal.c_str());
}

template<typename T>
int SpinBox<T>::spin_cb(Ihandle *,int pos) {
    //std::cout << " pos " << pos << std::endl;
    preValueChangedSig(value);
    value=(step*pos)+min;
    //strVal=std::to_string(value);
    strVal = MiupUtil::string::getString(value, 5);
    setAttr("VALUE",strVal.c_str());
    postValueChangedSig(value);
    return IUP_DEFAULT;
}

template<typename T>
int SpinBox<T>::k_any(Ihandle *object,int c) {
    if(c==65363) {
        int spinval=IupGetInt(obj,"SPINVALUE");
        spinval+=1;
        if (spinval<=ambitus) {
            IupSetInt(obj,"SPINVALUE",spinval);
            spin_cb(object,spinval);
        }
    }

    else if(c==65361) {
        int spinval=IupGetInt(obj,"SPINVALUE");
        spinval-=1;
            if(spinval>=0) {
                IupSetInt(obj,"SPINVALUE",spinval);
                spin_cb(object,spinval);
            }
    } else if((char)c == K_CR) {
        setValue(MiupUtil::string::getNumber<double>(getAttr("VALUE")));
        //std::cout << "value : " << getAttr("VALUE") << std::endl;
    }
        //std::cout << "key  : " << c  << std::endl;
    return IUP_DEFAULT;
}

template class SpinBox<int>;
template class SpinBox<float>;
template class SpinBox<double>;



