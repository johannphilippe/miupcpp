
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef SPINBOX_H
#define SPINBOX_H

#include"Core/miupobj.h"
#include"utils/miuputil.h"
#include<typeinfo>
#include<cstdlib>
#include<vector>
#include"iup_class_cbs.hpp"
#include<limits>

template<typename T>
class SpinBox : public MiupObj
{
public:
    //constructor destructor
    SpinBox(T initval= 0.,T minval=0.,T maxval = std::numeric_limits<T>::max(),T stepval=1.);
    virtual ~SpinBox();

    //deprecated
    lsignal::signal<void(int)> setValueCallback;
    lsignal::signal<void(T)> preValueChangedSig;
    lsignal::signal<void(T)> postValueChangedSig;


    //methods
    T getValue(); // to change
    void setValue(T val);
    void setValueNoCallback(T val);


private:
    //fields
    T value;
    std::string strVal, strPos;
    T init,min,max,ambitus;
    T step;

protected:
    //callback
    virtual IUP_CLASS_DECLARECALLBACK_IFni(SpinBox,spin_cb)
    virtual IUP_CLASS_DECLARECALLBACK_IFni(SpinBox,k_any)
};



#endif // SPINBOX_H
