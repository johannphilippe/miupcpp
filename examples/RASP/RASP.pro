TARGET = RASP
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
include($$PWD/../../MiupCpp.pro) 
SOURCES += examples/RASP/main.cpp \ 
    infotab.cpp

HEADERS += \
    listview.h \
    listitem.h \
    HTTPRequest/include/HTTPRequest.hpp \
    global.h \
    infotab.h

INCLUDEPATH += include/ /usr/include/x86_64-linux-gnu/curl
