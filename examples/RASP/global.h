#ifndef GLOBAL_H
#define GLOBAL_H

#define OK "1"
#define NOT_OK "-1"

#define LOGIN_SALARIE "2"
#define LOGIN_CLIENT "1"
#define LOGIN_BOSS "10"
#define LOGIN_FALSE "-1"

#include"utils/miupdatatype.h"
#include<functional>
#include<iostream>

namespace RASP {

struct EventData {
  EventData(std::function<void(void)> f) {
        func=f;
  }
    std::function<void(void)> func;
};

static concurrent_queue<EventData *>queue;
static EventData *current_event;

        static void EventLoop() {
            if(RASP::queue.try_pop(RASP::current_event)) {
                current_event->func();
            }
            std::cout << "RASP QUEUE TRY POP : " << RASP::queue.try_pop(RASP::current_event)  << std::endl;
        }

        static void pushQueue(EventData *data) {
            RASP::queue.push(data);
            //RASP::queue.notify();
        }

}

#endif // GLOBAL_H
