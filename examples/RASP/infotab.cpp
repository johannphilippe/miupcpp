#include "infotab.h"

InfoTab::InfoTab()  : timer((1000 * 60) * 10), Vbox(), filterSalarie(0,"Salariés"), filterClient(0,"Clients"),
    filterArret(0,"Arrêt de travail"), filterAvis(0,"Avis"), filterReclamation(0,"Réclamations"),
    filterConge(0, "Congés"), filterCommande(0,"Commande"),filterRoleBox(&filterSalarie, &filterClient),
    filterTypeBox(&filterArret, &filterAvis, &filterCommande, &filterConge, &filterRDV, &filterReclamation)
{
    this->push(&listView);
    this->push(&filterRoleBox);
    this->push(&filterTypeBox);

    thread_is_running=false;

    getInfoAsync(nullptr);
    timer.timer_sig.connect([this](){
        getInfoAsync(nullptr);
    });
    timer.start();
   //eventLoopSig.connect(this, &InfoTab::ProcessDataList);
}

InfoTab::~InfoTab() {
    if(timer.isRunning()) {
        timer.stop();
    }

    while(listQueue.try_pop(curData)) {
        delete curData;
    }

}

void InfoTab::getInfoAsync(const char *filter)
{
    std::cout << " get info async" << std::endl;

    if(!thread_is_running) {
        info_thread = std::thread(&InfoTab::getInfo, this,filter);
        info_thread.detach();
    }
}


void InfoTab::getInfo(const char *filter)
{
    thread_is_running=true;
    if(filter==nullptr) { // global request for all info
            http::Request request("http://51.83.46.199/eventlist");
            std::string urlEncoded="email=rasp@test.fr&password=azerty";
            http::Response response = request.send("GET", urlEncoded);
            std::string resultStr(response.body.begin(), response.body.end());
           std::cout << resultStr << std::endl;

           json res=json::parse(resultStr);

           //check return status.

           if(res.find("status") != res.end()) {
                std::string status=res.find("status").value().get<std::string>();
                if(status==OK) {
                    std::cout << "OK status" << std::endl;

                    if(res.find("data")!= res.end()) {
                        std::cout << "found data " << std::endl;

                        for(auto & it : res["data"].items()) {

                            std::string firstname=it.value()["firstname"].get<std::string>();
                            std::string secondname=it.value()["secondname"].get<std::string>();
                            std::string date=it.value()["date"].get<std::string>();
                            std::string type=it.value()["type"].get<std::string>();

                            /*
                            RASP::pushQueue(new RASP::EventData([firstname,secondname,date,type,this]() ->void {
                                push_back(new ListItem(type, firstname, secondname, date));
                            }));

                            */

                            this->listQueue.push(new ListData(firstname, secondname, date,type));
                            /*
                            RASP::queue.push([firstname, secondname, date, type, this]() {
                                this->push_back(new ListItem(type, firstname, secondname, date));
                            });
                            */
                            std::cout << "RASP QUEUE  size : " << RASP::queue.size() << std::endl;
                            std::cout << "Queue size :" << listQueue.size() << std::endl;
                        }
                    }

                } else {
                    std::cout << "not ok status" << std::endl;
                }
           } else {
               std::cout << "no status " << std::endl;
               return;
           }

    } else { // then filter

    }
    thread_is_running=false;
}



void InfoTab::ProcessDataList()
{
    listView.clearAll();
    if(listQueue.try_pop(curData)) {
        //std::shared_ptr<ListItem> item(new ListItem(curData->type, curData->firstname, curData->secondname, curData->date));
        std::cout << curData->type << " " << curData->firstname << std::endl;

        listView.push_back(ListItem::create(curData->type, curData->firstname, curData->secondname, curData->date));
    }
}


