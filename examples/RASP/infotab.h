#ifndef INFOTAB_H
#define INFOTAB_H

#include"listitem.h"
#include"listview.h"
#include"Core/miup.h"
#include<thread>
#include"Dependencies/nlohmann/json.hpp"
#include"HTTPRequest/include/HTTPRequest.hpp"
#include"global.h"
#include"Widgets/button.h"
#include<atomic>
#include"utils/timer.h"
#include"Widgets/toggle.h"
#include"Boxes/radio.h"

using json=nlohmann::json;


class InfoTab : public Vbox
        //ListView<ListItem>
{
public:

    struct ListData {
      ListData(std::string fname = "name" , std::string sname = "lastname" , std::string curdate =" 01/01/2010", std::string t="rdv") {
        firstname=fname;
        secondname=sname;
        date=curdate;
        type=t;
      }

        std::string firstname, secondname, date, type;
    };

    InfoTab();
    ~InfoTab();

    void getInfoAsync(const char *filter=nullptr);

    lsignal::signal<void(std::function<void()>) > emitInfoSig;

    void ProcessDataList();

protected:

private:
    void getInfo(const char *filter);


    std::thread info_thread;
    std::atomic<bool> thread_is_running;

    Timer timer;

    concurrent_queue<ListData *> listQueue;
    ListData *curData;

    std::string currentFilter;
    Toggle filterSalarie, filterClient, filterRDV, filterCommande, filterArret, filterConge, filterReclamation, filterAvis;

    ListView<ListItem> listView;
    Hbox filterTypeBox, filterRoleBox;

};

#endif // INFOTAB_H
