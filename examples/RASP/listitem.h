#ifndef LISTITEM_H
#define LISTITEM_H

#include"Core/miupobj.h"
#include"Boxes/hbox.h"
#include"Text/label.h"
#include"Widgets/flatseparator.h"

class ListItem : public Hbox
{
public:
    ListItem(std::string type="Conges", std::string firstname="prenom", std::string secondname="nom", std::string date="01/01/2019") :
        typeLabel(type), firstnameLabel(firstname), secondnameLabel(secondname), dateLabel(date), sep1("VERTICAL"), sep2("VERTICAL"),
        sep3("VERTICAL"),
        /*Hbox(&typeLabel, &firstnameLabel, &secondnameLabel, &dateLabel) */ Hbox()
    {
        push(&typeLabel, &sep1, &firstnameLabel, &sep2, &secondnameLabel, &sep3 , &dateLabel);
        this->setAttr("EXPAND","HORIZONTAL");
        this->setAttr("NORMALIZESIZE","HORIZONTAL");
    }

    static std::shared_ptr<ListItem> create(std::string type="Conges", std::string firstname="prenom", std::string secondname="nom", std::string date="01/01/2019")
    {
        //std::shared_ptr<ListItem> ptr =
        return std::make_shared<ListItem>(type,firstname, secondname, date);
        //return ptr;
    }

    ~ListItem() {
    }

private:
   FlatSeparator sep1, sep2, sep3;
   Label typeLabel, firstnameLabel, secondnameLabel, dateLabel;
};




#endif // LISTITEM_H
