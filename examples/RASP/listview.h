#ifndef LISTVIEW_H
#define LISTVIEW_H

#include"Core/miupobj.h"
#include"Boxes/scrollbox.h"
#include"Boxes/vbox.h"
#include"Boxes/menu.h"
#include"Widgets/flatseparator.h"

template<typename T>
class ListView : public ScrollBox
{

public:
    ListView() : vbox(new Vbox()), ScrollBox() {
       this->push(vbox);
    }

    ListView(T *o) : vbox(new Vbox(o)), ScrollBox() {
        this->push(vbox);
    }


    ~ListView() {
        for(auto & it : separators) {delete it;}
        for(auto & it : items) {delete it;}
        separators.clear();
        items.clear();
        delete vbox;
    }

    void push_back(T *o) {
        vbox->push(o);
        separators.push_back(new FlatSeparator());
        separators.back()->setAttr("ORIENTATION","HORIZONTAL");
        separators.back()->setAttr("EXPAND","HORIZONTAL");
        vbox->push(separators.back());
    }

    void push_back(std::shared_ptr<T> o) {
        vbox->push(o);
        separators.push_back(new FlatSeparator());
        separators.back()->setAttr("ORIENTATION","HORIZONTAL");
        separators.back()->setAttr("EXPAND","HORIZONTAL");
        vbox->push(separators.back());
    }

    void clearAll() {
        vbox->clear();
        for(auto & it : items) {
            delete it;
        }
        items.clear();
    }

protected:


private:
    Vbox *vbox;
    std::vector<FlatSeparator *> separators;
    std::vector<T *>items;

};



#endif // LISTVIEW_H
