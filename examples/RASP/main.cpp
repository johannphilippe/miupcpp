#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>
#include"HTTPRequest/include/HTTPRequest.hpp"
#include"infotab.h"



int main(int argc,char *argv[])
{

    Miup::Init();
    //eventLoopSig.connect(&RASP::EventLoop);

     InfoTab infoTab;
     eventLoopSig.connect(&infoTab, &InfoTab::ProcessDataList);

     Button test("test");
     test.buttonClickOffSig.connect([&infoTab](){
     });

     Vbox vbx(&infoTab,new Hbox(&test));
    vbx.setAttr("EXPAND","BOTH");

    Dialog dlg(&vbx);

    dlg.setAttr("SIZE","FULLxFULL");
    dlg.show();
    dlg.closeDialogSig.connect(&Miup::StopLoop);

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

