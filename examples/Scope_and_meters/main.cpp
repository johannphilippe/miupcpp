#include<stdio.h>
#include<stdlib.h>

#include"Core/miup_wrapper.h"
#include<iostream>

/** TODO :
--dialog
**/


int main(int argc, char *argv[])
{

using namespace std::placeholders;

    std::string curDir = MiupUtil::filesystem::getCurrentDirectory(); //get current dir
    std::cout << "Current directory : " << curDir << std::endl;
    Miup::Init(Miup::ModeList::native,argc,argv); // init MIUP
    std::string csdPath=curDir + "/../Scope_and_meters/scope_meter.csd"; // find CSD path

    CsoundThreaded cs;
    cs.Compile(csdPath.c_str());
    cs.setRefreshRate(512);

    Button *play=new Button("PLAY");
    play->buttonClickOffSig.connect(&cs,&CsoundThreaded::startSlot);
    play->buttonClickOffSig.connect(&cs,&CsoundThreaded::performAndResetSlot);

    Button *stop=new Button("STOP");
    stop->buttonClickOffSig.connect(&cs,&CsoundThreaded::stopAndResetSlot);

    LevelMeter *dbfs = new LevelMeter(-90.0,-90.0,6.0);
    dbfs->setSize(500,20);
    cs.pushMethodCallback("dbfs",[&dbfs](double val) {dbfs->setAttrDouble("VALUE",val);});

    LevelMeter *db=new LevelMeter(-90.0,-90.0,6.0);
    db->setSize(500,20);
    cs.pushMethodCallback("dbfs2",[&db](double val){db->setAttrDouble("VALUE",val);});

    LevelMeter *lin = new LevelMeter(0,0,1);
    lin->setSize(500,20);
    cs.pushMethodCallback("lin",[&lin](double val){lin->setAttrDouble("VALUE",val);});

    ScopePlot *pl=new ScopePlot(1024);
    cs.pushMethodCallback("toplot",[pl](double val){pl->push(val);});

    //Hbox *hb=new Hbox(play,stop);
    Hbox hb(play,stop);
    Vbox vbx(&hb,dbfs,db,lin,pl);

    std::vector<LevelMeter *>lv;
    Vbox *meter_box=new Vbox();

    for(int i=0;i<10;i++) {
        lv.push_back(new LevelMeter(0,0,1));
        std::string channel="midichan" + std::to_string(i+1);
        cs.pushMethodCallback(MiupUtil::string::iterateChar(channel),[&lv,i](double val){lv.at(i)->setAttrDouble("VALUE",val);});
        meter_box->push(lv[i]);
    }
    vbx.push(meter_box);

    Dialog dlg(&vbx);

    dlg.closeDialogSig.connect([&](){
        cs.Stop();
        Miup::StopLoop();
        return IUP_CLOSE;

    });

    dlg.setAttr("SIZE","FULLxFULL");
    dlg.show();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

