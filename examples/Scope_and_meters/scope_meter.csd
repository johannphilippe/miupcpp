
<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>

<CsInstruments>
sr     = 48000
ksmps  = 1
nchnls = 2

opcode rangefilteri,i,ppp
        ival,imin,imax xin
        ires init ival
        if(ival < imin) then
                ires=imin
        endif
        if(ival>imax) then
                ires=imax
        endif
        xout ires
endop


opcode rangefilterk,k,kpp
        kval,imin,imax xin
        kres = kval
        if(kval<imin) then
                kres=imin
        endif
        if(kval>imax) then
                kres=imax
        endif
        xout kres
endop


opcode mtofa,a,a
        amid xin
        aout=40 * exp(0.057762265 * (amid - 69))
        xout aout
endop


opcode mtofk,k,k
        kmid xin
        kout =40 * exp(0.057762265 * (kmid - 69))
        xout kout
endop

opcode readtaba,a,ppp
        ifn,idur,imode xin
        aout init 0
        aidx linseg 0,abs(idur),1
        if(imode==0) then ; pan
                if(ifn>=0) then
                        aout=ifn
                elseif(ifn<0) then
                        aout tablei aidx,abs(ifn),1
                endif
        elseif(imode==1) then ;env
                if(ifn>0) then
                        aout tablei aidx,abs(ifn),1
                elseif (ifn<0) then
                        aout linseg 0,0.001,1,abs(p3)-0.002,1,0.001,0
                endif
        elseif(imode==2) then ; cps avec conversion depuis midi
                if(ifn>0) then
                        aout=mtofa(a(ifn))
                elseif (ifn<0) then
                        aout= mtofa(a(tablei(aidx,abs(ifn),1)))
                endif
        elseif(imode==3) then ; cps sans conversion
                if(ifn>0) then
                        aout=abs(ifn)
                elseif(ifn<0) then
                        aout tablei aidx,abs(ifn),1
                endif
        endif
        xout aout
endop



opcode readtabk,k,ppp
        ifn,idur,imode xin
        kout init 0
        kidx linseg 0,abs(idur),1
        if(imode==0) then ; pan
                if(ifn>=0) then
                        kout=ifn
                elseif(ifn<0) then
                        kout tablei kidx,abs(ifn),1
                endif
        elseif(imode==1) then ;env
                if(ifn>0) then
                        kout tablei kidx,abs(ifn),1
                elseif (ifn<0) then
                        kout linseg 0,0.001,1,abs(p3)-0.002,1,0.001,0
                endif
        elseif(imode==2) then ; cps avec conversion depuis midi
                if(ifn>0) then
                        kout=mtofa(a(ifn))
                elseif (ifn<0) then
                        kout= mtofk(tablei(kidx,abs(ifn),1))
                endif
        elseif(imode==3) then ; cps sans conversion
                if(ifn>0) then
                        kout=abs(ifn)
                elseif(ifn<0) then
                        kout tablei kidx,abs(ifn),1
                endif
        endif
        xout kout

endop


instr 1
	ifn init p4
	aidx linseg 0,abs(p3),1
        aout tablei aidx,ifn,1

	//debug and qt visualisation
	iinst=p1-int(p1)
	Sinst sprintf "midichan%d",iinst*10

        chnset k(aout),Sinst
endin


instr 2
        aidx linseg 0,abs(p3),1
        aenv tablei aidx,3,1
        af tablei aidx,21,1
        afreq=(af*1000)+100
        aout oscili aenv,3
        chnset k(aout),"toplot"
	outs aout*0.2,aout*0.2
endin

instr 3
//        kidx phasor 0.5
	kidx linseg 0,abs(p3)/4,1,(abs(p3)/4)*3,0
        aout oscili kidx,200
        klin=rms(aout,10)
        kdb=dbfsamp(klin*30000)
        kdb2=k(aout)
        chnset kdb,"dbfs"
        chnset kdb2,"dbfs2"
        chnset klin,"lin"

endin

</CsInstruments>




<CsScore>
f 1 0 16385 -16 0 67 0 1 16198 0 1 118 0 0 
f 2 0 16385 -16 0 1638 0 1 1638 0 0.5 8191 0 0.5 4915 0 0 
f 3 0 16385 -16 0 659 5.25 1 15724 -6.62234 0 
f 4 0 16385 -16 0 169 1.05053 1 16214 -10 0 
f 5 0 16385 -16 0 3276 -3.83777 1 6553 3.07181 0.7 3276 -2.04255 0.4 3276 1.98936 0 
f 6 0 16385 -16 0 16214 10 1 169 0 0 
f 7 0 16385 -16 0 1638 -3.18994 1 14745 1.99255 0 
f 20 0 16385 -16 0 16384 0 1 
f 21 0 16385 -16 0 16384 3.40691 1 
f 22 0 16385 -16 0 16384 7.16755 1 
f 23 0 16385 -16 0 16384 -2.88564 1 
f 24 0 16385 -16 0 16384 -9.22074 1 
f 25 0 16385 -16 1 16384 0 0 
f 26 0 16385 -16 1 16384 -3.23936 0 
f 27 0 16385 -16 1 16384 -10 0 
f 28 0 16385 -16 1 16384 3.04787 0 
f 29 0 16385 -16 1 16384 10 0 

i 1.1 0 10 3
i 1.2 0 10 4
i 1.3 0 10 5 
i 1.4 0 10 2
i 1.5 0 10 7
i 1.6 0 10 6
i 1.7 0 10 23
i 1.8 0 10 28 
i 1.9 0 10 29
i 2.1 0 10
i 3.1 0 10


</CsScore>
</CsoundSynthesizer>
