#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>


int main(int args,char *argv[])
{
    Miup::Init();
    AudioDeviceList *output_list=new AudioDeviceList(AudioDeviceList::DeviceListMode::output);
    AudioDeviceList *input_list=new AudioDeviceList(AudioDeviceList::DeviceListMode::input);

    Vbox *vbx=new Vbox(input_list,output_list);

    Dialog dlg(vbx);
    dlg.show();
    dlg.closeDialogSig.connect([&](){
        Miup::StopLoop();
        return 0;
    });

    Miup::MainLoop();
    Miup::Close();
}
