#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int argc,char *argv[])
{
    Miup::Init();


    Vbox vbx(new Button("PLAY"),new Button("Stop"),new LevelMeter(0,0,1));
    Hbox hbx(&vbx,new Button("YO"));
    ScrollBox sb;

    std::vector<Matrix *>list;
    for(int i=0;i<6;i++) {
        list.push_back(new Matrix(200,8));
        list.back()->setAttr("SIZE","300x500");
        list.back()->setAttr("SCROLLBAR","NO");
        hbx.push(list.back());
    }

    sb.push(&hbx);


    Dialog dlg(&sb);
    dlg.closeDialogSig.connect([&]() {
        Miup::StopLoop();

        return IUP_DEFAULT;
    });
    dlg.show();
    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

