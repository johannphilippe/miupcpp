#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int argc,char *argv[])
{
    Miup::Init();


    Cells c(64,32);
    Vbox box(&c);


    Dialog dlg(&box);
    dlg.setAttr("SIZE","FULLxFULL");
    dlg.setAttr("RASTERSIZE","500x500");
    dlg.show();

    dlg.closeDialogSig.connect([&](){
       Miup::StopLoop();
       return IUP_DEFAULT;
    });


    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

