#!/bin/bash
mkdir $1
cd $1
touch main.cpp
touch "$1.pro"

main="#include\"Core/miup_wrapper.h\"
#include\"jo_tracker/jo_tracker_wrapper.h\"
#include<iostream>

int main(int argc,char *argv[])
{
    Miup::Init();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}
"


echo "$main" > main.cpp

projectfile="TARGET = $1
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
include(\$\$PWD/../../MiupCpp.pro) 
SOURCES += examples/$1/main.cpp \\ 
"

echo "$projectfile" > "$1.pro"
