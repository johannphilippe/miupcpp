#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int args,char *argv[])
{
    Miup::Init();

    Button but("Test");
    Button but2("And");
    Matrix mat(7,5);
    Vbox vbx(&but,&but2,&mat);
    Expander expander(&vbx);

    Dialog dlg(&expander);
    dlg.setAttr("SIZE","FULLxFULL");
    dlg.closeDialogSig.connect(&Miup::StopLoop);
    dlg.show();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

