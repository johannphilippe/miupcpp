#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>
#include"sndfile.h"
#include"utils/luaeditor.h"
#include"Scripting/Lua/lua_fft_api.h"
#include"Scripting/Lua/lua_math_utils.h"
#include"Scripting/Lua/lua_sndfile_api.h"


#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>
#include <stdint.h>
#include <math.h>
#include <fftw3.h>

void test()
{


double array[] = {0.1, 0.6, 0.1, 0.4, 0.5, 0, 0.8, 0.7, 0.8, 0.6, 0.1,0};
//double array2[] = {1, 6, 1, 4, 5, 0, 8, 7, 8, 6, 1,0};
double *out;
double *err;
int i,size = 12;

fftw_complex *out_cpx;

fftw_plan fft;
fftw_plan ifft;
out_cpx = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*(size / 2 + 1));
out = (double *) malloc(size*sizeof(double));
err = (double *) malloc(size*sizeof(double));

fft = fftw_plan_dft_r2c_1d(size, array, out_cpx, FFTW_ESTIMATE); //Setup fftw plan for fft
ifft = fftw_plan_dft_c2r_1d(size, out_cpx, out, FFTW_ESTIMATE); //Setup fftw plan for ifft

fftw_execute(fft);
fftw_execute(ifft);

//printf("Input: \tOutput: \tError:\n");
printf("Input: \tOutput:\n");
for(i=0;i<size;i++)
{
    out[i] = out[i]/size;
err[i] = abs(array[i] - out[i]);
printf("%f\t%f\n",(array[i]),out[i]);
//printf("%f\t%f\t%f\n",(array[i]),out[i],err[i]);
}

fftw_destroy_plan(fft);
fftw_destroy_plan(ifft);
fftw_free(out_cpx);

}


int main(int /*argc*/,char * /*argv*/ [])
{

    test();

    std::string default_path = "/home";

    Miup::Init();

    LuaEditor editor;
    editor.setAttr("EXPAND","YES");

    editor.registerSig.connect([&](Lua *l){
      //fftclass_open(l->getLuaState());
        sndlua_open(l->getLuaState());
        firlua_open(l->getLuaState());
        fftclass_open(l->getLuaState());
        ifftclass_open(l->getLuaState());
      l->Lregister("mathutils",mathutils_lua_api);
    });

    SpinBox<int> fft_length(1024, 64, 100000,  64);
    FFTPlot plot;
    Button but("Browse Soundfile");
    but.buttonClickOffSig.connect([&](){
        FileDialog dlg(FileDialog::OPEN, default_path.c_str());
        dlg.emitPath.connect([&](std::string path){
            plot.drawWith(path, fft_length.getValue());
            default_path = MiupUtil::filesystem::getFolderFromPath(path);
        });
        dlg.popup();
    });

    fft_length.postValueChangedSig.connect([&](int v) {
       plot.setFFTLength(v);
    });

    SpinBox<float> luminosity(1, 0.1, 1000.0, 0.1 );
    luminosity.postValueChangedSig.connect([&](float val) {
       plot.setLuminosity(val);
    });

    SpinBox<int> hop_factor(1, 1, 100, 1);
    hop_factor.postValueChangedSig.connect([&](int val) {
       plot.setHopFactor(val);
    });

    Button exec("ExecScript");
    exec.buttonClickOffSig.connect([&](){
       editor.execute();
    });


    Toggle linear(1, "Linear");
    Toggle logarithmic(0, "Logarithmic");
    Toggle bark(0, "Bark");
    Hbox tbox(&linear, &logarithmic, &bark);
    Radio r(&tbox);

    linear.toggleStateChangedSig.connect([&](int status){
        if(status == 1)
        plot.setScale(FFTPlot::FFTScale::LINEAR);
    });

    logarithmic.toggleStateChangedSig.connect([&](int status){
        if(status == 1)
        plot.setScale(FFTPlot::FFTScale::LOGARITHMIC);
    });

    bark.toggleStateChangedSig.connect([&](int status){
        if(status == 1)
        plot.setScale(FFTPlot::FFTScale::BARK);
    });

    Hbox control_box(&but, &luminosity, &fft_length, &hop_factor);

    Vbox box(&plot, &control_box, &r);
    Vbox script_box(&editor, &exec);

    Dialog dlg(&box);
    dlg.closeDialogSig.connect([&]() {
       Miup::StopLoop();
        return IUP_DEFAULT;
    });

    Dialog script_dlg(&script_box);

    dlg.setAttr("SIZE", "FULLxFULL");
    dlg.show();

    script_dlg.setAttr("SIZE","HALFxHALF");
    script_dlg.show();

    editor.initEditorAttributes();

    //std::cout << "plot size : " << plot.getAttr("SIZE") << std::endl;

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

