#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>


int main(int args,char *argv[])
{
    Miup::Init();

    std::string orc="nchnls=2 \n"
                    "0dbfs=1 \n"
                    "sr=44100 \n"
                    "ksmps=256 \n"
                    "\n"
                    "instr 1 \n"
                    "kenv linseg 0,p3/2,1,p3/4,0.3,p3/4,0 \n"
                    "ao oscili kenv,500 \n"
                    "klin=rms(ao,10) \n"
                    "kdb=dbfsamp(klin) \n"
                    "chnset kdb,\"level\" \n"
                    "outs ao,ao \n"
                    "endin";

    std::string sco="i 1 0 3";

    CsoundThreaded cs;
    cs.CompileOrc(orc.c_str());
    cs.ReadScore(sco.c_str());
    cs.SetOption("-odac:hw:1,0");

    GainMeter *meter=new GainMeter("VERTICAL");
    cs.pushMethodCallback("level",[meter](double val){meter->setValue(val);});


    Button play("Play >");
    play.buttonClickOffSig.connect([&cs](){cs.Start(); cs.Perform();});

    Button but("Change Value");
    but.buttonClickOffSig.connect([meter](){meter->setValue((std::rand() % 96)-90);});

    Button but2("Change Max");

    Vbox vbx(&play,&but,&but2,meter);
    Dialog dlg(&vbx);
    dlg.closeDialogSig.connect([](){std::cout << "closed dialog" << std::endl;Miup::StopLoop();});
    dlg.show();

    Miup::MainLoop();
    Miup::Close();

    return EXIT_SUCCESS;
}

