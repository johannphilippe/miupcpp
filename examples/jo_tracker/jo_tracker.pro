
#    Copyright (C) 2019-2020 Johann Philippe

#    This file is part of jo_tracker

#    The jo_tracker software is free software; you can redistribute it
#    and/or modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.

#    jo_tracker is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public
#    License along with jo_tracker; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#    02110-1301 USA

#    As a special exception, if other files instantiate templates or
#    use macros or inline functions from this file, this file does not
#    by itself cause the resulting executable or library to be covered
#    by the GNU Lesser General Public License. This exception does not
#    however invalidate any other reasons why the library or executable
#    file might be covered by the GNU Lesser General Public License.


TEMPLATE = app
TARGET = jo_tracker
VERSION = 3.0.1
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
include($$PWD/../../MiupCpp.pro) 

SOURCES += examples/jo_tracker/main.cpp

HEADERS += examples/jo_tracker/icons/mute.h \
examples/jo_tracker/icons/mute16.h

RC_ICONS = jt_icon.ico



