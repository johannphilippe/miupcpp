/*
 *
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of jo_tracker

    The jo_tracker software is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    jo_tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with jo_tracker; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.

*/


#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"

#include<iostream>
#include<thread>
#include<unordered_map>
#include<atomic>
#include<functional>

using namespace JoTracker;

int main(int argc, char *argv[])
{

    //   const std::string cur_dir = MiupUtil::filesystem::getCurrentDirectory();
    MiupUtil::filesystem::createDirectoryRecursively(getResDirectory());
   // std::cout << " res : " << getResDirectory() << " score " << getScoreDataPath() << std::endl;

    Miup::Init(Miup::ModeList::native,argc,argv);
    JoTracker::InitializeSoftware();

    JoTrackerParameters::loadParameters();

    json audio_parameters; // find if audio_parameters exists
    PerformanceHostData hostData;

    //if audio params exists, load it, else init a file
         if(MiupUtil::filesystem::isValidNonEmptyFile(std::string(getAudioParametersPath()))) {
             std::ifstream ifs(getAudioParametersPath());
             ifs >> audio_parameters;
             ifs.close();
         } else {
             audio_parameters["nchnls"]=2;
             audio_parameters["ksmps"]=32;
             audio_parameters["dbfs"]=1;
             audio_parameters["sample_rate"]=44100;
             audio_parameters["additional_options"] = "";
             std::ofstream ofs(getAudioParametersPath());
             ofs << audio_parameters.dump(4);
             ofs.close();
         }

    // Main transport tool
    JTransport transport; 		//transport box
            transport.masterLevelChanged.connect([&](double master_val) {
                if(is_playing)
                        ::cs->SetControlChannel("Master",master_val);
            });

    //trackset, main tab
     JTrackSet trackset;

    trackset.setAttr("TABTITLE","Tracks");
    trackset.orderedPlaySig.connect([&](json data) {
        if(is_playing) return;
       play_ordered_performance(&hostData, audio_parameters, data);
       Miup::StartIdle();
    });


    // box with gen editors tabs
    GenEditorTab genbox;
    genbox.setAttr("TABTITLE","GEN Editor");

    //Csound code editor
    CsoundEditorBox editor;
    editor.setAttr("TABTITLE","Csound Editor");


    JOscilloscopeDialog oscilloscopeDialog;


    hostData.editor=&editor;
    hostData.genbox=&genbox;
    hostData.trackset=&trackset;
    hostData.transport=&transport;
    hostData.oscilloscope = &oscilloscopeDialog;

    //global_host_data = &hostData;

    //transport button clicked : doing something with csound performance
    transport.buttonClickedSig.connect([&](JTransport::ClickedButton which) {
        switch(which) {
        case JTransport::ClickedButton::play: // start performance from current line-seq
        {
            if(is_playing) return;
            play_performance(&hostData, audio_parameters, false);
            //std::thread t( [&](){play_performance(&hostData,audio_parameters, false);});
            //t.detach();
            Miup::StartIdle();
            JoTracker::registerRealtimeCallbacks(&hostData, audio_parameters);
            break;
        }
        case JTransport::ClickedButton::stop: // stop performance
        {
            is_playing=false;
            //if(is_playing)

            if(cs != nullptr
                    && cs->IsPlaying())  {
                  cs->stopAndResetSlot();
            }
                  delete cs;
                  cs=nullptr;

            Miup::StopIdle();
            JoTracker::unregisterRealtimeCallbacks();
            trackset.setMarkCell();


            Miup::pushCallbackMethod(0, [&](double /*v*/) {
                transport.clearAllMeters();
                oscilloscopeDialog.getPlot()->setAttr("CLEAR","YES");
            });


            Miup::processQueue();
            Miup::clearQueue();
            break;
        }
        case JTransport::ClickedButton::playseq:
        {
            if(is_playing) return;
            trackset.setCurlin(1);
            play_performance(&hostData, audio_parameters, false);
            Miup::StartIdle();
            JoTracker::registerRealtimeCallbacks(&hostData, audio_parameters);
            break;
        }
        case JTransport::ClickedButton::resumeplay: // resume and play from begining
        {
            if(is_playing) return;
            // find current seq and line, and format a statement
            //std::thread t( [&](){play_performance(&hostData,audio_parameters, true);});
            //t.detach();
            play_performance(&hostData, audio_parameters, true);
            Miup::StartIdle();
            JoTracker::registerRealtimeCallbacks(&hostData, audio_parameters);
            break;
        }
        case JTransport::ClickedButton::record: // record all performance in realtime
        {
            if(is_playing) return;
            record_performance(&hostData, audio_parameters);
            Miup::StartIdle();
            JoTracker::registerRealtimeCallbacks(&hostData, audio_parameters);
            break;
        }
        }
    });




    // Files menu definitions

    Item exportGensItem("Export GENS");
    exportGensItem.clickItemSig.connect([&](){
        export_gens(&genbox);
    });

    Item exportCsdItem("Export as .csd");
    exportCsdItem.clickItemSig.connect([&](){
        ExportAsCsd(&trackset, &genbox, &editor);
    });

    Item renderItem("Render");
    renderItem.clickItemSig.connect([&](){
       render(&hostData, audio_parameters);
    });


    Item renderInstanceItem("Render instances");
    renderInstanceItem.clickItemSig.connect([&](){
       render_instances(&hostData, audio_parameters);
    });


    Item renderSeqItem("Render current sequence");
    renderSeqItem.clickItemSig.connect([&](){
        render_seq(&hostData, audio_parameters);
    });

    Item renderSeqInstanceItem("Render sequence instances");
    renderSeqInstanceItem.clickItemSig.connect([&](){
       render_seq_instances(&hostData, audio_parameters);
    });


    Item saveProjectItem("Save Project as");
    saveProjectItem.setAttr("TITLEIMAGE","IUP_FileSave");
    saveProjectItem.clickItemSig.connect([&](){
        saveProject(&trackset, &genbox, &editor, audio_parameters);
    });

    Item quickSaveItem("Quick Save\tCtrl+S");
    quickSaveItem.setAttr("TITLEIMAGE", "IUP_FileSave");
    quickSaveItem.clickItemSig.connect([&](){
        std::cout << "Quick save " << std::endl;
        trackset.saveAt(getScoreDataPath());
        genbox.saveData();
        editor.saveJson();
        editor.saveMacroData();
    });

    Item saveProjectGatherItem("Gather and Save Project");
    saveProjectGatherItem.setAttr("TITLEIMAGE","IUP_FileSave");
    saveProjectGatherItem.clickItemSig.connect([&](){
       saveProject(&trackset, &genbox, &editor, audio_parameters, true);
    });

   // Item saveScoreItem("Save Score");
   // Item saveOrcItem("Save Orchestra");

   // Separator load_separator;
    Item loadProjectItem("Load Project");
    loadProjectItem.setAttr("TITLEIMAGE","IUP_FileOpen");
    loadProjectItem.clickItemSig.connect([&]() {
       load_project(&trackset, &genbox, &editor, &audio_parameters);
    });


    Item loadSequenceFromProject("Load sequence from project");
    loadSequenceFromProject.setAttr("TITLEIMAGE","IUP_FileOpen");
    loadSequenceFromProject.clickItemSig.connect([&](){
       SpinBox<int> fromBox(1, 1);
       SpinBox<int> toBox(1 , 1);
       Button okBut("OK");
       Hbox b(&fromBox, &toBox, &okBut);
       b.setAttr("GAP","15");
       Dialog loadseq(&b);

       okBut.buttonClickOffSig.connect([&](){
          FileDialog fdlg(FileDialog::OPEN, browse_path.c_str());
          fdlg.emitPath.connect([&](std::string path) {
              loadseq.hide();
              if(!MiupUtil::filesystem::isValidFilePath(path)) {
                  return;
              }
              int from = fromBox.getValue();
              int to = toBox.getValue();
              std::string fromstr = std::to_string(from);
              std::string tostr = std::to_string(to);
              std::ifstream ifs(path);
              json tmp;
              ifs >> tmp;
              ifs.close();
              if(tmp.find("score_data") != tmp.end() && tmp["score_data"].find("score") != tmp["score_data"].end()) {
                  std::cout << "Found score " << std::endl;
                  if(tmp["score_data"]["score"].find(fromstr) != tmp["score_data"]["score"].end()) {
                      std::cout << "found seq " << std::endl;
                      json *data = trackset.getData();
                      data->at("score")[tostr] = tmp["score_data"]["score"][fromstr];
                      trackset.recallSeq(to);
                  }
              }
          });
          fdlg.popup(IUP_CENTER, IUP_CENTER);
       });
       loadseq.popup();

    });


    Item loadOrchestraItem("Load orchestra from project");
    loadOrchestraItem.setAttr("TITLEIMAGE","IUP_FileOpen");
    loadOrchestraItem.clickItemSig.connect([&]() {
        load_orchestra(&editor);
    });


    Separator newProjectSeparator;

    Item newProjectItem("New Project");
    newProjectItem.setAttr("TITLEIMAGE","IUP_FileNew");
    newProjectItem.clickItemSig.connect([&](){
        clearProject(&trackset, &genbox, &editor);
        redrawMainDialog();
    });

    Separator save_export_separator;

    Menu filesMenu(&newProjectItem, &newProjectSeparator,&quickSaveItem,
                   &saveProjectItem, &saveProjectGatherItem,
                   &loadProjectItem  , &loadOrchestraItem, &loadSequenceFromProject,  &save_export_separator,
                   &exportCsdItem, &exportGensItem, &renderItem, &renderInstanceItem, &renderSeqItem, &renderSeqInstanceItem);

    SubMenu filesSubMenu("Files", &filesMenu);

    Item audioParamsItem("Audio");
    audioParamsItem.setAttr("TITLEIMAGE", "IUP_ToolsSettings");
    audioParamsItem.clickItemSig.connect([&](){ // audio parameter dialog

       if(MiupUtil::filesystem::isValidNonEmptyFile(std::string(getAudioParametersPath()))) {
          std::ifstream ifs(getAudioParametersPath());
          ifs >> audio_parameters;
          ifs.close();
       }

        ExportParametersDialog paramDlg; // put parameters in here if found some

        if(audio_parameters.find("sample_rate") != audio_parameters.end())  { // then everything should be here
           paramDlg.setSampleRate(audio_parameters["sample_rate"].get<int>());
           paramDlg.setNChnls(audio_parameters["nchnls"].get<int>());
           paramDlg.setDbfs(audio_parameters["dbfs"].get<int>());
           paramDlg.setKsmps(audio_parameters["ksmps"].get<int>());
        }

        if(audio_parameters.find("additional_options") != audio_parameters.end()) {
            std::string addit = audio_parameters["additional_options"].get<std::string>();
            paramDlg.setAdditionalOptions(addit);
        }

        paramDlg.buttonClicked.connect([&](bool ok) {
            paramDlg.hide();
            if(ok) {

                audio_parameters["sample_rate"]= paramDlg.getSampleRate();
                audio_parameters["nchnls"]=paramDlg.getNChnls();
                audio_parameters["ksmps"]=paramDlg.getKsmps();
                audio_parameters["dbfs"]=paramDlg.getDbfs();
                audio_parameters["additional_options"] = paramDlg.getAddtionalOptions();

             //   std::string additional = paramDlg.getAddtionalOptions();
             //   if(additional.size() >0) audio_parameters["additional_options"] = additional;

                std::ofstream ofs(getAudioParametersPath());
                ofs << audio_parameters.dump(4);
                ofs.close();
            }
        });
        paramDlg.popup(IUP_CENTER, IUP_CENTER);


    });


    Item globalParametersItem("Global parameters");
    globalParametersItem.setAttr("TITLEIMAGE", "IUP_ToolsSettings");
    globalParametersItem.clickItemSig.connect([&](){
       ParametersDialog paramdlg;


       paramdlg.buttonClickedSig.connect([&](bool is){

           if(is) {
               trackset.redrawTracks();
           } else {

           }

           std::cout << "is true :::: " << is << std::endl;
       });

       paramdlg.popup(IUP_CENTER, IUP_CENTER);

    });



    Menu parametersMenu(&audioParamsItem, &globalParametersItem);
    SubMenu parametersSubMenu("Parameters", &parametersMenu);




    Item oscilloscopeDisplayItem("Oscilloscope");
    oscilloscopeDisplayItem.setAttr("AUTOTOGGLE","YES");
    oscilloscopeDisplayItem.setAttr("IMPRESS","IUP_ActionOk");
    oscilloscopeDisplayItem.clickItemSig.connect([&](){
            oscilloscopeDialog.show();
            std::cout << "IS VISIBLE "  << oscilloscopeDialog.isVisible() << std::endl;
    });



    JToolBox toolbox(&trackset);

    Item toolboxDisplayItem("ToolBox");
    toolboxDisplayItem.setAttr("AUTOTOGGLE", "YES");
    toolboxDisplayItem.setAttr("IMPRESS", "IUP_ToolsSettings");
    toolboxDisplayItem.clickItemSig.connect([&](){
        toolbox.show();
    });



    MacroDialog macro_dialog;
    // assign the jotrackerutilities pointer to this object
    macro_dlg = &macro_dialog;

    macro_dialog.closeSig.connect([&](bool ok) {
        if(ok) {
          json macro_data = macro_dialog.getData();
          editor.setMacroData(&macro_data);
          editor.saveMacroData();
        }
    });

    Item macroDisplayDialogItem("Edit Global Code");
    macroDisplayDialogItem.clickItemSig.connect([&](){
        macro_dialog.setData(editor.getMacroData());
        macro_dialog.show();
    });

    Menu displayMenu(&oscilloscopeDisplayItem, &macroDisplayDialogItem, &toolboxDisplayItem);
    SubMenu displaySubMenu("View", &displayMenu);


    AboutDialog aboutDialog;

    Item aboutItem("About jo_tacker");
    aboutItem.clickItemSig.connect([&](){
        //aboutDialog.setAttr("SIZE","500x600");
        std::string aboutPath = getTrackerDocumentationPath() + "/about.html";
        aboutDialog.setAttr("SIZE", "800x400");
        aboutDialog.showWith(aboutPath);
    });


    Item docItem("Documentation");
    docItem.clickItemSig.connect([&](){
       std::string docPath = getTrackerDocumentationPath() + "/README.html";
       aboutDialog.setAttr("SIZE", "FULLxFULL");
       aboutDialog.showWith(docPath);
    });

    Menu helpMenu(&aboutItem, &docItem);
    SubMenu helpSubMenu("About", &helpMenu);

    // Main menu
    Menu menu(&filesSubMenu, &parametersSubMenu, &displaySubMenu, &helpSubMenu);
    menu.setHandle("mainmenu");

    Detachbox detachable_genbox(&genbox);

    Tabs tab(&trackset, &detachable_genbox,&editor);	 //push all boxes in tab system


    genbox.detachSig.connect([&](bool status) {

        std::cout << "detach :: " << status << std::endl;
        if(status) {
            detachable_genbox.detach_box();
            genbox.setFunctionNumber(1);
        } else {
            detachable_genbox.restore_box();
        }
    });
    detachable_genbox.setAttr("ORIENTATION","VERTICAL");
    detachable_genbox.setAttr("TABTITLE","GEN Editor");


    Hbox transport_box(&transport);
    transport_box.setAttr("ALIGNMENT","ACENTER");

    Vbox fbox(&transport_box , &tab);				//push tabs in a box

    Dialog dlg(&fbox);								//push the box in a dialog
    JoTracker::main_dialog_ptr = &dlg;


    dlg.setAttr("SIZE","FULLxFULL");
    dlg.setAttr("MENU","mainmenu");
    dlg.setAttr("TITLE","Jo_tracker");

    dlg.resizeDialogSig.connect([&](int w, int h) {
        dlg.setAttr("SIZE", NULL);
        dlg.setRasterSize(w,h + 50);
        std::cout << "Dial resized :: " << w << " " << h  << std::endl;
        tab.setMaxWidth(w);
        genbox.setMaxWidth(w);
        trackset.setMaxWidth(w);
        editor.setMaxWidth(w);
    });

    dlg.closeDialogSig.connect([&]() { // close dialog callback ==> for now only callback that actually need a return value
                trackset.saveAt(getScoreDataPath());
                genbox.saveData();
                editor.saveJson();
                editor.saveMacroData();
        Miup::StopLoop();   //when closes dialog, stops Miup MainLoop
        return IUP_DEFAULT; // or IUP_CLOSE - doesnt really matter since mainloop is stopped
    });

    dlg.openDialogSig.connect([&](){
        trackset.recallSeq(1);
    });

    // The scripted_curve tab doesn't draw the scintilla content at startup if not explicitely reloaded
    dlg.mapSig.connect([&](){
       genbox.updateContent(1);
    });

    trackset.recallSeq(1);

    IupSetGlobal("INPUTCALLBACKS","YES");
    IupSetFunction("GLOBALKEYPRESS_CB", (Icallback)global_key);

    global_key_sig.connect([&](int key, int stat){
       if(stat == 0)
       {
           switch(key)
           {
           case 32:
           {
               //transport.buttonClickedSig(JTransport::ClickedButton::play);
               break;
           };
           case 65307:
           {
               //transport.buttonClickedSig(JTransport::ClickedButton::stop);
               break;
           };

           case 536870991:
           {
               //load_project(&trackset, &genbox, &editor, &audio_parameters);
               break;
           };
           case 536870995:
           {
             quickSaveItem.clickItemSig();
             //saveProject(&trackset, &genbox, &editor, audio_parameters);
             break;
           };
           }

       }
    });



    //IupSetGlobal("DLGBGCOLOR", "2 5 50");
    //IupSetGlobal("DLGFGCOLOR", "150 150 150");

    //IupSetGlobal("MENUBGCOLOR", "100 100 100");
    //IupSetGlobal("MENUFGCOLOR", "200 200 200");
    //tab.setAttr("BGCOLOR" , COLOR_DEEPBLUE );
    //tab.setAttr("FGCOLOR", COLOR_LIGHTGREEN);

    dlg.show();			//shows dialog


    Miup::MainLoop();
    //Miup::MainLoopNoWait();


    Miup::Close();

    return EXIT_SUCCESS;
}

