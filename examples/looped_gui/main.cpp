#include"Core/miup_wrapper.h"
#include<iostream>

int main(int argc, char *argv[])
{
    Miup::Init();

    Vbox *vbx[4]; // declare an array of vertical boxes
    MiupSlider *sl[4]; //declare an array of sliders
    LevelMeter *lvmet[4];

    for(int i=0;i<4;i++) {
        std::string colname="column" + std::to_string(i+1); // with label name
        vbx[i]=new Vbox(new Label(colname.c_str()));  // init vbox
        vbx[i]->push(new SpinBox<int>(0,0,100,1)); //push spinbox
        vbx[i]->push(new Button("Button")); // push button
        vbx[i]->push(new Text());

        lvmet[i]=new LevelMeter(0.0,0.0,8.0);
        sl[i]=new MiupSlider("HORIZONTAL",0,0,8);
        sl[i]->emitValueSig.connect(lvmet[i],&LevelMeter::setLevelMeter); // to fix - not working with decimal
        sl[i]->emitValueSig.connect([](double val){
            std::cout << "value : " << val << std::endl;
        });

        vbx[i]->push(lvmet[i]); //... and so on
        vbx[i]->push(sl[i]);
        vbx[i]->push(new Toggle(0,"Toggle"));
        vbx[i]->push(new Matrix(5,2));
    }

    Hbox *hbx=new Hbox(vbx[0],vbx[1],vbx[2],vbx[3]); // put all the vboxes in a horizontal box
    ScrollBox *sb=new ScrollBox(hbx); // put the hbox in a scrollbox
    Dialog dlg(sb); // and put this in the dialog

    dlg.setAttr("SIZE","FULLxFULL");
    dlg.closeDialogSig.connect(&Miup::StopLoop);
    dlg.show();

    Miup::MainLoop();
    Miup::Close();

    return EXIT_SUCCESS;
}
