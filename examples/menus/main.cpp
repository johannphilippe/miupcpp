#include"Core/miup_wrapper.h"
#include<iostream>

int main(int args,char *argv[])
{
    Miup::Init();
    //create two submenu

    //SubMenu *sub1=new SubMenu("sub1",new Menu(new Item("item1"),new Item("item2"),new SubMenu("coucou",new Menu(new Item("test")))));
    //SubMenu *sub2=new SubMenu("sub2",new Menu(new Item("ITEM1")));


    Item *subitem=new Item("test");
    Menu *subitemMenu=new Menu(subitem);
    SubMenu *submenu=new SubMenu("submenu",subitemMenu);
    Item *item1=new Item("item1");
    Item *item2=new Item("item2");
    SubMenu *sub1=new SubMenu("sub1" ,new Menu(submenu,new Separator(),item1,item2));


    Item i1("ITEM1");
    SubMenu sub2("sub2",new Menu(&i1));

    //create one menu
    Menu menu(sub1,&sub2);
    //set its handle
    menu.setHandle("MainMenu");

    Dialog dlg(new Vbox(new Button("COUCOU")));
    //set the menu as dialog's menu
    dlg.setAttr("MENU","MainMenu");
    dlg.setAttr("SIZE","FULLxFULL");
    dlg.closeDialogSig.connect([](){Miup::StopLoop();});
    dlg.show();
    Miup::MainLoop();
    Miup::Close();

    return EXIT_SUCCESS;
}

