#include"Core/miup_wrapper.h"
#include<iostream>

int main(int args,char *argv[])
{
    Miup::Init();

    MiupSlider *slider=new MiupSlider("HORIZONTAL",0,0,1);
    MiupSlider *vert=new MiupSlider("VERTICAL",0,0,1);

    Button but("Change Color");
    but.buttonClickOffSig.connect([slider](){slider->setColor(180,20,20);});

    Vbox *vbx=new Vbox(&but,slider,vert);
    Dialog dlg(vbx);
    dlg.closeDialogSig.connect(&Miup::StopLoop);
    dlg.show();

    Miup::MainLoop();
    Miup::Close();
}

