#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int args,char *argv[])
{
    Miup::Init();

    //MiupSlider arr[100]("VERTICAL");
    MiupSlider *arr=new MiupSlider[100];
    Hbox hbx;
    for(int i=0;i<100;i++) {
        arr[i].setAttr("EXPAND","YES");
        arr[i].setSize(20,200);
        arr[i].setOrientation("VERTICAL");
        hbx.push(&arr[i]);
    }
    ScrollBox sb(&hbx);

    Dialog dlg(&sb);
    dlg.closeDialogSig.connect(&Miup::StopLoop);
    dlg.setAttr("SIZE","FULLxFULL");
    dlg.show();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

