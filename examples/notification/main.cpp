#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

#include"utils/notificationbase.h"

int main(int argc,char *argv[])
{
    Miup::Init();

    Notification n("test notification","it is now working", "/home/johann/Images/whatsapp-logo.png");
    n.pushAction("TRY",[](){std::cout << "trybut" <<std::endl;});
    n.pushAction("RETRY",[](){std::cout <<"retry"<< std::endl;});
    n.start();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

