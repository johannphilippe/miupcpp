#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int argc,char *argv[])
{
    Miup::Init();

    ScopePlot plot(512*2);

    Vbox box(&plot);

    Dialog dlg(&box);


    MiupTimer timer(33);



    CsoundThreaded  *cs = new CsoundThreaded;

    cs->SetOption("--output=dac");

    std::string orc = "nchnls = 1 \n"
                      "0dbfs = 1 \n"
                      "ksmps = 256 \n"
                      "sr = 48000 \n"
                      "\n"
                      "instr 1 \n"
                      "a1,a2 diskin2 \"/home/johann/Documents/UltimeOrgie_48.wav\" \n"
                      "outs a1,a2 \n"
                      "endin \n";

    std::string sco = "i 1 0 -1 \n";

    cs->CompileOrc(orc.c_str());
    cs->ReadScore(sco.c_str());


    cs->Start();

    plot.initDrawing(cs);

    timer.timerSig.connect([&](){
       plot.drawWith(cs);
    });


    cs->Perform();


    timer.start();

    dlg.setAttr("SIZE","FULLxFULL");
    dlg.show();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

