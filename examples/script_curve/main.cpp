#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int argc,char *argv[])
{
    Miup::Init();


    ScriptableCurve curve;

    Vbox box(&curve);

    Dialog dlg(&box);

    dlg.setAttr("SIZE","FULLxFULL");
    dlg.closeDialogSig.connect([&](){
       Miup::Close();
       return EXIT_SUCCESS;
    });

    dlg.show();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

