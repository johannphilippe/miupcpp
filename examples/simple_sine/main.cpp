#include<iostream>
#include"Core/miup_wrapper.h"


int main(int argc,char *argv[]) {
    /*
    Miup::Init(Miup::ModeList::native,argc,argv);

    std::string orc="nchnls=2 \n"
                    "0dbfs=1 \n"
                    "sr=44100 \n"
                    "ksmps=256 \n"
                    "\n"
                    "instr 1 \n"
                    "kfq init 500 \n"
                    "kvol init 0 \n"
                    "ao oscili kvol*0.3,kfq \n"
                    "kfq chnget \"freq\" \n"
                    "kvol chnget \"vol\" \n"
                    "klin=rms(ao,10) \n"
                    "kdb=dbfsamp(klin) \n"
                    "chnset kdb,\"level\" \n"
                    "printk2 kvol,10 \n"
                    "printk2 kfq, 20 \n"
                    "outs ao,ao \n"
                    "endin";

    std::string sco="i 1 0 5 \n";

    CsoundThreaded *cs=new CsoundThreaded();
    cs->CompileOrc(orc.c_str());
    cs->ReadScore(sco.c_str());
    cs->SetOption("-odac:hw:2,0");

    Button *play=new Button("PLAY");
    play->buttonClickOffSig.connect(cs,&CsoundThreaded::StartAndPerformSlot);

    Slider<int> *sl=new Slider<int>(500,200,1000,1,"HORIZONTAL");
    cs->SetControlChannel("freq",500);
    sl->postValueChangedSig.connect([cs](int val){cs->SetChannel("freq",(double)val);});

    Slider<double> *vol=new Slider<double>(0,0,1,0.05,"HORIZONTAL");
    cs->SetControlChannel("vol",0);
    vol->postValueChangedSig.connect([cs](double val){cs->SetChannel("vol",val);});

    LevelMeter<double> *lv=new LevelMeter<double>(-90.0,-90.0,6.0);
    cs->pushMethodCallback("level",[lv](double val){lv->setAttrDouble("VALUE",val);});

    Vbox *vbx=new Vbox(play,sl,vol,lv);
    vbx->setAttr("ALIGNMENT","ACENTER");

    Dialog dlg(vbx);
    dlg.closeDialogSig.connect(cs,&CsoundThreaded::Stop);
    dlg.closeDialogSig.connect(&Miup::StopLoop);
    dlg.show();

    Miup::MainLoop();
    Miup::Close();
    */

    Miup::Init();
    Button play("Play");
    play.setRasterSize(150,40);
    CsoundThreaded *csound;

    std::string orc =  "nchnls = 2 \n"
                       "0dbfs =1 \n"
                       "sr = 44100 \n"
                       "ksmps = 32 \n"
                       "instr 1 \n"
                       "ao oscil p4, p5 \n"
                       "outs ao,ao \n"
                       "endin \n";
    std::string score = "i 1 0 5 0.3 400";

    play.buttonClickOffSig.connect([&]() {
       csound->CompileOrc(orc.c_str());
       csound->ReadScore(score.c_str());
       csound->Start();
       csound->Perform();
    });

    Vbox container(&play);
    Dialog dlg(&container);
    dlg.closeDialogSig.connect([]() {
        Miup::StopLoop();
        return IUP_CLOSE;
    });
    dlg.show();

    Miup::MainLoop();
    Miup::Close();




}
