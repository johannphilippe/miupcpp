#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int args,char *argv[])
{
    Miup::Init();

    Button but("PLAY");
    Text txt(true);
    Vbox vbx(&but,&txt);


    SpinBox<int> sp;
    MiupSlider sl;
    Vbox vbx2(&sp,&sl);
    vbx.setAttr("EXPAND","YES");
    vbx2.setAttr("EXPAND","YES");

    SplitBox sb(&vbx,&vbx2);
    sb.setAttr("ORIENTATION","VERTICAL");

    Dialog dlg(&sb);
    dlg.setAttr("SIZE","800x800");
    dlg.closeDialogSig.connect(&Miup::StopLoop);
    dlg.show();

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

