#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int args,char *argv[])
{
    Miup::Init(Miup::ModeList::native,args,argv);
    Button but("PLAY");
    Button but2("Stop");
    Hbox vbx(&but,&but2);
    Dialog dlg(&vbx);
    dlg.show();

    dlg.closeDialogSig.connect(&Miup::StopLoop);
    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

