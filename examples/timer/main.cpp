#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>
/** This example demonstrates how the timer object can be used to push callbacks inside the main loop callback queue,
 *  It uses four objects : Slider, LevelMeter, MiupSlider, GainMeter  **/

int main(int args,char *argv[])
{
    Miup::Init();

    LevelMeter<int> lv(-90,-90,6);
    Slider<int> sl(-90,-90,6,1);	//Weird thing about IupVal (slider) is that if orientation=horizontal, then inverted=no but
                                    //if orientation=horizontal then inverted=yes by default. It is not true if set (horizontal)
                                    //before map (in constructor).
    sl.setAttr("ORIENTATION","HORIZONTAL");
    sl.setAttr("INVERTED","NO");
    MiupSlider msl("HORIZONTAL",-90,-90,6);
    GainMeter gm("HORIZONTAL"); //gain meter is only scaled between -90 and 6

    Timer t(100); //timer initialized at 250 milliseconds
    t.timer_sig.connect([&lv,&sl,&msl,&gm](){  //create a lambda that creates a random number in the range of sliders/meters and push them
        int val=(std::rand() % 96)-90;
        //here necessary to call Miup::pushCallbackMethod since timer is async (then is not allowed to refresh GUI)
        // to register a callback, simply push the value, and a lambda function with void return and double argument
        Miup::pushCallbackMethod(val,[&lv](double v){lv.setLevelMeter(v);}); // here pushes a simple callback to setValue
        Miup::pushCallbackMethod(val,[&sl](double v){sl.setAttrInt("VALUE",v);}); // here introduces the setAttr with Value argument attribute
        Miup::pushCallbackMethod(val,[&msl](double v){msl.setValue(v);});
        Miup::pushCallbackMethod(val,[&gm](double v){gm.setValue(v);});
    });

    Vbox vbx(&lv,&sl,&msl,&gm);
    vbx.setAttr("ALIGNMENT","ACENTER");
    Dialog dlg(&vbx);
    dlg.closeDialogSig.connect([](){Miup::StopLoop();}); // callback of close dialog will stop miup loop
    dlg.show();

    t.start(); // starts timer

    Miup::MainLoop();
    Miup::Close();

    return EXIT_SUCCESS;
}

