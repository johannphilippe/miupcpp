#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int args,char *argv[])
{
    Miup::Init();

    Button but("Tray");


    Vbox vbx(&but);
    Dialog dlg(&vbx);
    dlg.setAttr("SIZE","FULLxFULL");
    dlg.closeDialogSig.connect(&Miup::StopLoop);


    dlg.show();

    but.buttonClickOffSig.connect([&dlg](){
        dlg.setAttr("TRAYTIP","Hello");
        dlg.setAttr("TRAY","YES");
    });


    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

