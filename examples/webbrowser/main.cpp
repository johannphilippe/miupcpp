#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int argc,char *argv[])
{
    Miup::Init();

    WebBrowser browser;

    browser.setAttr("VALUE","https://www.google.fr");

    Vbox box(&browser);
    Dialog dlg(&box);
    dlg.setAttr("SIZE","FULLxFULL");
    dlg.showXY(IUP_CENTER,IUP_CENTER);

    Miup::MainLoop();
    Miup::Close();
    return EXIT_SUCCESS;
}

