#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"
#include<iostream>

int main(int argc,char *argv[])
{

    Miup::Init();

    Button but("Click");

    WebBrowser browser;


    Vbox box(&but, &browser);

    Dialog dlg(&box);
    dlg.setAttr("SIZE","FULLxFULL");
    dlg.show();

    Miup::MainLoop();
    Miup::Close();


/*
    IupOpen(&argc, &argv);

    Ihandle *but = IupButton("Click", NULL);
    Ihandle *box = IupVbox(but);
    Ihandle *dlg = IupDialog(box);

    IupSetAttribute(dlg, "SIZE", "FULLxFULL");

    IupShow(dlg);


    IupMainLoop();
    IupClose();
*/
    return 0;
}

