#!/usr/bin/ruby
#

require "fileutils"
require "zip"
require "zlib"
require "rubygems/package"
require "bundler/inline"

gemfile do
	source "https://rubygems.org"
	gem "down", require: true
end


def install_windows()

end

def install_linux()

end


print "Installation of MIUP and its components/dependencies - are you sure ? [y,n] "
val = gets


if val.downcase == "y" or val.downcase == "yes" 
	if RUBY_PLATFORM =~ /win32/
		puts "jo_tracker for Windows"
		install_windows
	elsif RUBY_PLATFORM =~ /linux/
		puts "jo_tracker for Linux"
		install_linux
	else
		puts "jo_tracker is not available on this platform, please install jo_tracker on a recent Linux or Windows system."
	end
	
else
	puts "Installation Cancelled"

end
