#!/bin/bash
#This script must be run with administrator permissions, since it is going to install dependencies
#sudo ./install_dependencies.sh

case "$OSTYPE" in
	solaris*) 	echo "SOLARIS" ;;
	darwin*) 	echo "OSX" ;;
	linux*) 	echo "LINUX"  
		#get Terra dependencies
		curl -L https://github.com/zdevito/terra/releases/download/release-2016-03-25/terra-Linux-x86_64-332a506.zip > terra.zip
		unzip terra.zip -d terra

		cd terra/terra-Linux-x86_64-332a506
		pwd
		cp lib/libterra.a /usr/lib
		cp -a -f include/terra/ /usr/include
		cd ../..
		rm -r terra

		#Put json library and lua scripting library in share
		mkdir -p /usr/share/jo_tracker/
		cp -f res/json.lua /usr/share/jo_tracker/terra
		cp -f res/jo_tracker_scripting.lua /usr/share/jo_tracker/terra
		
		#get libsndfile
		apt install libsndfile1-dev

		#get csound
		apt install csound
		apt install libcsound64-dev

		#get IUP dependencies || maybe check gtk or gcc version ? 
		#For now, ubuntu 18.04, kernel 4.15, gcc 7.3, GTK3.22
		mkdir iup
		mkdir cd
		mkdir im
		curl -L https://sourceforge.net/projects/iup/files/3.27/Linux%20Libraries/iup-3.27_Linux415_64_lib.tar.gz/download > iup/iup.tar.gz
		curl -L https://sourceforge.net/projects/canvasdraw/files/5.12/Linux%20Libraries/cd-5.12_Linux415_64_lib.tar.gz/download > cd/cd.tar.gz
		curl -L https://sourceforge.net/projects/imtoolkit/files/3.13/Linux%20Libraries/im-3.13_Linux415_64_lib.tar.gz/download > im/im.tar.gz

		cd iup
		tar xvzf iup.tar.gz
		./config_lua_module
		./install

		cd ../cd
		tar xvzf cd.tar.gz
		./config_lua_module
		./install

		cd ../im
		tar xvzf im.tar.gz
		./config_lua_module
		./install 

		cd ..

		rm -r iup
		rm -r cd
		rm -r im

		;;
	bsd*) 		echo "BSD" ;;
	cygwin*) 	echo "WINDOWS CYGWIN" ;;
	msys*) 		echo "WINDOWS MSYS" ;;
	*)
esac
