#ifndef JTOOLBOX_H
#define JTOOLBOX_H

#include"jo_tracker/jotrackerutilities.h"
#include"jo_tracker/jtrackset.h"
#include"Boxes/vbox.h"
#include"Boxes/hbox.h"

#include"Dialogs/dialog.h"
#include"Widgets/button.h"
#include"Widgets/spinbox.h"
#include"Text/label.h"
#include"Widgets/toggle.h"



class JToolBox : public Dialog
{
public:
    JToolBox(JTrackSet *t) : trackset(t), number_of_lines(1, 1), insert_line_button("Insert line"), remove_line_button("Remove line"),
    copy_bloc_button("Copy bloc"), copy_bloc_for_all_tracks_button("Copy bloc for all tracks"),
      paste_bloc("Paste bloc"), paste_bloc_for_all_tracks("Paste bloc for all tracks"), from(1, 1), to(1, 1), bloc_label("Blocs copy paste"),
      copy_empty_cells(0, "Copy even empty cells")
    {
        spbox.push(&from, &to);
        main_box.push(&number_of_lines, &insert_line_button, &remove_line_button, &bloc_label, &spbox, &copy_empty_cells, &copy_bloc_button, &paste_bloc, &copy_bloc_for_all_tracks_button, &paste_bloc_for_all_tracks);
        main_box.setAttr("GAP","5");

        insert_line_button.buttonClickOffSig.connect([&](){
           JoTracker::insert_line(trackset, number_of_lines.getValue());
        });

        remove_line_button.buttonClickOffSig.connect([&](){
           JoTracker::remove_line(trackset, number_of_lines.getValue());
        });

        copy_bloc_button.buttonClickOffSig.connect([&](){
           JoTracker::copy_bloc(trackset, from.getValue(), to.getValue(), copy_empty_cells.getStatus());
        });

        paste_bloc.buttonClickOffSig.connect([&](){
            JoTracker::paste_bloc(trackset);
        });

        copy_bloc_for_all_tracks_button.buttonClickOffSig.connect([&](){
           JoTracker::copy_bloc_for_all_track(trackset, from.getValue(), to.getValue(), copy_empty_cells.getStatus());
        });

        paste_bloc_for_all_tracks.buttonClickOffSig.connect([&](){
           JoTracker::paste_bloc_for_all_track(trackset);
        });

        this->construct(&main_box);
        this->setAttr("SIZE","200x400");
        this->setAttr("TITLE","ToolBox");

    }
    ~JToolBox() {}

private:
    JTrackSet *trackset;
    SpinBox<int> number_of_lines;
    Button insert_line_button, remove_line_button, copy_bloc_button, copy_bloc_for_all_tracks_button, paste_bloc, paste_bloc_for_all_tracks;
    SpinBox<int> from;
    SpinBox<int> to;
    Toggle copy_empty_cells;

    Label bloc_label;
    Hbox spbox;

    Vbox main_box;

};

#endif // JTOOLBOX_H
