
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/


#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include"Dialogs/dialog.h"

#include"Text/label.h"
#include"Boxes/hbox.h"
#include"Boxes/vbox.h"

#include"Core/image.h"

#include"jo_tracker/jdef.h"
#include"Widgets/webbrowser.h"

// Singleton aboutdialog - not working properly
class AboutDialog : public Dialog
{

public:

    AboutDialog() :
        mainbox()
    {
        mainbox.push(&browser);
        this->construct(&mainbox);
        this->setAttr("TITLE","About jo_tracker");
        this->setAttr("SIZE", "400x400");
    }

    ~AboutDialog() {

    }


    void showWith(std::string htmlPath)
    {
        std::string url = "file://" + htmlPath;
        browser.setAttr("VALUE", url.c_str());
        this->show();
    }
private:
    Vbox mainbox;
    WebBrowser browser;


};






#endif // ABOUTDIALOG_H
