
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "audiodevicelist.h"

AudioDeviceList::AudioDeviceList(DeviceListMode deviceMode) : List(), mode(deviceMode)
{
    setAttr("DROPDOWN","YES");
    setSize(150,30);
    refreshList();
    IUP_CLASS_INITCALLBACK(obj,AudioDeviceList);
    IUP_CLASS_SETCALLBACK(obj,"VALUECHANGED_CB",valuechanged_cb);
}


AudioDeviceList::~AudioDeviceList()
{
}

void AudioDeviceList::refreshList() {
    devices.clear();
    this->clear();
    CSOUND *cs=csoundCreate(NULL);
    csoundInitialize(NULL);

    csoundSetOption(cs, "-n");
    csoundSetOption(cs,"--messagelevel=0");

    csoundStart(cs);

    int isOut=(mode==output) ? 1 : 0;

    number_of_devices=csoundGetAudioDevList(cs,NULL,isOut);

    //std::cout << "number of devices : " << number_of_devices << std::endl;

    CS_AUDIODEVICE *devs=new CS_AUDIODEVICE[number_of_devices];

    csoundGetAudioDevList(cs,devs,isOut);
    devices.resize(number_of_devices);

    for(int i=0;i<number_of_devices;i++) {
        CS_AUDIODEVICE dev=devs[i];
        devices.push_back(dev);
        push_back(devices.back().device_name);
    }

    delete[] devs;
    csoundDestroy(cs);
}

int AudioDeviceList::getNumberOfDevices() {return number_of_devices;}

CS_AUDIODEVICE *AudioDeviceList::getCurrentDevice() {
    int nbr=getSelectedItemIndex();
    CS_AUDIODEVICE *ptr=&devices.at(nbr);
    return ptr;
}

CS_AUDIODEVICE *AudioDeviceList::getDevice(int nbr) {
    CS_AUDIODEVICE *ptr=&devices.at(nbr);
    return ptr;
}


//callbacks
int AudioDeviceList::valuechanged_cb(Ihandle *)
{
    deviceChangedSig(getCurrentDevice());
    return IUP_CLOSE;
}

