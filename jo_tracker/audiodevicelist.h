
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef AUDIODEVICELIST_H
#define AUDIODEVICELIST_H

#include"Widgets/list.h"
#include"Audio/csoundthreaded.h"

class AudioDeviceList : public List
{
public:
enum DeviceListMode {input,output};

    AudioDeviceList(DeviceListMode deviceMode=output);

    static std::shared_ptr<AudioDeviceList> CreateShared(DeviceListMode devicemode = output) {return std::make_shared<AudioDeviceList>(devicemode);}
    ~AudioDeviceList();

    void refreshList();

    CS_AUDIODEVICE *getCurrentDevice();
    CS_AUDIODEVICE *getDevice(int nbr);
    int getNumberOfDevices();

    lsignal::signal<void(CS_AUDIODEVICE *)> deviceChangedSig;

protected:
    IUP_CLASS_DECLARECALLBACK_IFn(AudioDeviceList,valuechanged_cb)

private:
    DeviceListMode mode;
    std::vector<CS_AUDIODEVICE> devices;
    int number_of_devices;
};

#endif // AUDIODEVICELIST_H
