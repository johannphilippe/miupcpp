
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef CSDFORMATTER_H
#define CSDFORMATTER_H

#include<iostream>
#include<list>

struct CsdGlobalParameters {
    CsdGlobalParameters(int sr =44100 , int k = 32, int chnls = 2, int db = 1, bool rt = true) {
        nchnls = chnls;
        sample_rate = sr;
        ksmps = k;
        dbfs = db;
        realtime = rt;
        record = false;
        record_filename = "\"~/jo_tracker_record.wav\"";
    }

    CsdGlobalParameters(const CsdGlobalParameters &param)
    {
        nchnls = param.nchnls;
        sample_rate = param.sample_rate;
        ksmps = param.ksmps;
        dbfs = param.dbfs;
        realtime = param.realtime;
        record = param.record;
        record_filename = param.record_filename;
    }

    CsdGlobalParameters(CsdGlobalParameters &param) {
        nchnls = param.nchnls;
        sample_rate = param.sample_rate;
        ksmps = param.ksmps;
        dbfs = param.dbfs;
        realtime = param.realtime;
        record = param.record;
        record_filename = param.record_filename;
    }

    std::string dump()
    {
        std::string res = "nchnls = " + std::to_string(nchnls) + "\nsample_rate = " + std::to_string(sample_rate) + "\nksmps = "  +std::to_string(ksmps) + "\0dbfs = " + std::to_string(dbfs);
        return res;
    }

    void setRecord(std::string filename) {
        this->record = true;
        this->record_filename = "\"" + filename + "\"";
    }

    int nchnls;
    int sample_rate;
    int ksmps;
    int dbfs;


    bool realtime;

    std::string record_filename;
    bool record;
};

class CsdFormatter {
public:
    CsdFormatter() {}
   // CsdFormatter(CsdGlobalParameters &params) : parameters(params), score(""), orchestra(""), tempo(""), options(""), macros("")
   // {
   // }

    CsdFormatter(CsdGlobalParameters &params) : parameters(params), score(""), orchestra(""), tempo(""), options(""), macros(""), pre_global(""), post_global("")
     {

    }

    ~CsdFormatter() {
    }

    void setParameters(CsdGlobalParameters param) {this->parameters = param;}
    void setOrchestra(std::string orc) {this->orchestra=orc; }
    void setScore(std::string sco){this->score=sco;}
    void setTempo(std::string tempo) {this->tempo=tempo;}
   void setMacros(std::string macros) {this->macros = macros;}
   void setOptions(std::string options) {this->options=options;}

   void setPreOrcGlobal(std::string pre_orc) {this->pre_global = pre_orc;}
   void setPostOrcGlobal(std::string pre_orc) {this->post_global = pre_orc;}
   void setPreScoreGlobal(std::string pre_score) {this->pre_score_global = pre_score;}


   void pushOrchestra(std::string instr) {this->orchestra += instr + " \n";}
   void pushScore(std::string statement) {this->score += statement + " \n";}
   void pushMacro(std::string macro) {this->macros += macro + " \n";}
   void pushOption(std::string opt) {this->options += opt + " ";}

    std::string getFullOrc() { // with macros
        std::string param = "nchnls = " + std::to_string(parameters.nchnls) +" \nnchnls_i = " + std::to_string(parameters.nchnls) +
                " \nksmps =" + std::to_string(parameters.ksmps) +
                " \n0dbfs =" + std::to_string(parameters.dbfs) + " \nsr =" + std::to_string(parameters.sample_rate) + " \n\n";
        std::string forc = macros + " \n" + param + getAudioGlobal() + " \n"  + orchestra + " \n" + getMasterInstr() + " \n";
        return forc;
    }

    std::string getFullScore() { // with tempo and all
        std::string fsco = tempo + " \n" + score + "\n";
        return fsco;
    }

    std::string getCsd() {
       std::string csd="<CsoundSynthesizer>\n\n<CsOptions>\n";
       csd += options + " \n";
       csd += "</CsOptions> \n<CsInstruments> \n\n";
       std::string global="sr = " + std::to_string(parameters.sample_rate) + "\nksmps = " + std::to_string(parameters.ksmps) +
               "\nnchnls = " + std::to_string(parameters.nchnls) +
                " \nnchnls_i = " + std::to_string(parameters.nchnls) +
               "\n0dbfs = " +  std::to_string(parameters.dbfs) +" \n\n" + getAudioGlobal() +  "\n\n\n";
       csd += global;
       csd += "\n" + pre_global + "\n";

       //csd += "//macros and global code \n" + macros + "\n//------------------------------\n";
       csd += orchestra + " \n";
       csd += getMasterInstr() + "\n";
       csd += post_global + "\n";
       csd += "</CsInstruments> \n\n<CsScore> \n";
       csd += this->pre_score_global + "\n";
       csd += score;
       csd += "</CsScore> \n</CsoundSynthesizer> \n";
       return csd;
    }


    std::string getMasterInstr() { // possibility to implement a routage matrix, in order to get  the ability to route signals with jack ?
        //std::string master("instr 999 \n;Instr  master \n");
        std::string master("instr master \n");

        //std::string clear("clear ");
        std::string clear("");
        std::string outch("outch ");

        for(int i = 1; i <= parameters.nchnls; i++) {
            std::string index = std::to_string(i-1);
            std::string out_index = std::to_string(i);

            //master += "a" + index + "=chnget:a(\"o" + index + "\")\n";
            //clear += "chnclear \"o" + index + "\"\n";

            master += "a" + index + " = gaOut[" + index + "] \n";
            //clear += "gaOut[" + index + "]";
            clear += "gaOut[" + index + "] = 0 \n";
            outch += out_index + ", " + "a" + index;

            /*

            master += "a" + index + "= ga" + index + " \n";
            clear += "ga" + index;
            outch += index + ", " + "a" + index;
                */

            if(i < parameters.nchnls) {
               // clear += ", ";
                outch += ", ";
            }
        }

        if(parameters.realtime) {
            std::string def("");
            //transform here a 0-1 scale value to db -90 6 or -90 3 value
            std::string rtmaster = "instr master \n"
                                   "kmast init 1 \n"
                                   "kold init 1 \n"
                                   "kcount init 1 \n"
                                   "kmast chnget \"Master\" \n"
                                   "kmast lineto kmast, 0.01 \n"
                                   "amast=a(kmast) \n";
            rtmaster += "ilim init 1024 \n\n";
            std::string loop("");
            loop += "\nif (kcount != ksmps) kgoto skipk\n";
            std::string record = "fout " + parameters.record_filename + ", 8 ";
            for(int i = 1; i <= parameters.nchnls; i++) {
                std::string index = std::to_string(i-1);
                std::string out_index = std::to_string(i);


                rtmaster += "a" + index + " = amast * gaOut[" + index + "] \n";

                def += "ka" + out_index + ", kdb" + out_index + ", ksc" + out_index;
                if (i<parameters.nchnls) def += ", ";
                record += ", a" + index;

                if(i <= 2) {
                        loop += "    ka" + out_index + " rms a" + index + ", 10 \n";
                        loop += "    kdb" + out_index + "=20 * log10(abs(ka" + out_index +")) \n";
                        loop += "    outvalue \"level_" + out_index + "\", kdb" + out_index + " \n";
                        loop += "    ksc" + out_index + "=k(a" + index + ") \n";  // for scope, not necessary now
                        loop += "    outvalue \"scope_" + out_index + "\", ksc" + out_index + " \n"; // idem
                }
            }

            record += " \n";
            if(!parameters.record) {
                std::cout << "Mode : No recording" << std::endl;
                record = "";
            }

            def += " init 0 \n";

            loop += "skipk: \nkcount = (kcount >= ilim) ? ksmps : kcount+ksmps \n";
            return  rtmaster + " \n" +record + " \n" + def +   " \n" + loop  + " \n"+ outch + "\n" + clear + "\nendin \n\n";
        } else {
            return master + outch + " \n" + clear + " \nendin \n\n";
        }
    }


    std::string getAudioGlobal() {
        std::string globalAudio("");
        globalAudio = "gaOut[] init " + std::to_string(parameters.nchnls) + " \n";

        return globalAudio;
    }

    std::string getOptions() {
        return options;
    }


    void clear() {
        orchestra.clear();
        score.clear();
        macros.clear();
        tempo.clear();
        options.clear();
    }

private:
    CsdGlobalParameters parameters;
    std::string orchestra;
    std::string score;
    std::string macros;
    std::string tempo;
    std::string options;
    std::string pre_global, post_global, pre_score_global;
};


// probably useless
namespace CsoundFormatter {
static std::string formatMacro(std::string name, std::string value) {
    std::string macro="#define " + name + " #" + value + "#";
    return macro;
}

class ScoreStatement {
public:
    ScoreStatement() {}

    ~ScoreStatement() {}
    void push(std::string val) {vals.push_back(val);}

    std::string getStatement() {
        std::string statement="i";
        for(auto &it:vals) {
            statement.append(" ");
            statement.append(it);
        }
    }

private:
    std::list<std::string> vals;
};

}


#endif // CSDFORMATTER_H
