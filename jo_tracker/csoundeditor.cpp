
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "csoundeditor.h"

CsoundEditor::CsoundEditor() : CodeEditor()
{

    setAttr("EXPAND","YES");

    IUP_CLASS_INITCALLBACK(obj,CsoundEditor);
    IUP_CLASS_SETCALLBACK(obj,"K_ANY",k_any);
    IUP_CLASS_SETCALLBACK(obj,"MARGINCLICK_CB",marginclick_cb);
    IUP_CLASS_SETCALLBACK(obj,"HOTSPOTCLICK_CB",hotspotclick_cb);
    IUP_CLASS_SETCALLBACK(obj,"MAP_CB",map_cb);
}


CsoundEditor::~CsoundEditor()
{

}

void CsoundEditor::initAttrs()
{
    setAttr("LEXERLANGUAGE","lua");

    setAttr("KEYWORDS0",csound_keywords.c_str());
    setAttr("STYLEFONT32","Consolas");
    setAttr("STYLEFONTSIZE32","11");
    setAttr("STYLECLEARALL","YES");

    setAttr("STYLEFGCOLOR1","0 128 0");
    setAttr("STYLEFGCOLOR2","0 128 0");
    setAttr("STYLEFGCOLOR3","128 0 0");
    setAttr("STYLEFGCOLOR5","0 0 255");
    setAttr("STYLEFGCOLOR6","160 20 20");
    setAttr("STYLEFGCOLOR7","128 0 0");
    setAttr("STYLEFGCOLOR9","0 0 255");
    setAttr("STYLEFGCOLOR10","255 0 255");

    setAttr("STYLEITALIC5","YES");
    setAttr("STYLEBOLD10","YES");
    setAttr("STYLEHOTSPOT6","YES");
    setAttr("MARGINWIDTH0","50");

    setAttr("PROPERTY","fold=1");
    setAttr("PROPERTY","fold.compact=0");
    setAttr("PROPERTY","fold.comment=1");
    setAttr("PROPERTY","fold.preprocessor=1");

    setAttr("MARGINWIDTH1","20");
    setAttr("MARGINTYPE1","SYMBOL");
    setAttr("MARGINMASKFOLDERS1","Yes");
    setAttr("MARKDEFINE","FOLDER=PLUS");
    setAttr("MARKDEFINE","FOLDEROPEN=MINUS");
    setAttr("MARKDEFINE","FOLDEREND=EMPTY");
    setAttr("MARKDEFINE","FOLDERMIDTAIL=EMPTY");
    setAttr("MARKDEFINE","FOLDEROPENMID=EMPTY");
    setAttr("MARKDEFINE","FOLDERSUB=EMPTY");
    setAttr("MARKDEFINE","FOLDERTAIL=EMPTY");
    setAttr("FOLDFLAGS","LINEAFTER_CONTRACTED");
    setAttr("MARGINSENSITIVE1","YES");

    setAttr("FONT","DejaVu, 10");
}

//CALLBACKS
int CsoundEditor::k_any(Ihandle * /*ih*/, int key)
{
    //std::cout << "key " << key  << " " << K_c0 << std::endl;
    if(key==K_c0) {
        setAttr("OVERWRITE","ON");
    } else if(key == K_cH) {
        std::cout << "control h" << std::endl;
    }
    emitKeySig(key);
    return 0;
}

int CsoundEditor::marginclick_cb(Ihandle *, int  /*i1*/, int /*i2*/, char */*s*/)
{

    return IUP_DEFAULT;
}

int CsoundEditor::hotspotclick_cb(Ihandle *, int /*i1*/, int /*i2*/, int /*i3*/, char * /*s*/)
{

    return IUP_DEFAULT;
}

int CsoundEditor::map_cb(Ihandle *)
{
    this->initAttrs();
    return IUP_DEFAULT;
}





/**					  	 CSOUND EDITOR DIALOG 					 **/

CsoundEditorBox::CsoundEditorBox() : Vbox(
                                          radio = new Radio(radioBox = new Hbox( instrmode = new Toggle(0, "Instrument"),  opcodemode = new Toggle(0,"UDO"))),
                                          button_box = new Hbox(instr_list = new List(), add=new Button("Add/Update"),del=new Button("Remove")),
                                          control=new Hbox(i_label=new Label("instr"), instr_name=new Text,  output_label = new Label("Output :"), output_def = new Text , input_label=new Label("Input :"),  input_def = new Text),
                                         description_frame = new Frame(instr_description = new Text),
                                         editor=new CsoundEditor(),endin = new Label("endin"), console=new Text(true)),
                                                //initialize mode
                                           mode("instr"), previous(new Button(NULL, "IUP_ArrowLeft")), forward(new Button(NULL, "IUP_ArrowRight")), prev_next_box(new Hbox())
{

    prev_next_box->push(previous, forward);


    web_browser = new WebBrowser();
    web_box = new Vbox(prev_next_box, web_browser);
    help_dialog=new Dialog(web_box);
    help_dialog->setAttr("SIZE","500x300");
    previous->buttonClickOffSig.connect([&](){web_browser->goBack();});
    forward->buttonClickOffSig.connect([&](){web_browser->goForward();});

    control->setAttr("GAP", "15");
    button_box->setAttr("GAP","15");
    button_box->setAttr("EXPAND","HORIZONTAL");
    instr_list->setAttr("EXPAND","HORIZONTAL");

    output_label->setAttr("FONT","Arial, 10");
    input_label->setAttr("FONT","Arial, 10");
    instr_description->setAttr("FONT","Arial, 9");
    description_frame->setAttr("TITLE","Comma separated p-field description - for example 'p4=freq, p5=amp'");
    description_frame->setAttr("FONT","Arial, 10");

    i_label->setAttr("FONT","Arial, Bold 14");
    endin->setAttr("FONT","Arial, Bold 14");
    instr_list->setAttr("DROPDOWN","YES");

    console->setAttr("EXPAND","HORIZONTAL");
    console->setAttr("SIZE","FULLx50");
    console->setAttr("READONLY","YES");
    console->setAttr("APPENDNEWLINE","YES");

    opcodemode->setHandle("udo");
    instrmode->setHandle("instr");

    instrmode->toggleStateChangedSig.connect(this, &CsoundEditorBox::setMode);
   // radio->setAttr("VALUE","instr");

    loadData(getUdoPath());
    loadData();
    loadMacroData();



    editor->emitKeySig.connect([&](int key) {
        if(key==K_cH) {
                //find current word
            std::string wordpos_str = "WORDPOS" + editor->getAttrStr("CARETPOS");
            //std::string wordpos = editor->getAttrStr(wordpos_str.c_str());
           const char *wordp = editor->getAttr(wordpos_str.c_str());

            if(wordp == NULL) {
                std::string url = "file://" + MiupUtil::filesystem::removeDoubleSlash(getCsoundDocumentationPath() + "/index.html");
                web_browser->setAttr("VALUE",url.c_str());
                help_dialog->show();
                return;
            }
            std::string wordpos(wordp);
            std::cout << "WORDPOS" << wordpos << std::endl;

            int beg,end;
            beg = MiupUtil::string::getNumber<int>(wordpos.substr(0, wordpos.find(":")));
            end = MiupUtil::string::getNumber<int>(wordpos.substr(wordpos.find(":") + 1));
            std::string word("");

            for(int i = beg; i<=end; i++)  {
                std::string id = "CHAR" + std::to_string(i);
                word += editor->getAttrStr(id.c_str());
            }
            //std::cout << "WORD " << word << std::endl;

            std::regex regex("[^()\\s]+");
            std::smatch match;
            if(std::regex_search(word, match, regex)) {
               // std::cout << "matched" << std::endl;
                word = match[0];
            }

            std::cout << "WORD : " << word << std::endl;

            std::string filepath = MiupUtil::filesystem::removeDoubleSlash(getCsoundDocumentationPath() + "/"  + word + ".html");
            if(MiupUtil::filesystem::isValidNonEmptyFile(filepath)) {
                std::string url = "file://" + filepath;
                web_browser->setAttr("VALUE",url.c_str());
                help_dialog->show();
            } else {
                std::string url = "file://" + MiupUtil::filesystem::removeDoubleSlash(getCsoundDocumentationPath() + "/index.html");
                web_browser->setAttr("VALUE",url.c_str());
                help_dialog->show();
            }

        }
    });


    add->buttonClickOffSig.connect(this,&CsoundEditorBox::setInstr);
    del->buttonClickOffSig.connect(this,&CsoundEditorBox::removeInstr);
    instr_list->valueChangedSig.connect(this,&CsoundEditorBox::updateEditor);
    updateList();
    instr_list->setAttrInt("VALUE",1);

    IUP_CLASS_INITCALLBACK(editor->getObj(),CsoundEditorBox);
    IUP_CLASS_SETCALLBACK(editor->getObj(),"MAP_CB",map_cb);

    setMode(1);
}

CsoundEditorBox::~CsoundEditorBox()
{
    saveJson();
    saveMacroData();

    delete previous;
    delete forward;
    delete prev_next_box;
    delete endin;
    delete instr_description;
    delete description_frame;
    delete button_box;
    delete input_label;
    delete output_label;
    delete input_def;
    delete output_def;
    delete i_label;
    delete opcodemode;
    delete instrmode;
    delete radioBox;
    delete radio;
    delete add;
    delete del;
    delete console;
    delete editor;
    delete control;

    delete web_browser;
    delete web_box;
    delete help_dialog;
}

void CsoundEditorBox::clearAll()
{
   js.clear();
   instr_list->clear();
   editor->clear();
   console->clear();
   instr_name->clear();
   input_def->clear();
   output_def->clear();
   instr_description->clear();

   loadData(getUdoPath());
   setUdoKeywords();
}

void CsoundEditorBox::setMode(int status)
{
    if(status==1) { // then instrument mode
        mode="instr";
        instr_description->clear();
        i_label->setAttr("TITLE","instr");
        endin->setAttr("TITLE","endin");
        input_def->setVisible(false);
        output_def->setVisible(false);
        input_label->setVisible(false);
        output_label->setVisible(false);
        description_frame->setVisible(true);
        control->refresh();
    } else { // then opcode udo mode
       mode="udo";
       i_label->setAttr("TITLE","opcode");
       endin->setAttr("TITLE","endop");

        input_def->setVisible(true);
        output_def->setVisible(true);
        input_label->setVisible(true);
        output_label->setVisible(true);
        description_frame->setVisible(false);

        control->refresh();
    }

    instr_name->clear();
    editor->clear();
    updateList();
}

void CsoundEditorBox::loadData(std::string path)
{
    std::cout << "Loading orc from " << path << std::endl;
    std::ifstream sz(path, std::ios::binary | std::ios::ate );
    int size=sz.tellg();
    sz.close();
    if(size>0) {
 //   std::cout << "file size : " << size << std::endl;
        std::ifstream ifs(path);
        if(ifs.is_open()) {
            json temp;
            ifs >> temp;
            if(temp.find("instr") != temp.end() && !temp["instr"].is_null()) js["instr"]=temp["instr"];
            if(temp.find("udo") != temp.end() && !temp["udo"].is_null()) js["udo"]=temp["udo"];

        } else {
            std::cout << "cannot open file orc_data.json" << std::endl;
        }
    } else {

        if(js.find("instr") == js.end()) js["instr"];
        if(js.find("udo") == js.end()) js["udo"];
    }

    setUdoKeywords();


}

void CsoundEditorBox::loadMacroData() {
    std::string path = getMacroDataPath();
    if(!MiupUtil::filesystem::isValidNonEmptyFile(path))
        return;

    std::ifstream macro_ifs(getMacroDataPath());
    macro_data.clear();
    macro_ifs >> macro_data;
    macro_ifs.close();
}


void CsoundEditorBox::setUdoKeywords()
{
    std::string kwords("");
    for(auto & it : js["udo"].items()) {
        kwords += it.key();
        kwords += "\t";
    }

   // std::cout << " keywords  == > " << kwords << std::endl;

    editor->setAttr("KEYWORDS1",kwords.c_str());
    editor->setAttr("STYLEFGCOLOR13","255 0 0");
    editor->setAttr("STYLEITALIC13","YES");
}

void CsoundEditorBox::saveJson(std::string path)
{
    //std::cout << "Saving orc at " << path << " \n orc is : " << js.dump(4) << std::endl;
    std::ofstream ofs(path);
    //std::cout << js.dump(4) << std::endl;;
    ofs << js.dump(4);
    ofs.close();

}

void CsoundEditorBox::saveMacroData() {
    //save macro data
    std::ofstream macro_ofs(getMacroDataPath());
    macro_ofs << macro_data.dump(4);
    macro_ofs.close();
}

void CsoundEditorBox::setInstr() // first check if csound compiles the instr, then push in orc_data
{
    if(instr_name->getAttr("COUNT")==0 || editor->getAttr("COUNT")==0) return;

    std::string name = instr_name->getAttrStr("VALUE");
    std::string body = editor->getAttrStr("VALUE");

    std::string bodyStr(body);
    std::regex regex("([^0-9\\s=\\(\\),\\.*+\\-(?:\\d+)]+)"); // looking for udo's in instrument body
    std::sregex_iterator next(bodyStr.begin(), bodyStr.end(), regex);
    std::sregex_iterator end;
    std::unordered_map<std::string, std::string> udo_map;

    while(next != end) { // looking for udo requirements in new instrment/udo body
        std::smatch match = *next;
        std::string matched = match.str(1);
      //  std::cout  << "matched " << matched <<std::endl;


        if(js["udo"].find(matched) != js["udo"].end() && udo_map.find(matched) == udo_map.end()) {
           // std::cout << "required : " << matched << std::endl;
            //std::string cur_udo(CsoundEditorBox::GetFormattedUdo(matched, js["udo"][matched]));
           // std::cout << "udo : " << CsoundEditorBox::GetFormattedUdo(matched, js["udo"][matched]) << std::endl;;
            if(name != matched) udo_map[matched]= CsoundEditorBox::GetFormattedUdo(matched, js["udo"][matched]);
           // std::cout<< "udo : " << std::endl;
        }

        next++;
    }


    if(!csoundEval(name.c_str(), body.c_str(), udo_map)) {
        std::cout << "Cannot compile csound instr or udo" << std::endl;
        return;
    }

    if(!exists(name.c_str())) {
        instr_list->push_back(name.c_str());
        std::cout << "Not exists, insert" << std::endl;
    } else {
        std::cout << " exists" << std::endl;
    }

    js[mode][name]["body"]=body;

    if(mode == "udo") {
        js[mode][name]["input"]=std::string(input_def->getAttr("VALUE"));
        js[mode][name]["output"]=std::string(output_def->getAttr("VALUE"));
    } else if(mode == "instr") {
        js[mode][name]["description"] = std::string(instr_description->getAttr("VALUE"));
    }

    js[mode][name]["udo_required"];
    js[mode][name]["udo_required"].clear();
    int udo_cnt=0;
    for(auto & it : udo_map)  {
      //  std::cout << "REQUIREMENTS " << it.first << std::endl;
        js[mode][name]["udo_required"][std::to_string(udo_cnt)] = it.first;
        udo_cnt++;
    }

    setUdoKeywords();
    saveJson();
}

void CsoundEditorBox::removeInstr()
{
    const char *name=instr_list->getSelectedItem();
    if(exists(name)) {
        js[mode].erase(name);
        updateList();
        saveJson();
        setUdoKeywords();
    }
}

bool CsoundEditorBox::exists(const char *name)
{
    if(js[mode].find(name) != js[mode].end()) {
        return true;
    }
    return false;
}

void CsoundEditorBox::updateEditor(const char *name)
{
    if (exists(name)) {
        instr_name->setAttr("VALUE",name);
        editor->setAttr("VALUE",js[mode][name]["body"].get<std::string>().c_str());
        if(mode == "udo" ) {
                input_def->setAttr("VALUE", js[mode][name]["input"].get<std::string>().c_str());
                output_def->setAttr("VALUE", js[mode][name]["output"].get<std::string>().c_str());
        } else if(mode == "instr") {
            instr_description->setAttr("VALUE", js[mode][name]["description"].get<std::string>().c_str());
        }
    }
}

void CsoundEditorBox::updateList()
{
    int cnt=1;
    instr_list->clear();
    std::string init_list;

    for(auto & it : js[mode].items()) {
        init_list+=std::to_string(cnt) + "=\"" + std::string(it.key()).c_str() + "\", ";
        cnt++;
    }
    instr_list->setAttrs(init_list.c_str());
}

bool CsoundEditorBox::csoundEval(const char * name, const char *body, std::unordered_map<std::string, std::string> &udo_map)
{

    console->setAttr("VALUE","");

    bool res=false;
    std::string buf;
    Csound cs;


    std::string body_str(body);
    std::string instrument_index("");
    int index_instr = 0;
    for(auto & it : js["instr"].items()) {
        if(body_str.find(it.key())) { //
            instrument_index += "gi" + it.key() + " = " + std::to_string(index_instr) + " \n";
            index_instr++;
        }
    }

    std::string to_eval;
    GlobalCsoundCode macros = getMacroString();


    to_eval += instrument_index;

    int nchnls = 2; // default
    json audio_parameters; // find if audio_parameters exists


    if(MiupUtil::filesystem::isValidNonEmptyFile(getAudioParametersPath())) {
        std::ifstream ifs(getAudioParametersPath());
        ifs >> audio_parameters;
        ifs.close();
    }

    if(audio_parameters.find("nchnls") != audio_parameters.end()) {
        nchnls = audio_parameters["nchnls"].get<int>();
    }

    std::string global("");

    global += "gaOut[] init " + std::to_string(nchnls) + "\n";

    global += "\n";
    to_eval += global;

    to_eval += macros.pre_orc + "\n";


    std::unordered_map<std::string,std::string> done_map(udo_map);
    std::unordered_map<std::string,std::string> to_do_map(udo_map);

    std::vector<std::string> ordered_list;
    for(auto & it : done_map) ordered_list.push_back(it.first);

    bool require_recursion=false;
        std::cout << "about to check requirements recursively " << std::endl;
    do {
        std::unordered_map<std::string,std::string> dummy_map;
        require_recursion=false;

        for(auto & it : to_do_map) {
              //  std::cout << "already mapped : " << it.first << std::endl;
            if(js["udo"][it.first].find("udo_required") !=  js["udo"][it.first].end()) {
                json requirements = js["udo"][it.first]["udo_required"];
                for(auto & r_it : requirements.items()) {
                        std::string required = r_it.value().get<std::string>();
                 //       std::cout << "required udo : " << required << std::endl;
                        if(done_map.find(required)==done_map.end() && js["udo"].find(required) != js["udo"].end()) {
                 //           std::cout << "is required and not mapped yet" << std::endl;
                            require_recursion=true;
                            std::string cur_udo= CsoundEditorBox::GetFormattedUdo(required,js["udo"][required]);
                            dummy_map[required]=cur_udo;
                            done_map[required]=cur_udo;
                            ordered_list.push_back(required);
//                            to_eval += cur_udo + " \n\n";
                        }
                }
            }
        }
        to_do_map = dummy_map;

    } while(require_recursion);

    // need to add udo definition first
   std::string f_udo_decl(";Forward declaration for Udos \n");
   for(auto & it : ordered_list) { // declarations
       std::string cur_decl = "opcode " + it + ", " + js["udo"][it]["output"].get<std::string>() + ", " + js["udo"][it]["input"].get<std::string>() + " \nendop \n";
       to_eval += cur_decl + "\n";
   }


    for(int i = ordered_list.size() -1; i>=0; i--) {
        to_eval += done_map[ordered_list.at(i)] + " \n";
    }

    if(mode == "instr") {
        to_eval += "instr 1 \n" + std::string(body) + "\nendin \n";
    } else if(mode =="udo") {
        to_eval += "opcode " + std::string(name) + ", " +
                output_def->getAttr("VALUE") + ", " +
                input_def->getAttr("VALUE") + "\n" +
                std::string(body) + " \nendop \n";
    }

    to_eval += "\n" + macros.post_orc + "\n";

    to_eval += "\nreturn 0\n";

    cs.CreateMessageBuffer(0);

    std::cout << "TO EVAL : " << to_eval << std::endl;

    cs.Start();
    MYFLT cs_res = cs.EvalCode(to_eval.c_str());

    std::cout << "EVAL RESULT ===> " << cs_res << std::endl;
    if((int)cs_res == 0) {
        buf+="Csound evaluated successfully\n";
        cs.Stop();
        cs.Cleanup();
        res=true;
    }

    while(cs.GetMessageCnt()>0) {
        buf+=std::string(cs.GetFirstMessage());
        cs.PopFirstMessage();
    }

    console->setAttr("APPEND",buf.c_str());
    cs.Stop();
    cs.Cleanup();
    return res;
}


//callbacks
int CsoundEditorBox::map_cb(Ihandle *)
{
    instr_name->setWidth(100);
    instr_list->setWidth(100);
    instr_description->setWidth(400);
    editor->initAttrs();
    if(instr_list->getAttrInt("COUNT")>0) {
        const char *current=instr_list->getItem(1);
        updateEditor(current);
        //std::cout << "MAPPED" << std::endl;
    }
    setUdoKeywords();
    return IUP_CLOSE;
}


std::pair<std::string,std::string> CsoundEditorBox::getOrc(std::unordered_map<std::string, int> &orc_map)
{

    std::string instrument_index("");
    std::string f_orc("");
    std::string instr_description("");
    f_orc += std::string(INSTR_METRO) + " \n";

    std::string f_udo("");

    std::unordered_map<std::string, std::string> udo_map;


    for(auto & it : orc_map) {
        if(js["instr"].find(it.first) != js["instr"].end()) { // matched instrument
            instrument_index += "gi" + it.first + " = " + std::to_string(it.second) + " \n";

            json instr = js["instr"][it.first];
            instr_description += ";Instr " + std::to_string(it.second) + " - > " + it.first + "\t ------- " + js["instr"][it.first]["description"].get<std::string>() + "\n";
            std::string body = instr["body"].get<std::string>();

            for (auto & need_it : instr["udo_required"].items()) {
                std::string required = need_it.value().get<std::string>();
                if(udo_map.find(required) == udo_map.end()  && js["udo"].find(required) != js["udo"].end()) { //
                    json cur_udo = js["udo"][required];
                    udo_map[required]=CsoundEditorBox::GetFormattedUdo(required,cur_udo);
                }
            }


            f_orc += "instr " + std::to_string(it.second) + " \n; "  + it.first + " "  + js["instr"][it.first]["description"].get<std::string>() + " \n" + body + "\nendin \n\n";
        }
    }


    std::unordered_map<std::string,std::string> done_map(udo_map);
    std::unordered_map<std::string,std::string> to_do_map(udo_map);

    std::vector<std::string> ordered_list;
    for(auto & it : done_map) ordered_list.push_back(it.first);

    bool require_recursion=false;
    do {
        std::cout << "about to check requirements recursively " << std::endl;
        std::unordered_map<std::string,std::string> dummy_map;
        require_recursion=false;

        for(auto & it : to_do_map) {
                //std::cout << "already mapped : " << it.first << std::endl;
            if(js["udo"][it.first].find("udo_required") !=  js["udo"][it.first].end()) {
                json requirements = js["udo"][it.first]["udo_required"];
                for(auto & r_it : requirements.items()) {
                        std::string required = r_it.value().get<std::string>();
                        //std::cout << "required udo : " << required << std::endl;
                        if(done_map.find(required)==done_map.end() && js["udo"].find(required) != js["udo"].end()) {
                          //  std::cout << "is required and not mapped yet" << std::endl;
                            require_recursion=true;
                            std::string cur_udo= CsoundEditorBox::GetFormattedUdo(required,js["udo"][required]);
                            dummy_map[required]=cur_udo;
                            done_map[required]=cur_udo;
                            ordered_list.push_back(required);
//                            to_eval += cur_udo + " \n\n";
                        }
                }
            }
        }
        to_do_map = dummy_map;

    } while(require_recursion);

   std::string f_udo_decl(";Forward declaration for Udos \n");
   for(auto & it : ordered_list) { // declarations
       std::string cur_decl = "opcode " + it + ", " + js["udo"][it]["output"].get<std::string>() + ", " + js["udo"][it]["input"].get<std::string>() + " \nendop \n";
       f_udo_decl += cur_decl;
   }

    for(int i = ordered_list.size() -1; i>=0; i--) {
        f_udo += done_map[ordered_list.at(i)] + " \n";
    }

    std::string full_orchestra =instrument_index + " \n" + f_udo_decl + " \n" +  f_udo + "\n" + f_orc;
    return {full_orchestra, instr_description};
}


const std::string CsoundEditorBox::GetFormattedUdo(std::string name, json udo)
{
        return std::string("opcode ") + name + ", " + udo["output"].get<std::string>() + ", " +
                udo["input"].get<std::string>() + " \n" + udo["body"].get<std::string>() + " \nendop \n";
}

const std::string CsoundEditorBox::GetFormattedInstr(std::string name, int number, json &js)
{
    std::string res("");
    if(js["instr"].find(name) != js["instr"].end()) {
        json instr_js = js["instr"][name];
        res += ";Instr " + std::to_string(number) + " - > " + name + "\t ------- " + js["instr"][name]["description"].get<std::string>() + "\n";
        res += "instr " + std::to_string(number) + "\n" + instr_js["body"].get<std::string>() + "\nendin \n";

    } else {
        std::cout << "Couldn't find instrument" << std::endl;
    }

    return res;
}



void CsoundEditorBox::setData(json *j)
{
    js.clear();
    js = *j;
}

json *CsoundEditorBox::getMacroData()
{
    return &macro_data;
}

void CsoundEditorBox::setMacroData(json *js)
{
    macro_data.clear();
    macro_data = *js;
}



void CsoundEditorBox::addData(json j)
{
    if(j.find("instr")!= j.end()) {
        for(auto & i_it : j["instr"].items()) {
            js["instr"][i_it.key()] = i_it.value();
        }
    }

    if(j.find("udo") != j.end()) {
        for(auto & u_it : j["udo"].items()){
            js["udo"][u_it.key()] = u_it.value();
        }
    }
}

GlobalCsoundCode CsoundEditorBox::getMacroString()
{

    std::string pre_res("");
    std::string post_res("");
    std::string pre_score("");
    if(macro_data.find("data") != macro_data.end()) {
        if(macro_data["data"].find("pre_orc") != macro_data["data"].end()) {
            pre_res += "//GLOBAL ORC : PRE ORC \n" +
                    macro_data["data"]["pre_orc"].get<std::string>();
        }

        if(macro_data["data"].find("post_orc") != macro_data["data"].end()) {
            post_res += "//GLOBAL ORC : POST ORC \n" +
                    macro_data["data"]["post_orc"].get<std::string>();
        }

        if(macro_data["data"].find("pre_score") != macro_data["data"].end()) {
            pre_score+=("// GLOBAL SCORE : PRE SCORE \n") +
                    macro_data["data"]["pre_score"].get<std::string>();
        }


    }

    GlobalCsoundCode g_code(pre_res, post_res, pre_score);
    return g_code;
}

json *CsoundEditorBox::getData()
{
    return &js;
}
