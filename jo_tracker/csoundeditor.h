
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef CSOUNDEDITOR_H
#define CSOUNDEDITOR_H
#include<fstream>
#include<thread>


#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Text/codeeditor.h"
#include"Text/text.h"
#include"Text/label.h"
#include"Widgets/button.h"
#include"Widgets/list.h"
#include"Widgets/toggle.h"
#include"Dialogs/dialog.h"
#include"Widgets/webbrowser.h"
#include"Dependencies/nlohmann/json.hpp"
#include"Boxes/radio.h"
#include"csound/csound.hpp"
#include"Boxes/frame.h"
#include<regex>
#include"jdef.h"


#include"res/languagekeywords.h"

struct GlobalCsoundCode {
    GlobalCsoundCode(std::string preorc,std::string postorc, std::string prescore) :
        pre_orc(preorc),
        post_orc(postorc),
        pre_score(prescore)
    {
    }

    GlobalCsoundCode() {

    }

    GlobalCsoundCode(const GlobalCsoundCode& that) :
        pre_orc(that.pre_orc), post_orc(that.post_orc), pre_score(that.pre_score)
    {

    }

    std::string pre_orc;
    std::string post_orc;
    std::string pre_score;
};

using json=nlohmann::json;
class CsoundEditor : public CodeEditor
{
public:
    CsoundEditor();
    static std::shared_ptr<CsoundEditor> CreateShared();

    virtual ~CsoundEditor();

    void initAttrs();
    lsignal::signal<void(int)>emitKeySig;
protected:
    IUP_CLASS_DECLARECALLBACK_IFni(CsoundEditor,k_any)
    IUP_CLASS_DECLARECALLBACK_IFniis(CsoundEditor,marginclick_cb)
    IUP_CLASS_DECLARECALLBACK_IFniiis(CsoundEditor,hotspotclick_cb)
    IUP_CLASS_DECLARECALLBACK_IFn(CsoundEditor,map_cb)
private:
};



/** CsoundEditorBox -- containing a CsoundEditor, console,database for orchestra,
to implement : csound help with URL and iupweb **/


class CsoundEditorBox : public Vbox
{
public:
    CsoundEditorBox();
    static std::shared_ptr<CsoundEditorBox> CreateShared();

    virtual ~CsoundEditorBox();

    std::pair<std::string, std::string> getOrc( std::unordered_map<std::string, int> &orc_map);


    void saveJson(std::string path = getOrcDataPath());
    void loadData(std::string path = getOrcDataPath());

    void saveMacroData();
    void loadMacroData();


    json *getData();
    void setData(json *j);

    void setMacroData(json *js);
    json *getMacroData();
    GlobalCsoundCode getMacroString();

    void clearAll();

    void updateEditor(const char *name);
    void updateList();
    void setMode(int status);

    void addData(json j);

    static const std::string GetFormattedUdo(std::string name, json udo);
    static const std::string GetFormattedInstr(std::string name, int number, json &js);
protected:

    void setInstr();
    void removeInstr();
    bool exists(const char *name);

    IUP_CLASS_DECLARECALLBACK_IFn(CsoundEditorBox,map_cb)


private:
    bool csoundEval(const char *name, const char *body, std::unordered_map< std::string,std::string> &udo_required);

    void setUdoKeywords();

    //members
    json js;
    json macro_data;

    std::string mode;

    CsoundEditor *editor;

    Radio *radio;
    Hbox *radioBox;
    Toggle *opcodemode, *instrmode;

    Button *add,*del;
    Text *console, *instr_name, *input_def, * output_def, *instr_description;
    Hbox *control,  *button_box;
    List *instr_list;
    Label *i_label, *endin, *input_label, *output_label;
    Frame *description_frame;

    Dialog *help_dialog;
    WebBrowser *web_browser;
    Vbox *web_box;
    Button *previous, *forward;
    Hbox *prev_next_box;
};

#endif // CSOUNDEDITOR_H
