
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef EXPORTPARAMETERSDIALOG_H
#define EXPORTPARAMETERSDIALOG_H

#include"Dialogs/dialog.h"
#include"Boxes/vbox.h"
#include"Text/label.h"
#include"Widgets/button.h"
#include"Widgets/list.h"
#include"Widgets/spinbox.h"
#include"Boxes/frame.h"
#include"Boxes/hbox.h"
#include"csdformatter.h"
#include"Text/text.h"
#include"Widgets/toggle.h"


class ExportParametersDialog : public Dialog {
public:
    ExportParametersDialog() : Dialog(box = new Vbox( srFrame =  new Frame (srList = new List()),
                                                      ksmpsFrame = new Frame(ksmpsBox = new SpinBox<int>(32,1,NULL,1)),
                                                      chnlsFrame = new Frame( chnlsBox = new SpinBox<int>(2,1,NULL,1)) ,
                                                      dbfsFrame= new Frame(dbfsBox = new SpinBox<int>(1,1,NULL,1)),
                                                      additionalFrame = new Frame(additional_options = new Text()),
                                                      buttonBox = new Hbox( gather_soundfiles = new Toggle(0, "Gather Soundfiles"),
                                                                            okBut = new Button("Ok"), cancelBut = new Button("Cancel"))))

    {
                sr = 44100;

                gather_soundfiles->setAttr("VISIBLE","NO");

                chnlsFrame->setAttr("TITLE","Number of channels");
                srFrame->setAttr("TITLE", "Sample rate");
                ksmpsFrame->setAttr("TITLE","Ksmps");
                dbfsFrame->setAttr("TITLE","dbfs");
                additionalFrame->setAttr("TITLE","Additional options");

                additional_options->setAttr("SIZE","300x");

                srList->push_back("44100");
                srList->push_back(("48000"));
                srList->push_back("88200");
                srList->push_back("96000");
                srList->push_back("192000");
                srList->setAttr("DROPDOWN","YES");


                srList->valueChangedSig.connect([=](const char *val) {
                   sr = MiupUtil::string::getNumber<int>(val);
                });

                okBut->buttonClickOffSig.connect([&](){
                   this->buttonClicked(true);
                });

                cancelBut->buttonClickOffSig.connect([&]() {
                    this->buttonClicked(false);
                });

    }

   ExportParametersDialog(int sample_rate, int ksmps, int nchnls, int dbfs) : ExportParametersDialog() {
       srList->selectItemFromText(std::to_string(sample_rate));
       ksmpsBox->setValue(ksmps);
       chnlsBox->setValue(nchnls);
       dbfsBox->setValue(dbfs);
   }



    ~ExportParametersDialog() {
        delete okBut;
        delete cancelBut;
        delete srList;
       delete additional_options;
        delete ksmpsBox;
        delete dbfsBox;
        delete chnlsBox;

       delete additionalFrame;
        delete chnlsFrame;
        delete srFrame;
        delete ksmpsFrame;
        delete dbfsFrame;
        delete buttonBox;
        delete box;
    }

   void loadFromJson(json data) {
      // std::cout << "load from json" << std::endl;
       if(MiupUtil::json::findObjects(data, {"sample_rate","dbfs","nchnls","ksmps"})) {
        //   std::cout << " found objects" << std::endl;
                ExportParametersDialog dlg(data["sample_rate"].get<int>(), data["ksmps"].get<int>(), data["nchnls"].get<int>(), data["dbfs"].get<int>());
                setNChnls(data["nchnls"].get<int>());
                setDbfs(data["dbfs"].get<int>());
                setKsmps(data["ksmps"].get<int>());
                setSampleRate(data["sample_rate"].get<int>());
       }
       if(data.find("additional_options") != data.end()) {
           setAdditionalOptions(data["additional_options"].get<std::string>());
       }
   }


   void setGatherButtonVisible(bool visible = true) {
        gather_soundfiles->setAttr("VISIBLE",(visible) ? "YES" : "NO");
   }

    int getSampleRate() {
        return getNumber<int>(srList->getSelectedItem());
    }

    int getKsmps() {
        return ksmpsBox->getValue();
    }

    int getDbfs() {
        return dbfsBox->getValue();
    }

    int getNChnls() {
        return chnlsBox->getValue();
    }

    bool gatherSoundfiles() {
        return gather_soundfiles->getStatus();
    }


    std::string getAddtionalOptions() {return std::string(additional_options->getAttr("VALUE"));}

    void setSampleRate(int sr) {srList->selectItemFromText(std::to_string(sr));}
    void setKsmps(int ksmps) {ksmpsBox->setValue(ksmps);}
    void setDbfs(int db) {dbfsBox->setValue(db);}
    void setNChnls(int chnls) {chnlsBox->setValue(chnls);}
    void setAdditionalOptions(std::string additional) {
        additional_options_string = additional;
        additional_options->setAttr("VALUE",additional_options_string.c_str());

    }


    lsignal::signal<void(bool)> buttonClicked;
protected:


private:
    Vbox *box;
    Hbox *buttonBox;
    Button *okBut, *cancelBut;
    List *srList;
    SpinBox<int> *ksmpsBox, *dbfsBox, *chnlsBox;
    Frame *srFrame, *ksmpsFrame, *dbfsFrame, *chnlsFrame, *additionalFrame;
    Text *additional_options;
    Toggle *gather_soundfiles;

    std::string additional_options_string;

    int sr;

};




#endif // EXPORTPARAMETERSDIALOG_H
