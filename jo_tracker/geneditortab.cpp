
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "geneditortab.h"

std::string GenEditorTab::browse_path = std::string(MiupUtil::filesystem::getHomeDirectory());

GenEditorTab::GenEditorTab() : Vbox(control_box = new Hbox(
                                        gen_nbr_label = new Label("Function number"),
                                        gen_nbr = new SpinBox<int>(1,1,NULL,1),
                                        store_but = new Button("Store", "IUP_ActionOk"),
                                        clear_but = new Button("Clear", "IUP_FileNew"),
                                        remove_but = new Button("Remove", "IUP_EditErase"),
                                        text_label=new Label("Handwritten GEN"),
                                        text=new Text,
                                        text_button = new Button("Store handwritten"),
                                        copy_code_button = new Button(NULL, "IUP_EditCopy"),
                                        detach_button = new Toggle(0, "Detach")
                                        ),
                                    tab = new Tabs(
                                        curvebox = new Vbox(
                                            curve_control_box = new Hbox(
                                                curve_size = new SpinBox<int>(2048,64,NULL,1)
                                            ),
                                            curveplot = new CurvePlot("Curve Editor", 0,1,0,1,0.1,0.1)
                                         ),
                                        waveformbox = new Vbox(
                                            sample_control_box = new Hbox(
                                                load_sample_but = new Button("Load"),
                                                sample_name = new Label("Filename")),
                                            waveformplot = new MultichannelWaveform()),
                                        matrixbox = new Vbox(
                                            radio_mat = new Radio(
                                                radio_box = new Hbox(
                                                    mode_gen2 = new Toggle(1,"Gen 2"),
                                                    mode_gen9 = new Toggle(0,"Gen 9")
                                                )
                                            ),
                                            matrix_contol_box = new Hbox(
                                                matrix_line_number_label = new Label("Number of lines"),
                                                lignbr = new SpinBox<int>(4,1,NULL,1)
                                            ),
                                            matrix=new Matrix(4,1)
                                        ),
                                        scriptable_curve = new ScriptableCurve

                                    )
                                )

{

    detach_button->toggleStateChangedSig.connect([&](int status){
        if(status == 1) {
            detachSig(true);
            detach_button->setStatus(1, false);
        } else {
            detachSig(false);
            detach_button->setStatus(0, false);
        }
    });

    matrix_contol_box->setAttr("GAP", "5");
    curve_control_box->setAttr("GAP", "10");
    sample_control_box->setAttr("GAP","5");

    gen_nbr_label->setAttr("FONT","Arial, Bold 10");

    control_box->setAttr("GAP","10");

    curveplot->setDomain(2048);

    curvebox->push(curveplot->getRadio());
    curvebox->setAttr("TABTITLE","Curve Editor");


    load_sample_but->buttonClickOffSig.connect(this, &GenEditorTab::browseFile);

    waveformbox->setAttr("TABTITLE","Waveform");


    lignbr->postValueChangedSig.connect(matrix, &Matrix::setNumlin);

    mode_gen2->toggleStateChangedSig.connect([&](int val){
       matrix->clear();
       matrix->setNumcol( (val == 1) ? 1 : 3 );
    });

    matrixbox->setAttr("TABTITLE","Matrix based Gens");

    scriptable_curve->setAttr("TABTITLE","Scriptable Curve");

    clear_but->buttonClickOffSig.connect(this, &GenEditorTab::clearAll);
    store_but->buttonClickOffSig.connect(this, &GenEditorTab::storeCurrent);
    gen_nbr->postValueChangedSig.connect(this,&GenEditorTab::updateContent);

    copy_code_button->setAttr("TIP","Copy gen code to clipboard");
    copy_code_button->buttonClickOffSig.connect([&](){
        int index = gen_nbr->getValue();
        std::string code = getGen(gen_data[std::to_string(index)], index);
       MiupUtil::clipboard::copyToClipBoard(code.c_str());
    });


    remove_but->buttonClickOffSig.connect([=](){
        this->removeAt(gen_nbr->getAttrInt("VALUE"));
    });


    text->setAttr("SIZE", "300x");
    text_button->buttonClickOffSig.connect(this, &GenEditorTab::storeHandwrittenGen);


    loadData();


}


GenEditorTab::~GenEditorTab() {
    delete matrix_line_number_label;
    delete matrix_contol_box;

    delete curve_control_box;
    delete copy_code_button;

    delete text;
    delete text_label;
    delete text_button;

    delete store_but;
    delete clear_but;
    delete load_sample_but;
    delete remove_but,

    delete gen_nbr_label;

    delete gen_nbr;
    delete curve_size;
    delete waveformplot;
    delete curveplot;
    delete scriptable_curve;

    delete mode_gen2;
    delete mode_gen9;
    delete radio_mat;
    delete radio_box;
    delete lignbr;
    delete matrix;
    delete matrixbox;
    delete tab;
    delete sample_name;

    delete control_box;
    delete curvebox;
    delete waveformbox;
    delete sample_control_box;
}

void GenEditorTab::clearAllData()
{
    gen_data.clear();
    clearAll();
}

void GenEditorTab::clearAll() {
    curveplot->clear();
    curveplot->setMode(CURVE_GEN16);
    waveformplot->clear();
    scriptable_curve->clear();
    matrix->clear();
    mode_gen2->setStatus(1);
    text->clear();
    sample_name->setAttr("TITLE", "");
}

void GenEditorTab::removeAt(int i)
{
    std::string index = std::to_string(i);
    gen_data.erase(index);
    saveData();
    clearAll();
}

void GenEditorTab::setFunctionNumber(int nbr)
{
    gen_nbr->setValue(nbr);
}


void GenEditorTab::updateContent(int idx)
{
    this->clearAll();
    std::string index = std::to_string(idx);

    if(gen_data.find(index) != gen_data.end() &&  gen_data[index]["valid"].get<bool>() == true) {
         std::string type = gen_data[index]["type"].get<std::string>();
         if(type == std::string(GEN_CURVE)) { // it is a curve
             std::string mode  = gen_data[index]["mode"].get<std::string>();
             curveplot->setMode(mode.c_str());

             tab->setAttrInt("VALUEPOS",0);  // set right tab
             curveplot->setMode(gen_data[index]["mode"].get<std::string>().c_str()); // set right mode


             int count = 1;
             if(gen_data[index].find("count") != gen_data[index].end()) count =
                     MiupUtil::string::getNumber<int>(gen_data[index]["count"].get<std::string>());


             for(int i = 0; i < count; i++) {
                json point = gen_data[index]["points"][std::to_string(i)];
                double x = point["x"].get<double>();
                double y = point["y"].get<double>();
                double crv = point["crv"].get<double>();
                curveplot->insertPoint(x,y,crv);
             }

             curveplot->redraw();

             if(gen_data[index].find("size") == gen_data[index].end()) gen_data[index]["size"] = 2048;
             this->curve_size->setValue(gen_data[index]["size"].get<int>());


         } else if (type == std::string(GEN_WAVEFORM)) {
             std::string path = gen_data[index]["path"].get<std::string>();
             if(MiupUtil::filesystem::isValidFilePath(path) && AudioFile::IsAudioFile(path)) {
                 tab->setAttrInt("VALUEPOS",1);
                 waveformplot->setSoundFile(gen_data[index]["path"].get<std::string>());
                 std::string filename = MiupUtil::filesystem::getFilenameFromPath(path);
                 sample_name->setAttr("TITLE", filename.c_str());
             } else { // should I delete then ?
                 std::string msg = "The adress of file '" + path + "' is invalid, or file is not an audio file. Please make sure to keep files in a safe place";
                 int res =  IupAlarm("File not found", msg.c_str(), "OK", "Delete reference",NULL);
                 if(res==2) removeAt(gen_nbr->getAttrInt("VALUE"));
             }
         } else if(type == std::string(GEN_MATRIX)) {
             std::string mode = gen_data[index]["mode"].get<std::string>();
             tab->setAttrInt("VALUEPOS",2);

             if(gen_data[index].find("points") == gen_data[index].end() )
                 return;

             if(gen_data[index].find("lignbr") != gen_data[index].end()) lignbr->setValue( gen_data[index]["lignbr"].get<int>() );

             if(mode == std::string(GEN_02)) {
                 mode_gen2->setStatus(1);
                 for(auto & it : gen_data[index]["points"].items()) {
                        int lin = MiupUtil::string::getNumber<int>( it.key() );
                        matrix->setValueAt(lin, 1, it.value().get<std::string>().c_str() );
                 }
             } else if(mode == std::string(GEN_09)) {
                 mode_gen2->setStatus(0);
                 mode_gen9->setStatus(1);
                 for(auto & it : gen_data[index]["points"].items()) {
                        int lin = MiupUtil::string::getNumber<int>( it.key() );
                        json line = it.value();
                        for(auto & c_it : line.items()) {
                            int col = MiupUtil::string::getNumber<int>(c_it.key());
                            matrix->setValueAt(lin,col, c_it.value().get<std::string>().c_str());
                        }
                 }
             }
             matrix->refresh();
         } else if(type == std::string(GEN_HANDWRITTEN)) {
             text->setAttr("VALUE", gen_data[index]["data"].get<std::string>().c_str());
         } else if(type == std::string(GEN_SCRIPTABLE)) {
             tab->setAttrInt("VALUEPOS",3);
             scriptable_curve->setContent(gen_data[index]["script"].get<std::string>());
         }
    }
}



void GenEditorTab::storeCurrent() {
   std::string index = gen_nbr->getAttrStr("VALUE");
//   std::cout << "current tab" << tab->getAttr("VALUEPOS") << std::endl;
   json data;
   switch(tab->getAttrInt("VALUEPOS")) { //which tab is current
   case 0: // curveplot
       data = curveplot->getData();
       if(data["valid"].get<bool>() == true) {
            data["size"]=curve_size->getAttrInt("VALUE");
            gen_data[index] = data;
       } else {
           int res = IupAlarm("Invalid curve","The selected curve is not valid, and cannot be saved. Make sure it has at least one sample.","OK", NULL, NULL);
       }
       break;
   case 1: // waveform plot
       data = waveformplot->getFileInfo();
       if(data["valid"].get<bool>() == true ) {
                gen_data[index] = data;
       } else {
           int res = IupAlarm("Invalid soundfile","The selected soundfile is not valid, and cannot be saved. Make sure this is a valid soundfile","OK", NULL, NULL);
       }
       break;
   case 2: // matrix data
   {
        int nlin = matrix->getAttrInt("NUMLIN");
                data["type"] = GEN_MATRIX;
                data["valid"]=true;
                data["lignbr"] = matrix->getAttrInt("NUMLIN");

                if(std::string(mode_gen2->getAttr("VALUE")) == std::string("ON") ) {
                    data["mode"]=GEN_02;
                    for(int i = 1; i <= nlin; i++) {
                        std::string idx = std::to_string(i);
                        data["points"][idx] = matrix->getValueAt(i,1);
                    }
                } else {
                    data["mode"]=GEN_09;
                    for(int i = 1; i <= nlin; i++) {
                        std::string idx = std::to_string(i);
                        for(int c = 1; c<=3; c++) {
                            data["points"][idx][std::to_string(c)] = matrix->getValueAt(i,c);
                        }
                    }
                }
             gen_data[index]=data;

       break;
   }
   case 3: // scriptable curve
   {
       std::string script = scriptable_curve->getScript();
       std::string store_path = getSamplePath() + "scriptable_curve_" + index + ".raw";
       json data;
       data["script"] = script;
       data["valid"] = true;
       data["type"] = GEN_SCRIPTABLE;
       data["path"] = store_path;
       data["size"] = scriptable_curve->getSize();
       gen_data[index] = data;

       scriptable_curve->storeRaw(store_path);
       break;
   }
   }

   saveData();
}

void GenEditorTab::storeHandwrittenGen()
{
    std::string txtval = text->getAttrStr("VALUE");
    if(txtval.size() < 5 || txtval[0] != 'f' ) return;

    json data;

    std::regex reg("(?:\\s+([0-9]+(?:\\.[0-9]+)?))");
    std::sregex_iterator next(txtval.begin(), txtval.end(), reg);
    std::sregex_iterator end;

    while(next != end)
    {
        std::smatch match = *next;
        std::cout << "Matched :: " << match.str() << std::endl;
        std::string m = match.str();
        std::string r = MiupUtil::string::removeCharactersFromString(m, " ");
        data["points"].push_back(r);
        next++;
    }

    // then store it

    data["type"] = GEN_HANDWRITTEN;
    data["valid"] = true;
    data["data"] = text->getAttrStr("VALUE");

    std::string index = gen_nbr->getAttrStr("VALUE");
    gen_data[index] = data;

    saveData();
}


void GenEditorTab::browseFile()
{
    FileDialog dlg(FileDialog::OPEN, browse_path.c_str());
    dlg.emitPath.connect([&](std::string path) {
        std::cout << " emit path : " << path << std::endl;
        dlg.hide();
       waveformplot->setSoundFile(path);
       browse_path = MiupUtil::filesystem::getFolderFromPath(path);
    });
    dlg.popup();
}


void GenEditorTab::saveData(std::string path)
{
    std::ofstream ofs(path);
    ofs << gen_data.dump(4);
    ofs.close();
}


json *GenEditorTab::getData()
{
      return &gen_data;
}

void GenEditorTab::loadData(std::string path)
{
    std::ifstream sz(path, std::ios::binary | std::ios::ate);
    int size = sz.tellg();
    sz.close();


    if(size > 0) {
        std::ifstream ifs(path);
        ifs >> gen_data;
        ifs.close();
    }

    updateContent(this->gen_nbr->getAttrInt("VALUE"));

}


std::string GenEditorTab::ToHandwrittenGen(json &data, int index)
{
    if(data["type"].get<std::string>() != std::string(GEN_HANDWRITTEN)) return "error";

    std::string res("f ");

    bool isindex = true;
    for(auto & it : data["points"])
    {
        if(isindex) {
            res += std::to_string(index) + " ";
        } else {
            res += MiupUtil::string::getString(it) + " ";
        }
        isindex = false;
    }
    res += "\n";
    res = MiupUtil::string::removeCharactersFromString(res, "\"");

    return res;
}


std::string GenEditorTab::ToGen8(json &data, int index) {
    // format = f # date size 8 ay timebtw by timebtw  cy...

    if(data["type"].get<std::string>() != std::string(GEN_CURVE)) return "error";

    std::string res("");

    if(data.find("valid") != data.end() && data["valid"].get<bool>() == true && data.find("points") != data.end() ) {
        res = "f " + std::to_string(index) + " 0 " + std::to_string(data["size"].get<int>() + 1 ) + " -8 "; // +1 for guard point
        int domain = data["size"].get<int>();
        int seen = 0;
        //for (auto & it : data["points"].items()) {
        int count = 1;
        if(data.find("count") != data.end()) count = MiupUtil::string::getNumber<int>(data["count"].get<std::string>());

        for(int i=0; i< count; i++) {
            std::string p_idx = std::to_string(i);
            json cur_point = data["points"][p_idx];
            //json cur_point = it.value();
              res += MiupUtil::string::getString<double>(cur_point["y"].get<double>())+ "  " ;
                if(data["points"].find(std::to_string(i+1)) != data["points"].end())  {
                    double time = data["points"][std::to_string(i+1)]["x"].get<double>() * domain;
                    res += MiupUtil::string::getString<double>( floor(time - seen) ) + " ";
                    seen +=(time - seen);
               }
        }
    }
    return res;
}


std::string GenEditorTab::ToGen16(json &data, int index)
{
    if(data["type"].get<std::string>() != std::string(GEN_CURVE)) return "error";
    std::string res("error");

    if(data.find("points") != data.end() ) {
        res = "f " + std::to_string(index) + " 0 " + std::to_string(data["size"].get<int>() + 1 ) + " -16 ";
        int seen = 0;
        int domain = data["size"].get<int>();
        int count = 1;
        if(data.find("count") != data.end()) count = MiupUtil::string::getNumber<int>(data["count"].get<std::string>());

        //for (auto & it : data["points"].items()) {
        for(int i =0; i < count; i++) {
            std::string p_idx = std::to_string(i);
            json cur_data = data["points"][p_idx];
            //json cur_data = it.value();
            double y = cur_data["y"].get<double>();
            double crv = cur_data["crv"].get<double>();

            // check if next y is superior, if so invert the curve
            if(i < count - 1) {
                double nexty = data["points"][std::to_string(i + 1)]["y"].get<double>();
                if(nexty > y) crv *= -1;
            }

            res += MiupUtil::string::getString<double>(y) + " ";

            //int idx = MiupUtil::string::getNumber<int>(it.key());
            if(data["points"].find(std::to_string(i +1 )) != data["points"].end()) {
                double time = data["points"][std::to_string(i + 1 )]["x"].get<double>() * domain;
                res += MiupUtil::string::getString<double>( floor(time - seen) ) + " " + MiupUtil::string::getString<double>(crv) + " " ;
                seen += (time - seen);
            }
        }
    }
    return res;
}

std::string GenEditorTab::ToGen23(json &data, int index)
{
    if(data["type"].get<std::string>() != std::string(GEN_SCRIPTABLE)) return "error";
    std::string res("error");
    if(data.find("path") != data.end()) {
        res = "f " + std::to_string(index) + " 0 " + std::to_string(data["size"].get<int>()) + " -23 \"" + data["path"].get<std::string>() + "\"";
    }

    return res;
}


std::string GenEditorTab::ToGenBezier(json &data, int index)
{
    if(data["type"].get<std::string>() != std::string(GEN_CURVE)) return "error";
    std::string res("error");

    if(data.find("points") != data.end() ) {
        res = "f " + std::to_string(index) + " 0 " + std::to_string(data["size"].get<int>() + 1 ) + " \"quadbezier\" ";
        int domain = data["size"].get<int>();
        int count = 1;

        if(data.find("count") != data.end()) count = MiupUtil::string::getNumber<int>(data["count"].get<std::string>());

        //for (auto & it : data["points"].items() {
         for(int i = 0; i < count; i++) {
             std::string p_idx = std::to_string(i);
            //int idx = MiupUtil::string::getNumber<int>(it.key());
            if(data["points"].find(std::to_string(i - 1 )) != data["points"].end()) { // then add x
                double x = data["points"][std::to_string(i)]["x"].get<double>() * domain;
                res += std::to_string((int)floor(x)) + " " ;
            }

            //json cur_data = it.value();
            json cur_data = data["points"][p_idx];
            double y = cur_data["y"].get<double>();
            res += MiupUtil::string::getString<double>(y) + " ";
        }
    }
    return res;
}


std::string GenEditorTab::ToGen1(json &data, int index)
{
    if(data["type"].get<std::string>() != std::string(GEN_WAVEFORM)) return "error";
    std::string res("");
    if(data.find("valid") != data.end() && data.find("path") != data.end()) {
        res ="f " + std::to_string(index) + " 0 0 1 \"" + data["path"].get<std::string>() + "\" 0 0 0";
    }
    return res;
}

std::string GenEditorTab::ToGen2(json &data, int index)
{
   int size = data["lignbr"].get<int>();
   std::string res = "f " + std::to_string(index) + " 0 " + std::to_string(size) + " -2 ";
   if(data.find("points") == data.end()) return "error";

   std::regex j_tracker_macro_scale ("s(\\d+),(\\d+),(\\d+)");

   json cur_data = data;
   for(auto & c_it : cur_data["points"].items() ) {
            if(std::regex_match(c_it.value().get<std::string>(), j_tracker_macro_scale)) {
                //std::cout << "MATCHED " << std::endl;
                std::smatch match;
                std::string to_evaluate = c_it.value().get<std::string>();
                std::regex_search(to_evaluate, match, j_tracker_macro_scale);
                ScaleCurve sc;
                sc.f_nbr = MiupUtil::string::getNumber<int>(match.str(1));
                sc.scale_min = MiupUtil::string::getNumber<int>(match.str(2));
                sc.scale_max = MiupUtil::string::getNumber<int>(match.str(3));
                int gen_idx = scaleListAddifNotExists(sc);
                c_it.value() = std::to_string(gen_idx * -1 );
                //cur_data["points"][c_it.key()] = std::to_string(gen_idx);
            }
   }

   for(int i=1; i <=size; i++) {
        std::string idx = std::to_string(i);
        if(cur_data["points"].find(idx) != cur_data["points"].end()) {
            res += cur_data["points"][idx].get<std::string>() + " ";
        }
   }
   return res;
}

std::string GenEditorTab::ToGen9(json &data, int index)
{
   int size = data["lignbr"].get<int>();
   std::string res = "f " + std::to_string(index) + " 0 2049 9 ";
   if(data.find("points") == data.end()) return "error";
   for(int l = 1; l <= size; l++) {
      std::string line = std::to_string(l);
      if(data["points"].find(line) == data["points"].end()) continue;
      for(int c = 1; c <= 3; c++) {
          std::string col = std::to_string(c);
          if(data["points"][line].find(col) == data["points"][line].end()) continue;
          res += data["points"][line][col].get<std::string>() + " ";
      }
   }
   return res;
}

std::string GenEditorTab::getGen(json &data ,int index) {
    std::string res("error");
    if(data.find("valid") != data.end() && data["valid"].get<bool>() == true ) {
        std::string type = data["type"].get<std::string>();
        if(type == std::string(GEN_CURVE)) {
            std::string mode = data["mode"].get<std::string>();
            if(mode == std::string(CURVE_GEN08)) {
                res = GenEditorTab::ToGen8(data,index);
            } else if(mode == std::string(CURVE_GEN16)) {
                 res = GenEditorTab::ToGen16(data,index);
            } else if(mode == std::string(CURVE_GENBEZIER)) {
                res = GenEditorTab::ToGenBezier(data,index);
            }
        } else if (type == std::string(GEN_WAVEFORM)) {
            res = GenEditorTab::ToGen1(data,index);
        } else if(type == std::string(GEN_MATRIX)) {
                std::string mode = data["mode"].get<std::string>();
                if(mode == std::string(GEN_02)) res = GenEditorTab::ToGen2(data, index);
                else if(mode == std::string(GEN_09)) res = GenEditorTab::ToGen9(data,index);
        } else if(type == std::string(GEN_HANDWRITTEN)) {
            res = GenEditorTab::ToHandwrittenGen(data, index);
        } else if(type == std::string(GEN_SCRIPTABLE)) {
            res = GenEditorTab::ToGen23(data, index);
        }
    }
    return res;
}


std::string GenEditorTab::getAllGen()
{
   //std::regex j_tracker_macro_scale ("s(\\d+),(\\d+),(\\d+)");

    std::string res("");
    for(auto & it : gen_data.items()) {
        json cur = it.value();
        int index = MiupUtil::string::getNumber<int>(it.key());
        res += GenEditorTab::getGen(cur,index);
        res += " \n";
    }
    return res;
}


int GenEditorTab::scaleListAddifNotExists(ScaleCurve scale_param)
{
   std::string base_gen_index = std::to_string(scale_param.f_nbr);
   std::string identifier =  std::to_string(scale_param.f_nbr) + "_" + std::to_string(scale_param.scale_min) + "_" + std::to_string(scale_param.scale_max);
   if(scale_list.find(identifier) != scale_list.end()) { // then return index
       return scale_list[identifier].index;
   }  else { // not exists, insert
       //first check if valid curve else return -1
       if(gen_data.find(base_gen_index) == gen_data.end()) return -1;
       if(gen_data[base_gen_index]["type"].get<std::string>() != std::string(GEN_CURVE)) return -1;
       //find max gen number in current project
       int max = 0;
       for(auto& it : gen_data.items()) if( MiupUtil::string::getNumber<int>(it.key()) > max ) max = MiupUtil::string::getNumber<int>(it.key());
       scale_param.index =scale_list.size() + max +1;
       //add mode
       if(gen_data[base_gen_index].find("mode") != gen_data[base_gen_index].end())
           scale_param.mode = gen_data[base_gen_index]["mode"].get<std::string>();

       scale_list[identifier] = scale_param;
       return scale_param.index;
   }
}

void GenEditorTab::scaleListClear()
{
    scale_list.clear();
}

std::string GenEditorTab::scaleListFormat() //public
{
    std::string res(";scaled gen from jo_tracker score editor \n");
    for(auto & it : scale_list) {
        res += getScaledGen(it.second) + " \n";
    }
    return res;
}

std::string GenEditorTab::getScaledGen(ScaleCurve scale_param)
{
    std::string gen_base_index = std::to_string(scale_param.f_nbr);
    json temp = gen_data[gen_base_index];
    double ambitus = scale_param.scale_max - scale_param.scale_min;

    for(auto & it : gen_data[gen_base_index]["points"].items()) {
        int p_idx = MiupUtil::string::getNumber<int>(it.key());
        double y = it.value()["y"].get<double>();
        y *= ambitus;
        y += scale_param.scale_min;
        if(y == (int)floor(y)) temp["points"][std::to_string(p_idx)]["y"] = (int)floor(y);
        else temp["points"][std::to_string(p_idx)]["y"] =  y;
    }

    return GenEditorTab::getGen(temp, scale_param.index);
}


void GenEditorTab::setData(json *j)
{
    gen_data.clear();
    gen_data = *j;
}

std::vector<std::string> GenEditorTab::getSoundfilePaths() {
    std::vector<std::string> res;
    for(auto & it : gen_data.items()) {
        json cur_gen = it.value();


        if(cur_gen.find("type") != cur_gen.end() && (cur_gen["type"].get<std::string>() == std::string(GEN_WAVEFORM) || cur_gen["type"].get<std::string>() == std::string(GEN_SCRIPTABLE) )) {
            if(cur_gen.find("path") != cur_gen.end() && !cur_gen["path"].is_null()) {
                    std::string path = cur_gen["path"].get<std::string>();
                    if(!path.empty()) {
                        res.push_back(path);
                    }
            }
            if(cur_gen.find("peakpath") != cur_gen.end() && !cur_gen["peakpath"].is_null()) {
                    std::string peakpath = cur_gen["peakpath"].get<std::string>();
                    if(!peakpath.empty()) {
                        res.push_back(peakpath);
                    }
            }
        }
    }

        return res;
}

json GenEditorTab::getGatheredData()
{
    json gathered = gen_data;
    for(auto & it : gathered.items()) {
        json cur_gen = it.value();

        if(cur_gen.find("type") != cur_gen.end() && cur_gen["type"].get<std::string>() == std::string(GEN_WAVEFORM)) {

            if(cur_gen.find("path") != cur_gen.end() && !cur_gen["path"].is_null()) {
                    std::string path = cur_gen["path"].get<std::string>();
                    if(!path.empty()) {
                        std::string fname =MiupUtil::filesystem::getFilenameFromPath(path);
                        gathered[it.key()]["path"] =  fname;
                    }
            }
            if(cur_gen.find("peakpath") != cur_gen.end() && !cur_gen["peakpath"].is_null()) {
                    std::string peakpath = cur_gen["peakpath"].get<std::string>();
                    if(!peakpath.empty()) {
                        std::string fname = MiupUtil::filesystem::getFilenameFromPath(peakpath);
                        gathered[it.key()]["peakpath"] = fname;
                    }
            }
        }
    }
    return gathered;
}

void GenEditorTab::resolvePath(std::string base_path)
{
    for(auto & it : gen_data.items()) {
        json cur_gen = it.value();


        if(cur_gen.find("type") != cur_gen.end() && cur_gen["type"].get<std::string>() == std::string(GEN_WAVEFORM)) {
            if(cur_gen.find("path") != cur_gen.end() && !cur_gen["path"].is_null()) {
                    std::string path = cur_gen["path"].get<std::string>();
                    if(!path.empty()) {
                        std::string folder_based_path = base_path + "/" + MiupUtil::filesystem::getFilenameFromPath(path);
                        if(MiupUtil::filesystem::isValidFilePath(folder_based_path) ) {
                            std::cout << "found " << folder_based_path << std::endl;
                            gen_data[it.key()]["path"] = folder_based_path;
                        }
                    }
            }
            if(cur_gen.find("peakpath") != cur_gen.end() && !cur_gen["peakpath"].is_null()) {
                    std::string peakpath = cur_gen["peakpath"].get<std::string>();
                    if(!peakpath.empty()) {
                        std::string folder_based_path = base_path + "/" + MiupUtil::filesystem::getFilenameFromPath(peakpath);
                        if(MiupUtil::filesystem::isValidFilePath(folder_based_path) ) {
                            std::cout << "found " << folder_based_path << std::endl;
                            gen_data[it.key()]["peakpath"] = folder_based_path;
                        }
                    }
            }
        }
    }

}


std::string GenEditorTab::getGatheredGen()
{
    json temp = gen_data;
    gen_data = getGatheredData();

    std::string gens = getAllGen();
    gen_data = temp;
    return gens;

}
