
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef GENEDITORTAB_H
#define GENEDITORTAB_H

#include<algorithm>

#include"Dialogs/filedialog.h"
#include"Plots/curveplot.h"
#include"Plots/waveformplot.h"
#include"Plots/multichannelwaveform.h"
#include"Plots/scriptablecurve.h"

#include"Boxes/tabs.h"
#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Widgets/spinbox.h"
#include"Widgets/button.h"
#include"Widgets/flatseparator.h"
#include"Widgets/matrix.h"
#include"Text/label.h"
#include"Text/text.h"

#include"jo_tracker/jdef.h"

#include"Dependencies/nlohmann/json.hpp"

#define GEN_MATRIX "matrix"
#define GEN_02 "gen2"
#define GEN_09 "gen9"
#define GEN_HANDWRITTEN "gen_handwritten"
#define GEN_SCRIPTABLE "scriptable_curve"

using json = nlohmann::json;

struct CurveData {
    std::string type;
    std::vector< std::pair < double, double > > points;
    std::vector<double> curvetab;
};

struct ScaleCurve {
  int f_nbr;
  double scale_min;
  double scale_max;
  int index;
  std::string mode;
};


class GenEditorTab : public Vbox
{
public:
    GenEditorTab();
    static std::shared_ptr<GenEditorTab> CreateShared();
    ~GenEditorTab();


    void clearAll();
    void storeCurrent();

    //void saveData(std::string path = GEN_DATA_PATH);
    void saveData(std::string path = getGenDataPath());

    //void loadData(std::string path = GEN_DATA_PATH);
    void loadData(std::string path = getGenDataPath());

    json *getData();

    void updateContent(int index);

    void removeAt(int i);

    static std::string ToGen23(json &data, int index);
    static std::string ToGen8(json &data, int index);
    static std::string ToGen16(json &data, int index);
    static std::string ToGenBezier(json &data, int index);
    std::string ToGen2(json &data, int index);
    static std::string ToGen9(json &data, int index);

    static std::string ToGen1(json &data, int index);

    std::string getGen(json &data, int index);
    std::string getAllGen();

    int scaleListAddifNotExists(ScaleCurve);
    void scaleListClear();
    std::string scaleListFormat();

    std::string getScaledGen(ScaleCurve scale_param);

    void setData(json *j);

    void clearAllData();

    //three methods when loading from a gathered folder (filepaths etc)
    std::vector<std::string> getSoundfilePaths();
    json getGatheredData();
    void resolvePath(std::string base_path);
    std::string getGatheredGen();


    void storeHandwrittenGen();
    static std::string ToHandwrittenGen(json &data, int index);

    void setFunctionNumber(int nbr);

    lsignal::signal<void(bool)> detachSig;
private:
    void browseFile();

    static std::string browse_path;

    //member
    json gen_data;

    std::unordered_map< std::string, ScaleCurve> scale_list;

    //Widgets
    Label *gen_nbr_label;
    SpinBox<int> *gen_nbr, *curve_size;
    CurvePlot *curveplot;
    Vbox * curvebox;

    Vbox*waveformbox;
    //WaveformPlot *waveformplot;
    MultichannelWaveform *waveformplot;
    Button *load_sample_but;

    Toggle *mode_gen2, *mode_gen9;
    Radio *radio_mat;
    Hbox *radio_box;
    SpinBox<int> *lignbr;
    Matrix *matrix;
    Vbox *matrixbox;

    Button *store_but;
    Button *clear_but;
    Button * remove_but;
    Hbox *control_box;

    Label *sample_name, *matrix_line_number_label;
    Hbox * sample_control_box, *curve_control_box, *matrix_contol_box;
    Button *copy_code_button;
    Tabs *tab;

    ScriptableCurve *scriptable_curve;


    //text for handwritten GEN
    Text *text;
    Label *text_label;
    Button *text_button;
    Toggle *detach_button;


};

#endif // GENEDITORTAB_H
