#include"global_data.h"


namespace JoTracker {
namespace GlobalData {

    // global copy of audio samples based on oscilloscope nsamps (circular buffer)

    std::mutex mtx;
    std::vector<std::vector<MYFLT>> samples;

    void init(CsoundThreaded  *cs, int n_samps)
    {
        for(auto & it : samples) it.clear();
            samples.clear();
            samples.resize(cs->GetNchnls());

        for(int ch = 0; ch< cs->GetNchnls(); ch++) {
                // resize vector
                samples[ch].resize(n_samps);
                ::memset(samples[ch].data(), 0, sizeof(double) * n_samps );
        std::string ds_name = "channel " + std::to_string(ch);
        }
    }

    void updateSamples(CsoundThreaded *cs, int n_samps)
    {
        size_t offset = 0;
        // calculate the difference between csound buffer size and oscillo buffer size

        if(n_samps > cs->GetKsmps()) {
        offset = n_samps - cs->GetKsmps() - 1;
        for(int ch = 0; ch < cs->GetNchnls(); ch++) {
            ::memcpy(samples[ch].data(), samples[ch].data() + cs->GetKsmps(), sizeof(double) * offset);
        }

        } else if(n_samps == cs->GetKsmps()) {
        offset = 0;
        // no copy
        // n_samps is inferior to, then only draw part of the signal // or downsampled signal
        } else {
        // no copy
        // one sample on two ?
        //or only the begining of buffer
        //or forbidden
        }

        for(int ch = 0; ch < cs->GetNchnls(); ch++) {
            for(int i = 0; i < cs->GetKsmps(); i++) {
                if(i == n_samps) break;
                samples[ch][i + offset] = cs->GetSpoutSample(i, ch);
            }
        }
    }
}
}
