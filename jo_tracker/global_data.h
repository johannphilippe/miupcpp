#ifndef GLOBAL_DATA_H
#define GLOBAL_DATA_H
#include<vector>
#include<iostream>
#include"Audio/csoundthreaded.h"


namespace JoTracker {
namespace GlobalData {

    // global copy of audio samples based on oscilloscope nsamps (circular buffer)
    extern std::mutex mtx;
    extern std::vector<std::vector<MYFLT>> samples;

    extern void init(CsoundThreaded  *cs, int n_samps);
    extern void updateSamples(CsoundThreaded *cs, int n_samps);
}
}

#endif // GLOBAL_DATA_H
