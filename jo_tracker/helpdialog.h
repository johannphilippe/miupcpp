
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef HELPDIALOG_H
#define HELPDIALOG_H

#include"Dialogs/dialog.h"
#include"Boxes/vbox.h"
#include"Text/label.h"
#include"Text/text.h"
#include"Widgets/webbrowser.h"


class HelpDialog : public Dialog
{
public:
    HelpDialog(std::string title_str, std::string help_content) : title(title_str), main_text(help_content.c_str(), true),  Dialog()
    {
        //title.setAttr("VALUE", title_str.c_str());
        main_text.setAttr("READONLY","YES");
        //main_text.setAttr("VALUE",help_content.c_str());
        main_box.push(&title, &main_text);
        this->construct(&main_box);

        main_text.setAttr("EXPAND","YES");

        this->setAttr("SIZE","500x250");
    }

    ~HelpDialog(){

    }

    void setText(std::string text) {
        main_text.setAttr("VALUE",text.c_str());
    }

    void setTitle(std::string title_) {
        title.setAttr("VALUE",title_.c_str());
    }

private:
    Text main_text;
    Label title;
    Vbox main_box;
};


class WebHelpDialog : public Dialog
{

public:
    WebHelpDialog() : Dialog() {
        main_box.push(&web);

        this->construct(&main_box);
        this->setAttr("SIZE","500x300");
    }

    ~WebHelpDialog(){

    }

    void setFile(std::string path) {
        std::string formatted = "file://" + path;
        web.setAttr("VALUE",formatted.c_str());
    }

    void setUrl(std::string url) {

    }

private:
    WebBrowser web;
   Vbox main_box;


};


#endif // HELPDIALOG_H
