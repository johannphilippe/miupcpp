#ifndef JCOLOR_H
#define JCOLOR_H


constexpr static const char * COLOR_LIGHTORANGE =  "244 190 86";
constexpr static const char * COLOR_LIGHTBLUE_GREY = "86 244 234";
constexpr static const char * COLOR_LIGHTPURPLE = "166 156 255";
constexpr static const char * COLOR_LIGHTGREEN = "156 255 175";
constexpr static const char * COLOR_LIGHTRED = "255 157 156";
constexpr static const char * COLOR_DARKRED = "142 2 0";
constexpr static const char * COLOR_WHITE = "255 255 255";
constexpr static const char * COLOR_BLACK = "0 0 0";
constexpr static const char * COLOR_DEEPBLUE = "5 6 126";

#endif // JCOLOR_H
