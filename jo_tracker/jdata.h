#ifndef JDATA
#define JDATA

#include<iostream>
//Data Structure for one Seq/Track
struct TrackData {
    TrackData(int lin=8,int col=8) {
        resize(lin,col);
    }

    void setValueAt(int l, int c,std::string val) {
        if(l<data.size() && c<data[l].size()) {
            data[l][c]=val;
        }
    }

    std::string getValueAt(int l,int c) {
        if(l<data.size() && c<data[l].size()) {
            if(data[l][c].size()==0) return "NULL";
            return data[l][c];
        }
    }

    void resize(int lin, int col) {
        if(lin<=0 || col<=0) return;
        if(lin!=data.size()) {
            data.resize(lin);
        }
            for(int i=0;i<data.size();i++) data[i].resize(col);
    }
    void setNumcol(int col) {resize(data.size(),col);}
    void setNumlin(int lin) {resize(lin,data[0].size());}
    int lignbr() {return data.size();}
    int colnbr(int lin=0) {return data[lin].size();}

    void clear() {data.clear();}

    std::vector < std::vector < std::string> > data;
};

// Main score data structure
struct ScoreData {
    ScoreData(int seq, int track) {
        resize(seq,track);
    }

    void resize(int seq,int track) {
        if(seq<=0 || track <=0) return;
        if(seq!=data.size()) {
            data.resize(seq);
        }
            for(int i=0;i<data.size();i++) data[i].resize(track);
    }

    void setValueAt(int seq,int track,int lin,int col,std::string val) {
        if(seq>=0 && seq<data.size() && track>=0 && track<data[seq].size()) {
            data[seq][track].setValueAt(lin,col,val);
        }
    }

    std::string getValueAt(int seq,int track,int lin, int col) {
        if(seq>=0 && seq<data.size() && track>=0 && track<data[seq].size()) {
            return data[seq][track].getValueAt(lin,col);
        }
    }

    void setNumlin(int seq,int lin) {for(int i=0;i<data[seq].size();i++) data[seq][i].setNumlin(lin);}

    void setNumcol(int seq,int track,int col) {data[seq][track].setNumcol(col);}
    void setNumtrack(int track) {resize(data.size(),track);}
    void setNumseq(int seq) {resize(seq,data[0].size());}

    void clear() {
        data.clear();
    }

    void clearSeq(int seq) {
        if(seq<data.size()) {
            data[seq].clear();
        }
    }

    void clearTrack(int seq,int track) {
        data[seq][track].clear();
    }

    void clearTrackDeep(int track) {
        for(int s=0;s<data.size();s++) {
            data[s][track].clear();
        }
    }

    int seqnbr() {return data.size();}
    int tracknbr(int seq=0) {return data[seq].size();}
    int lignbr(int seq=0) {return data[seq][0].lignbr();}
    int colnbr(int seq=0,int track=0) {return data[seq][track].colnbr();}
    std::vector < std::vector < TrackData > > data;
};






//tempo data
struct TempoData {
    TempoData(int seq=1) {
        resize(seq);
    }

    void resize(int seq) {
        if(seq>0) data.resize(seq);
        for(int i=0;i<data.size();i++) data[i].setNumcol(2);
    }

    void setNumlin(int seq,int lin) {for(int i=0;i<data.size();i++) data[i].setNumlin(lin);}
    void setNumseq(int seq) {resize(seq);}

    void setValueAt(int seq,int lin,int col,std::string val) {
        if(seq>=0 && seq<data.size()) data[seq].setValueAt(lin,col,val);
    }

    std::string getValueAt(int seq,int lin, int col) {
        if(seq>=0 && seq<data.size()) {
            return data[seq].getValueAt(lin,col);
        }
    }

    void clearSeq(int seq) {
        if(seq>=0 && seq<data.size()) {
            data[seq].clear();
        }
    }

    void clear() {data.clear();}

    int seqnbr() {return data.size();}

    std::vector<TrackData> data;
};

#endif // JDATA

