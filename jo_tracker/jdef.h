
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JDEF_H
#define JDEF_H

#include"utils/miuputil.h"


#ifdef __linux__
    #define TRACKER_DIR "/.local/share/jo_tracker"
    #define RESOURCE_DIR "/.local/share/jo_tracker/res"
#elif _WIN32
    #define TRACKER_DIR "/AppData/Local/jo_tracker"
    #define RESOURCE_DIR "/AppData/Local/jo_tracker/res"
#elif __APPLE__
#endif
//instr metro
constexpr static const char *INSTR_METRO = "instr 1 \n"
                                         ";Metro instrument \n"
                                         "kbpm = 60/p3 \n"
                                         "chnset kbpm, \"tempo\" \n"
                                         "print p4 \n"
                                         "kgoto skip_k \n"
                                         "outvalue \"T_sync\", p4 \n"
                                         "skip_k: \n"
                                         "endin \n";

// same instrument with a regression factor (500) so that it doesn't refresh interface to much.
constexpr static const char *INSTR_METRO_EXPANDED = "instr 1 \n"
                                                    ";Metro instrument \n"
                                                    "kbpm = 60 / p3 \n"
                                                    "chnset kbpm, \"tempo\" \n"
                                                    "iRegression = floor( (60 / p3 ) / 500 ) + 1 \n"
                                                    "if ( (p4%iRegression) !=0) igoto skip \n"
                                                    "kgoto skip \n"
                                                    "outvalue \"T_sync\", p4 \n"
                                                    "skip: \n"
                                                    "endin \n";

static std::string getTrackerDirectory()
{
    //std::string res = MiupUtil::filesystem::getHomeDirectory() + "/.local/share/jo_tracker";
    std::string res = MiupUtil::filesystem::getHomeDirectory() + TRACKER_DIR;
    return res;
}

static std::string getResDirectory()
{
    //std::string res = MiupUtil::filesystem::getHomeDirectory() + "/.local/share/jo_tracker/res";
    std::string res = MiupUtil::filesystem::getHomeDirectory() + RESOURCE_DIR;
    return res;
}

static std::string getScoreDataPath()
{
    std::string res = getResDirectory() + "/score_data.json";
    return res;
}

static  std::string getOrcDataPath()
{
    std::string res = getResDirectory() + "/orc_data.json";
    return res;
}

static std::string getGenDataPath()
{
    std::string res = getResDirectory() + "/gen_data.json";
    return res;
}

static std::string getMacroDataPath()
{
    std::string res = getResDirectory() + "/macro_data.json";
    return res;
}

static std::string getScriptDataPath()
{
    std::string res = getResDirectory() + "/script_data.json";
    return res;
}

static std::string getAudioParametersPath() // audio parameters
{
    std::string res = getResDirectory() + "/audio_params.json";
    return res;
}

static std::string getParametersPath() // parameters (gui and general)
{
    std::string res = getResDirectory() + "/parameters.json";
    return res;
}

static std::string getUdoPath()
{
    std::string res = getResDirectory() + "/udo.json";
    return res;
}


static std::string getTerraDirectory()
{
    std::string res = getTrackerDirectory() + "/terra";
    return res;
}

static std::string getVimDocumentationPath()
{
    std::string res = getTrackerDirectory() + "/doc/vim_doc";
    return res;
}

static std::string getLuaApiDocumentationPath()
{
    std::string res = getTrackerDirectory() + "/doc/lua_api.html";
    return res;
}

static std::string getCsoundDocumentationPath()
{
    std::string res = getTrackerDirectory() + "/doc/html/";
    return res;
}

static std::string getSamplePath()
{
    std::string res = getTrackerDirectory() + "/samples/";
    return res;
}


static std::string getTrackerDocumentationPath()
{
    std::string res = getTrackerDirectory() + "/doc/jo_tracker_documentation";
    return res;
}

// Extension to IUP Keyboard codes (for non numeric pad computers)
#define K_cs1 805306417
#define K_cs2 805306418
#define K_cs3 805306419
#define K_cs4 805306420
#define K_cs5 805306421
#define K_cs6 805306422
#define K_cs7 805306423
#define K_cs8 805306424
#define K_cs9 805306425

#define K_as1 1342177329
#define K_as2 1342177330
#define K_as3 1342177331
#define K_as4 1342177332
#define K_as5 1342177333
#define K_as6 1342177334
#define K_as7 1342177335
#define K_as8 1342177336
#define K_as9 1342177337

#define K_cp 536870992
#define K_mp 1073741904


#ifdef __linux__
constexpr static const char *JO_TRACKER_FOLDER = "/usr/share/jo_tracker/";
constexpr static const char *JO_TRACKER_RES_FOLDER = "/usr/share/jo_tracker/res";
constexpr static const char *SCORE_DATA_PATH =  "/usr/share/jo_tracker/res/score_data.json";
constexpr static const char *ORCHESTRA_DATA_PATH = "/usr/share/jo_tracker/res/orc_data.json";
constexpr static const char *GEN_DATA_PATH =  "/usr/share/jo_tracker/res/gen_data.json";
constexpr static const char *AUDIO_PARAMETERS_PATH = "/usr/share/jo_tracker/res/params.json";
constexpr static const char *SCRIPT_DATA_PATH = "/usr/share/jo_tracker/res/script_data.json";
constexpr static const char *LOGO_PATH = "/usr/share/icons/jo_tracker/jo_tracker_icon.png";
#elif _WIN32
#elif __APPLE__
#endif



constexpr static const char *scripter_help = "This is a scripter HELP";

#endif // JDEF_H
