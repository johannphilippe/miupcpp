
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JIMPORTORCHESTRADIALOG_H
#define JIMPORTORCHESTRADIALOG_H

#include"Core/miupobj.h"

#include"Dialogs/dialog.h"
#include"Dialogs/filedialog.h"

#include"Boxes/hbox.h"
#include"Boxes/vbox.h"

#include"jo_tracker/csoundeditor.h"

#include"Widgets/button.h"
#include"Widgets/list.h"

#include"Dependencies/nlohmann/json.hpp"

using json = nlohmann::json;

class JImportOrchestraDialog : public Dialog
{
public:
    JImportOrchestraDialog() : Dialog( mainbox = new Vbox(
                listbox = new Hbox(
                        proposition_list=new List,
                        importbox = new Vbox( import = new Button("Import"), remove = new Button("Remove")),
                        to_import_list = new List
                ),
                //editor = new CsoundEditor(),
                acceptbox = new Hbox(ok = new Button("ok"), cancel = new Button("Cancel"))
                ))
    {

        proposition_list->setAttr("SIZE","250x200");
        to_import_list->setAttr("SIZE","250x200");


        import->buttonClickOffSig.connect(this, &JImportOrchestraDialog::importItem);
        remove->buttonClickOffSig.connect(this, &JImportOrchestraDialog::removeItem);
        ok->buttonClickOffSig.connect([&](){buttonClickedSig(true);});
        cancel->buttonClickOffSig.connect([&](){buttonClickedSig(false);});
       // this->setAttr("SIZE","800x500");


        //proposition_list->valueChangedSig.connect(this,&JImportOrchestraDialog::displayItem);


        lookForProject();

        this->mapSig.connect([&](){
            for(auto & it : namelist) {
                proposition_list->push_back(it);
            }
        });
    }

    ~JImportOrchestraDialog() {
                delete to_import_list;
                delete proposition_list;
                delete import;
                delete cancel;
                delete ok;
                delete remove;
                delete listbox;
                delete acceptbox;
                delete mainbox;
                delete importbox;
    }

    lsignal::signal<void(bool)> buttonClickedSig;

    void lookForProject() {
        FileDialog dlg("OPEN");

        dlg.emitPath.connect([&](std::string path) {
            std::cout << "PATH == " << path << std::endl;
               json tmp;
               std::ifstream ifs(path);
               ifs >> tmp;
               ifs.close();

               if(tmp.find("orc_data") != tmp.end()) {
                   infile = tmp["orc_data"];

                   if(infile.find("instr") != infile.end()) {
                       for(auto i_it : infile["instr"].items()) {
                           std::string todisplay = i_it.key() + " - " + "instr";
                           std::cout << todisplay << std::endl;
                           namelist.push_back(todisplay);
                           //proposition_list->push_back(namelist[namelist.size() - 1].c_str());
                       }
                   }

                   if(infile.find("udo") != infile.end()) {
                       for(auto u_it : infile["udo"].items()) {
                           std::string todisplay = u_it.key() + " - " + "udo";
                           std::cout << todisplay << std::endl;
                           namelist.push_back(todisplay);
                           //proposition_list->push_back(namelist[namelist.size() - 1].c_str());
                       }
                   }
               }
               return IUP_CLOSE;
        });

        dlg.popup();
        std::cout << "Popped up" << std::endl;
    }

    void importItem() {
        if(!to_import_list->itemExists(proposition_list->getSelectedItem()))
            to_import_list->push_back(proposition_list->getSelectedItem());
    }

    void removeItem() {
        to_import_list->removeSelectedItem();
    }

    json getImportedData() {
        std::regex reg ("(?:([a-zA-Z]+)\\s-\\s([a-zA-Z]+))");

        for(int i =0; i<= to_import_list->count(); i++) {
            std::smatch match;
            std::string cur = to_import_list->getItem(i);
            if(std::regex_search(cur, match,reg)) {
                std::string name = match[0];
                std::string type = match[1];

                if(infile[type].find(name) != infile[type].end()) {
                    outfile[type][name] = infile[type][name];
                }
            }
        }
        return outfile;
    }


private:

    List *to_import_list, *proposition_list;
    Button *import, *cancel, *ok, *remove;

    Hbox *listbox, *acceptbox;
    Vbox *mainbox, *importbox;

    std::vector<std::string> namelist;
    json outfile;
    json infile;
};

#endif // JIMPORTORCHESTRADIALOG_H
