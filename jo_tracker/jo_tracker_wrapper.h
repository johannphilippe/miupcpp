
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JO_TRACKER_WRAPPER
#define JO_TRACKER_WRAPPER


//widgets
#include"jo_tracker/jtrack.h"
#include"jo_tracker/jtrackset.h"
#include"jo_tracker/jtransport.h"
#include"jo_tracker/jscripterview.h"
#include"jo_tracker/csoundeditor.h"
#include"jo_tracker/audiodevicelist.h"
#include"jo_tracker/geneditortab.h"
#include"jo_tracker/csdformatter.h"

//dialogs
#include"jo_tracker/exportparametersdialog.h"
#include"jo_tracker/jimportorchestradialog.h"
#include"jo_tracker/parametersdialog.h"
#include"jo_tracker/joscilloscopedialog.h"
#include"jo_tracker/macrodialog.h"
#include"jo_tracker/jotrackerutilities.h"
#include"jo_tracker/aboutdialog.h"

#include"jo_tracker/jdef.h"
#include"jo_tracker/jcolor.h"

#include"jo_tracker/JToolBox.h"
#endif // JO_TRACKER_WRAPPER

