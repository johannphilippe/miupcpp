
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JOSCILLOSCOPEDIALOG_H
#define JOSCILLOSCOPEDIALOG_H

#include"Plots/scopeplot.h"
#include"Dialogs/dialog.h"
#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Widgets/spinbox.h"

class JOscilloscopeDialog : public Dialog
{

public:

    JOscilloscopeDialog() : plot(512), samples(512, 32, 16384, 16)
    {
        samples.postValueChangedSig.connect([&](int val){
           plot.setDomain(val);
        });
        parameters.push(&samples);
        mainbox.push(&parameters, &plot);
        this->construct(&mainbox);
        this->setAttr("SIZE","800x200");
        this->setAttr("TITLE","Oscilloscope");
    }

    ScopePlot *getPlot() {return &plot;}

    int getNsmps() {return samples.getValue();}
private:
    Vbox mainbox;
    ScopePlot plot;
    SpinBox<int> samples;
    Hbox parameters;
};

#endif // JOSCILLOSCOPEDIALOG_H
