/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef JOTRACKERUTILITIES_H
#define JOTRACKERUTILITIES_H

#include"jo_tracker/jo_tracker_wrapper.h"
#include"Audio/csoundthreaded.h"
#include"Core/miup_wrapper.h"

#include"examples/jo_tracker/icons/mute.h"
#include"jo_tracker/global_data.h"

namespace JoTracker {




static std::string browse_path = MiupUtil::filesystem::getHomeDirectory();


// Creates images, icons, etc for static use
static void InitializeSoftware() {
    Image mute = Image(create_image_mute());
    mute.setAttr("RESIZE", "24x24");
    mute.setHandle("mute_icon");
}


// SOME GLOBAL OBJECTS
    MacroDialog *macro_dlg;


    CsoundThreaded *cs = nullptr;
    CsoundMutex *cs_mutex;
    CsoundThreadLock *cs_lock;

    //JTrackSet trackset;
    bool is_playing = false;


    Dialog * main_dialog_ptr;

    struct CallbackValues
    {
      MYFLT level_1, level_2;
      int t_sync;
    };

    static CallbackValues cbk_vals;


//Audio channel names - refreshed when starting realtime - designed to refresh level meters values
    std::vector<std::string> channels;

    struct PerformanceHostData {
        JTrackSet *trackset;
        JTransport *transport;
        GenEditorTab *genbox;
        CsoundEditorBox *editor;
        JOscilloscopeDialog *oscilloscope;
    };

    //PerformanceHostData *global_host_data;

    static void outvalueCallback(CSOUND * /*cs */, const char * channel_name, void * value_ptr, const void * /*chn_type*/) {
        double * valptr = (double *)value_ptr;
        //PerformanceHostData *hostData = (PerformanceHostData *)csoundGetHostData(cs);

        std::string ch_name(channel_name);

        if(ch_name == std::string("T_sync")) {
            cbk_vals.t_sync = int(*valptr);
            /*
            Miup::pushCallbackMethod(*valptr, [=](double v) {
                hostData->trackset->setLineSelection((int)v);
            });
            */

        } else if( ch_name == std::string("level_1")) {
            cbk_vals.level_1 = *valptr;
            /*
            Miup::pushCallbackMethod(*valptr, [=](double v) {
               hostData->transport->setGainMeter(1,v);
            });
            */
        } else if(ch_name == std::string("level_2")) {
            cbk_vals.level_2 = *valptr;
            /*
            Miup::pushCallbackMethod(*valptr, [=](double v) {
               hostData->transport->setGainMeter(2,v);
            });
            */
        }

    }

    constexpr const char * const ChannelNames[] = {
        "level_1",
        "level_2",
        "T_sync",
    };

    enum ChannelList {
        LevelMeter1 = 0,
        LevelMeter2 = 1,
        TrackerSync = 2,
    };



    static void registerRealtimeCallbacks(PerformanceHostData *hostData, json /*&audio_parameters*/)
    {


        std::function<void()> callbackMethod = [=]()
        {
            hostData->transport->setGainMeter(1, cbk_vals.level_1);
            hostData->transport->setGainMeter(2, cbk_vals.level_2);
            hostData->trackset->setLineSelection(cbk_vals.t_sync);

        };

        CallbackData cbk("TrackerPerf", callbackMethod, 1);
        Miup::registerCallback(cbk);
        hostData->oscilloscope->getPlot()->initDrawing(cs);
        std::function<void()> scope_cbk_method = [=]()
        {
          if(hostData->oscilloscope->isVisible() && cs->IsPlaying() )
          {
                hostData->oscilloscope->getPlot()->drawWith(cs);
          }
        };

        int nsmps = hostData->oscilloscope->getNsmps();
        JoTracker::GlobalData::init(cs, nsmps);

        cs->ksmps_sig.connect([=](){
           JoTracker::GlobalData::updateSamples(cs, nsmps);
        });

        CallbackData scope_cbk("ScopeCallback", scope_cbk_method, 1);
        Miup::registerCallback(scope_cbk);
    }


    static void unregisterRealtimeCallbacks()
    {
        Miup::unregisterCallback("TrackerPerf");
        Miup::unregisterCallback("ScopeCallback");
    }


    static void gatherFiles(std::vector<std::string> path_list, std::string folder_path) {
            for(auto & it : path_list) {
                std::string filename = MiupUtil::filesystem::getFilenameFromPath(it);
                std::string file_path = folder_path + "/" + filename;
                MiupUtil::filesystem::copyFile(it, file_path);
            }
        }

        static void ExportAsCsd(JTrackSet *trackset, GenEditorTab *gentab, CsoundEditorBox *editor)
        {

            ExportParametersDialog paramDialog;
            paramDialog.setGatherButtonVisible(true);

            if(MiupUtil::filesystem::isValidNonEmptyFile(std::string(getAudioParametersPath()))) {
               // std::cout << "param exists "<< std::endl;
                json data;
                std::ifstream ifs(getAudioParametersPath());
                ifs >> data;
                ifs.close();
                paramDialog.loadFromJson(data);
            }

            paramDialog.buttonClicked.connect([&](bool is_ok) {
                paramDialog.hide();
                if(is_ok) {

                        CsdGlobalParameters param(paramDialog.getSampleRate(),  paramDialog.getKsmps(),paramDialog.getNChnls(),  paramDialog.getDbfs() ,false);
                           FileDialog filedialog(FileDialog::SAVE, browse_path.c_str());
                           filedialog.emitPath.connect([&](std::string pre_path) {


                               std::string path = MiupUtil::filesystem::setPathExtension(pre_path, ".csd");

                               bool gather = paramDialog.gatherSoundfiles();
                               std::cout << "Gather soundfiles and samples : " << ((gather) ? "true" : "false") << std::endl;


                                    std::pair< std::string, std::unordered_map < std::string, int> > score_pair =
                                            trackset->getFullScore(gentab);
                                    std::string gens = (gather) ? gentab->getGatheredGen() : gentab->getAllGen();

                                    std::string scaled_gens = gentab->scaleListFormat();
                                    std::pair<std::string,std::string> orchestra = editor->getOrc(score_pair.second);

                                   GlobalCsoundCode macros = editor->getMacroString();

                                   CsdFormatter csd(param);
                                   csd.pushOption("-o dac"); // ? Add options in exportparameterdialog
                                   csd.pushOption("--input=adc");
                                   csd.setPreOrcGlobal(macros.pre_orc);
                                   csd.setPostOrcGlobal(macros.post_orc);
                                   csd.setPreScoreGlobal(macros.pre_score);
                                   csd.setOrchestra(orchestra.first);
                                   csd.pushScore(gens);
                                   csd.pushScore(scaled_gens);
                                   csd.pushScore(orchestra.second); // instrument decription
                                   csd.pushScore(score_pair.first);

                                   std::string full_csd = csd.getCsd();

                                   std::ofstream ofs(path);
                                   ofs << full_csd;
                                   ofs.close();



                                  if(gather) {
                                      std::cout << "GATHER FILES " << std::endl;
                                     std::vector<std::string> path_list = gentab->getSoundfilePaths();
                                     if(path_list.size() > 0) {
                                         std::thread t(gatherFiles, path_list, MiupUtil::filesystem::getFolderFromPath(path));
                                         t.detach();
                                     }
                                  }


                           });

                           filedialog.popup();

                }
            });
            paramDialog.popup(IUP_CENTER, IUP_CENTER);
        }



        //static void play_performance(JTrackSet *trackset, GenEditorTab *genbox, CsoundEditorBox *editor, CsoundThreaded *csound, json &audio_parameters, bool from_start = true) {
        static void play_performance(PerformanceHostData *hostData, json &audio_parameters, bool from_start = true) {
            if(cs != nullptr && cs->IsPlaying()) {
                cs->stopAndResetSlot();
            }


            delete cs;
            channels.clear();

            CsdGlobalParameters params;
            if(audio_parameters.find("sample_rate") != audio_parameters.end()) {
                params.sample_rate = audio_parameters["sample_rate"].get<int>();
                params.dbfs = audio_parameters["dbfs"].get<int>();
                params.ksmps = audio_parameters["ksmps"].get<int>();
                params.nchnls = audio_parameters["nchnls"].get<int>();
                for(int i = 1; i <= params.nchnls; i++) {
                    std::string ch = "level_" + std::to_string(i);
                    channels.push_back(ch);
                }
            }

            std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, false, from_start);
            //std::cout << "SCORE \n" << score_pair.first << std::endl;

            std::string gens = hostData->genbox->getAllGen();
            std::string scaled_gens = hostData->genbox->scaleListFormat();
            std::pair<std::string, std::string> orchestra = hostData->editor->getOrc(score_pair.second);

            GlobalCsoundCode macros = hostData->editor->getMacroString();


            CsdFormatter csd(params);

            csd.pushOption("-o dac"); //? Add options in exportparameterdialog like audio device selector
            csd.pushOption("--input=adc");
            csd.pushOption("--realtime"); //
            //csd.pushOption("--nodisplays");
            //csd.pushOption("-m0");

            if(audio_parameters.find("additional_options") != audio_parameters.end()) {
                std::string additional = audio_parameters["additional_options"].get<std::string>();
                csd.pushOption(additional);
            }

            csd.setPreOrcGlobal(macros.pre_orc);
            csd.setPostOrcGlobal(macros.post_orc);
            csd.setPreScoreGlobal(macros.pre_score);

            csd.setOrchestra(orchestra.first);
            csd.pushScore(gens);
            csd.pushScore(scaled_gens);
            csd.pushScore(orchestra.second);
            csd.pushScore(score_pair.first);

            cs = new CsoundThreaded();

            std::cout << "CSD >> \n" << csd.getCsd() << std::endl;


            //csd.pushOption("-v");

            int res = cs->CompileCsdText(csd.getCsd().c_str());
            //int res = cs->Compile(csd.getCsd().c_str());
            //int res = cs->Compile("/home/johann/Documents/jo_test.csd");


            cs->SetHostData((void *)hostData);

            channelCallback_t f_ptr = (channelCallback_t) &outvalueCallback;
            cs->SetOutputChannelCallback(f_ptr);


            cs->setRefreshRate(64);


            if(res == 0) res = cs->Start();
            else return;


            cs->stop_sig.connect([&](){
                Miup::pushCallbackMethod(0.0, [&](double /*v*/) {
                        //reset transport
                });
            });



            if(res == 0) cs->Perform();
            else return;



            is_playing = true;


            cs->SetControlChannel("Master",hostData->transport->getMasterValue());
        }






        //static void play_performance(JTrackSet *trackset, GenEditorTab *genbox, CsoundEditorBox *editor, CsoundThreaded *csound, json &audio_parameters, bool from_start = true) {
        static void record_performance(PerformanceHostData *hostData, json &audio_parameters) {
            if(cs != nullptr && cs->IsPlaying()) {
                cs->stopAndResetSlot();
            }

            delete cs;
            channels.clear();


            FileDialog fdlg("SAVE");
            fdlg.emitPath.connect([&](std::string path) {
                    CsdGlobalParameters params;
                    if(audio_parameters.find("sample_rate") != audio_parameters.end()) {
                        params.sample_rate = audio_parameters["sample_rate"].get<int>();
                        params.dbfs = audio_parameters["dbfs"].get<int>();
                        params.ksmps = audio_parameters["ksmps"].get<int>();
                        params.nchnls = audio_parameters["nchnls"].get<int>();
                        for(int i = 1; i <= params.nchnls; i++) {
                            std::string ch = "level_" + std::to_string(i);
                            channels.push_back(ch);
                        }
                    }



                    std::string final_path = MiupUtil::filesystem::setPathExtension(path, ".wav");

                    std::cout << "record path : " << final_path << std::endl;
                    params.setRecord(final_path);

                    std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, false, true);

                    std::string gens = hostData->genbox->getAllGen();
                    std::string scaled_gens = hostData->genbox->scaleListFormat();
                    std::pair<std::string,std::string> orchestra = hostData->editor->getOrc(score_pair.second);
                    GlobalCsoundCode macros = hostData->editor->getMacroString();

                    CsdFormatter csd(params);

                    csd.pushOption("-o dac"); //? Add options in exportparameterdialog like audio device selector
                    csd.pushOption("--input=adc");
                    csd.pushOption("--realtime"); //
                    csd.pushOption("--nodisplays");
                    csd.pushOption("-m0");


                    if(audio_parameters.find("additional_options") != audio_parameters.end()) {
                        std::string additional = audio_parameters["additional_options"].get<std::string>();
                        csd.pushOption(additional);
                    }

                    csd.setPreOrcGlobal(macros.pre_orc);
                    csd.setPostOrcGlobal(macros.post_orc);
                    csd.setPreScoreGlobal(macros.pre_score);
                    csd.setOrchestra(orchestra.first);
                    csd.pushScore(gens);
                    csd.pushScore(scaled_gens);
                    csd.pushScore(orchestra.second);
                    csd.pushScore(score_pair.first);

                    cs = new CsoundThreaded();

                    std::cout << "CSD >> \n" << csd.getCsd() << std::endl;


                    //csd.pushOption("-v");

                    int res = cs->CompileCsdText(csd.getCsd().c_str());
                    //int res = cs->Compile(csd.getCsd().c_str());
                    //int res = cs->Compile("/home/johann/Documents/jo_test.csd");

                    cs->SetHostData((void *)hostData);

                     channelCallback_t f_ptr = (channelCallback_t) &outvalueCallback;
                    cs->SetOutputChannelCallback(f_ptr);
                    cs->setRefreshRate(64);

                    if(res == 0) res = cs->Start();
                    else return;

                    cs->stop_sig.connect([&](){
                        Miup::pushCallbackMethod(0.0, [&](double /*v*/) {
                                //reset transport
                        });
                    });

                    if(res == 0) cs->Perform();
                    else return;
                    is_playing = true;

                    cs->SetControlChannel("Master",hostData->transport->getMasterValue());
            });

           fdlg.popup();
        }

        static void play_ordered_performance(PerformanceHostData *hostData, json &audio_parameters, json /*data*/) {
            if(cs != nullptr && cs->IsPlaying()) {
                cs->stopAndResetSlot();
            }

            delete cs;
            channels.clear();

            CsdGlobalParameters params;
            if(audio_parameters.find("sample_rate") != audio_parameters.end()) {
                params.sample_rate = audio_parameters["sample_rate"].get<int>();
                params.dbfs = audio_parameters["dbfs"].get<int>();
                params.ksmps = audio_parameters["ksmps"].get<int>();
                params.nchnls = audio_parameters["nchnls"].get<int>();
                for(int i = 1; i <= params.nchnls; i++) {
                    std::string ch = "level_" + std::to_string(i);
                    channels.push_back(ch);
                }
            }

            std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, true, true);

          //  std::cout << "SCORE \n" << score_pair.first << std::endl;

            std::string gens = hostData->genbox->getAllGen();
            std::string scaled_gens = hostData->genbox->scaleListFormat();
            std::pair<std::string, std::string> orchestra = hostData->editor->getOrc(score_pair.second);
            GlobalCsoundCode macros = hostData->editor->getMacroString();

            CsdFormatter csd(params);

            csd.pushOption("-o dac"); //? Add options in exportparameterdialog like audio device selector
            csd.pushOption("--input=adc");
            csd.pushOption("--realtime"); //
            csd.pushOption("--nodisplays");
            csd.pushOption("-m0");


            if(audio_parameters.find("additional_options") != audio_parameters.end()) {
                std::string additional = audio_parameters["additional_options"].get<std::string>();
                csd.pushOption(additional);
            }

            csd.setPreOrcGlobal(macros.pre_orc);
            csd.setPostOrcGlobal(macros.post_orc);
            csd.setPreScoreGlobal(macros.pre_score);
            csd.setOrchestra(orchestra.first);
            csd.pushScore(gens);
            csd.pushScore(scaled_gens);
            csd.pushScore(orchestra.second);
            csd.pushScore(score_pair.first);

            cs = new CsoundThreaded();

            std::string csd_text  = csd.getCsd();
            std::cout << "CSD >> \n" << csd_text << std::endl;

            int res = cs->CompileCsdText(csd_text.c_str());
            //int res = cs->CompileCsdText(csd.getCsd().c_str());

            cs->SetHostData((void *)hostData);

             channelCallback_t f_ptr = (channelCallback_t) &outvalueCallback;
            cs->SetOutputChannelCallback( f_ptr);
            cs->setRefreshRate(64);

            if(res == 0) res = cs->Start();


            cs->stop_sig.connect([&](){
                Miup::pushCallbackMethod(0.0, [&](double /*v*/) {
                        //reset transport
                });
            });

            if(res == 0) cs->Perform();
            else return;
            is_playing = true;

            cs->SetControlChannel("Master",hostData->transport->getMasterValue());
        }


        static void render(PerformanceHostData *hostData, json &audio_parameters) {

            json p;
            ExportParametersDialog param(audio_parameters["sample_rate"].get<int>(), audio_parameters["ksmps"].get<int>(),
                    audio_parameters["nchnls"].get<int>(), audio_parameters["dbfs"].get<int>());

            if(audio_parameters.find("additional_options") != audio_parameters.end() && !audio_parameters["additional_options"].is_null())
                param.setAdditionalOptions(audio_parameters["additional_options"].get<std::string>());
            param.setGatherButtonVisible(false);

            bool val = false;
            param.buttonClicked.connect([&](bool validate){
                param.hide();
                val = validate;
                if(validate) {
                   p["ksmps"] = param.getKsmps();
                   p["sample_rate"] = param.getSampleRate();
                   p["nchnls"]=param.getNChnls();
                   p["dbfs"]= param.getDbfs();
                   p["additional_options"]=param.getAddtionalOptions();

                    FileDialog dlg("SAVE");
                    dlg.emitPath.connect([&](std::string path) {

                        std::string filepath_opt = "--output=";

                        std::string formatted_path(path);

                        if(path.find(".") != path.npos) {
                            std::size_t pos = path.find_last_of(".");
                             formatted_path = path.substr(0,pos);
                        }
                        formatted_path += ".wav";

                        filepath_opt += formatted_path;

                        std::string filename("");
                         if(path.find("/") != path.npos) {
                               std::size_t pos = path.find_last_of("/");
                             filename = path.substr(pos);
                         } else {
                           filename = path;
                         }

                            if(cs != nullptr && cs->IsPlaying()) {
                                cs->stopAndResetSlot();
                            }

                            delete cs;
                            channels.clear();

                            CsdGlobalParameters params;
                            if(audio_parameters.find("sample_rate") != audio_parameters.end()) {
                                params.sample_rate = p["sample_rate"].get<int>();
                                params.dbfs = p["dbfs"].get<int>();
                                params.ksmps = p["ksmps"].get<int>();
                                params.nchnls = p["nchnls"].get<int>();
                                for(int i = 1; i <= params.nchnls; i++) {
                                    std::string ch = "level_" + std::to_string(i);
                                    channels.push_back(ch);
                                }
                            }

                            std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, false, true);
                            //std::cout << "SCORE \n" << score_pair.first << std::endl;

                            std::string gens = hostData->genbox->getAllGen();
                            std::string scaled_gens = hostData->genbox->scaleListFormat();
                            std::pair<std::string, std::string> orchestra = hostData->editor->getOrc(score_pair.second);
                            GlobalCsoundCode macros = hostData->editor->getMacroString();

                            CsdFormatter csd(params);

                            csd.pushOption(filepath_opt);
                            csd.pushOption("-v");
                            if(p.find("additional_options") != p.end()) {
                                csd.pushOption(p["additional_options"].get<std::string>());
                            }

                            csd.setPreOrcGlobal(macros.pre_orc);
                            csd.setPostOrcGlobal(macros.post_orc);
                            csd.setPreScoreGlobal(macros.pre_score);
                            csd.setOrchestra(orchestra.first);
                            csd.pushScore(gens);
                            csd.pushScore(scaled_gens);
                            csd.pushScore(score_pair.first);

                            cs = new CsoundThreaded();

                            //std::cout << "CSD >> \n" << csd.getCsd() << std::endl;


                            int res = cs->CompileCsdText(csd.getCsd().c_str());

                            cs->SetHostData((void *)hostData);

                            cs->SetControlChannel("Master",1.0);
                           //  channelCallback_t f_ptr = (channelCallback_t) &outvalueCallback;
                           // cs->SetOutputChannelCallback(f_ptr);
                           // cs->setRefreshRate(64);

                            if(res == 0) res = cs->Start();
                            else return;

                            cs->stop_sig.connect([&](){
                                Miup::pushCallbackMethod(0,[&](double /*val*/) {
                                    IupAlarm("Render done","File is successfully rendered","Ok",NULL,NULL);

                                    cs->stop_sig.disconnect_all();
                                });
                            });
                       // ProgressDialog progress(100);


                            if(res == 0) cs->Perform();
                            else return;
                            is_playing = true;

                    });

                    dlg.popup();



                }
            });

            param.popup(IUP_CENTER, IUP_CENTER);



        }



        static void get_data_for_one_seq(json &js_, PerformanceHostData *hostData)
        {

            int sq = hostData->trackset->getCurrentSeq();
            std::string sq_str = std::to_string(sq);
            json *data = hostData->trackset->getData();
            if(data->at("score").find(sq_str) == data->at("score").end())
            {
                int res = IupAlarm("Error", "No such sequence", "Ok", NULL, NULL);
                return;
            }

            js_["score"]["1"] = data->at("score")[sq_str];
            js_["tempo"]["1"] = data->at("tempo")[sq_str];
            js_["seqnbr"] = 1;
            js_["tracknbr"] = data->at("tracknbr");
            js_["colnbr"]["1"] = data->at("colnbr")[sq_str];
            js_["lignbr"]["1"] = data->at("lignbr")[sq_str];

        }


        static void render_seq(PerformanceHostData *hostData, json &audio_parameters)
        {
            json p;
            ExportParametersDialog param(audio_parameters["sample_rate"].get<int>(), audio_parameters["ksmps"].get<int>(),
                    audio_parameters["nchnls"].get<int>(), audio_parameters["dbfs"].get<int>());

            if(audio_parameters.find("additional_options") != audio_parameters.end() && !audio_parameters["additional_options"].is_null())
                param.setAdditionalOptions(audio_parameters["additional_options"].get<std::string>());
            param.setGatherButtonVisible(false);

            bool val = false;
            param.buttonClicked.connect([&](bool validate){
                param.hide();
                val = validate;
                if(validate) {
                   p["ksmps"] = param.getKsmps();
                   p["sample_rate"] = param.getSampleRate();
                   p["nchnls"]=param.getNChnls();
                   p["dbfs"]= param.getDbfs();
                   p["additional_options"]=param.getAddtionalOptions();

                    FileDialog dlg("SAVE");
                    dlg.emitPath.connect([&](std::string path) {

                        std::string filepath_opt = "--output=";

                        std::string formatted_path(path);

                        if(path.find(".") != path.npos) {
                            std::size_t pos = path.find_last_of(".");
                             formatted_path = path.substr(0,pos);
                        }
                        formatted_path += ".wav";

                        filepath_opt += formatted_path;

                        std::string filename("");
                         if(path.find("/") != path.npos) {
                               std::size_t pos = path.find_last_of("/");
                             filename = path.substr(pos);
                         } else {
                           filename = path;
                         }

                            if(cs != nullptr && cs->IsPlaying()) {
                                cs->stopAndResetSlot();
                            }

                            delete cs;
                            channels.clear();

                            CsdGlobalParameters params;
                            if(audio_parameters.find("sample_rate") != audio_parameters.end()) {
                                params.sample_rate = p["sample_rate"].get<int>();
                                params.dbfs = p["dbfs"].get<int>();
                                params.ksmps = p["ksmps"].get<int>();
                                params.nchnls = p["nchnls"].get<int>();
                                for(int i = 1; i <= params.nchnls; i++) {
                                    std::string ch = "level_" + std::to_string(i);
                                    channels.push_back(ch);
                                }
                            }



                            // Swap jsdata with the seq only, then put it back

                            json seq_data;
                            get_data_for_one_seq(seq_data, hostData);

                            json backup_data = *hostData->trackset->getData();
                            hostData->trackset->setData(&seq_data);

                            std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, false, true);

                            hostData->trackset->setData(&backup_data);
                            //std::cout << "SCORE \n" << score_pair.first << std::endl;

                            std::string gens = hostData->genbox->getAllGen();
                            std::string scaled_gens = hostData->genbox->scaleListFormat();
                            std::pair<std::string, std::string> orchestra = hostData->editor->getOrc(score_pair.second);
                            GlobalCsoundCode macros = hostData->editor->getMacroString();

                            CsdFormatter csd(params);

                            csd.pushOption(filepath_opt);
                            //csd.pushOption("-v");
                            if(p.find("additional_options") != p.end()) {
                                csd.pushOption(p["additional_options"].get<std::string>());
                            }

                            csd.setPreOrcGlobal(macros.pre_orc);
                            csd.setPostOrcGlobal(macros.post_orc);
                            csd.setPreScoreGlobal(macros.pre_score);
                            csd.setOrchestra(orchestra.first);
                            csd.pushScore(gens);
                            csd.pushScore(scaled_gens);
                            csd.pushScore(score_pair.first);

                            cs = new CsoundThreaded();

                            //std::cout << "CSD >> \n" << csd.getCsd() << std::endl;


                            std::cout << "CSD Text for seq rendering : " << std::endl;
                            std::cout << csd.getCsd() << std::endl;

                            int res = cs->CompileCsdText(csd.getCsd().c_str());

                            cs->SetHostData((void *)hostData);

                            cs->SetControlChannel("Master",1.0);
                           //  channelCallback_t f_ptr = (channelCallback_t) &outvalueCallback;
                           // cs->SetOutputChannelCallback(f_ptr);
                           // cs->setRefreshRate(64);

                            if(res == 0) res = cs->Start();
                            else return;

                            cs->stop_sig.connect([&](){
                                Miup::pushCallbackMethod(0,[&](double /*val*/) {
                                    IupAlarm("Render done","File is successfully rendered","Ok",NULL,NULL);

                                    cs->stop_sig.disconnect_all();
                                });
                            });
                       // ProgressDialog progress(100);


                            if(res == 0) cs->Perform();
                            else return;
                            is_playing = true;

                    });

                    dlg.popup();



                }
            });

            param.popup(IUP_CENTER, IUP_CENTER);

        }



       std::atomic<int> thread_count;
       std::atomic<int> done_count;
       std::atomic<int> render_index;
       static ProgressionDialog *progression = nullptr;

        static void instance_thread_routine(std::string csd)
        {
            std::cout << "CSD \n" << csd << std::endl;

            Csound cs_;// = new Csound;
            int res = cs_.CompileCsdText(csd.c_str());
            cs_.SetControlChannel("Master",1.0);

            if(res == 0) res = cs_.Start();
            else {
                thread_count--;
                //done_count++;
                return;
            }

            if(res==0) {
                while(cs_.PerformKsmps() == 0)
                {
                }
            } else {
                thread_count--;
                //done_count++;
                return;
            }

            thread_count--;
            done_count++;
        }


        static void render_instance_loop(std::pair<json , std::unordered_map<std::string,int>>score_pair,
                                         std::string gens, std::string scaled_gens, std::string orchestra,
                                         std::string path, CsdGlobalParameters params_, json p,
                                         GlobalCsoundCode macros)
        {

           std::cout << "PARAMS : \n" << params_.dump() << std::endl;

                            json::iterator it = score_pair.first.begin();

                            thread_count = 0;
                            done_count = 0;
                            render_index = 0;

                            int instance_number = score_pair.first.size();
                            std::cout << "COUNT OF INSTANCES : " << instance_number << std::endl;

                            while(render_index < instance_number) {
                                if(thread_count < 3 && render_index < instance_number) { // create thread

                                    std::cout << "CREATED THREAD " << std::endl;
                                   CsdFormatter csd(params_);



                                    std::string filepath_opt = "--output=" + path + "_" + it.key() + ".wav";
                                    csd.pushOption(filepath_opt);
                                    if(p.find("additional_options") != p.end()) {
                                        csd.pushOption(p["additional_options"].get<std::string>());
                                    }
                                    csd.pushOption("-m0");
                                    if(p["additional_options"].get<std::string>().find("--defer-gen1") == std::string::npos)
                                        csd.pushOption("--defer-gen1");

                                    csd.setPreOrcGlobal(macros.pre_orc);
                                    csd.setPostOrcGlobal(macros.post_orc);
                                    csd.setPreScoreGlobal(macros.pre_score);
                                    csd.setOrchestra(orchestra);
                                    csd.pushScore(gens);
                                    csd.pushScore(scaled_gens);
                                    csd.pushScore(it.value().get<std::string>());

                                    std::thread(instance_thread_routine, csd.getCsd()).detach();

                                    it++;

                                    render_index++;
                                    thread_count++;

                                    if(it == score_pair.first.end()) {
                                        thread_count++;
                                        break;
                                    }
                                } else {
                                    std::this_thread::sleep_for(std::chrono::seconds(3));
                                }
                            }
        }

        // Issues
        // - Thread problem - crashing
        // - instances - when no instance is defined, ist renders as the same : put instance numbers ?  or what ?


        static void render_instances(PerformanceHostData *hostData, json audio_parameters)
        {

            GlobalCsoundCode macros = hostData->editor->getMacroString();
            json p;
            ExportParametersDialog param(audio_parameters["sample_rate"].get<int>(), audio_parameters["ksmps"].get<int>(),
                    audio_parameters["nchnls"].get<int>(), audio_parameters["dbfs"].get<int>());

            if(audio_parameters.find("additional_options") != audio_parameters.end() && !audio_parameters["additional_options"].is_null())
                param.setAdditionalOptions(audio_parameters["additional_options"].get<std::string>());
            param.setGatherButtonVisible(false);

            bool val = false;
            param.buttonClicked.connect([&](bool validate){
                param.hide();
                val = validate;
                if(validate) {
                   p["ksmps"] = param.getKsmps();
                   p["sample_rate"] = param.getSampleRate();
                   p["nchnls"]=param.getNChnls();
                   p["dbfs"]= param.getDbfs();
                   p["additional_options"]=param.getAddtionalOptions();

                    FileDialog dlg("SAVE");
                    dlg.emitPath.connect([&](std::string path) {

                        dlg.detach();
                            if(cs != nullptr && cs->IsPlaying()) {
                                cs->stopAndResetSlot();
                            }

                            delete cs;
                            channels.clear();

                            CsdGlobalParameters params_;
                            if(audio_parameters.find("sample_rate") != audio_parameters.end()) {
                                params_.sample_rate = p["sample_rate"].get<int>();
                                params_.dbfs = p["dbfs"].get<int>();
                                params_.ksmps = p["ksmps"].get<int>();
                                params_.nchnls = p["nchnls"].get<int>();
                                for(int i = 1; i <= params_.nchnls; i++) {
                                    std::string ch = "level_" + std::to_string(i);
                                    channels.push_back(ch);
                                }
                            }
                            //std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, false, true);
                            std::pair< json, std::unordered_map<std::string,int> >score_pair = hostData->trackset->getScoreInstances(hostData->genbox);

                            std::string gens = hostData->genbox->getAllGen();
                            std::string scaled_gens = hostData->genbox->scaleListFormat();
                            std::pair<std::string, std::string> orchestra = hostData->editor->getOrc(score_pair.second);



                            delete progression;
                            progression = new ProgressionDialog("Csound instances rendering", score_pair.first.size());
                            progression->show();

                            std::thread(render_instance_loop, score_pair, gens,
                                        scaled_gens, orchestra.first, path, params_, p, macros).detach();

                    });
                    dlg.popup();

                }
            });

            param.popup(IUP_CENTER, IUP_CENTER);

        }


        static void render_seq_instances(PerformanceHostData *hostData, json audio_parameters)
        {
            std::cout << "render seq instances " << std::endl;
            GlobalCsoundCode macros = hostData->editor->getMacroString();
            json p;
            ExportParametersDialog param(audio_parameters["sample_rate"].get<int>(), audio_parameters["ksmps"].get<int>(),
                    audio_parameters["nchnls"].get<int>(), audio_parameters["dbfs"].get<int>());

            if(audio_parameters.find("additional_options") != audio_parameters.end() && !audio_parameters["additional_options"].is_null())
                param.setAdditionalOptions(audio_parameters["additional_options"].get<std::string>());
            param.setGatherButtonVisible(false);

            bool val = false;
            std::cout << "params " << std::endl;
            param.buttonClicked.connect([&](bool validate){
                param.hide();
                val = validate;
                if(validate) {
                   p["ksmps"] = param.getKsmps();
                   p["sample_rate"] = param.getSampleRate();
                   p["nchnls"]=param.getNChnls();
                   p["dbfs"]= param.getDbfs();
                   p["additional_options"]=param.getAddtionalOptions();

                    FileDialog dlg("SAVE");
                    dlg.emitPath.connect([&](std::string path) {

                        dlg.detach();
                            if(cs != nullptr && cs->IsPlaying()) {
                                cs->stopAndResetSlot();
                            }

                            delete cs;
                            channels.clear();

                            CsdGlobalParameters params_;
                            if(audio_parameters.find("sample_rate") != audio_parameters.end()) {
                                params_.sample_rate = p["sample_rate"].get<int>();
                                params_.dbfs = p["dbfs"].get<int>();
                                params_.ksmps = p["ksmps"].get<int>();
                                params_.nchnls = p["nchnls"].get<int>();
                                for(int i = 1; i <= params_.nchnls; i++) {
                                    std::string ch = "level_" + std::to_string(i);
                                    channels.push_back(ch);
                                }
                            }


                            json seq_data;
                            get_data_for_one_seq(seq_data, hostData);

                            json backup_data = *hostData->trackset->getData();
                            hostData->trackset->setData(&seq_data);

                            //std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, false, true);

                            //std::pair< std::string, std::unordered_map < std::string, int> > score_pair = hostData->trackset->getFullScore(hostData->genbox, false, true);
                            std::pair< json, std::unordered_map<std::string,int> >score_pair = hostData->trackset->getScoreInstances(hostData->genbox);
                            hostData->trackset->setData(&backup_data);

                            std::string gens = hostData->genbox->getAllGen();
                            std::string scaled_gens = hostData->genbox->scaleListFormat();
                            std::pair<std::string, std::string> orchestra = hostData->editor->getOrc(score_pair.second);



                            delete progression;
                            progression = new ProgressionDialog("Csound instances rendering", score_pair.first.size());
                            progression->show();

                            std::thread(render_instance_loop, score_pair, gens,
                                        scaled_gens, orchestra.first, path, params_, p, macros).detach();

                    });
                    dlg.popup();

                }
            });

            param.popup(IUP_CENTER, IUP_CENTER);

        }


        static bool clearProject(JTrackSet *trackset, GenEditorTab *genbox, CsoundEditorBox *editor)
        {
               int res = IupAlarm("New project","This will delete all non-saved data, are you sure ? ", "Yes","No",NULL);
               if(res == 1) {
                   macro_dlg->clear();
                   json j  = macro_dlg->getData();
                   editor->setMacroData(&j);
                   editor->saveMacroData();

                   trackset->clearAll();
                   genbox->clearAllData();
                   genbox->saveData();
                   editor->clearAll();
                   editor->updateList();

                   // clear samples repository
                   if(MiupUtil::filesystem::isValidFilePath(getSamplePath())) {
                       MiupUtil::filesystem::removeRecursive(getSamplePath());
                   }

                   return true;
               }
               return false;
        }


        static void redrawMainDialog()
        {
            main_dialog_ptr->redraw();
        }




        static void saveProject(JTrackSet *trackset, GenEditorTab *genbox, CsoundEditorBox *editor, json &audio_parameters, bool gather = false)
        {
               FileDialog dlg(FileDialog::SAVE, browse_path.c_str());
               dlg.emitPath.connect([&](std::string pre_path){

                   std::string path = MiupUtil::filesystem::setPathExtension(pre_path, ".jo");

                  std::cout << "GATHER ? " << gather << std::endl;

                  json full_data;
                  full_data["score_data"]=*trackset->getData();
                  full_data["orc_data"]=*editor->getData();

                  if(gather) full_data["gen_data"] = genbox->getGatheredData();
                  else full_data["gen_data"] = *genbox->getData();

                  full_data["script_data"]= *trackset->getScriptData();
                  full_data["macro_data"] = *editor->getMacroData();
                  full_data["params"]=audio_parameters;

                  std::ofstream ofs(path);
                  ofs << full_data.dump(4);
                  ofs.close();

                  trackset->saveAt(getScoreDataPath());
                  editor->saveJson(getOrcDataPath());
                  genbox->saveData();

                  std::ofstream of(getAudioParametersPath());
                  of << audio_parameters.dump(4);
                  of.close();

                  std::string dir = MiupUtil::filesystem::getFolderFromPath(path);

                  if(gather) {
                      std::cout << "GATHER FILES " << std::endl;
                     std::vector<std::string> path_list = genbox->getSoundfilePaths();
                     if(path_list.size() > 0) {
                         std::thread t(gatherFiles, path_list, dir);
                         t.detach();
                     }
                  }

                  browse_path = dir;

               });

               dlg.popup();
        }




        static void load_project(JTrackSet *trackset, GenEditorTab *genbox, CsoundEditorBox *editor, json *audio_parameters) {
            FileDialog dlg(FileDialog::OPEN, browse_path.c_str());
            dlg.emitPath.connect([&](std::string path) {
               if(!MiupUtil::filesystem::isValidNonEmptyFile(path) || !MiupUtil::filesystem::verifyExtension(path,".jo")) {
                   IupAlarm("Invalid file", "This file is not a valid jo_tracker project file. Make sure to select a \".jo\" file coming from a recent version of jo_tracker. ",
                            "Ok",NULL,NULL);
                   return;
                  }

               if(!clearProject(trackset, genbox,editor)) {
                   std::cout << "Could not clear project" << std::endl;
                   return;
               }

               json full_data;
               std::ifstream ifs(path);
               ifs >> full_data;
               ifs.close();

               if(full_data.find("score_data") != full_data.end()) {
                   trackset->setData(&full_data["score_data"]);
               }

               if(full_data.find("orc_data") != full_data.end()) {
                   editor->setData(&full_data["orc_data"]);
               }

               if(full_data.find("gen_data") != full_data.end()) {
                   genbox->setData(&full_data["gen_data"]);
                   genbox->resolvePath(MiupUtil::filesystem::getFolderFromPath(path));
               }

               if(full_data.find("macro_data") != full_data.end()) {
                   editor->setMacroData(&full_data["macro_data"]);
               }

               if(full_data.find("script_data") != full_data.end()) {
                   trackset->setScriptData(&full_data["script_data"]);
               }

               if(full_data.find("params") != full_data.end()) {
                   *audio_parameters = full_data["params"];
               }


               trackset->updateScripterList();
               genbox->updateContent(1);
               editor->setMode(1);
               editor->updateList();


               trackset->saveAt();
               genbox->saveData();
               editor->saveJson();

               browse_path = MiupUtil::filesystem::getFolderFromPath(path);

               trackset->recallSeq(1);
               main_dialog_ptr->update();

            });

            dlg.popup();
        }


        static void load_orchestra(CsoundEditorBox *editor) {
            JImportOrchestraDialog dlg;
            dlg.buttonClickedSig.connect([&](bool res) {
                dlg.hide();
                if (res) {
                  editor->addData(dlg.getImportedData());
                }
            });

            dlg.popup();
        }


        static void export_gens(GenEditorTab *tab) {
            std::string gens = tab->getAllGen();
            FileDialog dlg(FileDialog::SAVE);
            dlg.emitPath.connect([&](std::string path) {
                std::ofstream ofs(path);
                ofs << gens;
                ofs.close();
            });

            dlg.popup();
        }

        lsignal::signal<void(int,int)> global_key_sig;
        static int global_key(int key, int stat)
        {
            global_key_sig(key,stat);
            return IUP_DEFAULT;
        }








/*
 *
 * EDITING TOOLS
 *
*/



        // Will insert a line after the current selected line
        // will only affect current sequence
        void insert_line(JTrackSet *trackset, int nbr = 1) {
            json *data = trackset->getData();
            int sq = trackset->getCurrentSeq();
            std::string sq_str = std::to_string(sq);
            if(data->find("lignbr") != data->end() && data->at("lignbr").find(sq_str) != data->at("lignbr").end()) {
                int lignbr = data->at("lignbr")[sq_str].get<int>() + nbr;
                trackset->setNumlin(lignbr);

                int tracknbr = data->at("tracknbr").get<int>();
                // move data to next line after current position
                int curlin = trackset->getCurrentLine();
                std::cout << "current line : " << curlin << std::endl;

                for(int t = 0; t < tracknbr; t++) {
                    std::string tk_str = std::to_string(t);
                    for(int i = lignbr - nbr; i >= curlin; i--) {
                        std::string ln = std::to_string(i);
                        std::string nlin = std::to_string(i + nbr);
                        data->at("score")[sq_str][tk_str][nlin].clear();
                        if(data->at("score")[sq_str][tk_str].find(ln) != data->at("score")[sq_str][tk_str].end()) {
                            data->at("score")[sq_str][tk_str][nlin] = data->at("score")[sq_str][tk_str][ln];
                        }
                    }

                }
                for(int i = lignbr - nbr; i >= curlin; i--) {
                    std::string ln = std::to_string(i);
                    std::string nlin = std::to_string(i + nbr);

                    if(data->at("tempo").find(sq_str) != data->at("tempo").end()) {
                        if(data->at("tempo")[sq_str].find(nlin) != data->at("tempo")[sq_str].end()) {
                            data->at("tempo")[sq_str][nlin].clear();
                        }
                        if(data->at("tempo")[sq_str].find(ln) != data->at("tempo")[sq_str].end()) {

                            std::cout << "ok tempo : line : "  << i << std::endl;
                            data->at("tempo")[sq_str][nlin] = data->at("tempo")[sq_str][ln];
                        }
                    }
                }

                // clear the new lines
                for(int i = curlin; i < curlin + nbr; i++) {
                    std::string ln = std::to_string(i);
                    for(int t = 0; t < tracknbr; t++) {
                        std::string tk_str = std::to_string(t);
                        data->at("score")[sq_str][tk_str][ln].clear();
                    }
                    data->at("tempo")[sq_str][ln].clear();
                }
            }

            trackset->recallSeq(sq);
        }

        // Same for removing a line
        void remove_line(JTrackSet * trackset, int nbr = 1) {
            json *data = trackset->getData();
            int sq = trackset->getCurrentSeq();
            std::string sq_str = std::to_string(sq);
            if(data->find("lignbr") != data->end() && data->at("lignbr").find(sq_str) != data->at("lignbr").end()) {
                int lignbr = data->at("lignbr")[sq_str].get<int>();

                int curlin = trackset->getCurrentLine();
                std::cout << "current line : " << curlin << std::endl;
                int tracknbr = data->at("tracknbr").get<int>();
                for(int t = 0; t < tracknbr; t++) {
                    std::string tk_str = std::to_string(t);
                    for(int i = curlin; i < lignbr + nbr; i++) {
                        std::string ln = std::to_string(i);
                        std::string nlin = std::to_string(i + nbr);

                        data->at("score")[sq_str][tk_str][ln].clear();
                        if(data->at("score")[sq_str][tk_str].find(nlin) != data->at("score")[sq_str][tk_str].end())
                        {
                            data->at("score")[sq_str][tk_str][ln] = data->at("score")[sq_str][tk_str][nlin];
                        }
                    }
                }

                for(int i = curlin; i < lignbr + nbr; i++) {
                    std::string ln = std::to_string(i);
                    std::string nlin = std::to_string(i + nbr);
                    if(data->at("tempo").find(sq_str) != data->at("tempo").end()) {
                        if(data->at("tempo")[sq_str].find(ln) != data->at("tempo")[sq_str].end()) {
                            data->at("tempo")[sq_str][ln].clear();
                        }
                        if(data->at("tempo")[sq_str].find(nlin) != data->at("tempo")[sq_str].end()) {
                            data->at("tempo")[sq_str][ln] = data->at("tempo")[sq_str][nlin];
                        }
                    }
                }


                trackset->setNumlin(lignbr - nbr);
            }
            trackset->recallSeq(sq);
        }


        json bloc;
        json bigbloc;


        void copy_bloc(JTrackSet *trackset, int from, int to, bool cp_empty)
        {
            if(to <= from) return;

            bloc.clear();
            int sq =  trackset->getCurrentSeq();
            std::string sq_str = std::to_string(sq);
            json *data = trackset->getData();

            int curtrack = trackset->getCurrentTrack();
            std::string tstr = std::to_string(curtrack);
            bloc[tstr];
            for(int i = from; i <= to; i++) {
                std::string ln = std::to_string(i - from);
                std::string nln = std::to_string(i);
                if(cp_empty) {
                    bloc["data"][ln] = data->at("score")[sq_str][tstr][nln];
                } else {
                    if(data->at("score")[sq_str][tstr][nln].find("1") != data->at("score")[sq_str][tstr][nln].end() &&
                            !data->at("score")[sq_str][tstr][nln]["1"].is_null()) {
                        bloc["data"][ln] = data->at("score")[sq_str][tstr][nln];
                    }
                }
            }
            bloc["size"] = to - from;
            bloc["column"] = data->at("colnbr")[sq_str][tstr].get<int>();
        }

        void paste_bloc(JTrackSet *trackset) {
            if(bloc.find("data") == bloc.end()) return;

            int sq =  trackset->getCurrentSeq();
            std::string sq_str = std::to_string(sq);
            json *data = trackset->getData();

            int curtrack = trackset->getCurrentTrack();
            std::string tstr = std::to_string(curtrack);
            int curlin = trackset->getCurrentLine();

            int cur_cnbr = data->at("colnbr")[sq_str][tstr].get<int>();

            if(cur_cnbr < bloc["column"].get<int>()) trackset->setColnbr(curtrack, bloc["column"].get<int>());

            for(auto & it : bloc["data"].items()) {
                int l = getNumber<int>(it.key());
                l += curlin;
                std::string ln = std::to_string(l);

                data->at("score")[sq_str][tstr][ln] = it.value();
            }

            trackset->recallSeq(sq);

        }

        void copy_bloc_for_all_track(JTrackSet *trackset, int from, int to, bool cp_empty)
        {
            if(to <= from) return;

            bigbloc.clear();
            int sq =  trackset->getCurrentSeq();
            std::string sq_str = std::to_string(sq);
            json *data = trackset->getData();

            int tracknbr = data->at("tracknbr").get<int>();


            for(int t = 0; t < tracknbr; t++) {
                std::string tstr = std::to_string(t);
                bigbloc[tstr];
                for(int i = from; i <= to; i++) {
                    std::string ln = std::to_string(i - from);
                    std::string nln = std::to_string(i);
                    if(cp_empty) {
                        bigbloc["data"][tstr][ln] = data->at("score")[sq_str][tstr][nln];
                    } else {
                        if(data->at("score")[sq_str][tstr][nln].find("1") != data->at("score")[sq_str][tstr][nln].end() &&
                                !data->at("score")[sq_str][tstr][nln]["1"].is_null()) {
                            bigbloc["data"][tstr][ln] = data->at("score")[sq_str][tstr][nln];
                        }
                    }
                }
                bigbloc["column"][tstr] = data->at("colnbr")[sq_str][tstr].get<int>();
            }
            bigbloc["size"] = to - from;
        }

        void paste_bloc_for_all_track(JTrackSet *trackset)
        {
            if(bigbloc.find("data") == bigbloc.end()) return;

            int sq =  trackset->getCurrentSeq();
            std::string sq_str = std::to_string(sq);
            json *data = trackset->getData();

            int curlin = trackset->getCurrentLine();

            std::cout << "curlin : " << curlin << std::endl;

            for(auto & tit : bigbloc["data"].items()) {
                const int cur_cnbr = data->at("colnbr")[sq_str][tit.key()].get<int>();
                const int t = getNumber<int>(tit.key());

                if(cur_cnbr < bigbloc["column"][tit.key()].get<int>()) trackset->setColnbr(t, bigbloc["column"][tit.key()].get<int>());

                for(auto & lit : tit.value().items()) {
                    int l = getNumber<int>(lit.key());
                    l += curlin;
                    std::cout << "t / l " << t << " " << l << std::endl;

                    std::string ln = std::to_string(l);
                    data->at("score")[sq_str][tit.key()][ln] = lit.value();
                }
            }

            trackset->recallSeq(sq);

        }

}

#endif // JOTRACKERUTILITIES_H
