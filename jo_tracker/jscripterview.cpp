
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "jscripterview.h"


JScripterView::JScripterView(json * tracker_data_ptr) :
    Vbox(),
    editor(),
    console(),
    script_name(),
    evaluate_button("Evaluate", "IUP_ActionOk"),
    help_button("Help", "IUP_MessageHelp"),
    execute_button("Execute", "IUP_MediaPlay"),
    store_button("Store","IUP_FileSave"),
    remove_button("Remove","IUP_EditErase"),
    button_box(),
    list_box(),
    script_list(),
    load_script_button("Load script from", "IUP_FileOpen"),
    name_frame(),
    list_frame(),
    help_dialog("Jo_tracker Scripter Dialog", scripter_help),
    help()
{
    help.setAttr("SIZE","800x300");

    tracker_data=tracker_data_ptr;

    lua = nullptr;

   evaluate_button.setAttr("TIP","Evaluate script");
   execute_button.setAttr("TIP","Execute script");
   help_button.setAttr("TIP","Need help ?");
   store_button.setAttr("TIP","Store script in project data");
   remove_button.setAttr("TIP","Remove selected script");
   load_script_button.setAttr("TIP","Load script from project");

   remove_button.buttonClickOffSig.connect(this, &JScripterView::removeScript);

    evaluate_button.buttonClickOffSig.connect([&](){
                this->evaluateLua(this->editor.getAttr("VALUE"), false);
    });


    execute_button.buttonClickOffSig.connect([&]() {
        this->evaluateLua(this->editor.getAttr("VALUE"), true);
    });


    store_button.buttonClickOffSig.connect(this, &JScripterView::storeScript);

    help_button.buttonClickOffSig.connect([&](){
        //this->help_dialog.setText(scripter_help);
        std::string docpath = getLuaApiDocumentationPath();
        //std::cout << "DOC PATH " << docpath << std::endl;
        help.show();
        if(MiupUtil::filesystem::isValidNonEmptyFile(docpath)) {
                help.setFile(docpath);
        }


        //this->help_dialog.show();
    });

    load_script_button.buttonClickOffSig.connect([&](){
       FileDialog dlg(FileDialog::OPEN, MiupUtil::filesystem::getHomeDirectory().c_str());
       dlg.emitPath.connect([&](std::string path) {
           if(!MiupUtil::filesystem::isValidNonEmptyFile(path)) return;
           std::ifstream ifs(path);
           json j;
           ifs >> j;
           ifs.close();

           int res = IupAlarm("Possibly erases data", "Be careful, this can erase some data. Are you sure ? ", "Yes", "Cancel", NULL);
           if(res != 1) return;
           if(j.find("script_data") != j.end()) {
               for(auto & it : j["script_data"].items()) {
                   std::cout << it.key() << std::endl;
                   script_data[it.key()] = it.value();
               }
               updateList();
           }

       });
       dlg.popup();

    });

    script_name.setAttr("SIZE","120x12");
    name_frame.push(&script_name);
    name_frame.setAttr("TITLE","Script name");
    name_frame.setAttr("FONT","Arial, Bold 12");

    script_list.setAttr("DROPDOWN","YES");
    script_list.valueChangedSig.connect(this, &JScripterView::selectScript);

    script_list.setAttr("MINSIZE","230x12");
    list_frame.push(&script_list);
    list_frame.setAttr("TITLE","Script list");
    list_frame.setAttr("FONT","Arial, Bold 10");

    list_box.push(&name_frame, &list_frame);

    button_box.push(&evaluate_button, &execute_button, &store_button, &help_button,&remove_button, &load_script_button);
    button_box.setAttrInt("GAP",5);


    this->push(&button_box, &list_box);
    this->push(&editor, &console);

    script_name.buttonReleaseSig.connect([&](){
        if( this->script_name.getAttrStr("BGCOLOR") == std::string("255 0 0")) {
            this->script_name.setAttr("BGCOLOR","255 255 255");
        }
    });

    console.setAttr("EXPAND","HORIZONTAL");
    console.setAttr("SIZE","FULLx60");
    console.setAttr("READONLY","YES");

    editor.setAttr("EXPAND","YES");


        IUP_CLASS_INITCALLBACK(editor.getObj(), JScripterView);
        IUP_CLASS_SETCALLBACK(editor.getObj(), "MAP_CB",editor_map_cb);

//       std::string resource_dir = MiupUtil::filesystem::getCurrentDirectory() + "/res";
       // std::cout << "resource_dir >> " << resource_dir << std::endl;
        //if(!MiupUtil::filesystem::isValidFilePath(resource_dir)) MiupUtil::filesystem::createDirectory(resource_dir);
        //loadScriptData();
        //terra->registerFunction("MyFunc",MyFunc);
}

JScripterView::~JScripterView()
{
}

void JScripterView::initEditorAttributes()
{
    editor.setAttr("LEXERLANGUAGE","lua");

    editor.setAttr("KEYWORDS0",lua_keywords.c_str());
    editor.setAttr("KEYWORDS1", lua_secondary_keywords.c_str());
    editor.setAttr("KEYWORDS2", LUA_SCRIPT_KEYWORDS);
    editor.setAttr("KEYWORDS4", csound_keywords.c_str());
    

    editor.setAttr("STYLEFONT32","Consolas");
    editor.setAttr("STYLEFONTSIZE32","11");
    editor.setAttr("STYLECLEARALL","YES");

    editor.setAttr("STYLEFGCOLOR1","0 128 0");
    editor.setAttr("STYLEFGCOLOR2","0 128 0");
    editor.setAttr("STYLEFGCOLOR3","128 0 0");
    editor.setAttr("STYLEFGCOLOR5","0 0 255");
    editor.setAttr("STYLEFGCOLOR6","160 20 20");
    editor.setAttr("STYLEFGCOLOR7","128 0 0");
    editor.setAttr("STYLEFGCOLOR9","0 0 255");
    editor.setAttr("STYLEFGCOLOR10","255 0 255");

    editor.setAttr("STYLEFGCOLOR13","255 0 0");
    editor.setAttr("STYLEFGCOLOR14","128 0 128");
    editor.setAttr("STYLEFGCOLOR15", "0 128 128");
    editor.setAttr("STYLEFGCOLOR16", "128 128 0");

    editor.setAttr("STYLEITALIC5","YES");
    editor.setAttr("STYLEBOLD10","YES");
    editor.setAttr("STYLEHOTSPOT6","YES");
    editor.setAttr("MARGINWIDTH0","50");

    editor.setAttr("PROPERTY","fold=1");
    editor.setAttr("PROPERTY","fold.compact=0");
    editor.setAttr("PROPERTY","fold.comment=1");
    editor.setAttr("PROPERTY","fold.preprocessor=1");

    editor.setAttr("MARGINWIDTH1","20");
    editor.setAttr("MARGINTYPE1","SYMBOL");
    editor.setAttr("MARGINMASKFOLDERS1","Yes");
    editor.setAttr("MARKDEFINE","FOLDER=PLUS");
    editor.setAttr("MARKDEFINE","FOLDEROPEN=MINUS");
    editor.setAttr("MARKDEFINE","FOLDEREND=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDERMIDTAIL=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDEROPENMID=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDERSUB=EMPTY");
    editor.setAttr("MARKDEFINE","FOLDERTAIL=EMPTY");
    editor.setAttr("FOLDFLAGS","LINEAFTER_CONTRACTED");
    editor.setAttr("MARGINSENSITIVE1","YES");

    editor.setAttr("FONT","DejaVu, 10");

    initEditor();
}

void JScripterView::initEditor()
{

  //  std::cout << "INIT EDITOR ////// =====>>>>>> YOUPI" << std::endl;
    std::string path = getTrackerDirectory() + "/terra/?.lua";

    //std::string required = "package.path = package.path .. \";" + path + ";res/?.lua;\" \n"
                           //"local json = require\"json\" \n"
                           //"local jt = require\"jo_tracker_scripting\"\n\n";
    std::string comments = "--Use jt.setAt(seqnbr, tracknbr, lignbr, colnbr, value) to insert a value in the array \n"
                           "--Use jt.getAt(seqnbr, tracknbr, lignbr, colnbr) to retrieve a value from tracker score data \n"
                           "--Write your script here : \n\n\n\n\n\n"
                           "--Be careful, if Sequences, Lines and columns indexes start at 1, tracks indexes start at 0 \n";

    //std::string to_eval =required +  comments +  "local score_data_json=json.stringify(jt.score_data) \nreturn score_data_json";

    editor.setAttr("VALUE",  comments.c_str());
    //editor.setAttr("VALUE",to_eval.c_str());
}

void JScripterView::clearAll()
{
   script_data.clear();
   editor.clear();
   console.clear();
   initEditor();

   script_name.clear();
   script_list.clear();
}

void JScripterView::storeScript()
{
    std::string title = script_name.getAttrStr("VALUE");
    if(title.empty() || title.size() == 0) {
        script_name.setAttr("BGCOLOR","255 0 0");
        return;
    }
    std::string script_text = editor.getAttrStr("VALUE");
    script_data[title] = script_text;
    saveScriptData();
    updateList();
}

void JScripterView::updateList()
{
    std::list<std::string> list;
    script_list.clear();
    for(auto & it : script_data.items()) {
     //   std::cout << "script list : " << it.key() << std::endl;
        list.push_back(it.key());
    }
        script_list.push_list(list);
}

void JScripterView::saveScriptData(std::string path)
{
    if(script_data.is_null()) return;

    std::ofstream ofs(path);
    ofs << script_data.dump(4);
    ofs.close();
}

void JScripterView::loadScriptData(std::string path)
{
//    std::cout << "load script data" << std::endl;
    if(!script_data.is_null())
        script_data.clear();

    script_list.clear();
    if(MiupUtil::filesystem::isValidNonEmptyFile(path)) {
        std::ifstream ifs(path);
        ifs >> script_data;
        ifs.close();
    }

    std::list<std::string>list;
    for(auto & it : script_data.items()) {
        std::cout << "Script loaded name :  " << it.key() << std::endl;
        list.push_back(it.key().c_str());
    //    std::cout << " script name >> " << it.key() << std::endl;
    }
    script_list.push_list(list);
}


void JScripterView::removeScript()
{
    std::string name = script_name.getAttrStr("VALUE");
    if(name.empty()) return;
    if(script_data.find(name) != script_data.end()) script_data.erase(name);
    editor.clear();
    script_name.clear();
    script_list.removeSelectedItem();
    saveScriptData();
}

void JScripterView::selectScript(std::string name)
{
    if(script_data.find(name) != script_data.end() &&
            !script_data[name].get<std::string>().empty()) {
        script_name.setAttr("VALUE",name.c_str());
        editor.setAttr("VALUE",script_data[name].get<std::string>().c_str());
    }
}


int JScripterView::editor_map_cb(Ihandle *)
{
    initEditorAttributes();
    return IUP_DEFAULT;
}

bool JScripterView::evaluateLua(const char *text, bool execute)
{
    //if (lua != nullptr) delete lua;
    delete lua;
    lua = nullptr;

    console.clear();
    lua = new Lua();

    if(execute) {
        //recalculateSeqTrackSig.connect(this, calculateSeqTrack);
        recalculateSeqTrackSig.connect([&](int s, int t) {
           calculateSeqTrack(s,t);
        });
        recalculateSeqSig.connect([&](int s) {
            calculateSeq(s);
        });
        score_data_ptr = tracker_data;
    } else {
        recalculateSeqTrackSig.disconnect_all();
        recalculateSeqSig.disconnect_all();
        score_data_ptr = new json();
    }

    lua->Lregister("jt", jt_api);
    fftclass_open(lua->getLuaState());
    luaopen_ffi(lua->getLuaState());
    lua->Lregister("mathutils", mathutils_lua_api);

    if(lua->loadString(text)) {
        console.setAttr("APPEND", lua->toString(-1).c_str());
        return false;
    }


    if(execute) { // if execute then apply to data
        scriptedDoneSig();
    } else delete score_data_ptr;

    return true;
}


json *JScripterView::getData()
{
    return &script_data;
}

void JScripterView::setData(json *j)
{
    script_data.clear();
    script_data = *j;
}


void JScripterView::setOrcPointer(json *orc_ptr)
{
    orc_data_ptr = orc_ptr;
}

void JScripterView::setOrcKeywords()
{
    orc_data = *orc_data_ptr;
    orc_keywords = "";
    for(auto & it : orc_data["instr"].items()) {
        orc_keywords +=  it.key();
        orc_keywords += "\n";
    }

    editor.setAttrStr("KEYWORDS3", orc_keywords);

    editor.setAttr("STYLEFGCOLOR15", "128 128 0");
    editor.setAttr("STYLEITALIC15","YES");

}
