
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JSEQORGANIZER_H
#define JSEQORGANIZER_H

#include<iostream>

#include"Core/miupobj.h"
#include"Core/miup.h"

#include"utils/miuputil.h"

#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Boxes/scrollbox.h"
#include"Boxes/frame.h"

#include"Widgets/spinbox.h"
#include"Widgets/button.h"

#include"Text/label.h"

class JSeqOrganizerItem : public Hbox
{

public:
    JSeqOrganizerItem() :  seq(1,1,NULL,1),  loop(1,1,NULL,1), Hbox()
    {
        push(&seq,&loop);
        setAttr("GAP","3");

    }

    ~JSeqOrganizerItem() {}

    std::pair<int,int> getValues() {return {seq.getValue(), loop.getValue()};}

private:
    SpinBox<int> seq, loop;
};


class JSeqOrganizer : public Vbox
{
public:
    JSeqOrganizer() :
        play_button("Play"), write_button("Write as Seq"),
        main_box(), play_box(), add_remove_box(),
        fill(), sqnbr(1,1,NULL,1), old_value(1), item_frame(), Vbox()
    {

        play_button.setAttr("MINSIZE","100x");
        write_button.setAttr("MINSIZE","100x");

        play_button.setAttr("IMAGE","IUP_MediaPlay");
        write_button.setAttr("IMAGE","IUP_EditCopy");

        add_remove_box.push(&sqnbr);
        play_box.push(&play_button, &write_button);

        add_remove_box.setAttr("ALIGNMENT","ACENTER");
        add_remove_box.setAttr("GAP","3");
        play_box.setAttr("ALIGNMENT","ACENTER");
        play_box.setAttr("GAP","3");

        item_frame.push(&main_box);
        item_frame.setAttr("TITLE","Sequence number /  Number of loops");

        scrollbox.push(&item_frame);
        scrollbox.setAttr("SCROLLBAR","VERTICAL");

        this->push(&add_remove_box, &scrollbox, &fill, &play_box);
        sqnbr.postValueChangedSig.connect([&](int val) {
            if( val > old_value) this->addItem();
            else if( val < old_value ) this->removeItem();

            old_value = val;
        });

        play_button.buttonClickOffSig.connect(this, &JSeqOrganizer::playClicked);
        write_button.buttonClickOffSig.connect(this, &JSeqOrganizer::writeClicked);


        //main_box.setAttr("EXPAND","YES");
        setAttr("MINSIZE","235x");
        addItem();
    }

    ~JSeqOrganizer() {
        for(auto & it : list) {delete it;}
    }

    lsignal::signal<void(bool, std::vector<int> *)> emitOrderSig;
protected:

private:
    void addItem() {
            list.push_back(new JSeqOrganizerItem());
            main_box.push(list.back());
    }

    void removeItem() {
        if(list.size() <=1) return;

        JSeqOrganizerItem *item = list.back();
        delete item;
        list.pop_back();
        main_box.refresh();
        item_frame.update();
    }

    void checkOrder() {
        order.clear();
        for(auto & it : list) {
            std::pair<int,int> pair = it->getValues();
            for(int i = 0; i < pair.second; i++) order.push_back(pair.first);
        }
    }

    void playClicked() {
        checkOrder();
        emitOrderSig(false, &order);
    }

    void writeClicked() {
        checkOrder();
        emitOrderSig(true, &order);
    }

    int old_value;

    Vbox main_box;
    Fill fill;

    std::vector<int> order;
    std::vector<JSeqOrganizerItem*> list;
    ScrollBox scrollbox;
    Hbox play_box, add_remove_box;
    Button play_button, write_button;
    SpinBox<int> sqnbr;
    Frame item_frame;
};

#endif // JSEQORGANIZER_H
