
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "jtrack.h"

JTrack::JTrack(int numlin,int idx) :
    Frame(m_expander = new Expander(vbx=new Vbox(hbx=new Hbox(solo_button=new Toggle(NULL),mute_button=new Toggle(NULL), show_hide=new Toggle(0,"show"),
                                    param_button = new Button(NULL),  numcol_chooser=new SpinBox<int>(8,4,NULL,1)
                                   ),mat=new Matrix(numlin,8)))), index(idx), mute(false), solo(false)
{

    m_expander->setAttr("BARPOSITION","LEFT");
    m_expander->setAttr("BACKCOLOR","210 210 210 150");
    m_expander->setAttr("FRAME","NO");
    //m_expander->setAttr("STATEREFRESH","NO");

    // defaults to no auto calculation
    auto_calculate_mode = AutoCalculateMode::none;

    title="t_" + std::to_string(index);
    this->setAttr("TITLE",title.c_str());

    mat->setAttr("NUMCOL","4");
    mat->setAttr("WIDTH2","20");

    mat->setAttr("WIDTHDEF","40");
    mat->setAttr("HEIGHTDEF","6");

    mat->setAttr("HEIGHT0","6");
    mat->setAttr("WIDTH0","20");

    mat->setAttr("UNDOREDO","YES");
    mat->setAttr("RESIZEMATRIX","YES");
    mat->setAttr("MARKMODE","CELL");
    mat->setAttr("MARKMULTIPLE","YES");
    mat->setAttr("MARKAREA","NOT_CONTINUOUS");
    mat->setAttr("COPYKEEPSTRUCT","YES");
    mat->setAttr("LASTERROR","MARKEDCONSISTENCY");
    mat->setAttr("HIDEFOCUS","NO");
    mat->setAttr("EXPAND","VERTICAL");

    mat->setAttr("SCROLLBAR","YES");
    //mat->setAttr("SCROLLBAR","NO");
    mat->setAttr("EDITFITVALUE","YES");


    solo_button->setAttr("IMAGE","IUP_EditFind");
    param_button->setAttr("IMAGE","IUP_ToolsSettings");
    show_hide->setAttr("IMAGE","IUP_ViewFullScreen");
    //mute_button->setAttr("IMAGE","IUP_ActionCancel");
    mute_button->setAttr("IMAGE","mute_icon");

    solo_button->setAttr("TIP","Solo Track");
    param_button->setAttr("TIP","Track parameters");
    show_hide->setAttr("TIP","Show - Hide track");
    mute_button->setAttr("TIP","Mute Track");

    hbx->setAttr("GAP","4");
    real_colnbr=new MiupUtil::Value<int>(8);

    IUP_CLASS_INITCALLBACK(mat->getObj(),JTrack);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"ENTERITEM_CB",enteritem_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"LEAVEITEM_CB",leaveitem_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"VALUE_EDIT_CB",value_edit_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"MAP_CB",map_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"K_ANY", k_any);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"EDITION_CB", edition_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"COLRESIZE_CB",colresize_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(), "SCROLLTOP_CB",scrolltop_cb);

    numcol_chooser->postValueChangedSig.connect(real_colnbr,&MiupUtil::Value<int>::setValue);
    real_colnbr->postValueChangedSig.set_lock(true);
    real_colnbr->postValueChangedSig.connect(mat,&Matrix::setNumcol);
    real_colnbr->postValueChangedSig.connect(this,&JTrack::calculateSize);

    numcol_chooser->postValueChangedSig.connect(this,&JTrack::numcolEmitter);

    solo_button->toggleStateChangedSig.connect(this, &JTrack::soloEmitter);
    mute_button->toggleStateChangedSig.connect(this, &JTrack::muteEmitter);


    show_hide->setAttr("3STATE","YES");
    show_hide->toggleStateChangedSig.connect(this,&JTrack::enableResize);

    param_button->buttonClickOffSig.connect(this, &JTrack::displayTrackParameters);


}

JTrack::~JTrack()
{
    delete vbx;
    delete hbx;
    delete param_button;
    delete mute_button;
    delete solo_button;
    delete show_hide;
    delete numcol_chooser;
    delete mat;
}

void JTrack::enableResize(int status)
{

    std::cout << "track show/hide state : "  << status << std::endl;

    json params = JoTrackerParameters::getParameters();
    double offset = params[ParameterNames[ParameterList::TRACK_WIDTH_OFFSET]].get<double>();
    double multiplier = params[ParameterNames[ParameterList::TRACK_WIDTH_MULTIPLIER]].get<double>();


    switch(status) {
    case 0:
    {
        //if(numcol_chooser->postValueChangedSig.is_locked()==false) numcol_chooser->postValueChangedSig.set_lock(true);
        if(real_colnbr->postValueChangedSig.is_locked()==false) real_colnbr->postValueChangedSig.set_lock(true);

        mat->setAttrInt("NUMCOL",4);

        std::cout << "OFFSET " << offset << std::endl;
        double sum= offset;
        for(int i = 0; i <= 4; i++) {
            std::string a = "WIDTH" + std::to_string(i);
            //sum += mat->getAttrInt(a.c_str());
            sum += mat->getAttrDouble(a.c_str()) + (mat->getAttrDouble(a.c_str()) * multiplier);
        }

        std::cout <<"sum" << sum << std::endl;
        //std::string s = std::to_string(sum) + "x";
        mat->setWidth(sum);
        mat->refresh();

        isVisibleSig(index,false);
    }
        break;

    case 1:
    {
        //if(numcol_chooser->postValueChangedSig.is_locked()==true) numcol_chooser->postValueChangedSig.set_lock(false);
        if(real_colnbr->postValueChangedSig.is_locked()==true) real_colnbr->postValueChangedSig.set_lock(false);
        mat->setAttrInt("NUMCOL",real_colnbr->getValue());


        double sum=offset;
        for(int i = 0; i <= mat->getAttrInt("NUMCOL"); i++) {
            std::string a = "WIDTH" + std::to_string(i);
            sum += mat->getAttrDouble(a.c_str()) + (mat->getAttrDouble(a.c_str()) * multiplier);
            //std::cout << " mat redraw : index : " << i << " val " << mat->getAttrInt(a.c_str()) << std::endl;;
        }


        std::cout <<"sum" << sum << std::endl;
        //std::string s = std::to_string(sum) + "x";
        mat->setWidth(sum);
        mat->refresh();

       isVisibleSig(index,true);
    }
        break;
    }


        updateTitles();

}

void JTrack::soloEmitter(int status)
{
    switch(status) {
    case 0:
    {
        soloSig(index, false);
        solo = false;
        solo_button->setAttr("BGCOLOR", IupGetGlobal("DLGBGCOLOR"));
        break;
    }
    case 1:
    {
        if(soloSig(index, status))
        {
            solo = true;
            solo_button->setAttr("BGCOLOR", "50 0 250");
        } else{
            solo_button->setStatus(0, false);
        }
        break;
    }
    };

    // repaint in blue
}

void JTrack::muteEmitter(int status)
{
    switch(status) {
    case 0:
    {
        mute = false;
        mute_button->setAttr("BGCOLOR", IupGetGlobal("DLGBGCOLOR"));
        break;
    }
    case 1:
    {
        mute = true;
        mute_button->setAttr("BGCOLOR", "200 0 100");
        break;
    }
    };
    // repain in red
}

void JTrack::updateTitles()
{
    for(std::size_t i=0; i< std::size_t(mat->getAttrInt("NUMCOL")); i++) {
        if(i<descriptionList.size()) {
            this->setValueAt(0,i+1,descriptionList.at(i).c_str());
        }
    }
}

void JTrack::calculateSize(int /*col*/)
{
        int sum=65;
        for(int i = 0; i <= mat->getAttrInt("NUMCOL"); i++) {
            std::string a = "WIDTH" + std::to_string(i);
            //sum += mat->getAttrInt(a.c_str());
            sum += mat->getAttrInt(a.c_str()) + (mat->getAttrInt(a.c_str()) * 0.12);
            //std::cout << " mat redraw : index : " << i << " val " << mat->getAttrInt(a.c_str()) << std::endl;;
        }
    mat->setWidth(sum);
    mat->refresh();
    updateTitles();
}

void JTrack::clearTrack() {
    mat->clear();
}

void JTrack::clearTrackEmitter() {
    mat->clear();
    clearTrackSig(index);
}


void JTrack::setValueAt(int lin, int col, const char *val)
{
    mat->setValueAt(lin,col,val);
}

void JTrack::setValueAtCurrentCell(const char *val)
{
    mat->setAttr("VALUE",val);
}

void JTrack::insertAtCurrentCell(const char *val)
{
    mat->setAttr("INSERT",val);
}

const char *JTrack::getValueAt(int lin, int col)
{
    return mat->getValueAt(lin,col);
}

void JTrack::unselectCurrent() {
   mat->setAttr("EDITMODE","NO");
}

void JTrack::selectCurrent() {
    mat->setAttr("EDITMODE","YES");
}

void JTrack::selectAt(int lin, int col)
{
    std::string mark="MARK" + std::to_string(lin) + ':' + std::to_string(col);
    mat->setAttr(mark.c_str(),"1");
    //std::cout << "coucou item " << std::endl;
    curlin=lin;
    curcol=col;
}


void JTrack::setNumlin(int lin)
{
    mat->setNumlin(lin);
    for(int l=1;l<=mat->getAttrInt("NUMLIN");l++) {
        mat->setValueAt(l,0,std::to_string(l).c_str());
    }
   // mat->redraw();
}

void JTrack::setNumcol(int col)
{
    numcol_chooser->setValue(col);
    if(!real_colnbr->postValueChangedSig.is_locked()) mat->setNumcol(col);
}

void JTrack::numcolEmitter(int col)
{
    real_colnbr->setValue(col);
    colnbrChangedSig(index,col);
}

int JTrack::getNumcol()
{
    return numcol_chooser->getAttrInt("VALUE");
}

void JTrack::scrollTo(int line)
{
    std::string s = std::to_string( (line > 0) ? line : 1 ) + ":*";
    mat->setAttr("ORIGIN",s.c_str());
    //mat->setAttr("SHOW", s.c_str());
}


void JTrack::redrawMat()
{
    mat->redraw();
}


void JTrack::setDescriptionList(std::vector<std::string>* val)
{
    descriptionList.clear();
    mat->setAttr("CLEARVALUE0:*", "ALL");
    for(int i=0; i < val->size() ; i++) {
        descriptionList.push_back(val->at(i));
        if(i+1 <= mat->getAttrInt("NUMCOL")) {
            //std::cout << "value : " << val->at(i) << std::endl;
            setValueAt(0,i+1, val->at(i).c_str());
        }
    }
}

void JTrack::redrawDescriptionList()
{
    for(std::size_t i = 0; i < descriptionList.size() ; i++ ) {
        setValueAt(0, i+1, descriptionList.at(i).c_str());
    }
}

void JTrack::displayTrackParameters()
{
    displayTrackParameterSig(index);
    //track_parameters->popup(IUP_MOUSEPOS, IUP_MOUSEPOS);
}

JTrack::AutoCalculateMode JTrack::getAutoCalculateMode() {return auto_calculate_mode;}

void JTrack::setAutoCalculateMode(AutoCalculateMode mode) {
    auto_calculate_mode = mode;
    autoCalculateModeChangedSig(index, auto_calculate_mode);
}


//audio
void JTrack::selectLine(int line)
{
    mat->setAttr("MARKMODE","LIN");
    std::string mark = "MARK" + std::to_string(line) + ":0";
    mat->setAttr(mark.c_str(), "1");
}

void JTrack::setMarkCell()
{
    mat->setAttr("MARKMODE","CELL");
}

void JTrack::colorLines(int gridstep)
{
    if(gridstep <0 ) return;
    for(int i = 1; i <= mat->getAttrInt("NUMLIN") ; i++) {
        std::string attr = "BGCOLOR" + std::to_string(i) + ":*";
        if( gridstep != 0 && ((i-1)%gridstep) == 0) {
            mat->setAttrRGB(attr.c_str(), 180,180,180);
        } else {
            mat->setAttrRGB(attr.c_str(), 255,255,255);
        }
    }

   // mat->redraw();
}


int JTrack::scrolltop_cb(Ihandle *, int lin, int /*col*/)
{
    scrollSig(lin);
    return IUP_DEFAULT;
}

//Callbacks
int JTrack::enteritem_cb(Ihandle *, int lin, int col) {
    std::string mark="MARK" + std::to_string(lin) + ':' + std::to_string(col);
    mat->setAttr(mark.c_str(),"1");
    curlin=lin;
    curcol=col;
    emitCellSig(index, lin, col);
    return IUP_CLOSE;
}

int JTrack::leaveitem_cb(Ihandle *, int lin, int col) {
    buffer.clear();
    std::string mark="MARK" + std::to_string(lin) + ':' + std::to_string(col);
    mat->setAttr(mark.c_str(),"0");
    std::string v(mat->getValueAt(lin,col));
//    std::cout << "VALUE IN MATRIX == > " << mat->getValueAt(lin,col) << std::endl;;
    if(v.size() >0) emitValueSig(index,lin,col,v.c_str());
    return IUP_CLOSE;
}

int JTrack::value_edit_cb(Ihandle *, int lin, int col, char */*newval*/) { // seems to have a bug
    buffer.clear();
    std::string mark="MARK" + std::to_string(lin) + ':' + std::to_string(col);
    mat->setAttr(mark.c_str(),"0");
    //std::cout << "VALUE IN MATRIX == > " << mat->getValueAt(lin,col);
    std::string v(mat->getValueAt(lin,col));
    if(v.size() >0) emitValueSig(index,lin,col,v.c_str());


    /*std::cout << "lin : col " << lin << " " << col << std::endl;
    std::cout << "value : " << mat->getValueAt(lin,col) << std::endl;
    std::string v(mat->getValueAt(lin,col));
    if(v.size() > 0) mat->setValueAt(lin,col,v.c_str());
        */
    return IUP_DEFAULT;
}

int JTrack::map_cb(Ihandle *)
{
    enableResize(0);
    return IUP_CLOSE;
}

int JTrack::k_any(Ihandle *, int value)
{
   std::cout << "key value " << value << std::endl;

    if(iup_isprint(value)) { //key is a printable character
    //    std::cout << "isprint" << std::endl;
        if(curcol == 1) { // then display a menu for help
            //std::cout << value << std::endl;
            std::string curval = getValueAt(curlin,curcol);
            char v = (char)value;
            curval += v;
            setValueAt(curlin,curcol,curval.c_str());
            displayInstrumentListSig(index,curlin, false);
        }

    } else if (iup_isCtrlXkey(value)){
        std::cout << "control key" << std::endl;

        switch(value) {
        // Keys for non numeric pad computers
        case K_cs1:
            emitVimBufferRegister(index,1, true);
            break;
        case K_cs2:
            emitVimBufferRegister(index,2, true);
            break;
        case K_cs3:
            emitVimBufferRegister(index,3, true);
            break;
        case K_cs4:
            emitVimBufferRegister(index,4, true);
            break;
        case K_cs5:
            emitVimBufferRegister(index,5, true);
            break;
        case K_cs6:
            emitVimBufferRegister(index,6, true);
            break;
        case K_cs7:
            emitVimBufferRegister(index,7, true);
            break;
        case K_cs8:
            emitVimBufferRegister(index,8, true);
            break;
        case K_cs9:
            emitVimBufferRegister(index,9, true);
            break;

        // Numeric pad computers
        case K_c1:
            emitVimBufferRegister(index,1, true);
            break;
        case K_c2:
            emitVimBufferRegister(index,2, true);
            break;
        case K_c3:
            emitVimBufferRegister(index,3, true);
            break;
        case K_c4:
            emitVimBufferRegister(index,4, true);
            break;
        case K_c5:
            emitVimBufferRegister(index,5, true);
            break;
        case K_c6:
            emitVimBufferRegister(index,6, true);
            break;
        case K_c7:
            emitVimBufferRegister(index,7, true);
            break;
        case K_c8:
            emitVimBufferRegister(index,8, true);
            break;
        case K_c9:
            emitVimBufferRegister(index,9, true);
            break;

            // copy full line
        case K_cp:
            copyFullLine(0);
            std::cout << "copy line" << std::endl;
            break;
        }
    } else if(iup_isAltXkey(value)) {
        std::cout << "Alt key " << std::endl;
        switch(value) {


        // Keys for non numeric pad computers
        case K_as1:
            emitVimBufferRegister(index,1, false);
            break;
        case K_as2:
            emitVimBufferRegister(index,2, false);
            break;
        case K_as3:
            emitVimBufferRegister(index,3, false);
            break;
        case K_as4:
            emitVimBufferRegister(index,4, false);
            break;
        case K_as5:
            emitVimBufferRegister(index,5, false);
            break;
        case K_as6:
            emitVimBufferRegister(index,6, false);
            break;
        case K_as7:
            emitVimBufferRegister(index,7, false);
            break;
        case K_as8:
            emitVimBufferRegister(index,8, false);
            break;
        case K_as9:
            emitVimBufferRegister(index,9, false);
            break;


        // Numeric pad computers
        case K_m1:
            emitVimBufferRegister(index,1, false);
            break;
        case K_m2:
            emitVimBufferRegister(index,2, false);
            break;
        case K_m3:
            emitVimBufferRegister(index,3, false);
            break;
        case K_m4:
            emitVimBufferRegister(index,4, false);
            break;
        case K_m5:
            emitVimBufferRegister(index,5, false);
            break;
        case K_m6:
            emitVimBufferRegister(index,6, false);
            break;
        case K_m7:
            emitVimBufferRegister(index,7, false);
            break;
        case K_m8:
            emitVimBufferRegister(index,8, false);
            break;
        case K_m9:
            emitVimBufferRegister(index,9, false);
            break;


         //Press alt L to display instrument list
        case K_mL:
            //std::cout << "alt L is pushed" << std::endl;
            displayInstrumentListSig(index, curlin, true);
            break;

            // paste full line
        case K_mp:
            copyFullLine(1);
            std::cout << "paste line " << std::endl;
            break;


        }


    } else { //key is not printable character
        switch (value) {

        // Return key is pressed, removed value in a cell
        case 8:
                setValueAt(curlin,curcol,"");
                emitValueSig(index,curlin,curcol,"");
            break;
        }
    }
  //  std::cout << "is print : " << iup_isprint(value) << " char : " << value   << " col : " << curcol <<  std::endl;
    return IUP_DEFAULT;
}


int JTrack::edition_cb(Ihandle *, int lin, int col, int mode, int update)
{
    std::string val(getValueAt(lin,col));
    editmode=mode;
    editionCallbackSig(index, lin, col, mode, update, val);

    //if((col==1 && mode == 1)) { // then instrument name is being edited so a list of instruments is displayed
    //    return IUP_IGNORE;
    //} else { // then any other cell, so enabling text mode editing
        return IUP_DEFAULT;
    //}

}

int JTrack::colresize_cb(Ihandle *, int /*width*/, int /*height*/)
{
    this->enableResize( show_hide->getAttrInt("VALUE"));

    return IUP_DEFAULT;
}

std::pair<int, int> JTrack::getCellPosition(int lin, int col)
{
    std::string posLC= "CELLOFFSET"  + std::to_string(lin) + ":" + std::to_string(col);
    std::pair<int,int> posPair=mat->getAttrPairInt(posLC.c_str());
    std::string sizeLC = "CELLSIZE" + std::to_string(lin) + ":" + std::to_string(col);
    std::pair<int,int> sizePair=mat->getAttrPairInt(sizeLC.c_str());
    std::pair<int,int> widgetPos = mat->getAttrPairInt("SCREENPOSITION");
    //std::cout << "positions  X : " << posPair.first << " " << sizePair.first << " " << widgetPos.first << std::endl;
    //std::cout << "positions  Y: " << posPair.second  << " " << sizePair.second<< " " << widgetPos.second<< std::endl;
    return {posPair.first  /*+ sizePair.first */+ widgetPos.first, posPair.second + sizePair.second + widgetPos.second};
}

int JTrack::getEditMode() {return editmode;}














/** JTempo
 *
 * Tempo track for jo_tracker.
 * **/
JTempo::JTempo(int /*numlin*/) : Frame(vbx=new Vbox(clear_but=new Button(NULL), mat=new Matrix(8,2)))
{
    setAttr("TITLE","Tempo");

    mat->setNumcol(2);
    mat->setAttr("WIDTHDEF","40");
    mat->setAttr("HEIGHTDEF","6");

    mat->setAttr("HEIGHT0","6");
    mat->setAttr("WIDTH0","20");
    mat->setAttr("UNDOREDO","YES");
    mat->setAttr("RESIZEMATRIX","YES");
    mat->setAttr("MARKMODE","CELL");
    mat->setAttr("MARKMULTIPLE","YES");
    mat->setAttr("MARKAREA","NOT_CONTINUOUS");
    mat->setAttr("COPYKEEPSTRUCT","YES");
    mat->setAttr("LASTERROR","MARKEDCONSISTENCY");
    mat->setAttr("HIDEFOCUS","NO");
    mat->setAttr("EXPAND","VERTICAL");
    //mat->setAttr("SCROLLBAR","VERTICAL");
    mat->setAttr("SCROLLBAR","NO");

    clear_but->setAttr("IMAGE","IUP_EditErase");
    clear_but->setAttr("TIP","Clear tempo for current sequence");

    IUP_CLASS_INITCALLBACK(mat->getObj(),JTempo);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"K_ANY",k_any);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"ENTERITEM_CB",enteritem_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"LEAVEITEM_CB",leaveitem_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(),"VALUE_EDIT_CB",value_edit_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(), "MAP_CB", map_cb);
    IUP_CLASS_SETCALLBACK(mat->getObj(), "SCROLLTOP_CB",scrolltop_cb);

    clear_but->buttonClickOffSig.connect(mat,&Matrix::clear);
    clear_but->buttonClickOffSig.connect(this,&JTempo::clearTrackEmitter);
}

JTempo::~JTempo()
{
    delete vbx;
    delete clear_but;
    delete mat;
}

void JTempo::clearTrack()
{
    mat->clear();
}

void JTempo::clearTrackEmitter() {
    clearTrackSig(-1);
}

void JTempo::setValueAt(int lin, int col, const char *val)
{
    mat->setValueAt(lin,col,val);
}

const char *JTempo::getValueAt(int lin, int col)
{
    return mat->getValueAt(lin,col);
}

void JTempo::setNumlin(int lin)
{
    if(lin>0) mat->setNumlin(lin);
    for(int l=1;l<=mat->getAttrInt("NUMLIN");l++) {
        mat->setValueAt(l,0,std::to_string(l).c_str());
    }
    //mat->redraw();
}

void JTempo::selectLine(int line)
{
    mat->setAttr("MARKMODE","LIN");
    std::string mark = "MARK" + std::to_string(line) + ":0";
    mat->setAttr(mark.c_str(), "1");
    //mat->redraw();
}

void JTempo::setMarkCell()
{
    mat->setAttr("MARKMODE","CELL");
    //std::cout << mat->getAttr("MARKMODE") << std::endl;
}

void JTempo::colorLines(int gridstep)
{
    if(gridstep <0 ) return;
    for(int i = 1; i <= mat->getAttrInt("NUMLIN") ; i++) {
        std::string attr = "BGCOLOR" + std::to_string(i) + ":*";
        if(gridstep != 0 && ((i-1)%gridstep) == 0) {
            mat->setAttrRGB(attr.c_str(), 180,180,180);
        } else {
            mat->setAttrRGB(attr.c_str(), 255,255,255);
        }
    }

  // mat->redraw();
}

void JTempo::scrollTo(int line)
{
    std::string s = std::to_string( (line > 0) ? line : 1 ) + ":*";
    mat->setAttr("ORIGIN",s.c_str());
}

void JTempo::redrawMat()
{
    mat->redraw();
}

//callbacks
int JTempo::enteritem_cb(Ihandle *, int lin, int col)
{
    std::string mark="MARK" + std::to_string(lin) + ':' + std::to_string(col);
    mat->setAttr(mark.c_str(),"1");
    curlin=lin;
    curcol=col;
    emitCellSig(-1, lin,col);
    return IUP_CLOSE;
}

int JTempo::leaveitem_cb(Ihandle *, int lin, int col)
{
    std::string mark="MARK" + std::to_string(lin) + ':' + std::to_string(col);
    mat->setAttr(mark.c_str(),"0");
    emitValueSig(lin,col,mat->getValueAt(lin,col));
    return IUP_CLOSE;
}

int JTempo::value_edit_cb(Ihandle *, int /*lin*/, int /*col*/, char * /*val*/)
{
    //emitValueSig(lin,col,val);
    return IUP_CLOSE;
}

int JTempo::k_any(Ihandle *, int value)
{
    if(iup_isprint(value)) { //key is a printable character
    } else { //key is not printable character
        switch (value) {

        case 8:
            setValueAt(curlin,curcol,"");
            break;
        }
    }
    return IUP_DEFAULT;
}

int JTempo::map_cb(Ihandle *)
{
   //mat->setWidth(130);
   mat->setAttr("SIZE","130x");
   mapSig();
   return IUP_DEFAULT;
}

int JTempo::scrolltop_cb(Ihandle *, int lin, int /*col*/)
{
    scrollSig(lin);
    return IUP_DEFAULT;
}
