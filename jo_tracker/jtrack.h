
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JTRACK_H
#define JTRACK_H

#include"Boxes/hbox.h"
#include"Boxes/vbox.h"
#include"Boxes/frame.h"
#include"Boxes/menu.h"
#include"Widgets/matrix.h"
#include"Widgets/button.h"
#include"Widgets/toggle.h"
#include"Widgets/spinbox.h"
#include"Widgets/flatbutton.h"
#include"Boxes/expander.h"
#include"utils/value.h"

#include"jo_tracker/parametersdialog.h"



/** Jtrack, normal score track **/
class JTrack : public Frame
{
public:
    //instance means auto calculate p3 for all lines with same instances in the track
    //occurence means auto calculate p3 for all lines in the track
    // none means no auto calculate, user is responsible to manage p3
    enum AutoCalculateMode{instance, occurence, none};

    JTrack(int numlin=8,int index=1);
    virtual ~JTrack();

    void setValueAt(int lin, int col,const char *val);
    void setValueAtCurrentCell(const char *val);
    void insertAtCurrentCell(const char *val);

    const char *getValueAt(int lin, int col);

    void unselectCurrent();
    void selectCurrent();
    void selectAt(int lin, int col);

    int getEditMode();

    void setNumlin(int lin);
    void setNumcol(int col);
    int getNumcol();
    void enableResize(int status);
    void clearTrack();

    void numcolEmitter(int col);

    void soloEmitter(int status);
    void muteEmitter(int status);
    bool mute, solo;
    lsignal::signal<bool(int idx, int status)> soloSig;

    std::pair<int, int> getCellPosition(int lin, int col);


    void setDescriptionList(std::vector<std::string> *val);
    void redrawDescriptionList();


    AutoCalculateMode getAutoCalculateMode();
    void setAutoCalculateMode(AutoCalculateMode mode);

    //audio callback
    void selectLine(int line);
    void setMarkCell();

    void colorLines(int gridstep);

    void scrollTo(int line);

    void redrawMat();


    lsignal::signal<void(int idx, int lin, bool full)>displayInstrumentListSig;
    lsignal::signal<void(int idx,bool isVisible)>isVisibleSig;
    lsignal::signal<void(int idx,int lig,int col,const char *val)> emitValueSig;
    lsignal::signal<void(int idx,int lig, int col)> emitCellSig;
    lsignal::signal<void(int idx,int colnbr)> colnbrChangedSig;
    lsignal::signal<void(int idx)>clearTrackSig;
    lsignal::signal<void(Ihandle *)>dropSig;

    lsignal::signal<void(int index,int lin, int col, int mode, int update, std::string value)> editionCallbackSig;

    lsignal::signal<void(int, AutoCalculateMode)> autoCalculateModeChangedSig;
    lsignal::signal<void(int)> displayTrackParameterSig;

    lsignal::signal<void(int t_idx, int buffer_nbr, bool record)> emitVimBufferRegister;
    lsignal::signal<void(int)>scrollSig;

    // if arg is 0 then copy, if 1 then paste.
    lsignal::signal<void(int)> copyFullLine;
protected:
    IUP_CLASS_DECLARECALLBACK_IFni(JTrack, k_any)
    IUP_CLASS_DECLARECALLBACK_IFnii(JTrack,leaveitem_cb)
    IUP_CLASS_DECLARECALLBACK_IFnii(JTrack,enteritem_cb)
    IUP_CLASS_DECLARECALLBACK_IFniis(JTrack,value_edit_cb)
    IUP_CLASS_DECLARECALLBACK_IFn(JTrack,map_cb)
    IUP_CLASS_DECLARECALLBACK_IFniiii(JTrack,edition_cb)
    IUP_CLASS_DECLARECALLBACK_IFnii(JTrack, colresize_cb)
    IUP_CLASS_DECLARECALLBACK_IFnii(JTrack, scrolltop_cb)
    //virtual IUP_CLASS_DECLARECALLBACK_IFnnii(JTrack,drop_cb)
private:
    AutoCalculateMode auto_calculate_mode;

    void clearTrackEmitter();
    void calculateSize(int col);

    void updateTitles();

    void displayTrackParameters();

    //members

    int editmode;
    int curlin, curcol;
    int index;
    std::string title;
    Matrix *mat;
    SpinBox<int> *numcol_chooser;
    Button  *param_button;
    Toggle *show_hide, *solo_button, *mute_button;
    Hbox *hbx;
    Vbox *vbx;
    MiupUtil::Value<int> *real_colnbr;
    Expander *m_expander;

    std::string buffer; // cell buffer
    std::vector<std::string> descriptionList;
};







/** JTEMPO **/
class JTempo : public Frame
{
public:
    JTempo(int numlin=8);
    virtual ~JTempo();

    void setValueAt(int lin, int col,const char *val);
    const char *getValueAt(int lin,int col);

    void setNumlin(int lin);
    void clearTrack();

    void redrawMat();

    void selectLine(int line);
    void setMarkCell();
    void colorLines(int gridstep);

    void scrollTo(int line);

    lsignal::signal<void(int idx)>clearTrackSig;
    lsignal::signal<void(int lig,int col, const char *val)> emitValueSig;
    lsignal::signal<void(int idx,int lig, int col)> emitCellSig;
    lsignal::signal<void(int)>scrollSig;
    lsignal::signal<void(void)>mapSig;
protected:
    IUP_CLASS_DECLARECALLBACK_IFni(JTempo, k_any)
    IUP_CLASS_DECLARECALLBACK_IFnii(JTempo,leaveitem_cb)
    IUP_CLASS_DECLARECALLBACK_IFnii(JTempo,enteritem_cb)
    IUP_CLASS_DECLARECALLBACK_IFniis(JTempo,value_edit_cb)
    IUP_CLASS_DECLARECALLBACK_IFn(JTempo, map_cb)
    IUP_CLASS_DECLARECALLBACK_IFnii(JTempo, scrolltop_cb)
private:
    int curlin, curcol;
    void clearTrackEmitter();
    Matrix *mat;
    Button *clear_but;
    Vbox *vbx;
};

#endif // JTRACK_H
