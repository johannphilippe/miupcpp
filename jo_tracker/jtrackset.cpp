﻿
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "jtrackset.h"


JTrackSet::JTrackSet() :
    Vbox(/*new Vbox()*/),track_box(new Hbox()), parameter_icon(load_image_parameter_icon(), std::string("parameter_icon")), curlin(0), curtrack(0)
  , fill_control(), organizer(), organizer_expander(&organizer),
    is_ordered(false), has_solo(false)
{

    organizer_expander.setAttr("BARPOSITION","LEFT");
    organizer_expander.setAttr("TITLE","Organiser");
    organizer_expander.setAttr("STATE","CLOSE");

    organizer.setAttr("EXPAND","VERTICAL");
    organizer_expander.setAttr("EXPAND","VERTICAL");
    organizer_expander.emitStateChangedSig.connect([&](bool isOpen) {
      if(isOpen==1) {
          //this->redrawTracks();
          //this->redraw();
      }
    });

    organizer.emitOrderSig.connect(this, &JTrackSet::orderedPlayWrite);

    autocalc_instance = new Item("Instances");
    autocalc_instance->setHandle("instance");
    autocalc_none = new Item("None");
    autocalc_none->setHandle("none");
    autocalc_occurrence = new Item ("Occurrence");
    autocalc_occurrence->setHandle("occurrence");

    autocalc_menu = new Menu(autocalc_instance, autocalc_occurrence,autocalc_none);
    autocalc_menu->setAttr("RADIO","YES");
    autocalc_submenu = new SubMenu("Auto Calculate Method", autocalc_menu);


    deleteTrackItem= new Item("Delete Track");
    //deleteTrackItem.setHandle("delete_track");
    clearTrackItem = new Item("Clear Track");
    //clearTrackItem.setHandle("clear_track");
    deleteTrackItem->setAttr("TITLEIMAGE", "IUP_ActionCancel");
    clearTrackItem->setAttr("TITLEIMAGE","IUP_EditErase");

    track_parameters = new  Menu(autocalc_submenu, clearTrackItem, deleteTrackItem);




    deleteTrackItem->clickItemSig.connect([&](){

    });

    clearTrackItem->clickItemSig.connect([&](){
        std::cout << "curtrack : " << curtrack << std::endl;
        track_list[curtrack]->clearTrack();
        clearTrackData(curtrack);
    });

    autocalc_instance->clickItemSig.connect([&](){
        this->track_list[curtrack]->setAutoCalculateMode(JTrack::AutoCalculateMode::instance);
        jsdata["trackmode"][std::to_string(curtrack)] = JTrack::AutoCalculateMode::instance;
        recalculateAllTrack(curtrack, JTrack::AutoCalculateMode::instance);
    });
    autocalc_none->clickItemSig.connect([&](){
        this->track_list[curtrack]->setAutoCalculateMode(JTrack::AutoCalculateMode::none);
        jsdata["trackmode"][std::to_string(curtrack)] = JTrack::AutoCalculateMode::none;
        recalculateAllTrack(curtrack, JTrack::AutoCalculateMode::none);
    });
    autocalc_occurrence->clickItemSig.connect([&](){
        this->track_list[curtrack]->setAutoCalculateMode(JTrack::AutoCalculateMode::occurence);
        jsdata["trackmode"][std::to_string(curtrack)] = JTrack::AutoCalculateMode::occurence;
        recalculateAllTrack(curtrack, JTrack::AutoCalculateMode::occurence);
    });

    number_of_tracks=0;

    autocomplete_instr_list = new List();


    autocomplete_dialog = new  Dialog(new Vbox(autocomplete_instr_list)); //= new Dialog(list);
    autocomplete_dialog->setAttr("RESIZE","NO");
    autocomplete_dialog->setAttr("MENUBOX","NO");
    autocomplete_dialog->setAttr("MAXBOX","NO");
    autocomplete_dialog->setAttr("MINBOX","NO");
    autocomplete_dialog->setAttr("NOFLUSH","Yes");


    autocomplete_instr_list->keyPressedSig.connect([=](int key) {
        switch(key) {
        case K_ESC:
            this->autocomplete_dialog->hide();
            break;
        case K_CR:
            this->setInstr(autocomplete_instr_list->getSelectedItem());
            break;
        case K_BS:
        {
            std::string cur(track_list[curtrack]->getValueAt(curlin,curcol));
            cur = cur.substr(0,cur.size()-1);
            track_list[curtrack]->setValueAtCurrentCell(cur.c_str());
            track_list[curtrack]->redraw();
            this->displayInstrumentList(curtrack,curlin);
            break;
        }
        default:
        {
            if(iup_isprint(key)) {
          //          std::cout << "CHAR : " << (char)key << std::endl;
                    std::string current = std::string(track_list.at(curtrack)->getValueAt(curlin,curcol)) ;
                    current += (char)key;
                    track_list.at(curtrack)->setValueAtCurrentCell(current.c_str());
                    track_list.at(curtrack)->redraw();
                    this->displayInstrumentList(curtrack, curlin);
            }
        }
            break;
        }
    });

    autocomplete_instr_list->doubleClickedSig.connect([=](int index, const char *val) {
       this->setInstr(val);
    });

    //controllers
    lignbr=new SpinBox<int>(8,1,NULL,1);
    seqnbr=new SpinBox<int>(1,1,NULL,1);
    tracknbr=new SpinBox<int>(1,1,NULL,1);
    grid_step = new SpinBox<int>(0,0,NULL,1);

    clearSeqBut = new Button("Clear Sequence", "IUP_EditErase");
    clearSeqBut->buttonClickOffSig.connect([&](){
        for(size_t i = 0; i < track_list.size(); i++) {
            this->clearTrackData(i);
        }
        this->recallSeq(seqnbr->getValue());
    });

    buttons_box = new Hbox(clearSeqBut);
    buttons_frame = new Frame(buttons_box);
    buttons_frame->setAttr("TITLE","Controls");
    buttons_frame->setAttr("FONT", "Arial, Bold 10");

    //frames
    l_frame=new Frame(lignbr);
    l_frame->setAttr("TITLE","LIGNBR");
    l_frame->setAttr("FONT","Arial, Bold 10");
    s_frame=new Frame(seqnbr);
    s_frame->setAttr("TITLE","SEQNBR");
    s_frame->setAttr("FONT","Arial, Bold 10");
    t_frame=new Frame(tracknbr);
    t_frame->setAttr("TITLE","TRACKNBR");
    t_frame->setAttr("FONT","Arial, Bold 10");
    grid_frame = new Frame(grid_step);
    grid_frame->setAttr("TITLE","GRID STEP");
    grid_frame->setAttr("FONT","Arial, Bold 10");

    scripter_button = new Button(NULL, "IUP_Lua");
    scripter_button->setAttr("TIP","Lua scripter dialog");
    scripter_button->setAttr("FONT","Arial, 8");
    scripter_button->setAttr("ALIGNMENT","ARIGHT:ARIGHT");
    scripter_button->setAttr("PADDING","0x3");

    control_box=new Hbox(s_frame,t_frame,l_frame ,grid_frame, buttons_frame, &fill_control ,  scripter_button);
    control_box->setAttr("GAP","10");

    //tracklist
    addTrack();
    track_box->setAttr("GAP","6");

    //tempo_track
    tempo_track=new JTempo(8);
    //tempo_track->setAttr("MAXSIZE","150x");
    tempo_track->setAttr("EXPAND","VERTICAL");
    tempo_track->clearTrackSig.connect(this,&JTrackSet::clearTrackCallback);
    tempo_track->emitValueSig.connect(this, &JTrackSet::setTempoValue);
    tempo_track->emitCellSig.connect(this, &JTrackSet::cellChanged);

    expander=new Expander(tempo_track);
    expander->setAttr("BARPOSITION","RIGHT");
    expander->setAttr("TITLE","Show Tempo");
    expander->setAttr("STATEREFRESH","NO");

    m_fill = new Fill();
    m_fill->setAttr("EXPAND","YES");
    sb=new ScrollBox(track_box);


    hbox=new Hbox(&organizer_expander, sb, m_fill, expander);

    this->push(control_box,hbox);

    //connections
    lignbr->postValueChangedSig.connect(this,&JTrackSet::lignbrChanged);
    tracknbr->postValueChangedSig.connect(this,&JTrackSet::setNumTracks);

    //seqnbr->preValueChangedSig.connect([this](int  /*c_seq */){
      //  this->saveAt(getScoreDataPath());
    //});


    seqnbr->postValueChangedSig.connect(this,&JTrackSet::recallSeq);

    grid_step->postValueChangedSig.connect([&](int value) {
   //     std::cout << "GRID STEP :: " << value << std::endl;
        if(seqnbr->getValue() >0) this->jsdata["grid_step"][std::to_string(seqnbr->getValue())] = value;
        for(auto & it : track_list) {
                it->colorLines(value);
                it->redrawMat();
        }
        tempo_track->colorLines(value);
        tempo_track->redrawMat();
    });

    //open script dialog
    this->scripter_view=new JScripterView(&jsdata);

    scripter_view->calculateSeqTrack.connect( [&](int s, int t) {
      recalculateTrackAtSeq(s,t);
    });

    scripter_view->calculateSeq.connect([&](int s) {
       recalculateSeq(s);
    });


    this->scripter_dialog = new Dialog(scripter_view);
    scripter_view->scriptedDoneSig.connect([&](){
        this->recallSeq(this->seqnbr->getValue());
        //for(std::size_t i = 0; i < track_list.size(); i++) recalculateAllTrack(int(i),track_list[i]->getAutoCalculateMode());
    }) ;


    scripter_button->buttonClickOffSig.connect([&](){
        updateOrcList();
        this->scripter_dialog->setAttr("SIZE","700x300");
        this->scripter_dialog->show();
        this->scripter_view->setOrcPointer(&orc_list);
        this->scripter_view->setOrcKeywords();
    });

    /*
    track_list.front()->scrollSig.connect([&](int line) {
        for(std::size_t i = 1; i<track_list.size(); i++) {
                track_list[i]->scrollTo(line);
        }
        tempo_track->scrollTo(line);
    });
    */

    tempo_track->scrollSig.connect([&](int line) {
        for(auto &it: track_list) it->scrollTo(line);
    });


    //recallFromFile("res/score_data.json");
    updateOrcList();
    loadFromFile(getScoreDataPath());

}


JTrackSet::~JTrackSet()
{

    delete m_fill;

    delete scripter_button;
    delete scripter_view;
    delete scripter_dialog;

    delete autocalc_instance;
    delete autocalc_none;
    delete autocalc_occurrence;
    delete autocalc_submenu;
    delete autocalc_menu;
    delete track_parameters;
    delete clearTrackItem;
    delete deleteTrackItem;

    for(auto & it : track_list) {
        delete it; // see if I need to disconnect all signals (not sure)
    }

    delete tempo_track;
    delete t_frame;
    delete l_frame;
    delete s_frame;
    delete tracknbr;
    delete lignbr;
    delete seqnbr;
    delete clearSeqBut;
    delete hbox;
    delete sb;
    delete control_box;
    delete expander;
    delete autocomplete_instr_list;
    delete autocomplete_dialog;

    delete buttons_box;
    delete buttons_frame;
}

bool JTrackSet::setCurlin(int line)
{
    if(line < 0 || line > lignbr->getValue() )
    {
        return false;
    } else {
        curlin = line;
        return true;
    }

}


int JTrackSet::getCurrentSeq()
{
    return seqnbr->getValue();
}

int JTrackSet::getCurrentLine()
{
    return curlin;
}

int JTrackSet::getCurrentTrack()
{
    return curtrack;
}

int JTrackSet::getCurrentColumn()
{
    return curcol;
}

void JTrackSet::setColnbr(int track, int nbr)
{
    if(size_t(track) >= track_list.size()) return;
    track_list[track]->setNumcol(nbr);
}

void JTrackSet::setSize(int width, int height)
{
    hbox->setRasterSize(width,height);
    Vbox::setRasterSize(width,height);
}

void JTrackSet::setMaxSize(int width, int height)
{
    hbox->setMaxSize(width,height);
    Vbox::setMaxSize(width,height);
}

//methods
void JTrackSet::setNumlin(int lin)
{
    //lignbr->setAttrInt("VALUE",lin);
    lignbr->setValue(lin);
    //for(int i=0;i<track_list.size();i++) track_list.at(i)->setNumlin(lin);
    //tempo_track->setNumlin(lin);
}

void JTrackSet::redrawTracks()
{
    for(auto & it : track_list)
    {
        it->enableResize(0);
    }
}

void JTrackSet::setNumTracks(int num)
{
    if(num>number_of_tracks) {
        for(int i=number_of_tracks;i<num;i++) {
            addTrack();
        }
    } else if(num<number_of_tracks) {
        for(int i=number_of_tracks;i>num;i--) {
            removeTrack();
        }
    }
    number_of_tracks=num;
    //grid_step->postValueChangedSig(grid_step->getValue());
    jsdata["tracknbr"]=num;
}

void JTrackSet::addTrack()
{
    track_list.push_back(new JTrack(lignbr->getAttrInt("VALUE"),number_of_tracks));
    track_list.back()->emitValueSig.connect(this,&JTrackSet::storeValue);
    track_list.back()->colnbrChangedSig.connect(this,&JTrackSet::colnbrChanged);
    track_list.back()->isVisibleSig.connect(this,&JTrackSet::isTrackVisible);
    track_list.back()->displayInstrumentListSig.connect(this,&JTrackSet::displayInstrumentList);
    track_list.back()->autoCalculateModeChangedSig.connect(this, &JTrackSet::recalculateAllTrack);
    track_list.back()->displayTrackParameterSig.connect(this, &JTrackSet::showTrackParameters);
    track_list.back()->emitCellSig.connect(this, &JTrackSet::cellChanged);
    track_list.back()->clearTrackSig.connect(this, &JTrackSet::clearTrackData);
    track_list.back()->emitVimBufferRegister.connect(this,&JTrackSet::vimBufferCallback);
    track_list.back()->soloSig.connect(this, &JTrackSet::soloSlot);
    track_list.back()->copyFullLine.connect(this, &JTrackSet::copyFullLineSlot);

    jsdata["trackmode"][std::to_string(curtrack)] = track_list.back()->getAutoCalculateMode();
    number_of_tracks++;
    if(track_box==nullptr) {
        track_box=new Hbox(track_list.back());
    } else {
        track_box->push(track_list.back());
        track_box->refresh();
    }

    //set colnbr for new track - in each seq
    if(jsdata.find("seqnbr") == jsdata.end()) return;
   int number_of_seq = jsdata["seqnbr"].get<int>();
   for(int i= 1; i <= number_of_seq ; i++) {
       std::string s_str = std::to_string(i);
       std::string t_str = std::to_string(track_list.size() - 1);

       if(jsdata["colnbr"].find(s_str) != jsdata["colnbr"].end() && jsdata["colnbr"][s_str].find(t_str) != jsdata["colnbr"][s_str].end())
       {}
       else {
        jsdata["colnbr"][s_str][t_str] = 8;
       }
        //jsdata["score"][s_str][t_str];
   }

   track_list.back()->colorLines(grid_step->getValue());
}

// TO IMPLEMENT REMOVE TRACK AT #
void JTrackSet::removeTrack()
{
    if(track_list.size()>1) {
        std::string t_str = std::to_string(track_list.size() -1 );

//        track_list.front()->scrollSig.disconnect();
        //remove data of this track in every seq
        for(auto & it : jsdata["colnbr"].items()) {
            if(it.value().find(  t_str ) != it.value().end() )
                it.value().erase( t_str );
            if(jsdata["colnbr"][it.key()].find(t_str) != jsdata["colnbr"][it.key()].end()) jsdata["colnbr"][it.key()].erase(t_str);
        }

        number_of_tracks--;
        delete track_list.back();
        track_list.pop_back();

    }
    //remove associated data
}

//      ERASE USELESS DATA IN CURRENT SEQUENCE WHEN LIGNBR IS REDUCED
void JTrackSet::lignbrChanged(int val) {
    std::string sq=std::to_string(seqnbr->getAttrInt("VALUE"));
    int nlin=jsdata["lignbr"][sq].get<int>();
    if(val<nlin) {
        for(int l=val+1;l<=nlin;l++) {
                for(size_t t=0;t<track_list.size();t++) {
                    if(jsdata["score"][sq][std::to_string(t)].find(std::to_string(l)) != jsdata["score"][sq][std::to_string(t)].end())
                    jsdata["score"][sq][std::to_string(t)].erase(std::to_string(l));
                }
                if(jsdata["tempo"][sq].find(std::to_string(l)) != jsdata["tempo"][sq].end())
                        jsdata["tempo"][sq].erase(std::to_string(l));
        }
    }

    for(size_t i=0;i<track_list.size();i++) {
        track_list.at(i)->setNumlin(val);
    }
    tempo_track->setNumlin(val);

    jsdata["lignbr"][std::to_string(seqnbr->getAttrInt("VALUE"))]=val;

//    grid_step->postValueChangedSig(grid_step->getValue());
}

void JTrackSet::updateScripterList()
{
    scripter_view->updateList();
}

//      ERASE USELESS DATA WHEN COLNBR IS REDUCED IN A TRACK -- ONLY FOR TRACK IN CURRENT SEQUENCE
void JTrackSet::colnbrChanged(int t_idx, int col) {
    std::string c_seq = std::to_string(seqnbr->getValue());
    std::string tk=std::to_string(t_idx);

    if(jsdata["colnbr"].find(std::to_string(seqnbr->getValue())) == jsdata["colnbr"].end() || jsdata["colnbr"][c_seq].find(std::to_string(t_idx)) == jsdata["colnbr"][c_seq].end()) return;
    int colnbr=jsdata["colnbr"][std::to_string(seqnbr->getAttrInt("VALUE"))][std::to_string(t_idx)];
    //std::cout << "cols : " << colnbr << " " << col << std::endl;
    if(col<colnbr) { //erase data
        int numlin=jsdata["lignbr"][c_seq].get<int>();
        if(jsdata.find("score")!=jsdata.end()
                && jsdata["score"].find(c_seq)!= jsdata["score"].end()
                && jsdata["score"][c_seq].find(tk)!=jsdata["score"][c_seq].end()) {
            for(int l=0;l<numlin;l++) {
                for(int c=col+1;c<=colnbr;c++) {
                    if(jsdata["score"][c_seq][tk][std::to_string(l+1)].find(std::to_string(c)) != jsdata["score"][c_seq][tk][std::to_string(l+1)].end())
                    jsdata["score"][c_seq][tk][std::to_string(l+1)].erase(std::to_string(c));
                }
            }
        }
    }

    jsdata["colnbr"][c_seq][tk]=col;
    //grid_step->postValueChangedSig(grid_step->getValue());
    track_list.at(t_idx)->colorLines(grid_step->getValue());
}

void JTrackSet::cellChanged(int t_idx, int lin, int col)
{
       curlin = lin;
       curcol = col;
       curtrack = t_idx;

       if(t_idx == -1) return; // tempo track

       std::string curSeq = seqnbr->getAttrStr("VALUE");
       std::string curTk = std::to_string(t_idx);
       std::string curL = std::to_string(lin);
       //std::string val = jsdata["score"][curSeq][curTk][curL][std::to_string(col)].get<std::string>();

       if(jsdata["score"][curSeq].find(curTk) == jsdata["score"][curSeq].end()) return;

       if(jsdata["score"][curSeq][curTk].find(curL) != jsdata["score"][curSeq][curTk].end()) { // look if line exists, and i yes, get it's instr name, to retrieve instrument description
           json line_data = jsdata["score"][curSeq][curTk][curL];
           if(line_data.find("1") != line_data.end()) {
               std::string instr = line_data["1"].get<std::string>();
               std::vector<std::string> vals = getInstrDescription(instr);
               track_list.at(t_idx)->setDescriptionList(&vals);
           }
        //   std::cout << "cell changed" << std::endl;
           autoCalculateDuration(seqnbr->getValue(), t_idx, lin);
       }

       cellSig(seqnbr->getValue(), curlin, jsdata);
}


void JTrackSet::clearTrackData(int t_idx)
{
    std::cout << "Track Index : " << t_idx << std::endl;
    std::string trackStr= std::to_string(t_idx);
    std::string curSeq(seqnbr->getAttrStr("VALUE"));


    if(t_idx == -1) {//tempo
        if(jsdata["tempo"].find(curSeq) != jsdata["tempo"].end()) jsdata["tempo"].erase(curSeq);
    } else {
        if(jsdata["score"].find(curSeq) == jsdata["score"].end()) return;
        if(jsdata["score"][curSeq].find(trackStr) != jsdata["score"][curSeq].end()) {
            jsdata["score"][curSeq].erase(trackStr);
        }
    }
}


// NOT IMPLEMENTED YET
void JTrackSet::clearTrackCallback(int idx)
{
    //to rewrite with jsdata
    if(idx==-1) { // tempo
    } else {
    }
}

void JTrackSet::copyFullLineSlot(int index)
{
    //
    std::cout << "COPY FULL LINE SLOT CALLED" << std::endl;;
    if(index == 0) {
        std::cout << " copy " << std::endl;
        cp_sq_pos = seqnbr->getValue();
        cp_lin_pos = curlin;
    }  else if(index == 1) {
        std::cout << "paste " << std::endl;
        if(cp_sq_pos == 0 || cp_lin_pos == 0) return;
        std::string sq = std::to_string(cp_sq_pos);
        std::string ln = std::to_string(cp_lin_pos);
        std::string cl = std::to_string(curlin);
        std::string cs = std::to_string(seqnbr->getValue());
        if(jsdata["score"].find(sq) != jsdata["score"].end()) {
            for(auto & t_it : jsdata["score"][sq].items()) {
                std::cout << t_it.key() << std::endl;
                int t_idx = getNumber<int>(t_it.key());
                if(t_it.value().find(ln) != t_it.value().end()) {
                    std::cout << "line found " << std::endl;
                    json l = t_it.value()[ln];
                    for(auto & colit : jsdata["colnbr"][sq].items()) {
                        int t = getNumber<int>(colit.key());
                        int v = colit.value().get<int>();
                        if(v > track_list[t]->getNumcol()) {
                            track_list[t]->setNumcol(v);
                        }
                    }
                    if(l.find("1") != l.end() && (!l["1"].is_null())) {
                        std::cout << "line is valid : " << std::endl;
                        for(auto & c_it : l.items()) {
                            std::string str = c_it.value().get<std::string>();
                            std::cout << "str is : " << str << std::endl;
                            std::cout << "copying : " << sq << " " << ln << " to : " << cs << " " << cl << std::endl;
                            jsdata["score"][cs][t_it.key()][cl][c_it.key()] = str;
                            track_list[t_idx]->setValueAt(curlin, getNumber<int>(c_it.key()), str.c_str());
                            track_list[t_idx]->redrawMat();
                        }
                    }
                }
            }
        }
    }
}


bool JTrackSet::soloSlot(int t_idx, int status)
{
    if(status == 1) {
    if(has_solo) return false;
    has_solo = true;
    solo_idx = t_idx;
    return true;
    } else {
        std::cout << "no solo " << std::endl;
        has_solo = false;
        return false;
    }
}

//              LOAD ALL DATA FROM A JSON FILE
void JTrackSet::loadFromFile(std::string filepath)
{
    scripter_view->loadScriptData();
    //load or open json file
    std::ifstream sz(filepath, std::ios::binary | std::ios::ate );
    int size=sz.tellg();
    sz.close();

//    std::cout << "file size : " << size << std::endl;

    if(size>0) {
        std::ifstream fs(filepath);
        jsdata.clear();
        fs >> jsdata;
        fs.close();

        clearAllTrack();

        if(jsdata.find("seqnbr")==jsdata.end()) jsdata["seqnbr"]=1;
        if(jsdata.find("tracknbr")==jsdata.end()) jsdata["tracknbr"]=1;

        std::cout << "Tracker -- recall from trackset" << std::endl;
        //Removed next line because now it's done in main at startup, in the dialog.openDialogSig (avoid to do it twice)
        //recallSeq(seqnbr->getAttrInt("VALUE"));

    } else { // init
        init();
        std::cout << "prepare for init seq" << std::endl;
    }
}

void JTrackSet::initSeq() { // initialize data for current seq
    std::string sq=std::to_string(seqnbr->getAttrInt("VALUE"));
    //std::cout << "init current seq: " << sq << std::endl;
    if(jsdata.find("lignbr") == jsdata.end() || jsdata["lignbr"].find(sq) == jsdata["lignbr"].end())
            jsdata["lignbr"][sq]=8;
    //for each track, set colnbr
    for(std::size_t i=0;i<track_list.size();i++) {
        if(jsdata.find("colnbr") != jsdata.end() &&
                jsdata["colnbr"].find(sq) != jsdata["colnbr"].end() &&
                jsdata["colnbr"][sq].find(std::to_string(i)) != jsdata["colnbr"][sq].end())
            continue;
        //else
        jsdata["colnbr"][sq][std::to_string(i)]=8;
    }

    if(jsdata.find("seqnbr")==jsdata.end()) jsdata["seqnbr"]=1;
    if(jsdata.find("tracknbr") == jsdata.end()) jsdata["tracknbr"]=1;
    if(jsdata.find("grid_step")== jsdata.end() || jsdata["grid_step"].find(sq) == jsdata["grid_step"].end())
        jsdata["grid_step"][sq]=0;

}

void JTrackSet::init() { // init data
    std::string sq= std::to_string(seqnbr->getAttrInt("VALUE"));
    std::string tk=std::to_string(0);

    jsdata["seqnbr"]=1;
    jsdata["tracknbr"]=1;
    jsdata["lignbr"][sq]=8;
    jsdata["colnbr"][sq][tk]=8;
    jsdata["score"];
    jsdata["tempo"];
}

void JTrackSet::clearAll()
{
    jsdata.clear();
    seqnbr->setValue(1);
    tracknbr->setValue(1);
    lignbr->setValue(8);
    initSeq();
    recallSeq(seqnbr->getValue());
    scripter_view->clearAll();
}

void JTrackSet::storeValue(int track_idx, int lin, int col, const char *value)
{
    //auto t1 = std::chrono::high_resolution_clock::now();


    std::string c_seq=std::string(seqnbr->getAttr("VALUE"));
    std::string c_tk=std::to_string(track_idx);
    std::string l=std::to_string(lin);
    std::string c=std::to_string(col);

    if(std::string(value) == std::string("NULL") /*|| std::string(value) == std::string("") */ ) { // erase from data
        std::cout << "Null string emitted" << std::endl;
        if(jsdata.find("score") != jsdata.end() &&
                jsdata["score"].find(c_seq) != jsdata["score"].end() &&
                jsdata["score"][c_seq].find(c_tk) != jsdata["score"][c_seq].end() &&
                jsdata["score"][c_seq][c_tk].find(l) != jsdata["score"][c_seq][c_tk].end() &&
                jsdata["score"][c_seq][c_tk][l].find(c) != jsdata["score"][c_seq][c_tk][l].end()) {
            jsdata["score"][c_seq][c_tk][l].erase(c);
            if(col == 1 ) {
                jsdata["score"][c_seq][c_tk].erase(l);
                recallSeq(seqnbr->getAttrInt("VALUE")); // also recalculate (because deletion cause false calculations
            }
        }
    } else { // store in data
    //    std::cout << "STORE IN DATA" << track_idx << " " << lin << " " << col << "  " << value << std::endl;
        jsdata["score"][c_seq][c_tk][l][c]=std::string(value);


        if(jsdata["score"][c_seq][c_tk][l]["1"].is_null() || jsdata["score"][c_seq][c_tk][l]["1"].get<std::string>().empty()) {
            jsdata["score"][c_seq][c_tk].erase(l);
    //        std::cout << "STORE ERASE :: " << std::endl;
            recallSeq(seqnbr->getAttrInt("VALUE"));
        }


        if(col==1) {
            std::string val(value);
            if(orc_list["instr"].find(val) == orc_list["instr"].end()) jsdata["score"][c_seq][c_tk].erase(l);
        }

        if(col == 2 ||   col == 3 || col == 4)
        {
            autoCalculateDuration(seqnbr->getAttrInt("VALUE"), track_idx, lin);
        }

    }

}

void JTrackSet::setTempoValue(int lin, int col, const char *val)
{
    std::string c_seq=std::string(seqnbr->getAttr("VALUE"));
    std::string l=std::to_string(lin);
    std::string c=std::to_string(col);

    if(val != "NULL") {
        jsdata["tempo"][c_seq][l][c]=std::string(val);
    } else {
        if(jsdata.find("tempo") != jsdata.end() &&
                jsdata["tempo"].find(c_seq) != jsdata["tempo"].end() &&
                jsdata["tempo"][c_seq].find(l) != jsdata["tempo"][c_seq].end() &&
                jsdata["tempo"][c_seq][l].find(c) != jsdata["tempo"][c_seq][l].end()) {
            jsdata["tempo"][c_seq][l].erase(c);
        }
    }
}


void JTrackSet::storeSeq(int /*c_seq*/)   //called just before the seq value is changed
{
}


// This method cleans data. It is a workaround because some ugly data seems to appear in my json sometimes...
void JTrackSet::cleanData()
{
//    std::cout << "BEFORE CLEAN " << jsdata["colnbr"].dump(4) << std::endl;


    int tk_nbr = 1;
    if(jsdata.find("tracknbr") != jsdata.end()) {
        tk_nbr = jsdata["tracknbr"].get<int>();
    }
    int sq_nbr = 1;
    if(jsdata.find("seqnbr") != jsdata.end()) {
        sq_nbr = jsdata["seqnbr"].get<int>();
    }

    for(auto & it : jsdata.items()) {
        std::vector<json::object_t::key_type> seqToRemove;
        if(it.key() == std::string("colnbr")) {
            for(auto & s_it : it.value().items()) {
                int s_idx = getNumber<int>(s_it.key());
                if(s_idx > sq_nbr || s_idx <= 0 ) {
                    //it.value().erase(s_it.key());
                    seqToRemove.push_back(s_it.key());
                    continue;
                }
                std::vector<json::object_t::key_type> trackToRemove;
                for(auto & t_it : s_it.value().items()) {
                        int t_idx = MiupUtil::string::getNumber<int>(std::string(t_it.key()));
                        if( (t_idx >= (tk_nbr) ) || (t_idx < 0 ) ) {
                            //std::cout << "CLEAN DATA - erase colnbr seq " << s_idx << " and track : " << t_idx << " and track number : " << tk_nbr << std::endl;
                            //s_it.value().erase(t_it.key());
                            trackToRemove.push_back(t_it.key());
                        }
                }
                removeJSEntries(&s_it.value(), trackToRemove);
            }
            removeJSEntries(&it.value(), seqToRemove);
        } else if(it.key() == std::string("grid_step")) {
            std::vector<json::object_t::key_type> seqToRemove;
            for(auto & s_it : it.value().items()) {
                int s_idx = getNumber<int>(s_it.key());
                if(s_idx > sq_nbr || s_idx <=0) {
                    //it.value().erase(s_it.key());
                    seqToRemove.push_back(s_it.key());
                }
            }
            removeJSEntries(&it.value(), seqToRemove);
        } else if(it.key() == std::string("lignbr")) {
            std::vector<json::object_t::key_type> seqToRemove;
            for(auto & s_it : it.value().items()) {
                int s_idx = getNumber<int>(s_it.key());
                if(s_idx > sq_nbr || s_idx <= 0) {
                    //it.value().erase(s_it.key());
                    seqToRemove.push_back(s_it.key());
                }
            }
            removeJSEntries(&it.value(), seqToRemove);
        } else if(it.key() == std::string("tempo")) {
            std::vector<json::object_t::key_type> seqToRemove;
            for(auto & s_it : it.value().items()) {
                int s_idx = getNumber<int>(s_it.key());
                if(s_idx > sq_nbr || s_idx <= 0) {
                    //it.value().erase(s_it.key());
                    seqToRemove.push_back(s_it.key());
                    continue;
                }
                if(s_it.value().is_null()) continue;
                std::vector<json::object_t::key_type> lineToRemove;
                for(auto & l_it : s_it.value().items()) {
                    int l_idx = getNumber<int>(l_it.key());
                    int l_nbr = 8;
                    if(jsdata["lignbr"].find(s_it.key()) != jsdata["lignbr"].end()) l_nbr = jsdata["lignbr"][s_it.key()].get<int>();
                    if(l_idx > l_nbr || l_idx <=0) {
                        //s_it.value().erase(l_it.key());
                        lineToRemove.push_back(l_it.key());
                        continue;
                    }
                    if(l_it.value().is_null() || l_it.value().empty()) {
                        lineToRemove.push_back(l_it.key());
                        continue;
                    }
                    // iter on two cells as well
                    std::vector<json::object_t::key_type> cellToRemove;
                    for(auto & c_it : l_it.value().items()) {
                       if(c_it.value().is_null() || c_it.value().empty() || (c_it.value().get<std::string>() == std::string("") ) )
                       {
                           cellToRemove.push_back(c_it.key());
                       }
                    }
                    removeJSEntries(&l_it.value(), cellToRemove);
                }
                removeJSEntries(&s_it.value(), lineToRemove);
            }
            removeJSEntries(&it.value(), seqToRemove);
        } else if(it.key() == std::string("score")) {
            std::vector<json::object_t::key_type> seqToRemove;
            for(auto & s_it : it.value().items()) {
                int s_idx = getNumber<int>(s_it.key());
                //std::cout << "s_idx : " << s_idx << "  & sq nbr : " << sq_nbr << std::endl;
                if(s_idx > sq_nbr || s_idx <= 0) {
                    it.value().erase(s_it.key());
                    continue;
                }
                if(s_it.value().is_null()) continue;


                std::vector<json::object_t::key_type> trackToRemove;
                for(auto & t_it : s_it.value().items()) {
                    int t_idx = getNumber<int>(t_it.key());

            //    std::cout << "t_idx : " << t_idx << "  & tk nbr : " << tk_nbr << std::endl;
                    if(t_idx > (tk_nbr-1) || t_idx < 0) {
                        //s_it.value().erase(t_it.key());
                        trackToRemove.push_back(t_it.key());
                        continue;
                    }
                    if(t_it.value().is_null()) continue;

                    std::vector<json::object_t::key_type> lineToRemove;
                    for(auto & l_it : t_it.value().items()) {
                        int l_idx = getNumber<int>(l_it.key());

                        if(l_it.value().is_null() || l_it.value().empty()) {
                            lineToRemove.push_back(l_it.key());
                            continue;
                        }

                        if( (l_it.value().find("1") == l_it.value().end() ) || (l_it.value()["1"].is_null() ) || (l_it.value()["1"].get<std::string>() == std::string("")) ) {
                            //t_it.value().erase(l_it.key());
                            lineToRemove.push_back(l_it.key());
                            continue;
                        }

                        int l_nbr = 8;
                        if(jsdata["lignbr"].find(s_it.key()) != jsdata["lignbr"].end()) l_nbr = jsdata["lignbr"][s_it.key()].get<int>();
                        if(l_idx > l_nbr || l_idx <= 0) {
                            //t_it.value().erase(l_it.key());
                            lineToRemove.push_back(l_it.key());
                            continue;
                        }

                        if(l_it.value().is_null()) continue;


                        std::vector<json::object_t::key_type> cellToRemove;
                        for(auto & c_it : l_it.value().items()) {
                                int c_idx = getNumber<int>(c_it.key());
                                int col_nbr = 8;
                                if(jsdata["colnbr"][s_it.key()].find(t_it.key()) != jsdata["colnbr"][s_it.key()].end()) {
                                    col_nbr = jsdata["colnbr"][s_it.key()][t_it.key()].get<int>();
                                }
                                if(c_idx > col_nbr || c_idx <=0) {
                                    //l_it.value().erase(c_it.key());
                                    cellToRemove.push_back(c_it.key());
                                }
                        }
                        removeJSEntries(&l_it.value(), cellToRemove);
                    }
                    removeJSEntries(&t_it.value(), lineToRemove);
                }
                removeJSEntries(&s_it.value(), trackToRemove);
            }
            removeJSEntries(&it.value(), seqToRemove);
        } else if(it.key() == std::string("trackmode")) {
            for(auto & t_it : it.value().items()) {
                int t_idx = getNumber<int>(t_it.key());
                if(t_idx > (tk_nbr - 1) || t_idx < 0) it.value().erase(t_it.key());
            }
        }

    }

 //   std::cout << "AFTER CLEAN " << jsdata["colnbr"].dump(4) << std::endl;
}

void JTrackSet::recallSeq(int c_seq)
{
    if(c_seq < 1) return;

    cleanData();

    //std::cout << "BEFORE RECALL " << jsdata["colnbr"].dump(4) << std::endl;

    std::string sq=std::to_string(c_seq);

    //first modify seqnbr in data if new seq
    if(jsdata.find("seqnbr") != jsdata.end()) {
     if(jsdata["seqnbr"].is_null()) {
         jsdata["seqnbr"]=1;
        // std::cout << "Init seqnbr to 1 !! " << std::endl;
     }
       if(c_seq > jsdata["seqnbr"].get<int>())
          jsdata["seqnbr"] = c_seq;
    }


    // Then clear track display
    clearAllTrack();


    //std::cout << "AFTER RECALL " << jsdata["colnbr"].dump(4) << std::endl;

        //recall tracknb
        if(jsdata.find("tracknbr")!=jsdata.end()) {
            tracknbr->setValue(jsdata["tracknbr"].get<int>());
            //setNumTracks(jsdata["tracknbr"].get<int>());
        }

        if(jsdata.find("tempo") == jsdata.end() ||  jsdata["tempo"].find("1") == jsdata["tempo"].end() ||
                 jsdata["tempo"]["1"].find("1") == jsdata["tempo"]["1"].end() ||
                 jsdata["tempo"]["1"]["1"].find("1") == jsdata["tempo"]["1"]["1"].end()  ||
                 jsdata["tempo"]["1"]["1"]["1"].get<std::string>() == std::string(""))
        {
            jsdata["tempo"]["1"]["1"]["1"]="60";
        }



        initSeq();

        int t_idx,l_idx,c_idx, lgnbr;

        // check if finds number of line, else define it.
        if(jsdata.find("lignbr") != jsdata.end()
                && jsdata["lignbr"].find(sq) != jsdata["lignbr"].end())
        {
            lgnbr=jsdata["lignbr"][sq].get<int>();
        } else
        {
          lgnbr = 8;
          jsdata["lignbr"][sq] = lgnbr;
        }
         lignbr->setAttrInt("VALUE",lgnbr);
         setNumlin(lgnbr);

        if(jsdata.find("colnbr") != jsdata.end()
                && jsdata["colnbr"].find(sq) != jsdata["colnbr"].end())
        {
            t_idx=0;
            for(auto & t_it : jsdata["colnbr"][sq].items()) {
                t_idx=getNumber<int>(std::string(t_it.key()).c_str());
                if(t_idx>=track_list.size()) break;
               // std::cout << "RECALL SEQ : colnbr at " << sq << " " << t_idx << " : " << t_it.value().get<int>() << std::endl;
                track_list[t_idx]->setNumcol(t_it.value().get<int>());

                        for(int l = 1 ; l <= lignbr->getValue() ; l++) {
                            track_list[t_idx]->setValueAt(l, 0,std::to_string(l).c_str());
                        }
            }
        }

        //matrix cells data are stored inside the "score" field
        // Here, recall it in tracks display
        if(jsdata.find("score") != jsdata.end() &&
                jsdata["score"].find(sq) != jsdata["score"].end()) {
                for(auto & t_it : jsdata["score"][sq].items()) {

                   // sortLines(seqnbr->getValue(), getNumber<int>(t_it.key()));

                   for(auto &l_it : t_it.value().items()) {
                       json cur_lin = l_it.value();
                        for(auto &c_it : l_it.value().items()) {
                            t_idx=getNumber<int>(std::string(t_it.key()).c_str());
                            l_idx=getNumber<int>(std::string(l_it.key()).c_str());
                            c_idx=getNumber<int>(std::string(c_it.key()).c_str());
                            if(t_idx < 0 || l_idx <1 || c_idx < 1) continue;

                            std::string val=c_it.value().get<std::string>();
                            if(val != "" && val != std::string("NULL")) {
                                    track_list[t_idx]->setValueAt(l_idx,c_idx, val.c_str());
                            }
                        }
                        if(cur_lin.find("1")!=cur_lin.end()) {
                                if(cur_lin.find("3") == cur_lin.end() ||
                                 cur_lin["3"].is_null() ||
                                        cur_lin["3"].get<std::string>().empty()) {
                                     jsdata["score"][sq][t_it.key()][l_it.key()]["3"]="0";
                                     track_list[t_idx]->setValueAt(l_idx, 3, "0");
                                }

                                 if(cur_lin.find("4") ==cur_lin.end() ||
                                 cur_lin["4"].is_null() ||
                                 cur_lin["4"].get<std::string>().empty())  {
                                     jsdata["score"][sq][t_it.key()][l_it.key()]["4"]="1";
                                     track_list[t_idx]->setValueAt(l_idx, 4, "1");
                                 }
                        }

                   }
                }
        }


        //idem for tempo
        if(jsdata.find("tempo") != jsdata.end() &&
                jsdata["tempo"].find(sq) != jsdata["tempo"].end()) {
                for(auto &l_it : jsdata["tempo"][sq].items()) {
                    for(auto &c_it : l_it.value().items()) {
                        l_idx=getNumber<int>(std::string(l_it.key()).c_str());
                        c_idx=getNumber<int>(std::string(c_it.key()).c_str());
                        std::string val=c_it.value().get<std::string>();
                        if(val != std::string("NULL")) {
                                tempo_track->setValueAt(l_idx,c_idx,val.c_str());
                        }
                    }
                }
        }


        // Recall tracks mode
        if(jsdata.find("trackmode") != jsdata.end()) {
            for(auto & it : jsdata["trackmode"].items()) {
                int track_index = getNumber<int>(it.key());
                JTrack::AutoCalculateMode c_mode = static_cast<JTrack::AutoCalculateMode>(it.value().get<int>());
                if(track_list[track_index]->getAutoCalculateMode() != c_mode)
                        track_list[track_index]->setAutoCalculateMode(  c_mode);
            }
        }



        // according to gridstep, color lines
        if(jsdata.find("grid_step") != jsdata.end() && jsdata["grid_step"].find(sq) != jsdata["grid_step"].end()) {
            int g_step = jsdata["grid_step"][sq].get<int>();
            //grid_step->setValue(g_step);

            grid_step->postValueChangedSig.set_lock(true);
            grid_step->setValue(g_step);
            grid_step->postValueChangedSig.set_lock(false);

            for(auto & it : track_list) {
                it->colorLines(g_step);
            }

            tempo_track->colorLines(g_step);
            //std::cout << "recall grid_step : " << g_step << std::endl;
        }

        // This redraw seems to be responsible to the slow performance when changing seq, as well as the sequence jump
        // See if it is necessary
    //this->redraw();

    saveAt(getScoreDataPath());

    // try to expand m_fill to avoid the resize issue.
    m_fill->redraw();
}

// ONLY CLEARS, DO NOT ERASE
void JTrackSet::clearAllTrack()
{
    for(std::size_t i=0;i<track_list.size();i++)  {
        track_list.at(i)->clearTrack();
    }
    tempo_track->clearTrack();
}


void JTrackSet::removeTrackDataForSeq(int seq, int track)
{
    std::string sq=std::to_string(seq);
    std::string tk=std::to_string(track);

    if((jsdata.find("score")!=jsdata.end())
            && (jsdata["score"].find(sq)!=jsdata["score"].end())
            && (jsdata["score"][sq].find(tk)!=jsdata["score"][sq].end()))
    {
        jsdata["score"][seq].erase(tk);
    }
}

// ?? TO CHECK
void JTrackSet::removeTrackData(int track) {
        int seqmax=MiupUtil::string::getNumber<int>(jsdata["seqnbr"].get<std::string>().c_str());
        for(int i=0;i<seqmax;i++) {removeTrackDataForSeq(i+1,track);}
}

// NOT IMPLEMENTED
void JTrackSet::removeSeqData(int seq)
{
        std::string sq=std::to_string(seq);
        //to complete
}

//json orc list - called before to display instrument list
void JTrackSet::updateOrcList()
{
    std::ifstream sz(getOrcDataPath(), std::ios::binary | std::ios::ate);
    int size=sz.tellg();
    sz.close();

    if(size>0) {
        std::ifstream ifs(getOrcDataPath());
        ifs >> orc_list;
    } else {
        orc_list.clear(); // maybe I should clear at begining of function to avoid missing that an instrument doesn't exist anymore
    }
}


// SAVE FULL SCORE DATA AS JSON
void JTrackSet::saveAt(std::string path)
{
        scripter_view->saveScriptData();
        std::ofstream ofs(path);
        ofs << jsdata.dump(4);
        ofs.close();
        return;

}



void JTrackSet::key_cbk(int t_idx, int lin, int col, int key)
{
//change this method with a menu popup (small menu popup already open)



    //std::cout << buffer << " " << lin << " " << col << std::endl;
/*
    if(col==1 && buffer.size()>1) {
        std::string match;
        int idx=0;
        for(auto & it : orc_list.items()) {
            match=std::string(it.key());
            if(match.find(buffer) != match.npos) {
                std::cout << "Matched : " << match << std::endl;
                idx++;
                IupSetAttribute(drop,MiupUtil::string::getCString(idx),match.c_str());
                IupSetAttribute(drop,"SHOWDROPDOWN","YES");
            }
        }
    }
    */
}


void JTrackSet::isTrackVisible(int t_idx, bool isVisible)
{
    //updateOrcList();
    std::string c_seq=std::string(seqnbr->getAttr("VALUE"));
    std::string c_tk=std::to_string(t_idx);


    if(isVisible) {
        //if( /*jsdata.find("score") != jsdata.end() &&*/
              /*  jsdata["score"].find(c_seq) != jsdata["score"].end() && */
             /*   jsdata["score"][c_seq].find(c_tk) != jsdata["score"][c_seq].end()  ) {*/
            int l,c;
            json cur_data = jsdata["score"][c_seq][c_tk];
            for (auto & l_it : cur_data.items())
            {
                json line = l_it.value();
                std::string l_idx = l_it.key();
                for(auto  &c_it : line.items()) {
                    json cell = c_it.value();
                    std::string c_idx = c_it.key();
                        std::string val=cell.get<std::string>();
                        if(val != "" && val != std::string("NULL")) {
                        l=getNumber<int>(std::string(l_it.key()).c_str());
                        c=getNumber<int>(std::string(c_it.key()).c_str());

                        track_list.at(t_idx)->setValueAt(l,c, val.c_str());

                    }
                }
            }

        }
   // } else {


  //  }
}

//              INSTRUMENT LIST TO DISPLAY
void JTrackSet::displayInstrumentList(int t_idx, int lin, bool full) // shows a menu when writing into the first column of a track, menu filled with all available instruments
{
    autocomplete_dialog->hide();

    std::cout << "display instrument list :  t_idx = " << t_idx << std::endl;
    curtrack=t_idx;
    curlin = lin;
    updateOrcList();

    autocomplete_instr_list->clear();

    std::string val(track_list.at(t_idx)->getValueAt(lin,1)); // to implement, filter according to letter
    std::string pattern = val + ".+";
    std::regex regex(pattern);


            int cnt=0;
            for(auto &it: orc_list["instr"].items()) {
                if(full) {
                    autocomplete_instr_list->push_back(it.key().c_str());
                    cnt++;
                    continue;
                }
                else if( std::regex_match(it.key(), regex)) {
                        autocomplete_instr_list->push_back(it.key().c_str());
                        cnt++;
                }
            }

  std::cout << "LIST COUNT : " <<  autocomplete_instr_list->getAttr("COUNT") << std::endl;

    autocomplete_instr_list->setAttr("EXPAND","YES");
    autocomplete_instr_list->setAttr("SCROLLBAR","VERTICAL");

    if(cnt>0) {

        std::pair<int,int> coordinates=track_list.at(t_idx)->getCellPosition(lin,1);

        std::cout << "Coordinates : " << coordinates.first << " " << coordinates.second << std::endl;
        if(coordinates.first==-1) {
            std::cout << "Invalid coordinates " << std::endl;
            return;
        }

        //autocomplete_dialog->show(); // must first call show, since GTK doesn't refresh position on hidden dialog.
        //std::string screenposition = std::to_string(coordinates.second) + "," + std::to_string(coordinates.first);
        //autocomplete_dialog->setAttr("POSITION",screenposition.c_str());

        autocomplete_dialog->show();
        autocomplete_dialog->showXY(coordinates.first, coordinates.second);
        //autocomplete_dialog->hide();
        //autocomplete_dialog->showXY(coordinates.first, coordinates.second);
    }
}


//              WHEN INSTRUMENT LIST SELECTED - SET INSTRUMENT
void JTrackSet::setInstr(const char *val)
{ // if selected then

    //autocomplete_dialog->hide(); // dont know why, but must be after setting attributes, and before select
    autocomplete_dialog->setVisible(false);

    track_list.at(curtrack)->setValueAt(curlin,1,val);
    track_list.at(curtrack)->setValueAt(curlin, 3,"0");
    track_list.at(curtrack)->setValueAt(curlin,4,"");

    storeValue(curtrack, curlin, 1,val);
    storeValue(curtrack,curlin, 3, "0");
    storeValue(curtrack, curlin, 4, "");


    std::vector<std::string> vals;
    vals.push_back(std::string(val));
    vals.push_back("I-nbr");
    vals.push_back("Date");
    vals.push_back("Duration");

    if(orc_list["instr"][std::string(val)].find("description") != orc_list["instr"][std::string(val)].end()) { // then fill line  0
        try{
            std::string pattern = "p(\\d+)=(.+?),";
            std::regex regex(pattern);

            std::string s = orc_list["instr"][std::string(val)]["description"].get<std::string>() + ",";
            std::sregex_iterator next(s.begin(), s.end(), regex);
            std::sregex_iterator end;
            while(next != end) {
                std::smatch match = *next;
                vals.push_back(match.str(2).c_str());
                next++;
            }

        } catch(std::regex_error &e) {
            //error
        }
    }

    track_list[curtrack]->selectAt(curlin,1);
    track_list[curtrack]->selectCurrent();

   track_list[curtrack]->setDescriptionList(&vals);

   autoCalculateDuration(seqnbr->getValue(), curtrack, curlin);

}



std::vector<std::string> JTrackSet::getInstrDescription(std::string instr)
{
    updateOrcList();

    std::vector<std::string> vals;
    vals.push_back(instr);
    vals.push_back("I-nbr");
    vals.push_back("Date");
    vals.push_back("Duration");

    if(orc_list["instr"].find(instr) == orc_list["instr"].end()) return vals;

    if(orc_list["instr"][instr].find("description") != orc_list["instr"][instr].end()) { // then fill line  0
        try{
            std::string pattern = "p(\\d+)=(.+?),";
            std::regex regex("p(\\d+)=(.+?),");

            std::string s = orc_list["instr"][instr]["description"].get<std::string>() + ",";
            std::sregex_iterator next(s.begin(), s.end(), regex);
            std::sregex_iterator end;
            while(next != end) {
                std::smatch match = *next;
                vals.push_back(match.str(2).c_str());
                next++;
            }

        } catch(std::regex_error &e) {
            //error
        }
    }

    return vals;
}





//              CALCULATE P3 FIELDS
void JTrackSet::autoCalculateDuration(int seq, int track, int line) {

    //std::cout << "auto calculate" << seq << " " << track << " " << line  << std::endl;

    std::string seqStr(std::to_string(seq));
    std::string trackStr(std::to_string(track));
    std::string lineStr(std::to_string(line));

    if(jsdata.find("score") == jsdata.end()) return;
    //std::cout << "found score" << std::endl;
    if(jsdata["score"].find(seqStr) == jsdata["score"].end()) return;
    //std::cout << "found seq" << std::endl;
    if(jsdata["score"][seqStr].find(trackStr) == jsdata["score"][seqStr].end()) return;
    //std::cout << "found track" << std::endl;
    if(jsdata["score"][seqStr][trackStr].find(lineStr) == jsdata["score"][seqStr][trackStr].end()) return;
    //std::cout << "found line" << std::endl;
    if(jsdata["score"][seqStr][trackStr][lineStr].find("1") == jsdata["score"][seqStr][trackStr][lineStr].end()) return;
    //std::cout << "found instr" << std::endl;


    //check if existing instrument
    updateOrcList();
    std::string cur_instr = jsdata["score"][seqStr][trackStr][lineStr]["1"].get<std::string>();
    //std::cout << "cur instrument " << cur_instr << std::endl;
    bool do_continue = false;
    if(orc_list.find("instr") == orc_list.end()) return;
    for(auto & it : orc_list["instr"].items()) {
        //std::cout << "instr proposed " << it.key() << std::endl;
        if(cur_instr == it.key()) {
            do_continue = true;
            break;
        }
    }
    //std::cout << "do continue"  << do_continue << std::endl;
    if(!do_continue) return;

    JTrack::AutoCalculateMode mode = track_list.at(track)->getAutoCalculateMode();
    std::function<bool(int, int , int , std::string , std::string )> matched;

  //  std::cout << "before lambda init" << std::endl;

   // std::cout << "mode == istance : " << (mode==JTrack::AutoCalculateMode::instance) << std::endl;




    if(mode == JTrack::AutoCalculateMode::none) {
      // std::cout << "mode none" << std::endl;
        return;
    } else if(mode == JTrack::AutoCalculateMode::instance) { // match exact same instance in the track
       // std::cout << "mode instance " << std::endl;
        matched = [&](int sq, int tk, int ln, std::string instr, std::string instance) {
           //     std::cout << " match lambda called" << std::endl;
          //      std::cout << "values : " << sq << " " << tk << " " << ln << " " << instr << " " << instance << std::endl;
            json data = jsdata["score"][std::to_string(sq)][std::to_string(tk)][std::to_string(ln)]; // line to match
               if(data.find("1") == data.end()) return false;
           if(instance.empty() || instance.size() == 0) { // then only match instr
          //     std::cout << "no instance " << std::endl;
                if( (data.find("2") == data.end()
                     || data["2"].get<std::string>().empty())
                        && data["1"].get<std::string>() == instr) return true;
           } else {
           //    std::cout << "instance "<< std::endl;
               if(data["1"].get<std::string>() != instr) return false;
               if(instance.empty()) {
                        if(data.find("2") == data.end() || data["2"].get<std::string>().empty()) return true;
               } else {
                        if(data.find("2") != data.end() && !data["2"].get<std::string>().empty() && data["2"].get<std::string>() == instance) return true;
               }
           }
                return false;
        };

    } else if(mode == JTrack::AutoCalculateMode::occurence) { // match any line score
      //  std::cout << "mode occurence " << std::endl;
        matched = [&](int sq, int tk, int ln, std::string instr, std::string instance) {
            json data = jsdata["score"][std::to_string(sq)][std::to_string(tk)][std::to_string(ln)]; // line to match
            if( data.find("1") == data.end()) return false;
            return true;
        };
    }


 //  std::cout << "init values " << std::endl;


   // if(jsdata["score"][seqStr][trackStr][lineStr].find("1") == jsdata["score"][trackStr][lineStr].end()) return;
    std::string instr = jsdata["score"][seqStr][trackStr][lineStr]["1"].get<std::string>();
    std::string instance;

   // std::cout << "finding instance " << std::endl;

    if(jsdata["score"][seqStr][trackStr][lineStr].find("2") != jsdata["score"][seqStr][trackStr][lineStr].end())
        instance = jsdata["score"][seqStr][trackStr][lineStr]["2"].get<std::string>();


 //   std::cout << "instance found" << std::endl;

    std::string p2Str = jsdata["score"][seqStr][trackStr][lineStr]["3"].get<std::string>();
    double p2 = getNumber<double>(p2Str);


    json::reverse_iterator curseq_rit(jsdata["score"].rbegin());
    for(; curseq_rit.key() != seqStr; ++curseq_rit);

    //json::reverse_iterator curseq_rit(jsdata["score"].rbegin());
    json::iterator curseq_it(jsdata["score"].find(seqStr));

 //   std::cout << "prepared for iteration" << std::endl;

       bool found = false;
    for(json::reverse_iterator s_it = curseq_rit; s_it != jsdata["score"].rend() ; ++s_it) { // iterate to previous instruction
        int s_idx = getNumber<int>( s_it.key() );
   //     std::cout << "seq idx " << s_idx << std::endl;
        if(s_idx < 1 ) break;

        json cur_seq = s_it.value();
        if(cur_seq.find(trackStr) == cur_seq.end()) continue;

        /*
        json::reverse_iterator l_it(cur_seq[trackStr].rbegin());
        if(curseq_rit == s_it) {
                for(;l_it.key() != lineStr; ++l_it);
        }
        */
      // json::reverse_iterator l_it ( /*(curseq_rit == s_it ) ? cur_seq[trackStr].find( std::to_string(line - 1) ) :*/ cur_seq[trackStr].rbegin());
        int base_line_idx = (seq == s_idx) ? line -1 : jsdata["lignbr"][s_it.key()].get<int>();
       //for(; l_it != cur_seq[trackStr].rend(); ++l_it) {
        for(int l_idx = base_line_idx; l_idx >= 1; l_idx--) {
            if(cur_seq[trackStr].find(std::to_string(l_idx)) == cur_seq[trackStr].end() ||
                    cur_seq[trackStr][std::to_string(l_idx)].is_null()) continue;
          // int l_idx = getNumber<int>( l_it.key() ); // current line

           if(line == l_idx && seq == s_idx) continue;
           std::string l_key = std::to_string(l_idx);

           if( cur_seq[trackStr][l_key].find("1") == cur_seq[trackStr][l_key].end() ||
                   cur_seq[trackStr][l_key]["1"].is_null())
                  // || cur_seq[trackStr][l_key]["1"].get<std::string>() )
               continue;
           //if( l_it.value().find("1") == l_it.value().end() || l_it.value()["1"].is_null() || l_it.value()["1"].get<std::string>().empty())  {continue;}

     //       std::cout << "calling lambda" << std::endl;
           if ( matched(s_idx, track, l_idx, instr,instance) == true ) {
     //          std::cout << "matched" << std::endl;
               //calculate distance
               double distance = 0;

               if( s_idx == seq ) { // match in same sequence
          //         std::cout << "match in same sequence " << std::endl;
                   //double cur_p2 = getNumber<double> ( l_it.value()["3"].get<std::string>() );
                   double cur_p2 = getNumber<double> (cur_seq[trackStr][l_key]["3"].get<std::string>());
                   distance = ( line - l_idx ) + ( p2 - cur_p2 ) ;
                   //l_it.value()["4"] = getString(distance);
                   if(seq == seqnbr->getValue()) track_list.at(track)->setValueAt(l_idx, 4, getString<double>(distance).c_str());
               } else { // then add number of lines of every iterated seq
        //           std::cout << "not match in same sequence " << std::endl;
                   //double cur_p2 = getNumber<double> ( l_it.value()["3"].get<std::string>() );
                   double cur_p2 = getNumber<double> (cur_seq[trackStr][l_key]["3"].get<std::string>());
                   //distance = jsdata["lignbr"][seqStr].get<int>() - l_idx;
                   distance = jsdata["lignbr"][s_it.key()].get<int>() - l_idx;
                   distance += line ;
                   if(s_idx +1 < seq ) { // probably useless
                        for( int seq_iter = s_idx +1 ; seq_iter < seq ; seq_iter ++ ) {
                               distance += jsdata["lignbr"][std::to_string(seq_iter)].get<int>();
                        }
                   }
                   distance += (p2 - cur_p2);
                   //l_it.value()["4"] = getString(distance);
               }

               if(jsdata["score"][s_it.key()][trackStr][l_key].find("4") != jsdata["score"][s_it.key()][trackStr][l_key].end()) {
                   std::string p3_str = jsdata["score"][s_it.key()][trackStr][l_key]["4"].get<std::string>();
                   if(p3_str == std::string("")) {

                   } else {
                           double val = getNumber<double>(p3_str);
                           if(val < distance) {
                                distance = val;
                           }
                   }
               }
                   jsdata["score"][s_it.key()][trackStr][l_key]["4"] = getString(distance);
                   //only if sametrack
       //            std::cout << "found  before : " << distance << std::endl;
                if(seqnbr->getValue() == s_idx) track_list.at(track)->setValueAt(l_idx, 4, MiupUtil::string::getString(distance).c_str());
               found=true;
               break;
           //then calculate distance
           }
       }
       if(found) break;
    }

    found = false;

    for(json::iterator s_it = curseq_it; s_it != jsdata["score"].end() ; ++s_it) { // iterate to next thing
        int s_idx = getNumber<int>( s_it.key() );
      //  std::cout << "s idx" << s_idx << std::endl;

       json cur_seq = s_it.value();
     //  json::iterator l_it  = (curseq_it == s_it ) ? cur_seq[trackStr].find( std::to_string( line + 1 )) : cur_seq[trackStr].begin();


       int begin_at_line = (s_idx == seq) ? line +1 : 1;
       if(jsdata["lignbr"].find(s_it.key()) == jsdata["lignbr"].end()) jsdata["lignbr"][s_it.key()]=8;
       for(int l_idx = begin_at_line; l_idx <= jsdata["lignbr"][s_it.key()].get<int>(); l_idx++) {
        //for(; l_it != cur_seq[trackStr].end(); ++l_it) {
          // int l_idx = getNumber<int>( l_it.key() ); // current line
         //std::cout << "lidx  " << l_idx << std::endl;

         std::string l_key = std::to_string(l_idx);
         if(cur_seq[trackStr].find(l_key) == cur_seq[trackStr].end()) continue;

           if(l_idx == line && s_idx == seq) continue;
           //if(l_it.value()["1"].is_null() || l_it.value()["1"].get<std::string>().empty()) continue;
           if(cur_seq[trackStr][l_key].find("1") == cur_seq[trackStr][l_key].end() ||
                   cur_seq[trackStr][l_key]["1"].is_null() ||
                   cur_seq[trackStr][l_key]["1"].get<std::string>().empty()) continue;


           if(matched(s_idx, track,l_idx, instr, instance)) {

               double distance = 0;
                //then calculate distance
                if( s_idx == seq ) { // match in same seq
                        //double cur_p2 = getNumber<double> ( l_it.value()["3"].get<std::string>());
                        double cur_p2 = getNumber<double>( cur_seq[trackStr][l_key]["3"].get<std::string>() );
                        distance = ( l_idx - line ) + ( cur_p2 - p2 );
                } else { // else add number of lines of every iterated seq
                        //double cur_p2 = getNumber<double> ( l_it.value()["3"].get<std::string>());
                        double cur_p2 = getNumber<double> (cur_seq[trackStr][l_key]["3"].get<std::string>());
                        distance = l_idx;
                        distance += jsdata["lignbr"][seqStr].get<int>() - line ;
                        if(seq +1 < s_idx) {
                                for(int seq_iter = seq + 1; seq_iter < s_idx; seq_iter++ ) { // add line number of every seq between
                                        distance += jsdata["lignbr"][std::to_string(seq_iter)].get<int>();
                                }
                        }
                        distance += cur_p2 - p2;
                }

                //check if
               if(jsdata["score"][seqStr][trackStr][lineStr].find("4") != jsdata["score"][seqStr][trackStr][lineStr].end() &&
                       !jsdata["score"][seqStr][trackStr][lineStr]["4"].is_null()) {
                   std::string p3_str = jsdata["score"][seqStr][trackStr][lineStr]["4"].get<std::string>();
                   if(p3_str == std::string("")) {

                   } else {
                           double val = getNumber<double>(p3_str);
                           if(val < distance) {
                                distance = val;
                           }
                   }
               }
                   jsdata["score"][s_it.key()][trackStr][lineStr]["4"] = getString(distance);
                //jsdata["score"][seqStr][trackStr][lineStr]["4"] = getString(distance);

                //only refresh in grid if sequence of this instruction is the current one
                if(seqnbr->getValue() == seq) track_list.at(track)->setValueAt(line, 4, MiupUtil::string::getString(distance).c_str());
               // std::cout << "seqnbr ====> " << seqnbr->getValue() << std::endl;
               // std::cout << " seq ==== > " << seq << std::endl;
        //        std::cout << "found  after : " << distance << std::endl;

                found=true;
                break;
            }
        }
        if(found) break;
    }
}




//              RECALCULATE p3 FIELDS OF THE FULL TRACK
void JTrackSet::recalculateAllTrack(int t_idx, JTrack::AutoCalculateMode mode)
{

    jsdata["trackmode"][std::to_string(t_idx)] = mode;
    bool iter= true;

    std::string trackStr = std::to_string(t_idx);
    for(auto & s_it : jsdata["score"].items()) {
        int s_idx = getNumber<int>(s_it.key());

        if(s_it.value().find(trackStr) != s_it.value().end()) {
            json cur_track = s_it.value()[trackStr];
            for(auto & l_it : cur_track.items()) {
                if(l_it.value().find("1") != l_it.value().end()) {
                    int l_idx = getNumber<int>(l_it.key());
                    if(iter) autoCalculateDuration(s_idx, t_idx, l_idx);
                    iter = !iter;

                }
            }
        }
    }


   // track_list.at(t_idx)->redrawMat();
}

void JTrackSet::recalculateTrackAtSeq(int s_idx, int t_idx)
{
    std::string s_id = std::to_string(s_idx);
    std::string t_id = std::to_string(t_idx);
    if(jsdata["score"].find(s_id) == jsdata["score"].end() || jsdata["score"][s_id].find(t_id) == jsdata["score"][s_id].end() ) return;
    for(auto & l_it : jsdata["score"][s_id][t_id].items()) {
        int l_idx = getNumber<int>(l_it.key());
        if(jsdata["score"][s_id][t_id][l_it.key()].find("1") != jsdata["score"][s_id][t_id][l_it.key()].end()) {
            autoCalculateDuration(s_idx, t_idx, l_idx);
        }
    }
}

void JTrackSet::recalculateSeq(int s_idx)
{
    std::string s_id = std::to_string(s_idx);
    if(jsdata["score"].find(s_id) == jsdata["score"].end()) return;
    for(auto & t_it : jsdata["score"][s_id].items())
    {
        int t_idx = getNumber<int>(t_it.key());
        for(auto & l_it : t_it.value().items()) {
            int l_idx = getNumber<int>(l_it.key());
            if(jsdata["score"][s_id][t_it.key()][l_it.key()].find("1") != jsdata["score"][s_id][t_it.key()][l_it.key()].end()) {
                autoCalculateDuration(s_idx, t_idx, l_idx);
            }
        }
    }
}


//              RETURNS CURRENT POSITION (sum of lines of previous sequences + current line)
int JTrackSet::getCurrentPosition()
{
    std::string cur_seq(seqnbr->getAttrStr("VALUE"));
    int where = 0;

    for(auto &s_it : jsdata["score"].items()) {
    //    std::cout << "iter seq == > " << s_it.key() << std::endl;
        if(s_it.value() == jsdata["score"][cur_seq]) break;
        if(jsdata["lignbr"].find(s_it.key()) != jsdata["lignbr"].end()) {
           where += jsdata["lignbr"][s_it.key()].get<int>();
//           std::cout <<"where == > " <<  std::to_string(where) << std::endl;
        }
    }

    where += (curlin > 0) ? curlin-1 : 0;
   return where;

}




//      FORMAT TEMPO STATEMENT
std::string JTrackSet::getTempo(json &data, bool from_start)
{
    std::string tempo_statement  = ";Tempo statement \nt 0 ";
    int totalcount_of_lines = 0;

    int number_of_seqs = 1;
    if(data.find("seqnbr") != data.end()) number_of_seqs = data["seqnbr"].get<int>();


    for(int c_seq = 1; c_seq<= number_of_seqs; c_seq++) {

        //int c_seq = getNumber<int>(it.key());

        std::string c_key = std::to_string(c_seq);

        if(! from_start && c_seq < seqnbr->getValue()) {
           if(data["lignbr"].find(c_key) != data["lignbr"].end()) // increment number of lines passed
                   totalcount_of_lines += data["lignbr"][c_key].get<int>();
            continue;
        }
        if(data["tempo"].find(c_key)== data["tempo"].end()) continue;

        json seq = data["tempo"][c_key];

        int nlin = 8;
        if(data["lignbr"].find(c_key)!=data["lignbr"].end()) nlin = data["lignbr"][c_key].get<int>();

        //for(auto & l_it : seq.items()) {
        for(int c_lin =1; c_lin <= nlin; c_lin++) {
            std::string l_key = std::to_string(c_lin);

            if(!from_start && c_seq == seqnbr->getValue() && c_lin < curlin) continue;

           // int c_lin = getNumber<int>(l_it.key());
           //json tempoline = l_it.value();
            json tempoline = seq[l_key];

           if(  (c_seq == 1 && c_lin == 1)   || (from_start == false && c_seq == seqnbr->getValue() && c_lin == curlin  )   ) {
               if(tempoline.find("1") != tempoline.end() && !tempoline["1"].empty() && !tempoline["1"].get<std::string>().empty()) {
                   std::string begin_val = tempoline["1"].get<std::string>();
                   /*if(begin_val != std::string("")) */

                   tempo_statement += begin_val + " " ;
                   if(from_start==false && tempoline.find("2") != tempoline.end()) {
                       std::string to_val = tempoline["2"].get<std::string>();
                       tempo_statement += "0 " +  to_val + " ";
                   }
                   continue;
               } else {
                   if(from_start==false) { // find last defined tempo
                       std::string prev("60");
                       bool found = false;


                       int distance = 1;

                       for(int sqr=c_seq; sqr >= 1; sqr-- ) {
                           std::string sqrs = std::to_string(sqr);
                           int startline;
                                if(sqr==c_seq) startline = (curlin-1>0) ? curlin -1 :  0;
                                else startline = jsdata["lignbr"][sqrs].get<int>();
                                if (startline==0) continue;

                                for(int lnr = startline; lnr>=1;lnr--) {
                                    std::string lnrs = std::to_string(lnr);
                                    if(jsdata["tempo"].find(sqrs) != jsdata["tempo"].end() &&
                                            jsdata["tempo"][sqrs].find(lnrs) != jsdata["tempo"][sqrs].end() &&
                                            jsdata["tempo"][sqrs][lnrs].find("1") != jsdata["tempo"][sqrs][lnrs].end() &&
                                            !jsdata["tempo"][sqrs][lnrs]["1"].get<std::string>().empty() &&
                                            !jsdata["tempo"][sqrs][lnrs]["1"].is_null() ) {
                                        //check if value

                                        std::string res = jsdata["tempo"][sqrs][lnrs]["1"].get<std::string>();
                                        if(!res.empty()) {
                                            prev = res;
                                            found = true;
                                            std::cout << "FIND PREVIOUS  : " << prev << std::endl;
                                        }

                                        if(jsdata["tempo"][sqrs][lnrs].find("2") != jsdata["tempo"][sqrs][lnrs].end() &&
                                                !jsdata["tempo"][sqrs][lnrs]["2"].get<std::string>().empty() &&
                                                !jsdata["tempo"][sqrs][lnrs]["2"].is_null()) {
                                            std::string sec = jsdata["tempo"][sqrs][lnrs]["2"].get<std::string>();
                                            prev = sec;
                                            std::cout << "FIND SECOND PREVIOUS " << prev << std::endl;
                                        }

                                        if(found) break;

                                    }
                                    distance++;
                                }
                                if(found) break;
                       }
                       //calculate interpolation (if found prev, find next, and do the math

                       found = false;
                       std::string next;
                       int total_distance = distance;
                       for(int nsq = c_seq; nsq < number_of_seqs; nsq++) {
                           for(int nln = c_lin; nln< nlin; nln++) {
                               std::string nsqstr = std::to_string(nsq);
                               std::string nlnstr = std::to_string(nln);
                               if(jsdata["tempo"].find(nsqstr) != jsdata["tempo"].end() &&
                                       jsdata["tempo"][nsqstr].find(nlnstr) != jsdata["tempo"][nsqstr].end() &&
                                       jsdata["tempo"][nsqstr][nlnstr].find("1") != jsdata["tempo"][nsqstr][nlnstr].end() &&
                                       !jsdata["tempo"][nsqstr][nlnstr]["1"].get<std::string>().empty()) {
                                   next = jsdata["tempo"][nsqstr][nlnstr]["1"].get<std::string>();
                                   found=true;
                                   break;
                               }
                               total_distance++;
                           }
                           if(found) break;
                       }


                       float prevtemp = getNumber<float>(prev);
                       float nexttemp = getNumber<float>(next);
                       float diff = nexttemp - prevtemp;
                       float grain = diff / total_distance;
                       float c_temp = prevtemp + (grain * distance);
                       std::string c_tempstr = std::to_string((int)c_temp);

                       std::cout << "DISTANCE : " << distance << " and TOTAL DISTANCE : " << total_distance << std::endl;
                       std::cout << "GRAIN " << grain << std::endl;

                       //tempo_statement += prev + " ";
                       tempo_statement += ( (found) ? c_tempstr : prev )+ " " ;
                       continue;
                   }
                   tempo_statement += "60 ";
                   continue;
               }
               //totalcount_of_lines += c_lin;
           }

                int at_position = -1;
                if(tempoline.find("1") != tempoline.end()) {
                    std::string val = tempoline["1"].get<std::string>();
                    at_position = totalcount_of_lines + (c_lin - 1 ) - play_offset;
                    if(val != std::string("")) tempo_statement += std::to_string( at_position ) + " " +  val + " ";
                }
                if(tempoline.find("2") != tempoline.end() && at_position != -1) {
                    std::string val = tempoline["2"].get<std::string>();
                    if(val != std::string("")) tempo_statement += std::to_string( at_position ) + " " +  val + " ";
                }
                //totalcount_of_lines += c_lin;
        }

        if(data["lignbr"].find(c_key) != data["lignbr"].end()) // increment number of lines passed
                totalcount_of_lines += data["lignbr"][c_key].get<int>();
    }

    return tempo_statement;
}



int JTrackSet::getNumberOfLines()
{
    int nline = 0;
    for(auto & it : jsdata["lignbr"].items()) nline += it.value().get<int>();
    return nline;
}

//              FORMAT METRONOME MACRO AND MASTER STATEMENT
std::string JTrackSet::getGlobalStatements(json &data, bool from_start)
{

    int total_number_of_line = 0  ; // number of steps for tempo macro
    for(auto & it : data["lignbr"].items()) {
        total_number_of_line += it.value().get<int>();
    }

    total_number_of_line -= play_offset;

    //format string for metreonome macro and master instrument statement
    std::string metro_macro = ";Metro Macro \n{ " + std::to_string(total_number_of_line) + " STEP_CNT" +
            " \n" + "i 1 [$STEP_CNT] 1 [$STEP_CNT] \n} \n\n;Master instrument statement \ni \"master\" 0 " + std::to_string(total_number_of_line) + " \n\n";
    return metro_macro;
}






//                      GET FULL SCORE -- SCORE FORMATTER
std::pair< std::string, std::unordered_map <std::string, int > > JTrackSet::getFullScore(GenEditorTab *geneditor, bool ordered, bool from_start, bool seq_only)

{

    is_ordered = ordered;
    json data = (ordered) ? ordered_data: jsdata;


    geneditor->scaleListClear();  // clears the scale function list

    play_offset = 0;
    if(!from_start) {
       for(int i = 1;  i < seqnbr->getValue() ; i++) {
           play_offset += data["lignbr"][std::to_string(i)].get<int>();
       }
       play_offset += (curlin -1);
   }

    std::unordered_map<std::string, int> orc_map;
    int instr_cnt=2;
    std::string score(getTempo(data, from_start) + std::string(" \n\n") + getGlobalStatements(data, from_start)+ std::string(" \n\n"));

   int totalcount_of_lines = 0;

   //std::regex j_tracker_macro_scale ("s(\\d+),(\\d+),(\\d+)");
   std::regex j_tracker_macro_scale ("s(\\d+),(\\d*\\.?\\d*),(\\d*\\.?\\d*)");

    //for (auto s_it : data["score"].items()) { // seq iter
   if(data.find("seqnbr") == data.end() ) data["seqnbr"]=1;
    for(int s_idx = 1; s_idx <= data["seqnbr"].get<int>(); s_idx++) {
        //json seq = s_it.value();
        std::string s_key = std::to_string(s_idx);
        json seq;
        if(data["score"].find(s_key) != data["score"].end()) seq = data["score"][s_key];
        else continue;

        if(!from_start && s_idx < seqnbr->getValue()) continue;


        for(auto & t_it : seq.items()) { // track iter
            score += "; seq " + s_key + " track " + t_it.key() + " \n";
            int t_idx = getNumber<int>(t_it.key());
            if(track_list[t_idx]->mute) continue;

            if(has_solo && solo_idx != t_idx) continue;
                // line iter with numeric index
            for(int ligidx = 1 ; ligidx <= data["lignbr"][s_key].get<int>(); ligidx++) {

               if(!from_start &&  s_idx == seqnbr->getValue() && ligidx < curlin) continue;

                std::string lidx_str = std::to_string(ligidx);
                if(t_it.value().find(lidx_str) == t_it.value().end()  ||  t_it.value()[lidx_str].is_null() ||
                        t_it.value()[lidx_str].empty() || t_it.value()[lidx_str].find("1") == t_it.value()[lidx_str].end()) continue;

                std::string i_statement("i ");
                json line = t_it.value()[lidx_str];

                int l_idx = ligidx;


                int ncol = data["colnbr"][s_key][t_it.key()].get<int>();
                 for(int c_idx = 1; c_idx <= ncol; c_idx++ ) {
                      std::string c_key = std::to_string(c_idx);
                     if(line[c_key].is_null() || line[c_key].empty() ) continue;

                  if(c_idx == 1 ) { // instr
                      std::string instr = line[c_key].get<std::string>();
                      if(orc_map.find(instr) == orc_map.end()) { // push in map if not exists
                          orc_map[instr]=instr_cnt;
                          instr_cnt++;
                      }

                      i_statement += std::to_string(orc_map[instr]);
                      if(line.find("2") == line.end() ) i_statement += " \t";
                  }

                  if(c_idx == 2) { // instance
                      if(!line[c_key].empty()) {
                          i_statement += "." + line[c_key].get<std::string>() + " \t";
                       } else {
                           i_statement += " \t";
                       }
                  }

                  if(c_idx == 3) { // date

                      double date = (totalcount_of_lines + (l_idx -1));
                      if(!from_start && s_idx == seqnbr->getValue() && l_idx >= curlin) date -= ( curlin-1 );
                      if(!line[c_key].get<std::string>().empty()) {
                          date += getNumber<double>(line[c_key].get<std::string>());
                      } else{

                      }
                      if(!from_start && s_idx > seqnbr->getValue()) {
                          date -= curlin - 1;
                      }
                      i_statement += getString(date /* - play_offset */) + " \t";
                  }

                  if(c_idx == 4) {
                      std::string p3val  = line[c_key].get<std::string>();
                      if(p3val == std::string("")) i_statement += "0\t";
                      else i_statement += p3val + " \t";
                  }

                  if(c_idx > 4) { // then check for jo_tracker macros - s for scale - #1,0,100 s1,0,100 with regex s(\d+),(\d+),(\d+)
                      if(line[c_key].get<std::string>() != std::string("")) {
                          //match macro ?
                          if(std::regex_match(line[c_key].get<std::string>(), j_tracker_macro_scale)) {
                                  std::smatch match;
                                  std::string to_evaluate = line[c_key].get<std::string>();
                                  std::regex_search(to_evaluate, match, j_tracker_macro_scale);
                                  int func_nbr;
                                  double scale_min, scale_max;
                                  func_nbr = getNumber<int>(match.str(1));
                                  scale_min = getNumber<double>(match.str(2));
                                  scale_max = getNumber<double>(match.str(3));
                                  ScaleCurve sc;
                                  sc.f_nbr=func_nbr;
                                  sc.scale_min=scale_min;
                                  sc.scale_max = scale_max;
                                  int index = geneditor->scaleListAddifNotExists(sc);
                                  i_statement += std::to_string(index * -1) + " \t"; // refers to negative index of gen routine
                          } else {
                         i_statement += line[c_key].get<std::string>() + " \t";
                          }
                      } else {
                         i_statement += "0 \t";
                      }
                      i_statement += " \t";
                  }

                }
                i_statement += " \n";
                score += i_statement;
            }
        }

        if(data["lignbr"].find(std::to_string(s_idx)) != data["lignbr"].end()) {
            totalcount_of_lines += data["lignbr"][std::to_string(s_idx)].get<int>();
        }
    }

    return std::make_pair(score, orc_map);
}



std::pair< json, std::unordered_map<std::string, int> > JTrackSet::getScoreInstances(GenEditorTab *geneditor)
{
    play_offset = 0;
    int totalcount_of_lines = 0;

    std::regex j_tracker_macro_scale ("s(\\d+),(\\d*\\.?\\d*),(\\d*\\.?\\d*)");

    std::string global_score = getTempo(jsdata, true) + std::string(" \n\n") + getGlobalStatements(jsdata, true)+ std::string(" \n\n");


    geneditor->scaleListClear();  // clears the scale function list
    //std::unordered_map<std::string, std::string> instance_map;
    json instances;

    std::unordered_map<std::string, int> orc_map;
    int instr_cnt=2;

    for(int s_idx = 1; s_idx <= jsdata["seqnbr"].get<int>(); s_idx++) {

        std::string s_key = std::to_string(s_idx);
        json seq;
        if(jsdata["score"].find(s_key) != jsdata["score"].end()) seq = jsdata["score"][s_key];
        else continue;

        for(auto & t_it : seq.items()) { // track iter
            int t_idx = getNumber<int>(t_it.key());
            if(track_list[t_idx]->mute) continue;
            if(has_solo && solo_idx != t_idx) continue;

            for(int l_idx = 1 ; l_idx <= jsdata["lignbr"][s_key].get<int>(); l_idx++) {
                std::string lidx_str = std::to_string(l_idx);
                if(t_it.value().find(lidx_str) == t_it.value().end()  ||  t_it.value()[lidx_str].is_null() ||
                        t_it.value()[lidx_str].empty() || t_it.value()[lidx_str].find("1") == t_it.value()[lidx_str].end()) continue;

                        std::string i_statement("i ");
                        json line = t_it.value()[lidx_str];


                        std::string instance_str;

                int ncol = jsdata["colnbr"][s_key][t_it.key()].get<int>();
                 for(int c_idx = 1; c_idx <= ncol; c_idx++ ) {
                      std::string c_key = std::to_string(c_idx);
                     if(line[c_key].is_null() || line[c_key].empty() ) continue;

                  if(c_idx == 1 ) { // instr
                      std::string instr = line[c_key].get<std::string>();
                      instance_str += instr;
                      if(orc_map.find(instr) == orc_map.end()) { // push in map if not exists
                          orc_map[instr]=instr_cnt;
                          instr_cnt++;
                      }

                      i_statement += std::to_string(orc_map[instr]);
                      if(line.find("2") == line.end() ) i_statement += " \t";
                  }

                  if(c_idx == 2) { // instance
                      if(!line[c_key].empty() && line[c_key].get<std::string>()!="") {
                          i_statement += "." + line[c_key].get<std::string>() + " \t";
                          instance_str += line[c_key].get<std::string>();
                       } else {
                           i_statement += " \t";
                       }
                  }

                  if(c_idx == 3) { // date

                      double date = (totalcount_of_lines + (l_idx -1));
                      if(!line[c_key].get<std::string>().empty()) {
                          date += getNumber<double>(line[c_key].get<std::string>());
                      } else{

                      }
                      i_statement += getString(date /* - play_offset */) + " \t";
                  }

                  if(c_idx == 4) {
                      std::string p3val  = line[c_key].get<std::string>();
                      if(p3val == std::string("")) i_statement += "0\t";
                      else i_statement += p3val + " \t";
                  }

                  if(c_idx > 4) { // then check for jo_tracker macros - s for scale - #1,0,100 s1,0,100 with regex s(\d+),(\d+),(\d+)
                      if(line[c_key].get<std::string>() != std::string("")) {
                          //match macro ?
                          if(std::regex_match(line[c_key].get<std::string>(), j_tracker_macro_scale)) {
                                  std::smatch match;
                                  std::string to_evaluate = line[c_key].get<std::string>();
                                  std::regex_search(to_evaluate, match, j_tracker_macro_scale);
                                  int func_nbr;
                                  double scale_min, scale_max;
                                  func_nbr = getNumber<int>(match.str(1));
                                  scale_min = getNumber<double>(match.str(2));
                                  scale_max = getNumber<double>(match.str(3));
                                  ScaleCurve sc;
                                  sc.f_nbr=func_nbr;
                                  sc.scale_min=scale_min;
                                  sc.scale_max = scale_max;
                                  int index = geneditor->scaleListAddifNotExists(sc);
                                  i_statement += std::to_string(index * -1) + " \t"; // refers to negative index of gen routine
                          } else {
                         i_statement += line[c_key].get<std::string>() + " \t";
                          }
                      } else {
                         i_statement += "0 \t";
                      }
                      i_statement += " \t";
                  }

                }
                i_statement += " \n";
                instances[instance_str].push_back(i_statement);
                //score += i_statement;
            } // line iter
            }  //track iter
            totalcount_of_lines += jsdata["lignbr"][std::to_string(s_idx)].get<int>();
        } // seq iter


    json result;
    for(auto & it : instances.items()) {
        json inst = it.value();
        std::string cur_inst(global_score);
        cur_inst += "\n";
        for(auto &elem : inst)
        {
            std::cout << "Elem : " << elem << std::endl;
            cur_inst += elem.get<std::string>();
        }
        result[it.key()] = cur_inst;
        //instance_map[it.key()] = cur_inst;
    }

    return {result, orc_map};

    //return std::make_pair(instance_map, orc_map);
    //return {instance_map, orc_map};

}



void JTrackSet::orderedPlayWrite(bool write, std::vector<int> *order)
{
    order_list.clear();
    ordered_data.clear();

    ordered_data["tracknbr"] = jsdata["tracknbr"];
    ordered_data["score"];
    ordered_data["lignbr"];
    ordered_data["tempo"];
    ordered_data["colnbr"];
    ordered_data["grid_step"];

    int counter = 0;
    for(auto & it : *order) {

        counter++;
        if(!write) order_list.push_back(it);

        std::string index = std::to_string(it);
        std::string cnt = std::to_string(counter);

        if(jsdata["score"].find(index) != jsdata["score"].end()) {ordered_data["score"][cnt] = jsdata["score"][index];}
        if(jsdata["lignbr"].find(index) != jsdata["lignbr"].end()) {ordered_data["lignbr"][cnt] = jsdata["lignbr"][index];}
        if(jsdata["tempo"].find(index) != jsdata["tempo"].end()) {ordered_data["tempo"][cnt] = jsdata["tempo"][index];}
        if(jsdata["colnbr"].find(index) != jsdata["colnbr"].end()) {ordered_data["colnbr"][cnt] = jsdata["colnbr"][index];}
        if(jsdata["grid_step"].find(index) != jsdata["grid_step"].end()) {ordered_data["grid_step"][cnt] = jsdata["grid_step"][index];}
    }

    ordered_data["seqnbr"] = counter;



    //std::cout << "DATA ============> \n" << ordered_data.dump(4) << std::endl;
    if(!write) {
        orderedPlaySig(ordered_data);
        return;
    } else if(write) {
        jsdata = ordered_data; // copying and recall
        recallSeq(seqnbr->getValue());
    }
}



void JTrackSet::setLineSelection(int nbr_iterated)
{


    if(nbr_iterated == last_iterated) return;
    last_iterated = nbr_iterated;

    int nbr = nbr_iterated + play_offset;

    int current_seq = 1;
    int current_line = 1;

    int passed = 0;

    int lig_nbr = (is_ordered) ? order_list.size() : jsdata["lignbr"].size();
    //std::cout << "lignbr count : " << lig_nbr << std::endl;

    for(int i =1; i <= lig_nbr ; i++ ) {
        int index = (is_ordered) ? order_list.at(i - 1) : i;
        if(jsdata["lignbr"].find(std::to_string(index)) == jsdata["lignbr"].end()) continue;
    //for(auto & it : jsdata["lignbr"].items()) {
        if(nbr >= jsdata["lignbr"][std::to_string(index)].get<int>() + passed ) {
            current_seq ++;
            passed += jsdata["lignbr"][std::to_string(index)].get<int>();
            current_line = nbr+1 - passed;
            continue;
        } else {
            if(current_seq == 1) current_line = nbr+1;
            break;
        }
    }

    int c_sq = (is_ordered) ? order_list[current_seq - 1] : current_seq;
//    std::cout << "seq iter : " << current_seq << " and c_sq " << c_sq << std::endl;

    if(seqnbr->getValue() != c_sq) {
        //seqnbr->setValue(current_seq);
        seqnbr->setValue(c_sq);
    }

    int clin = current_line - 5;
    for(auto & it : track_list) {
        //auto t1 = std::chrono::high_resolution_clock::now();

        it->selectLine(current_line);
        it->scrollTo(clin);

        //std::string s = "MARKMOVE=\"LIN\",MARK=\"" + std::to_string(current_line) +
        //        ":0\",ORIGIN=\"" + std::to_string((clin > 0) ? clin : 1) + ":*\"";

        //std::cout << "ATTRS : " << s << std::endl;
        //it->setAttrs(std::move(s));

        //auto t2 = std::chrono::high_resolution_clock::now();
        //auto dur = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
        //std::cout  << " SET LINE SELECTION - ATTRIBUTES GROUPED  " << dur << std::endl;
    }

    tempo_track->selectLine(current_line);
    tempo_track->scrollTo(clin);
  // tempo_track->redraw();

     std::cout << "current seq : " << current_seq << " & current line  : " << current_line << std::endl;


     //std::cout << "TOTAL DUR OF SETLINESELECTION " << dur << std::endl;
}



void JTrackSet::setMarkCell()
{
    for(auto & it : track_list) {
        it->setMarkCell();
    }
    tempo_track->setMarkCell();
}

void JTrackSet::showTrackParameters(int t_idx)
{
    curtrack = t_idx;
    JTrack::AutoCalculateMode mode = track_list.at(t_idx)->getAutoCalculateMode();

 //   std::cout << " track index : " << t_idx << "   mode : " << mode << std::endl;

    switch(mode) {
    case JTrack::AutoCalculateMode::instance:
        autocalc_instance->setAttr("VALUE","ON");
        autocalc_instance->setAttr("AUTOTOGGLE","YES");
   //     std::cout << "SET INSTANCE " << std::endl;
        break;
    case JTrack::AutoCalculateMode::occurence:
        autocalc_occurrence->setAttr("VALUE","ON");
        autocalc_occurrence->setAttr("AUTOTOGGLE","YES");
   //     std::cout << "SET OCCURRENCE" << std::endl;
        break;
    case JTrack::AutoCalculateMode::none:
        autocalc_none->setAttr("AUTOTOGGLE","YES");
        autocalc_none->setAttr("VALUE","ON");
  //      std::cout << "SET NONE " << std::endl;
        break;
    }
    track_parameters->popup(IUP_MOUSEPOS, IUP_MOUSEPOS);
}



void JTrackSet::vimBufferCallback(int t_idx, int buffer_idx, bool record)
{
    if(jsdata.find("buffers") == jsdata.end()) jsdata["buffers"];

    std::string curSeq = seqnbr->getAttrStr("VALUE");
    std::string curTk = std::to_string(t_idx);
    std::string curL = std::to_string(curlin);
    std::string cur_buffer = std::to_string(buffer_idx);

    if(record) {
        if(jsdata["score"][curSeq].find(curTk) == jsdata["score"][curSeq].end() || jsdata["score"][curSeq][curTk].find(curL) == jsdata["score"][curSeq][curTk].end()) return;
        json cur_lin = jsdata["score"][curSeq][curTk][curL];
        if(cur_lin.is_null() || cur_lin.find("1") == cur_lin.end() || cur_lin["1"].get<std::string>().empty()) return;
        json buf;
        for(auto & it : cur_lin.items()) {
            buf[it.key()] = it.value().get<std::string>();
        }
        jsdata["buffers"][std::to_string(buffer_idx)]=buf;

    } else { // recall if exists
        if(jsdata["buffers"].find(cur_buffer) == jsdata["buffers"].end() || jsdata["buffers"][cur_buffer].is_null()) return;
        if (jsdata["buffers"][cur_buffer].find("1") == jsdata["buffers"][cur_buffer].end() || jsdata["buffers"][cur_buffer]["1"].is_null() || jsdata["buffers"][cur_buffer]["1"].get<std::string>().empty()  ) return;
        for(auto & it : jsdata["buffers"][cur_buffer].items()) {
                if(it.value().is_null() || it.value().get<std::string>().empty()) continue;
                jsdata["score"][curSeq][curTk][curL][it.key()]= it.value().get<std::string>();
                track_list.at(t_idx)->setValueAt(curlin, getNumber<int>(it.key()), it.value().get<std::string>().c_str());
        }
    }
}


json *JTrackSet::getData()
{
    return &jsdata;
}


void JTrackSet::setData(json *j)
{
    jsdata.clear();
    jsdata = *j;
}

json *JTrackSet::getScriptData()
{
    return scripter_view->getData();
}

void JTrackSet::setScriptData(json *j)
{
    scripter_view->setData(j);
}
