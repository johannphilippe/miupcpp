
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JTRACKSET_H
#define JTRACKSET_H

#include"Boxes/menu.h"
#include<regex>
#include"Dialogs/popup.h"
#include"jo_tracker/jtrack.h"
#include"jo_tracker/csoundeditor.h"
#include"Core/miupobj.h"
#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Boxes/frame.h"
#include"Boxes/scrollbox.h"
#include"Boxes/expander.h"
#include"Widgets/button.h"
#include"Dependencies/nlohmann/json.hpp"
#include<fstream>
#include"Core/miup.h"
#include"Dialogs/dialog.h"
#include"jo_tracker/res/icon_parameters.c"
#include"jo_tracker/geneditortab.h"
#include"jdef.h"
#include"jo_tracker/jscripterview.h"
#include"Core/image.h"
#include"jo_tracker/res/icon_lua_32.c"

#include"jo_tracker/jseqorganizer.h"
#include"jo_tracker/jcolor.h"


using json=nlohmann::json;
using namespace MiupUtil::string;

class JTrackSet : public Vbox
{
public:
    JTrackSet();
    virtual ~JTrackSet();

//    Ihandle *getObj() override;

    void addTrack();
    void removeTrack();
    void setNumTracks(int num);
    void setNumlin(int lin);

    void storeValue(int track_idx,int lin,int col,const char *value);
    void storeSeq(int c_seq);
    void recallSeq(int c_seq);
    void cleanData();
    //void loadFromFile(std::string filepath);
    void loadFromFile(std::string filepath = getScoreDataPath());
    //void saveAt(std::string path);
    void saveAt(std::string path = getScoreDataPath());

    void clearTrackCallback(int idx); // all tracks and tempo clear button is connected to this

    void setSize(int width, int height) override;
    void setMaxSize(int width, int height) override;


    void cellChanged(int t_idx, int lin, int col);

    //data manipulation
    void removeTrackData(int track);
    void removeTrackDataForSeq(int seq,int track);
    void removeSeqData(int seq);

    void setTempoValue(int lin, int col, const char *val);

    void displayInstrumentList(int t_idx, int lin, bool full=false);

    void isTrackVisible(int t_idx, bool isVisible);




    std::pair<std::string, std::unordered_map<std::string, int> > getFullScore(GenEditorTab *geneditor, bool ordered = false, bool from_start = true, bool only_seq = false);
    std::pair<json, std::unordered_map<std::string,int>> getScoreInstances(GenEditorTab *geneditor);



    std::string getTempo(json & data, bool from_start = true);
    std::string getGlobalStatements(json & data,bool from_start = true); // master instrument and metronome macro statement
    int getCurrentPosition();
    int getNumberOfLines();

    void setLineSelection(int nbr);
    void setMarkCell();


    lsignal::signal<void(json &)> orderedPlaySig;

    //to save restore session
    json *getData();
    void setData(json *j);

    json *getScriptData();
    void setScriptData(json *j);

    void clearAllTrack();
    void clearTrackData(int t_idx);

    void updateScripterList();

    void clearAll(); // including data


    bool setCurlin(int line);


    lsignal::signal<void(int seq, int line, json &data_)> cellSig;


    void redrawTracks();

    bool soloSlot(int t_idx, int status);


    void copyFullLineSlot(int index);

    int getCurrentLine();
    int getCurrentSeq();
    int getCurrentTrack();
    int getCurrentColumn();


    void setColnbr(int track, int nbr);

    void autoCalculateDuration(int seq, int track, int line);
    void recalculateAllTrack(int t_idx, JTrack::AutoCalculateMode mode);
    void recalculateTrackAtSeq(int s_idx, int t_idx);
    void recalculateSeq(int s_idx);
protected:
private:
    int cp_sq_pos, cp_lin_pos;

    //
    void orderedPlayWrite(bool write, std::vector<int> *order);
    bool is_ordered;

    int play_offset; // used when play, if not playing from start
    int last_iterated;

        //Callbacks from widgets
    void lignbrChanged(int val);
    void colnbrChanged(int t_idx,int col);
    void key_cbk(int t_idx, int lin,int col,int key);

    void setInstr(const char *val);
    std::vector<std::string> getInstrDescription(std::string instr);


    //recall and init
    void initSeq();
    void init();


    //Store as Json
    void storeAsync();

    //number of tracks
    int number_of_tracks;

    void showTrackParameters(int t_idx);

    void vimBufferCallback(int t_idx, int buffer_idx, bool record);

    //Live Data
    json jsdata;
    json ordered_data;

    //orchestra list
    json orc_list;
    void updateOrcList();

    bool must_set_instr;

    List *autocomplete_instr_list;
    Dialog *autocomplete_dialog;


    int curlin,curcol, curtrack;


    std::vector<int> order_list;

    //GUI members

    SpinBox<int> *lignbr, *seqnbr, *tracknbr, *grid_step;
    std::vector<JTrack *> track_list;
    Hbox *hbox, *track_box, *control_box;
    ScrollBox *sb;
    Frame *l_frame,*s_frame, *t_frame, *grid_frame;
    Expander *expander;
    JTempo *tempo_track;
    Image parameter_icon;

    //scripter dialog
    Button *scripter_button;

    Frame *buttons_frame;
    Hbox *buttons_box;
    Button *clearSeqBut;

    JScripterView *scripter_view;

    Dialog *scripter_dialog;
    Fill fill_control;
    Fill *m_fill;

    //track parameter menu
    Menu *track_parameters, *autocalc_menu;
    SubMenu *autocalc_submenu;
    Item *autocalc_instance, *autocalc_occurrence, *autocalc_none;

    Item *deleteTrackItem, *clearTrackItem;

    Menu *manageTrackMenu;
    SubMenu *manageTrackSubMenu;


    //seq organizer
    JSeqOrganizer organizer;
    Expander organizer_expander;

    bool has_solo;
    int solo_idx;


};

#endif // JTRACKSET_H
