
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include "jtransport.h"

JTransport::JTransport()  : Vbox(), play_button("Play","IUP_MediaPlay"), play_seq_button("Play Seq", "IUP_MediaPlay"),
        stop_button("Stop","IUP_MediaStop"), meter1("HORIZONTAL"), meter2("HORIZONTAL"),
        record_button("Record","IUP_MediaRecord"),resume_play_button("Play from start", "IUP_MediaPlay"),
        slider(0,-90,6,0.05,"HORIZONTAL"), button_box(&play_button,&resume_play_button, &play_seq_button, &stop_button,&record_button),
        meter_box(&meter1, &slider, &meter2),
        control_box(),transport_label(""),
        ksmps(1)
{
    control_box.push(&meter_box);
    //this->push(&button_box,&meter1,&slider,&meter2);
    this->push(&button_box, &control_box);
    slider.setRasterSize(300,20);

    meter1.setRasterSize(300,20);
    meter2.setRasterSize(300,20);

    meter_box.setAttr("GAP","4");

    setAttr("EXPAND","HORIZONTAL");

    play_button.buttonClickOffSig.connect([&]() {buttonClickedSig(ClickedButton::play); });
    stop_button.buttonClickOffSig.connect([&](){buttonClickedSig(ClickedButton::stop);});
    resume_play_button.buttonClickOffSig.connect([&](){buttonClickedSig(ClickedButton::resumeplay);});
    record_button.buttonClickOffSig.connect([&](){buttonClickedSig(ClickedButton::record);});
    play_seq_button.buttonClickOffSig.connect([&](){buttonClickedSig(ClickedButton::playseq);});

    slider.postValueChangedSig.connect([&](double v) {
       double linear = (v + 90) / 96;
       masterLevelChanged(linear);
    });
}

JTransport::~JTransport()
{

}


double JTransport::getMasterValue()
{
    return ( slider.getAttrDouble("VALUE") + 90 ) / 96;
}


void JTransport::setGainMeter(int index, double val)
{
    switch(index) {
    case 1:
        meter1.setValue(val);
        break;
    case 2:
        meter2.setValue(val);
        break;
    }
}




void JTransport::clearAllMeters()
{
    meter1.setValue(-90);
    meter2.setValue(-90);
}


void JTransport::refreshTransport(int seq, int line, json &jsdata)
{

    int total_seqnbr;
    int total_lin;

    std::string sq("");
    if(jsdata.find("seqnbr") != jsdata.end()) {
        total_seqnbr = jsdata["seqnbr"].get<int>();
        sq += std::to_string(seq) + " / " + std::to_string(total_seqnbr);
    }

    std::string ln("");
    if(jsdata["lignbr"].find(std::to_string(seq)) != jsdata["lignbr"].end()){
        total_lin = jsdata["lignbr"][std::to_string(seq)].get<int>();
        ln += std::to_string(line) + " / " ;
    }

    //calculate total seconds and current second
    std::vector<std::pair<int, double>>map;

    double total_sec=0.0;
    double nsec=0.0;
    int cur_tempo, next_tempo;
    double interp=0.0;

    for(int s=1; s<=total_seqnbr; s++) {
        std::string sq= std::to_string(s);
        int nlin = jsdata["lignbr"][std::to_string(s)].get<int>();
        for(int l=1; l<=nlin; l++ )
        {
            std::string ln = std::to_string(l);

                if(jsdata["tempo"].find(sq) != jsdata["tempo"].end() &&
                        jsdata["tempo"][sq].find(ln) != jsdata["tempo"][sq].end())
                {

                    if(jsdata["tempo"][sq][ln].find("1") !=  jsdata["tempo"][sq][ln].end())
                    {
                        cur_tempo = MiupUtil::string::getNumber<int>( jsdata["tempo"][sq][ln]["1"].get<std::string>());
                        if(jsdata["tempo"][sq][ln].find("2") != jsdata["tempo"][sq][ln].end()) {
                                cur_tempo = MiupUtil::string::getNumber<int>( jsdata["tempo"][sq][ln]["2"].get<std::string>());

                        }
                    }
                }
        }
    }
}


