
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#ifndef JTRANSPORT_H
#define JTRANSPORT_H

#include"Boxes/vbox.h"
#include"Boxes/hbox.h"
#include"Widgets/levelmeter.h"
#include"Widgets/button.h"
#include"Widgets/slider.h"
#include"Widgets/gainmeter.h"
#include"Text/label.h"
#include"Plots/scopeplot.h"

#include"Dependencies/nlohmann/json.hpp"

using json = nlohmann::json;

class JTransport : public Vbox
{
public:
    enum ClickedButton{play, resumeplay, playseq, stop, record};

    JTransport();
    ~JTransport();

    double getMasterValue();

    void setGainMeter(int index, double val);

    void clearAllMeters();

    void refreshTransport(int seq, int line, json &jsdata);


    lsignal::signal<void(ClickedButton)> buttonClickedSig;
    lsignal::signal<void(double)> masterLevelChanged;

protected:

private:
    Button play_button, stop_button, record_button, resume_play_button, play_seq_button;

    GainMeter meter1, meter2;
    Slider<double> slider;
    Hbox button_box, control_box;
    Vbox meter_box, scope_box;
    Label transport_label;

    int ksmps;

};

#endif // JTRANSPORT_H
