
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef LUA_API_H
#define LUA_API_H
#include"Scripting/Lua/lua.hpp"
#include"Dependencies/nlohmann/json.hpp"
#include<algorithm>
#include"Dependencies/lsignal/lsignal.h"
#ifdef __linux__
#include"lua.h"
#include"lauxlib.h"
#include"lualib.h"
#elif _WIN32
#include"terra/lua.h"
#include"terra/lauxlib.h"
#include"terra/lualib.h"
#endif

#define LUA_TNONE		(-1)

#define LUA_TNIL		0
#define LUA_TBOOLEAN		1
#define LUA_TLIGHTUSERDATA	2
#define LUA_TNUMBER		3
#define LUA_TSTRING		4
#define LUA_TTABLE		5
#define LUA_TFUNCTION		6
#define LUA_TUSERDATA		7
#define LUA_TTHREAD		8

using json = nlohmann::json;

static json *score_data_ptr = nullptr;
static json *orc_data_ptr = nullptr;
/*
 * Clears a sequence data
 * jt.clearSeq(integer seq_nbr)
*/

static int jt_clearSeq(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;
    std::string seq = std::to_string(lua_tointeger(lua, 1));

    score_data_ptr->at("score")[seq].clear();

    return 0;
}

/*
 * Clears the track at seq index
 * jt.clearSeq(integer seq_nbr, integer tk_nbr)
*/

static int jt_clearTrackAtSeq(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;
    std::string seq = std::to_string(lua_tointeger(lua, 1));
    std::string track = std::to_string(lua_tointeger(lua, 2));

    score_data_ptr->at("score")[seq][track].clear();

    return 0;
}


/*
 * Sets number of lines in a sequence
 * jt.setLignbr(integer seq_nbr, integer lig_nbr)
*/

static int jt_setLignbr(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;

    std::string seq = std::to_string(lua_tointeger(lua, 1));
    int numlin = lua_tointeger(lua,2);

    score_data_ptr->at("lignbr")[seq] = numlin;
    return 0;
}

// Same as above

static int jt_setNumlin(lua_State *lua)
{
    return jt_setLignbr(lua);
}

/*
 * Sets number of columns in a track/sequence
 * jt.setColnbr(integer seq_nbr, integer track_nbr, integer col_nbr)
*/

static int jt_setColnbr(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;
    std::string seq = std::to_string(lua_tointeger(lua, 1));
    std::string track = std::to_string(lua_tointeger(lua, 2));
    int numcol = lua_tointeger(lua, 3);
    score_data_ptr->at("colnbr")[seq][track]= numcol;
    return 0;
}

// Same as above

static int jt_setNumcol(lua_State *lua)
{
   return jt_setColnbr(lua);
}

/*
 * Sets number of tracks
 * jt.setTracknbr(integer number_of_tracks)
*/

static int jt_setTracknbr(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;
    int tk = lua_tointeger(lua, 1);
    score_data_ptr->at("tracknbr") = tk;

     return 0;
}

/*
 * Sets tempo at indexes
 * jt.setTempoAt(integer seq_nbr,integer line_nbr, integer col_nbr, number_or_string value)
*/

static int jt_setTempoAt(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;
    std::string seq =  std::to_string(lua_tointeger(lua,1));
    std::string lin = std::to_string(lua_tointeger(lua,2));
    std::string col = std::to_string(lua_tointeger(lua,3));

    std::string val;
    if(lua_isnumber(lua, 5)) {
        val = std::string(lua_tostring(lua, 5));
        std::replace(val.begin(), val.end(), ',', '.');
    } else {
        val = std::string(lua_tostring(lua, 5));
    }

    score_data_ptr->at("tempo")[seq][lin][col] = val;

    return 0;
}

/*
 * Sets value at indexes
 * jt.setAt(integer seq_nbr, integer track_nbr, integer line_nbr, integer col_nbr, number_or_string value)
*/

static int jt_setAt(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;

    std::string seq =  std::to_string(lua_tointeger(lua,1));
    std::string track = std::to_string(lua_tointeger(lua, 2));
    std::string lin = std::to_string(lua_tointeger(lua,3));
    int c = lua_tointeger(lua, 4);
    std::string col = std::to_string(c);

    std::string val;
    if(lua_isnumber(lua, 5)) {
        val = std::string(lua_tostring(lua, 5));
    // replace lua coma with a dot if decimal number
        std::replace(val.begin(), val.end(), ',', '.');
    } else {
        val = std::string(lua_tostring(lua, 5));
    }

    // check if the line exist, so that it avoids crash
    if( (c == 1) ||( c != 1 &&
            (*score_data_ptr)["score"].find(seq) != (*score_data_ptr)["score"].end() &&
            (*score_data_ptr)["score"][seq].find(track) != (*score_data_ptr)["score"][seq].end() &&
            (*score_data_ptr)["score"][seq][track].find(lin) != (*score_data_ptr)["score"][seq][track].end() &&
            (*score_data_ptr)["score"][seq][track][lin].find("1") != (*score_data_ptr)["score"][seq][track][lin].end() &&
            !(*score_data_ptr)["score"][seq][track][lin]["1"].is_null())) {

        score_data_ptr->at("score")[seq][track][lin][col] = val;
    }

    return 0;
}


/*
 * Sets the line passed as argument. Performs a line size checking.
 * jt.setLine(integer seq_nbr, integer track_nbr, integer line_nbr, table values)
*/

static int jt_setLine(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;

    std::string seq = std::to_string(lua_tointeger(lua, 1));
    std::string track = std::to_string(lua_tointeger(lua, 2));
    std::string line  = std::to_string(lua_tointeger(lua, 3));



        luaL_checktype(lua, 4, LUA_TTABLE);
        std::size_t arr_len = lua_objlen(lua, 4);

        //verify if line is big enough, and if so change its colnbr
        if(score_data_ptr->at("colnbr").find(seq) != score_data_ptr->at("colnbr").end() &&
                score_data_ptr->at("colnbr")[seq].find(track) != score_data_ptr->at("colnbr")[seq].end())
        {
            //check if smaller
            if(std::size_t(score_data_ptr->at("colnbr")[seq][track].get<int>()) < arr_len)
                score_data_ptr->at("colnbr")[seq][track] = arr_len;

        } else // this value doesn't exist, set it
        {
            score_data_ptr->at("colnbr")[seq][track] = arr_len;
        }

        for(std::size_t i = 1; i <= arr_len; i++) {
            lua_rawgeti(lua, 4, i);
            std::string val;
            if(lua_isnumber(lua, -1)) {
                val = std::string(lua_tostring(lua, -1));
                // replace lua coma with a dot if decimal number
                std::replace(val.begin(), val.end(), ',', '.');
            } else {
                val = std::string(lua_tostring(lua, -1));
            }
            score_data_ptr->at("score")[seq][track][line][std::to_string(i)] = val;
        }
    return 0;
}

/*
 * Fills a column with a value
 * jt.setValueInColumn(integer seq_nbr, integer track_nbr, integer col_nbr, any value)
*/

static int jt_setValueInColumn(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;

    std::string seq = std::to_string(lua_tointeger(lua,1));
    std::string track = std::to_string(lua_tointeger(lua, 2));

    std::size_t col_index = lua_tointeger(lua, 3);
    std::string col = std::to_string(col_index);

    //std::cout << "seq track col " << seq << " " << track <<  " " << col << std::endl;


    //std::cout << "Checking colnbr" << std::endl;

    //verify if line is big enough, and if so change its colnbr
    if(score_data_ptr->at("colnbr").find(seq) != score_data_ptr->at("colnbr").end() &&
        score_data_ptr->at("colnbr")[seq].find(track) != score_data_ptr->at("colnbr")[seq].end())
    {
       // std::cout <<" Value of colnbr exists" << std::endl;
        //check if smaller
        if(std::size_t(score_data_ptr->at("colnbr")[seq][track].get<int>()) < col_index)
        {
            score_data_ptr->at("colnbr")[seq][track] = col_index;
    //        std::cout << "Value of colnbr is smaller, setting it" << std::endl;
        }

    } else // this value doesn't exist, set it
    {

        //std::cout << "Value of colnbr doesn't exist, creating it" << std::endl;
        score_data_ptr->at("colnbr")[seq][track] = col_index;
    }



    std::string val;
    if(lua_isnumber(lua, 4)) {
        val = std::string(lua_tostring(lua, 4));
    // replace lua coma with a dot if decimal number
        std::replace(val.begin(), val.end(), ',', '.');
    } else {
        val = std::string(lua_tostring(lua, 4));
    }

    //std::cout << "Value to set is : " << val << std::endl;

    if(score_data_ptr->at("lignbr").find(seq) != score_data_ptr->at("lignbr").end()) {
        std::size_t l_nbr = score_data_ptr->at("lignbr")[seq].get<int>();

        //std::cout << "Number of lines : " << l_nbr << std::endl;

        for(std::size_t l = 1; l <= l_nbr; l++)
        {
            //std::cout << "iterate on line " << l << std::endl;
            std::string line = std::to_string(l);
            if(score_data_ptr->at("score")[seq][track].find(line) != score_data_ptr->at("score")[seq][track].end() &&
               score_data_ptr->at("score")[seq][track][line].find("1") != score_data_ptr->at("score")[seq][track][line].end() &&
               !score_data_ptr->at("score")[seq][track][line]["1"].get<std::string>().empty())
            {
                //std::cout << "line exists, setting value" << std::endl;
                score_data_ptr->at("score")[seq][track][line][col] = val;
            }

        }
    }

    return 0;
}


/*
 * Fills a column with values from a table. Performs lignbr and colnbr checking.
 * jt.setColumn(integer seq_nbr, integer track_nbr, integer col, table values)
 *
*/
static int jt_setColumn(lua_State *lua)
{

    if(score_data_ptr == nullptr) return 0;

    std::string seq = std::to_string(lua_tointeger(lua,1));
    std::string track = std::to_string(lua_tointeger(lua, 2));

    std::size_t col_index = lua_tointeger(lua, 3);
    std::string col = std::to_string(col_index);

    //verify if line is big enough, and if so change its colnbr
    if(score_data_ptr->at("colnbr").find(seq) != score_data_ptr->at("colnbr").end() &&
        score_data_ptr->at("colnbr")[seq].find(track) != score_data_ptr->at("colnbr")[seq].end())
    {
        //check if smaller
        if(std::size_t(score_data_ptr->at("colnbr")[seq][track].get<int>()) < col_index)
        score_data_ptr->at("colnbr")[seq][track] = col_index;

    } else // this value doesn't exist, set it
    {
        score_data_ptr->at("colnbr")[seq][track] = col_index;
    }

    luaL_checktype(lua, 4, LUA_TTABLE);
    std::size_t arr_len = lua_objlen(lua, 4);

    //verify if number of lines is enough
    if(score_data_ptr->at("lignbr").find(seq) != score_data_ptr->at("lignbr").end()) {
        if(std::size_t(score_data_ptr->at("lignbr")[seq].get<int>()) < arr_len)
            score_data_ptr->at("lignbr")[seq] = arr_len;
    } else {
        score_data_ptr->at("lignbr")[seq] = arr_len;
    }

    for(std::size_t i = 1; i <= arr_len; i++) {
        lua_rawgeti(lua, 4, i);
        std::string val;

        if(lua_isnumber(lua, -1)) {
            val = std::string(lua_tostring(lua, -1));
            // replace lua coma with a dot if decimal number
            std::replace(val.begin(), val.end(), ',', '.');
        } else {
            val = std::string(lua_tostring(lua, -1));
        }

        std::string line = std::to_string(i);
        if(score_data_ptr->at("score")[seq][track].find(line) != score_data_ptr->at("score")[seq][track].end() &&
            score_data_ptr->at("score")[seq][track][line].find("1") != score_data_ptr->at("score")[seq][track][line].end() &&
            !score_data_ptr->at("score")[seq][track][line]["1"].get<std::string>().empty())
        {
           // std::cout << "line exists, setting value" << std::endl;
            score_data_ptr->at("score")[seq][track][line][col] = val;
        }
    }


    return 0;
}


/*
 * Returns value at indexes
 * string jt.getAt(integer seq_nbr, integer track_nbr, integer line_nbr, integer col_nbr)
*/

static int jt_getAt(lua_State *lua)
{
    if(score_data_ptr == nullptr) return 0;
    std::string seq = std::to_string(lua_tointeger(lua,1));
    std::string track = std::to_string(lua_tointeger(lua,2));
    std::string lin = std::to_string(lua_tointeger(lua,3));
    std::string col = std::to_string(lua_tointeger(lua,4));

    if(score_data_ptr->at("score").find(seq) != score_data_ptr->at("score").end() &&
            score_data_ptr->at("score")[seq].find(track) != score_data_ptr->at("score")[seq].end() &&
            score_data_ptr->at("score")[seq][track].find(lin) != score_data_ptr->at("score")[seq][track].end() &&
            score_data_ptr->at("score")[seq][track][lin].find(col) != score_data_ptr->at("score")[seq][track][lin].end()) {

        std::string res = score_data_ptr->at("score")[seq][track][lin][col].get<std::string>();
        lua_pushstring(lua, res.c_str());
    } else {
        lua_pushnil(lua);
    }
    return 1;
}

/*
 * Returns number of lines in a sequence
 * integer jt.getNumlin(integer seq_nbr)
*/

static int jt_getNumlin(lua_State *lua)
{
 std::string seq = std::to_string(lua_tointeger(lua,1));
 if( score_data_ptr->at("lignbr").find(seq) != score_data_ptr->at("lignbr").end()) {
     int res = score_data_ptr->at("lignbr")[seq].get<int>();
     lua_pushnumber(lua, res);

 } else {
     lua_pushnil(lua);
 }


 return 1;
}

/*
 * Same as previous with jt.getLignbr
*/

static int jt_getLignbr(lua_State *lua)
{
    return jt_getNumlin(lua);
}

/*
 * Returns number of columns of specific track in specific sequence
 * integer jt.getNumcol(integer seq_nbr, integer track_nbr)
*/

static int jt_getNumcol(lua_State *lua)
{
    std::string seq = std::to_string(lua_tointeger(lua, 1));
    std::string tk = std::to_string(lua_tointeger(lua, 2));

    if(score_data_ptr->find("colnbr") != score_data_ptr->end() &&
            score_data_ptr->at("colnbr").find(seq) != score_data_ptr->at("colnbr").end() &&
            score_data_ptr->at("colnbr")[seq].find(tk) != score_data_ptr->at("colnbr")[seq].end())
    {
        int res = score_data_ptr->at("colnbr")[seq][tk].get<int>();
        lua_pushnumber(lua, res);
    } else lua_pushnil(lua);
    return 1;
}

/*
 * Same as previous with jt.getColnbr
*/

static int jt_getColnbr(lua_State *lua)
{
    return jt_getNumcol(lua);
}

/*
 * Returns number of tracks
 * integer jt.getTracknbr()
*/

static int jt_getTracknbr(lua_State *lua)
{
    if(score_data_ptr->find("tracknbr") != score_data_ptr->end())
    {
        int res = score_data_ptr->at("tracknbr").get<int>();
        lua_pushnumber(lua, res);
    } else lua_pushnil(lua);
    return 1;
}


// MAY NOT WORK SINCE THERE IS ONLY POSITION, AND NO LINE/COLUMN
/*
 * Returns tempo value at specific indexes, or nil if no tempo at this index
 * string jt.getTempoAt(integer seq_nbr, integer position)
*/

static int jt_getTempoAt(lua_State *lua)
{
    std::string seq = std::to_string(lua_tointeger(lua, 1));
    std::string pos = std::to_string(lua_tointeger(lua, 2));
    if(score_data_ptr->at("tempo").find(seq) != score_data_ptr->at("tempo").end() &&
            score_data_ptr->at("tempo")[seq].find(pos) != score_data_ptr->at("tempo")[seq].end())
    {
        std::string res =  score_data_ptr->at("tempo")[seq][pos].get<std::string>();
        lua_pushstring(lua, res.c_str());
    } else lua_pushnil(lua);
    return 1;
}

/*
 * Returns a full line as a Lua table
 * table jt.getLine(integer seq_nbr, integer track_nbr, integer line_nbr)
*/

static int jt_getLine(lua_State *lua)
{
    std::string seq = std::to_string(lua_tointeger(lua, 1));
    std::string track = std::to_string(lua_tointeger(lua, 2));
    std::string line  = std::to_string(lua_tointeger(lua, 3));

    if(score_data_ptr->at("score").find(seq) != score_data_ptr->at("score").end() &&
            score_data_ptr->at("score")[seq].find(track) != score_data_ptr->at("score")[seq].end() &&
            score_data_ptr->at("score")[seq][track].find(line) != score_data_ptr->at("score")[seq][track].end()) {
        lua_newtable(lua);

        int cols = score_data_ptr->at("colnbr")[seq][track].get<int>();

        for(int i = 1; i <= cols; i++) {
            lua_pushstring(lua, score_data_ptr->at("score")[seq][track][line][std::to_string(i)].get<std::string>().c_str());
            lua_rawseti(lua, -2, i);
        }

    } else lua_pushnil(lua);
    return 1;
}




// Artistic and generative API methods

/*
 * Random scramble on one track to another sequence/track. NOT WORKING NOT
 * jt.scramble(integer seq_nbr, integer track_nbr, integer to_seq, integer to_track)
*/

static int jt_scrambleTo(lua_State *lua)
{
    std::string seq = std::to_string(lua_tointeger(lua, 1));
    std::string track = std::to_string(lua_tointeger(lua, 2));
    std::string to_seq = std::to_string(lua_tointeger(lua, 3));
    std::string to_track = std::to_string(lua_tointeger(lua, 4));

    if(score_data_ptr->at("score").find(seq) != score_data_ptr->at("score").end() &&
            score_data_ptr->at("score")[seq].find(track) != score_data_ptr->at("score")[seq].end()) {

        std::cout << "copying in data" << std::endl;

        json in_data = score_data_ptr->at("score")[seq][track];

        std::cout << "getting track_nbr" << std::endl;
        // get track nbr to init indexes
        int track_nbr = score_data_ptr->at("tracknbr").get<int>();

        std::cout << "init vector" << std::endl;
        std::vector<int> indexes;
        indexes.resize(track_nbr);

        // count valid entries first and copy its indexes
        int cnt = 0;

        std::cout << "find valid things" << std::endl;
        for(auto & it : in_data) {
            if(it.find("1") != it.end() && !it["1"].is_null()) {
                indexes[cnt] = cnt + 1; // + 1 because line indexes are starting from 1
            } else indexes[cnt] = -1;

            cnt++;
        }


        std::cout << "CHECK INDEXES" << std::endl;
        for(auto & it : indexes) std::cout << it << " " ;
        std::cout << std::endl;

        std::cout << "shuffle" << std::endl;
        // shake this
        std::random_shuffle(indexes.data(), indexes.data() + indexes.size());

        std::cout << "fill things in data" << std::endl;
        // then write to the good place
        for(std::size_t i = 0; i < indexes.size(); i++) {
            if(indexes[i] != -1) { // then valid
                json j =  in_data[std::to_string(indexes[i])];
                score_data_ptr->at("score")[to_seq][to_track][std::to_string(i + 1)] = j;
            }
        }
        std::cout << "OK" << std::endl;
    }
    return 0;
}


static lsignal::signal<void(int sq, int tk)> recalculateSeqTrackSig; // the track int this seq
static lsignal::signal<void(int sq)> recalculateSeqSig; // all tracks in the seq

static int jt_recalculateSeqTrack(lua_State *lua)
{
    int seq = lua_tointeger(lua,1);
    int tk = lua_tointeger(lua, 2);

    recalculateSeqTrackSig(seq,tk);

    return 0;
}

static int jt_recalculateSeq(lua_State *lua)
{
    int seq = lua_tointeger(lua,1);
    recalculateSeqSig(seq);

    return 0;
}


// Methods registration
static const luaL_Reg jt_api[] = {
    // Basic getters and setters
    {"setLignbr", jt_setLignbr},
    {"setNumlin", jt_setNumlin},
    {"setColnbr", jt_setColnbr},
    {"setNumcol", jt_setNumcol},
    {"setTracknbr", jt_setTracknbr},
    {"setAt", jt_setAt},
    {"setTempoAt", jt_setTempoAt},
    {"setLine", jt_setLine},
    {"setColumn", jt_setColumn},
    {"setValueInColumn", jt_setValueInColumn},

    {"getTracknbr", jt_getTracknbr},
    {"getNumlin", jt_getNumlin},
    {"getLignbr", jt_getLignbr},
    {"getNumcol", jt_getNumcol},
    {"getColnbr", jt_getColnbr},
    {"getTempoAt", jt_getTempoAt},
    {"getLine", jt_getLine},
    {"getAt" , jt_getAt},

    {"clearSeq", jt_clearSeq},
    {"clearTrackAtSeq", jt_clearTrackAtSeq},

    // Recalculate track/seq
    {"calculateSeqTrackDurations", jt_recalculateSeqTrack},
    {"calculateSeqDurations", jt_recalculateSeq},

    // Artistic and generative methods
    {"scrambleTo", jt_scrambleTo},


    {NULL, NULL}
};



#endif // LUA_API_H
