/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef MACRODIALOG_H
#define MACRODIALOG_H

#include"Core/miupobj.h"
#include"Dialogs/dialog.h"

#include"Widgets/button.h"
#include"Widgets/toggle.h"
#include"jo_tracker/csoundeditor.h"
#include"Text/text.h"
#include"Boxes/hbox.h"
#include"Boxes/vbox.h"
#include"Boxes/radio.h"

#include"Dependencies/nlohmann/json.hpp"

#include"csound/csound.hpp"

using json = nlohmann::json;

class MacroDialog : public Dialog
{

public:
    MacroDialog() :
        ok_button("Ok", "IUP_ActionOk"),
        cancel_button("Cancel", "IUP_ActionCancel"),
        clear_button("Clear", "IUP_EditErase"),
        console(true),
        pre_orc_mode(1, "Pre orc"),
        post_orc_mode(0, "Post orc"),
        pre_score_mode(0, "Pre score"),
        mode_status(0)

    {
        ok_button.setAttr("TIP","Evaluates Csound code and stores it on success");
        cancel_button.setAttr("TIP", "Close this window without modification to storage");
        clear_button.setAttr("TIP", "Clears global data");

        radio_box.push(&pre_orc_mode, &post_orc_mode, &pre_score_mode);
        mode_selector.push(&radio_box);

        // Callback when radio mode changes
        post_orc_mode.toggleStateChangedSig.connect([&](int v) {
           if  (v == 1) stateChange(0);
        });
        pre_orc_mode.toggleStateChangedSig.connect([&](int v) {
           if  (v == 1) stateChange(1);
        });
        pre_score_mode.toggleStateChangedSig.connect([&](int v) {
           if  (v == 1) stateChange(2);
        });


        this->openDialogSig.connect([&]() {this->stateChange(pre_orc_mode.getStatus());});

        button_box.push(&ok_button, &cancel_button, &clear_button);
        button_box.setAttr("GAP","10");

        mainbox.push(&mode_selector, &editor, &button_box, &console);
        this->construct(&mainbox);
        console.setAttr("EXPAND", "HORIZONTAL");
        console.setAttr("SIZE","x50");
        this->setAttr("SIZE","800x350");
        this->setAttr("TITLE","Csound Macro Dialog");

        ok_button.buttonClickOffSig.connect([&](){
            // first evaluate csound code
          bool res = this->evaluate();
          if(res) {

          std::string s;
          if(mode_status == 0) s = "pre_orc";
          else if(mode_status == 1) s = "post_orc";
          else if(mode_status == 2) s = "pre_score";

           std::cout << "GLOBAL MODE // ===>> )) " << s << std::endl;
           global_data["data"][s] = editor.getAttr("VALUE");
           closeSig(true);
          }
        });

        cancel_button.buttonClickOffSig.connect([&](){
           closeSig(false);
            this->hide();
        });


        clear_button.buttonClickOffSig.connect([&](){
            console.clear();
            editor.clear();

            global_data.clear();
        });
    }

    void stateChange(int status) {
        console.clear();
        editor.clear();
        if(status == 1) { // pre_orc mode
            mode_status = 0;
            std::cout << "STATUS PRE ORC" << std::endl;
        if(global_data.find("data") != global_data.end() &&
            global_data["data"].find("pre_orc") != global_data["data"].end()) {
            std::string content = global_data["data"]["pre_orc"].get<std::string>();
            editor.setAttr("VALUE", content.c_str());
        }
        } else if(status == 0) { // if(status == 0) { // post orc mode
            mode_status = 1;
            std::cout << "STATUS POST ORC" << std::endl;
        if(global_data.find("data") != global_data.end() &&
            global_data["data"].find("post_orc") != global_data["data"].end()) {
            std::string content = global_data["data"]["post_orc"].get<std::string>();
            editor.setAttr("VALUE", content.c_str());
        }
        } else if(status==2) { // pre score mode
            mode_status = 2;
            if(global_data.find("data") != global_data.end() &&
                    global_data["data"].find("pre_score") != global_data["data"].end()) {
                std::string content = global_data["data"]["pre_score"].get<std::string>();
                editor.setAttr("VALUE", content.c_str());
            }
        }
    }

    void setData(json *data) {
        this->global_data = *data;
        std::string s;
        if(mode_status == 0) s = "pre_orc";
        else if(mode_status == 1) s = "post_orc";
        else if(mode_status == 2) s = "pre_score";
        //std::cout << "MACRO DATA \n" << (*data).dump(4) << std::endl;
        if(global_data.find("data") != global_data.end() &&
                global_data["data"].find(s) != global_data["data"].end()) {
            std::string cur_data = global_data["data"][s].get<std::string>();
            editor.setAttr("VALUE", cur_data.c_str());
        }
    }

    json getData() {
        return global_data;
    }

    void clear() {
        editor.clear();
    }

    lsignal::signal<void(bool)> closeSig;

private:

    bool evaluate() {
        console.clear();
        bool res = false;

        Csound cs_test;
        std::string buf;

        std::string to_eval;


        if(mode_status == 0 ) {
            if(global_data.find("data") != global_data.end()) {
                if(global_data["data"].find("post_orc") != global_data["data"].end()) {
                to_eval += global_data["data"]["post_orc"].get<std::string>() + " \n";
                }
            }
        } else if(mode_status == 1) {
            if(global_data.find("data") != global_data.end()) {
                if(global_data["data"].find("pre_orc") != global_data["data"].end()) {
                to_eval += global_data["data"]["pre_orc"].get<std::string>() + " \n";
                }
            }
        } else if(mode_status == 2) {
        }


        to_eval += editor.getAttr("VALUE");


        to_eval += "\nreturn 0\n";

        std::cout << "TO EVAL \n" << to_eval << std::endl;

        cs_test.CreateMessageBuffer(0);

        cs_test.Start();

        MYFLT cs_res = cs_test.EvalCode(to_eval.c_str());
        if((int)cs_res == 0) {
            // success
            buf += "Csound evaluated successfully \n";
            res = true;
        }

        while(cs_test.GetMessageCnt() > 0) {
            buf += std::string(cs_test.GetFirstMessage());
            cs_test.PopFirstMessage();
        }

        console.setAttr("APPEND", buf.c_str());

        cs_test.Stop();
        cs_test.Cleanup();


        return res;
    }

    //std::string cur_data;
    Button ok_button, cancel_button, clear_button;
    CsoundEditor editor;
    Vbox mainbox;
    Hbox button_box;
    Text console;

    Toggle pre_orc_mode, post_orc_mode, pre_score_mode;
    Hbox radio_box;
    Radio mode_selector;

    json global_data;
    int mode_status;
};

#endif // MACRODIALOG_H
