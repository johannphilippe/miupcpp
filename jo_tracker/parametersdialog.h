
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef PARAMETERSDIALOG_H
#define PARAMETERSDIALOG_H

#include"Dialogs/dialog.h"

#include"Text/label.h"
#include"Widgets/spinbox.h"
#include"Widgets/button.h"
#include"Widgets/list.h"

#include"Boxes/vbox.h"
#include"Boxes/hbox.h"

#include"jo_tracker/jdef.h"

#include"Dependencies/nlohmann/json.hpp"
#include"Dependencies/nlohmann/jsonutil.h"

using json = nlohmann::json;

constexpr const char * const ParameterNames[] = {
    "Track_Width_Offset",
    "Track_Width_Multiplier"
};

enum ParameterList {
    TRACK_WIDTH_OFFSET = 0,
    TRACK_WIDTH_MULTIPLIER = 1,
};

namespace JoTrackerParameters {

    static json data = getJsonFromFile(getParametersPath());

    static void save()
    {
        std::ofstream ofs(getParametersPath());
        ofs << JoTrackerParameters::data.dump(4);
        ofs.close();
    }

    static void loadParameters()
    {
        if(MiupUtil::filesystem::isValidNonEmptyFile(getParametersPath())) {
        std::cout << "Parameter dialog -- non empty file" << std::endl;
        std::ifstream ifs(getParametersPath());
        ifs >> JoTrackerParameters::data;
        ifs.close();
        } else { // init param data
        std::cout << "Parameter dialog -- not existing, init" << std::endl;

        JoTrackerParameters::data[ParameterNames[ParameterList::TRACK_WIDTH_OFFSET]] = 15.0;
        JoTrackerParameters::data[ParameterNames[ParameterList::TRACK_WIDTH_MULTIPLIER]] = 0.15;

        std::ofstream ofs(getParametersPath());
        ofs << JoTrackerParameters::data.dump(4);
        ofs.close();
        }
    }


    template<typename T>
    static void setParameter(const char *name, T val )
    {
        data[name] = val;
    }

    template<typename T>
    static T getParameter(const char *name)
    {
        return data[name].get<T>();
    }


    static json getParameters()
    {
        json j;
        if(MiupUtil::filesystem::isValidNonEmptyFile(getParametersPath()))
        {
            std::ifstream ifs(getParametersPath());
            ifs >> j;
            ifs.close();

        } else {
            j[ParameterNames[ParameterList::TRACK_WIDTH_OFFSET]] = 20.0;
            j[ParameterNames[ParameterList::TRACK_WIDTH_MULTIPLIER]] = 0.15;
            std::ofstream ofs(getParametersPath());
            ofs << j.dump(4);
            ofs.close();
        }
        return j;
    }
}


class ParametersDialog : public Dialog
{
public:
    ParametersDialog() :  // for dialogs classes, only init non container widgets, then push it in
        //constructor body, and constructs dialog.
        track_width_offset_label("Track width offset"),
        track_width_offset_spinbox(
            JoTrackerParameters::data[ParameterNames[ParameterList::TRACK_WIDTH_OFFSET]], 0.0, 100.0, 0.1),
        track_width_multiplier_label("Track width multiplier"),
        track_width_multiplier_spinbox(
            JoTrackerParameters::data[ParameterNames[ParameterList::TRACK_WIDTH_MULTIPLIER]], 0.01, 10.0, 0.01),
        okButton("OK"), cancelButton("Cancel")
    {
        buttonBox.push(&okButton, &cancelButton);
        track_width_multiplier_box.push(&track_width_multiplier_label,&fill_param1, &track_width_multiplier_spinbox);
        track_width_offset_box.push(&track_width_offset_label, &fill_param2,&track_width_offset_spinbox);
        main_box.push(&track_width_offset_box, &track_width_multiplier_box, &buttonBox);
        this->construct(&main_box);


        main_box.setAttr("EXPAND","YES");
        track_width_multiplier_box.setAttr("EXPAND","YES");
        track_width_offset_box.setAttr("EXPAND","YES");

        std::cout << "Parameter dialog -- construct dialog" << std::endl;


        okButton.buttonClickOffSig.connect([&](){
            this->hide();

            JoTrackerParameters::setParameter(ParameterNames[ParameterList::TRACK_WIDTH_MULTIPLIER],
                    track_width_multiplier_spinbox.getValue());

            JoTrackerParameters::setParameter(ParameterNames[ParameterList::TRACK_WIDTH_OFFSET],
                    track_width_offset_spinbox.getValue());

            JoTrackerParameters::save();
            JoTrackerParameters::loadParameters();
            buttonClickedSig(true);
        });

        cancelButton.buttonClickOffSig.connect([&](){
            this->hide();
           buttonClickedSig(false);
        });


        std::cout << "Parameter dialog -- end of constructor" << std::endl;

    }


    ~ParametersDialog() {

    }

    lsignal::signal<void(bool)> buttonClickedSig;

private:
    Vbox main_box;

    //track width
    Hbox track_width_offset_box;
    Label track_width_offset_label;
    SpinBox<float> track_width_offset_spinbox;

    Fill fill_param1, fill_param2;

    Hbox track_width_multiplier_box;
    Label track_width_multiplier_label;
    SpinBox<float> track_width_multiplier_spinbox;

    Button okButton, cancelButton;
    Hbox buttonBox;

    json param_data;
};



#endif // PARAMETERSDIALOG_H
