//#include <QCoreApplication>

#include<stdio.h>
#include<stdlib.h>

#include"Core/miup_wrapper.h"
#include"jo_tracker/jo_tracker_wrapper.h"

/** TODO :
--dialog
**/


    MiupSlider *sl; //=new MiupSlider();
    MiupSlider *vertical; //=new MiupSlider("VERTICAL");
void cbk() {
    sl->setValue(0.5);
    vertical->setValue(1);
}


int main(int argc, char *argv[])
{

using namespace std::placeholders; // for std::bind (could also use lambda -> to check)

    const std::string CurDir = MiupUtil::filesystem::getCurrentDirectory();
    Miup::Init(Miup::ModeList::native,argc,argv);

    /*
    std::string csdPath=CurDir + "/../MiupCpp/res/scope_meter.csd";

    CsoundThreaded *cs=new CsoundThreaded();
    cs->Compile(csdPath.c_str());
    cs->setRefreshRate(512);
    */


    JTransport *transport=new JTransport();

    JTrackSet *trackset=new JTrackSet();
    trackset->setAttr("TABTITLE","TrackSet");
    CurvePlot *cp=new CurvePlot();
    Vbox *plotbox=new Vbox(cp);
    plotbox->push(cp->getRadio());
    plotbox->setAttr("TABTITLE","Plot");
    cp->setDomain(1024);

    CsoundEditorBox *editor=new CsoundEditorBox();
    editor->setAttr("TABTITLE","CSOUND EDITOR");

    Tabs *tab=new Tabs(trackset,plotbox,editor);
    Vbox *fbox=new Vbox(transport,tab);
    Dialog dlg(fbox);
    dlg.show();

    Miup::MainLoop();
    Miup::Close();

    return EXIT_SUCCESS;
   // return 0;
}

