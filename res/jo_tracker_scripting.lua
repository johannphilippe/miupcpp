local json = require"json"
local user_score_data = require"score_data"
local jt = {}

jt.score_data = json.parse(user_score_data)

function jt.setNumlin(seq, numlin) 
	local s = tostring(seq)
	if jt.score_data["lignbr"]==nil then jt.score_data["lignbr"]={} end
	jt.score_data["lignbr"][s]=numlin
end

function jt.setNumcol(seq,track,numcol)
	local s = tostring(seq)
	local t = tostring(track)
	if jt.score_data["colnbr"] == nil then jt.score_data["colnbr"]={} end
	if jt.score_data["colnbr"][s] == nil then jt.score_data["colnbr"][s] = {} end
	jt.score_data["colnbr"][s][t]=numcol
end

function jt.setTracknbr(tracknbr)
	jt.score_data["tracknbr"]=tracknbr
end

function jt.getNumlin(seq)
	local s = tostring(seq)
	if jt.score_data["lignbr"] == nil then jt.score_data["lignbr"] = {} end
	if jt.score_data["lignbr"][s] == nil then return nil end
	return jt.score_data["lignbr"][s]
end

function jt.getNumcol(seq,track)
	local s = tostring(seq)
	local t = tostring(track)
	if jt.score_data["colnbr"] == nil then jt.score_data["lignbr"] = {} end
	if jt.score_data["colnbr"][s] == nil then jt.score_data["colnbr"][s] = {} end
	if jt.score_data["colnbr"][s][t] == nil then return nil end
	return jt.score_data["colnbr"][s][t]
end

function jt.getTracknbr() 
	if jt.score_data["tracknbr"]==nil then return nil end
	return jt.score_data["tracknbr"]
end

function jt.setTempoAt(seq, lin, col, val)
	local s = tostring(seq)
	local l = tostring(lin)
	local c = tostring(col)

	if jt.score_data["tempo"] == nil then jt.score_data["tempo"] = {} end
	if jt.score_data["tempo"][s] == nil then jt.score_data["tempo"][s] = {} end
	if jt.score_data["tempo"][s][l] == nil then jt.score_data["tempo"][s][l] = {}
	jt.score_data["tempo"][s][l][c] = tostring(val)
end

function jt.getTempoAt(seq,lin,col)
	local s = tostring(seq)
	local l = tostring(lin)
	local c = tostring(col)

	local result = nil
	if jt.score_data["tempo"] ~= nil and
		jt.score_data["tempo"][s] ~= nil and
		jt.score_data["tempo"][s][l] ~= nil and 
		jt.score_data["tempo"][s][l][c] ~= nil then
		result = jt.score_data["tempo"][s][l][c]
	end
	return result
end

function jt.setAt(seq, tk, lin, col, val)

	if tk > jt.getTracknbr() then jt.setTracknbr(tk) end
	if lin > jt.getNumlin(seq) then jt.setNumlin(seq, lin) end
	if col > jt.getNumcol(seq,tk) then jt.setNumcol(seq,tk, col) end

	local s = tostring(seq)
	local t = tostring(tk)
	local l = tostring(lin)
	local c = tostring(col)

	local value = tostring(val)
	
	if js.score_data["score"] == nil then js.score_data["score"] = {} end
	if jt.score_data["score"][s]==nil then jt.score_data["score"][s]={} end
	if jt.score_data["score"][s][t]==nil then jt.score_data["score"][s][t]={} end
	if jt.score_data["score"][s][t][l]==nil then jt.score_data["score"][s][t][l]={} end
	jt.score_data["score"][s][t][l][c]=value
end


function jt.getAt(seq,tk,lin,col)
	local s = tostring(seq)
	local t = tostring(tk)
	local l = tostring(lin)
	local c = tostring(col)

	local result = nil

	if(jt.tracker_data[s] ~= nil and 
		jt.score_data[s][t] ~= nil and 
		jt.score_data[s][t][l] ~= nil and
		js.score_data[s][t][l][c] ~= nil) then
		
		result = js.score_data[s][t][l][c]
	end
	return result
end

return jt
