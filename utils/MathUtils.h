
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef MATHUTILS_H
#define MATHUTILS_H

#include<iostream>
#include<math.h>
#include<complex>

namespace MathUtils {
static double Hanning(int index, int length) {
    return  0.5 * (1 - cos(2 * M_PI * index / (length - 1 )));
}

static double Hamming(int index, int length) {
    return 0.54 - (0.46 * cos(2 * M_PI * index / (length - 1)));
}

static double Blackman(int index, int length) {
    return 0.42 - (0.5 * cos(2 * M_PI * index / (length - 1))) + (0.08 * cos(4 * M_PI * index / (length - 1)));
}

template<typename T>
static T ScaledLog(T val, T min, T max) {
    return ((log(val) - log(min)) / (log(max) - log(min))) * max;
}

static double BarkBand(int freq)
{
    return (13 * atan(0.00076 * double(freq) )) + (3.5 * atan(pow( double(freq) / 7500, 2)));
}


// Be careful, it is cutoff frequencies. The band 1, ends in cutoff 2. So the index 1 corresponds to start index 0, and width 1.
static constexpr int BarkBandsCutoffFrequencies[] = {
    20, 100,200,300,400,510,630, 770, 920, 1080, 1270, 1480, 1720, 2000, 2320, 2700, 3150, 3700, 4400, 5300, 6400, 7700, 9500, 12000, 15500
};

static constexpr int BarkBandWidths[] = {
  -1, 80, 100, 100, 100, 110, 120, 140, 150, 160, 190, 210, 240, 280, 320, 380, 450, 550, 700, 900, 1100, 1300, 1800, 2500, 3500
};


static int BarkFrequency(double band)
{
    int band_nbr = int(floor(band));
    if(band_nbr == 0) {
        std::cout << "Error, bark band cannot be 0" << std::endl;
        return -1;
    }
    int begin = BarkBandsCutoffFrequencies[band_nbr - 1];
    int width = BarkBandWidths[band_nbr];
    double decimal = band - band_nbr;
    double freq = begin + (decimal * double(width));

    return freq;
}


template<typename T>
static std::complex<T> ComplexCrossProduct(std::complex<T> n1, std::complex<T> n2)
{
    T real = (n1.real() * n2.real()) - (n1.imag() * n2.imag());
    T imag = (n1.real() * n2.imag()) + (n2.real() * n1.imag());
    return std::complex<T>(real, imag);
}



}

#endif // MATHUTILS_H
