
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef CUBICSPLINE_H
#define CUBICSPLINE_H

#include<iostream>
#include<vector>
#include<math.h>


class MatrixProcessing
{

public:

    static int findMax(std::vector<std::vector<double> > &mat, int column, int r) {
        double max = 0;
        int index = 0;
        for(std::size_t i = r+1; i< mat.size(); i++) {
            if(mat[i][column] > max) {
                max = mat[i][column];
                index = i;
            }
        }
        return index;
    }

    static void swapLines( std::vector< std::vector<double> > &mat, int l1, int l2) {
        mat[l1].swap(mat[l2]);
    }

    static void multiplyLine(std::vector<std::vector<double> > &mat, int line, double scalar) {
        if(scalar == 0) return;
        for(std::size_t i = 0; i < mat[line].size(); i++) {
            mat[line][i] *= scalar;
        }
    }

    static void addToLine(std::vector<std::vector<double> > &mat, int line, std::vector<double> to_add) {
        for(std::size_t i = 0; i < to_add.size(); i++) {
                mat[line][i] += to_add[i];
        }
    }

    static std::vector<double> getMultipleOfLine(std::vector<std::vector<double> > &mat, int line, double scalar) {
        std::vector<double> res;
        for(auto & it : mat[line]) {
            res.emplace_back( it * scalar);
        }
        return res;
    }



    static void gaussPivot(std::vector< std::vector<double> > &mat) {
        int r = -1;
        for(std::size_t j = 0; j <mat.size(); j++) {
                int k = findMax(mat, j, r);
                if( mat.size() > std::size_t(k) && mat[k].size() > j && mat[k][j] != 0) {
                    r +=1;
                    multiplyLine(mat, k, (1/mat[k][j]));

                    if(k != r) swapLines(mat, k,r);

                    for(std::size_t i = 0; i <mat.size(); i++) {
                        if(i != std::size_t(r) ) {
                            addToLine(mat, i,
                                      getMultipleOfLine(mat, r, (mat[i][j] * -1))
                                      );
                        }
                    }
                }
        }
    }
};



class CubicSpline
{

public:

    CubicSpline() {

    }

    ~CubicSpline() {

    }

    void print_matrix(std::vector<double> &mat, std::string info) {
        for (auto  it =mat.begin(); it!=mat.end(); ++it) {
            std::cout << info << " : " << *it << std::endl;;
        }
    }

    void print_square_matrix(std::vector< std::vector<double> > &mat, std::string info) {
        for(auto  it = mat.begin(); it!= mat.end(); ++it) {
            std::string s = info + " : ";
            for(auto  c_it = it->begin(); c_it != it->end(); ++c_it) {
                 s += std::to_string(*c_it) + "\t";
            }
            std::cout << s << std::endl;
        }
    }


    void setPoints(std::vector< std::pair<double,double >> &vals) {
        sort(vals.begin(), vals.end(), sort_values);
        for (auto & it : vals) {
             X.emplace_back(it.first);
             Y.emplace_back(it.second);
        }
        n = vals.size();

        print_matrix(X, "Xvals");
        print_matrix(Y,"Yvals");

        calculateIntervals();
        calculateFirstDerivative();
        calculateSquareMatrix();
        calculateAugmentedMatrix();
        calculateColumnM();
        calculateInterpolationTables();
    }

    void calculateIntervals() {
        for(int i = 0; i < n-1; i ++) {
          h.emplace_back( X[i+1] - X[i]);
        }
        print_matrix(h, "Intervals");
    }

    void calculateFirstDerivative() {
        F.emplace_back(0);
        for(int i = 1; i < n-1; i++) {
            double v = ( ( Y[i+1] - Y[i] ) / h[i] ) - ( ( Y[i] - Y[i-1] / h[i-1] ));
            F.emplace_back(v);
        }
        F.emplace_back(0);
        print_matrix(F, "First derivative");
    }

    void calculateSquareMatrix() {
        R.resize(n);
        for(int l = 0; l<n; l++) {
            for(int c = 0; c<n; c++) {
                R[l].emplace_back(0);
            }
        }
        R[0][0] = 1.0;
        R[n-1][n-1]=1.0;

        for(int l = 1; l < n-1; l++) {
            R[l][l] = ( ( h[l-1] + h[l] ) / 3 );
            R[l][l+1] = ( h[l] / 6);
            R[l][l-1] = ( h[l-1] / 6);
        }

        print_square_matrix(R, "Square Matrix R");
    }


    void calculateAugmentedMatrix() {
        R_id = R;
        MatrixProcessing::gaussPivot(R_id);

        print_square_matrix(R_id, "Identity matrix");


        for(int l = 0; l < n ; l++) {
            for(int c = 0; c < n ; c++) {
                R[l].emplace_back(R_id[l][c]);
               // std::cout << "Adding at  index : " << R[l].size() << " value : " << R_id[l][c] << std::endl;
               // std::cout << "Now value at : " << R[l].size() << "  is : " << R[l][c+n] << std::endl;
            }
        }


        print_square_matrix(R, "Augmented matrix");
        MatrixProcessing::gaussPivot(R);

        R_inv.resize(n);
        for(std::size_t l = 0; l < R.size(); l++) {
            for(int c = 0; c < n; c++) {
                R_inv[l].emplace_back(R[l][c+n]);
            }
        }
        print_square_matrix(R_inv, "Inverted matrix");

    }


    void calculateColumnM() {
        for(int l =0; l<n;l++) {
            double val = 0.0;
            for(std::size_t c = 0; c<F.size(); c++) {
                val += R_inv[l][c] * F[c];
            }
            M.emplace_back(val);
        }


        print_matrix(M, "M matrix");
    }

    void calculateInterpolationTables() {
        for(int l=0; l<n-1; l++) {
                double Cval = ( ( Y[l+1] - Y[l]  ) / h[l] ) - ( ( h[l]/6 ) * ( M[l+1] - M[l] ) );
                double C_pval = Y[l] - (M[l] * (pow(h[l],2)/6 ) );
                C.emplace_back(Cval);
                C_p.emplace_back(C_pval);
        }
        print_matrix(C, "C interp table");
        print_matrix(C_p, "C_p interp table");
    }

    double getInterpolationPoint(double x, int k) {

       // std::cout << "X value is : " << x << " and k is " << k << std::endl;
        /*
        int k =0;

        for(int l = 0; l < n - 1 ; l++) {
            if(x> X[l] && x < X[l+1]) {
                k = l ;
                break;
            }
        }
        */

        double res = ( M[k] * (pow( ( X[k+1] -x ), 3 ) / ( 6 * h[k]) ) ) +
                ( M[k+1] * (pow(( x-X[k]), 3 ) / 6 * h[k])) +
                (C[k] * (x-X[k])) +
                (C_p[k]);

       // std::cout << "Result point is " << res << std::endl;
        return res;
    }


private:




    static bool sort_values(std::pair<double,double> first, std::pair<double,double> sec) {
        return (first.first < sec.first);
    }

    std::vector< std::pair<double,double> > vals;
    std::vector<double> X,Y, h, F, M, C, C_p;
    std::vector < std::vector < double > > R, R_id, R_inv;

    int n;
};


#endif // CUBICSPLINE_H
