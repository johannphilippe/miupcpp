
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/
#include "luaeditor.h"

LuaEditor::LuaEditor() : CodeEditor(), lua(nullptr)
{
    IUP_CLASS_INITCALLBACK(obj, JScripterView);
    IUP_CLASS_SETCALLBACK(obj, "MAP_CB", map_cb);
    initEditorAttributes();
}

void LuaEditor::initEditorAttributes()
{

    this->setAttr("LEXERLANGUAGE","lua");

    this->setAttr("KEYWORDS0",lua_keywords.c_str());
    this->setAttr("KEYWORDS1", lua_secondary_keywords.c_str());


    this->setAttr("STYLEFONT32","Consolas");
    this->setAttr("STYLEFONTSIZE32","11");
    this->setAttr("STYLECLEARALL","YES");

    this->setAttr("STYLEFGCOLOR1","0 128 0");
    this->setAttr("STYLEFGCOLOR2","0 128 0");
    this->setAttr("STYLEFGCOLOR3","128 0 0");
    this->setAttr("STYLEFGCOLOR5","0 0 255");
    this->setAttr("STYLEFGCOLOR6","160 20 20");
    this->setAttr("STYLEFGCOLOR7","128 0 0");
    this->setAttr("STYLEFGCOLOR9","0 0 255");
    this->setAttr("STYLEFGCOLOR10","255 0 255");

    this->setAttr("STYLEFGCOLOR13","255 0 0");
    this->setAttr("STYLEFGCOLOR14","128 0 128");
    this->setAttr("STYLEFGCOLOR15", "0 128 128");
    this->setAttr("STYLEFGCOLOR16", "128 128 0");

    this->setAttr("STYLEITALIC5","YES");
    this->setAttr("STYLEBOLD10","YES");
    this->setAttr("STYLEHOTSPOT6","YES");
    this->setAttr("MARGINWIDTH0","50");

    this->setAttr("PROPERTY","fold=1");
    this->setAttr("PROPERTY","fold.compact=0");
    this->setAttr("PROPERTY","fold.comment=1");
    this->setAttr("PROPERTY","fold.preprocessor=1");

    this->setAttr("MARGINWIDTH1","20");
    this->setAttr("MARGINTYPE1","SYMBOL");
    this->setAttr("MARGINMASKFOLDERS1","Yes");
    this->setAttr("MARKDEFINE","FOLDER=PLUS");
    this->setAttr("MARKDEFINE","FOLDEROPEN=MINUS");
    this->setAttr("MARKDEFINE","FOLDEREND=EMPTY");
    this->setAttr("MARKDEFINE","FOLDERMIDTAIL=EMPTY");
    this->setAttr("MARKDEFINE","FOLDEROPENMID=EMPTY");
    this->setAttr("MARKDEFINE","FOLDERSUB=EMPTY");
    this->setAttr("MARKDEFINE","FOLDERTAIL=EMPTY");
    this->setAttr("FOLDFLAGS","LINEAFTER_CONTRACTED");
    this->setAttr("MARGINSENSITIVE1","YES");

    this->setAttr("FONT","DejaVu, 10");


}

int LuaEditor::map_cb(Ihandle */*ih*/)
{
   //initEditorAttributes();

    return IUP_DEFAULT;
}

void LuaEditor::execute()
{
    if(lua != nullptr) delete lua;
    lua = nullptr;

    lua = new Lua();

    registerSig(lua);

    std::string text = this->getAttrStr("VALUE");

    if(lua->loadString(text)) {
        std::cout << "Lua evaluated successfully \n " << lua->toString(-1).c_str() << std::endl;
    } else {
        //std::cout << "Error in lua evaluation \n" << lua->toString(-1).c_str() << std::endl;
    }

    scriptedDoneSig();
}
