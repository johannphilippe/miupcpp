
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef MIUPDATATYPE
#define MIUPDATATYPE

#include<functional>
#include<assert.h>
#include<queue>
#include<mutex>
#include<condition_variable>
#include<list>

//Csound Callback data structures
struct callback_method_data {
    callback_method_data(const char *channel,std::function<void(double)> m) {
        channelName=channel;
        method=m;
    }
    const char *channelName;
    std::function<void(double)> method;
};

struct callback_method_queue_data {
    callback_method_queue_data(double val,std::function<void(double)> m) {
        value=val;
        method=m;
    }
    double value;
    std::function<void(double)> method;
};

//Generic Value
//from https://stackoverflow.com/questions/25107807/list-of-template-classes-of-different-types
class Attribute {
private:
    struct Head {
        virtual ~Head() {}
        virtual void *copy() = 0;
        const std::type_info& type;
        Head(const std::type_info& type): type(type) {}
        void *data() { return this + 1; }
    };
    template <class T> struct THead: public Head {
        THead(): Head(typeid(T)) {}
        virtual ~THead() override { ((T*)data())->~T(); }
        virtual void *copy() override {
            return new(new(malloc(sizeof(Head) + sizeof(T)))
                THead() + 1) T(*(const T*)data()); }
    };
    void *data;
    Head *head() const { return (Head*)data - 1; }
    void *copy() const { return data ? head()->copy() : nullptr; }
public:
    Attribute(): data(nullptr) {}
    Attribute(const Attribute& src): data(src.copy()) {}
    Attribute(Attribute&& src): data(src.data) { src.data = nullptr; }
    template <class T> Attribute(const T& src): data(
      new(new(malloc(sizeof(Head) + sizeof(T))) THead<T>() + 1) T(src)) {}
    ~Attribute() {
        if(!data) return;
        Head* head = this->head();
        head->~Head(); free(head); }
    bool empty() const {
        return data == nullptr; }
    const std::type_info& type() const {
        assert(data);
        return ((Head*)data - 1)->type; }
    template <class T>
      T& value() {
        if (!data || type() != typeid(T))
            throw std::bad_cast();
        return *(T*)data; }
    template <class T>
      const T& value() const {
        if (!data || type() != typeid(T))
            throw std::bad_cast();
        return *(T*)data; }
    template <class T>
      void setValue(const T& it) {
        if(!data)
            data = new(new(malloc(sizeof(Head) + sizeof(T)))
                THead<T>() + 1) T(it);
        else {
            if (type() != typeid(T)) throw std::bad_cast();
            *(T*)data = it; }}
};

//Concurrent queue from CsoundThreaded by Michael Gogins
template<typename Data>
class concurrent_queue
{
private:
    std::queue<Data> queue_;
    std::mutex mutex_;
    std::condition_variable condition_variable_;
public:
    void push(Data const& data)
    {

        std::unique_lock<std::mutex> lock(mutex_);
            queue_.push(data);
            lock.unlock();
            condition_variable_.notify_one();
    }
    bool empty()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        return queue_.empty();
    }
    bool try_pop(Data& popped_value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        if (queue_.empty()) {
            lock.unlock();
            return false;
        }
            popped_value = queue_.front();
            queue_.pop();
            lock.unlock();
            return true;
    }

    void wait_and_pop(Data& popped_value)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        while (queue_.empty()) {
            condition_variable_.wait(lock);
        }
        popped_value = queue_.front();
        queue_.pop();
    }

    void notify() {
        condition_variable_.notify_all();
    }

    std::size_t size() {
        std::unique_lock<std::mutex> lock(mutex_);
        int s=queue_.size();
        condition_variable_.notify_all();
        return s;
    }
};




struct CallbackData
{
    CallbackData(std::string name_,
                 std::function<void()> callback_,
                 unsigned int granu_ = 1) :
        count(granu_),
        granularity(granu_),
        name(name_),
        callback(callback_)
    {
    }




    void call()
    {
        if(count == granularity)
        {
            this->callback();
        }
        if(granularity == 1 ) return;
        count++;
        if(count > granularity) count = 1;
    }

    unsigned int count;
    unsigned int granularity;
    std::string name;
    std::function<void()> callback;
};


class ConcurrentCallbackMap
{

public:
    ConcurrentCallbackMap()
    {

    }

    ~ConcurrentCallbackMap() {
    }


    void registerCallback(CallbackData data) {
        list.push_back(data);
    }

    void unregisterCallback(std::string name_)
    {
        for(std::size_t i = 0; i < list.size(); i++) {
            if(list[i].name == name_) {
               list.erase(list.begin() + i);
            }
        }
    }

    void callAll()
    {
        for(auto & it : list) {
            it.call();
        }
    }


    void clear()
    {
        list.clear();
    }

private:
    std::vector<CallbackData> list;
    std::mutex mutex_;
    std::condition_variable condition_variable_;
};


#endif // MIUPDATATYPE

