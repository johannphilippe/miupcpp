
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#include"miuptimer.h"

    MiupTimer::MiupTimer() //: MiupObj(IupTimer())
    {
        obj = IupTimer();
        IUP_CLASS_INITCALLBACK(obj, MiupTimer);
        IUP_CLASS_SETCALLBACK(obj, "ACTION_CB",action_cb);
    }

    MiupTimer::MiupTimer(int ms) //: MiupObj(IupTimer())
    {
        obj=IupTimer();
        setDuration(ms);
        IUP_CLASS_INITCALLBACK(obj, MiupTimer);
        IUP_CLASS_SETCALLBACK(obj, "ACTION_CB",action_cb);
    }

    MiupTimer::MiupTimer(const char *ms)
    {
        obj = IupTimer();
        IupSetAttribute(obj, "TIME",ms);
        IUP_CLASS_INITCALLBACK(obj, MiupTimer);
        IUP_CLASS_SETCALLBACK(obj, "ACTION_CB",action_cb);
    }

    MiupTimer::~MiupTimer() {}

    void MiupTimer::setDuration(int ms) {
        setAttrInt("TIME",ms);
    }

    void MiupTimer::start() {
        std::cout << "run timer " << std::endl;
        setAttr("RUN","YES");
        std::cout << " isrunning : " << isRunning() << std::endl;
    }

    void MiupTimer::stop() {
        setAttr("RUN","NO");
    }

    bool MiupTimer::isRunning() {
        return (getAttrInt("WID") == -1 ) ? false : true;
    }

    int MiupTimer::action_cb(Ihandle *)
    {
        timerSig();
        return IUP_DEFAULT;
    }
