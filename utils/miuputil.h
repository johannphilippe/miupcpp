
/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef MIUPUTIL
#define MIUPUTIL

#include<iostream>
#include<cstring>
#include<sstream>
#include<fstream>
#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>
#include"iup.h"
#include"Core/miupobj.h"
#include<vector>
#include<functional>

#include<sys/stat.h>
#include<sys/types.h>
#include<list>

//Use UNUSED to mark an argument as unused (or just omit its name)
#define UNUSED(expr) do { (void)(expr); } while(0)

#ifdef _WIN32
#include"io.h"
#elif __linux__
#include<unistd.h>
#endif


#include"Dependencies/nlohmann/json.hpp"

#ifdef _WIN32
    #include<windows.h>
    #include<direct.h>
#endif

namespace MiupUtil {
//STRING FUNCTIONS (CONVERSIONS FROM C++ TO C string, C to NUMBER ... etc)



template<typename T>
static void debug(T arg)
{
 #ifndef QT_DEBUG
    return;
#endif
    std::cout << arg << std::endl;
}

template<typename T, typename ... Args>
static void debug(T arg, Args ... args)
{
 #ifndef QT_DEBUG
    return;
#endif
    std::cout << arg << " ";
    debug(args...);
}

namespace clipboard {
static void copyToClipBoard(const char *text) {
    Ihandle *clipboard = IupClipboard();
    IupSetAttribute(clipboard, "TEXT", text);
    IupDestroy(clipboard);
}

static const char *getFromClipBoard() {
    Ihandle *clipboard = IupClipboard();
    const char *text = IupGetAttribute(clipboard, "TEXT");
    IupDestroy(clipboard);
    return text;
}

}

namespace number {

template<typename T>
static int getNumberPrecision(T number)
{
    std::stringstream ss;
    ss << number;

    int cnt = 0;
    bool isDecimal = false;
    int memo = 0;;

    for(auto & it : ss.str() )
    {
        if(isDecimal)
        {
            if(it == '0') {
                memo++;
            } else {
                cnt++;
                cnt += memo;
                memo = 0;
            }
        }
        if(it == '.') isDecimal=true;
    }
    return cnt;
}

}

namespace color {
    static std::string getRandomColorString()
    {
        int r = rand() %256;
        int g = rand() %256;
        int b = rand() %256;
        std::string res = std::to_string(r) + " " + std::to_string(g) + " " + std::to_string(b);
        return res;
    }

}

namespace string {
//convert any number to a C string


template<typename T>
static std::string getStringFromNumberAutoPrecision(T val)
{
    std::stringstream ss;
    ss << val;

    int precision = 0;
    bool isDecimal = false;
    int memo = 0;;

    for(auto & it : ss.str() )
    {
        if(isDecimal)
        {
            if(it == '0') {
                memo++;
            } else {
                precision++;
                precision += memo;
                memo = 0;
            }
        }
        if(it == '.') isDecimal=true;
    }

    ss.clear();

    ss.precision(precision);
    ss << val;
    return ss.str();


}

template<typename T>
static std::string getString(T val, int precision = 5) {
        std::ostringstream ss;
        ss.precision(precision);
        ss << val;
        return ss.str();
}

template<typename T> // to remove, causing memory leak
static const char *getCString(T val,int precision=5) {
    std::stringstream ss;
    ss.precision(precision);
    ss << val;
    char *cstr=new char[ss.str().length()+1];

    strcpy(cstr,ss.str().c_str());
    return cstr;
    //return getString<T>(val,precision).c_str();
}

//returns the size of a const char *
static int getCStringSize(const char *str)
{
    int size=0;
    while(str[size]!='\0') size++;
    return size;
}

static std::string removeCharacterFromString(std::string str,char ch)
{
    std::cout << "Input string = " << str << std::endl;
    std::string res("");
    for(std::size_t i = 0; i < str.size(); i++) {
        if(str[i] != ch) res += str[i];
    }
    return res;
}

static std::string removeCharactersFromString(std::string str, std::string chars)
{
    std::cout << "Input string = |" << str << "|" <<  std::endl;

    std::string res("");

    bool cp = true;
    for(std::size_t i = 0; i < str.size(); i++) {
        std::cout << "iterate on : " << str[i] << std::endl;
        cp = true;
        for(std::size_t c = 0; c < chars.size(); c++) {
            std::cout << "compare str char |" << str[i] << "| and chs |" << chars[c] << "|" << std::endl;
            if(str[i] == chars[c])
            {
                std::cout << "matched-- no copy" << std::endl;
                cp=false;
                break;
            }
        }
        if(cp) res += str[i];
    }
    std::cout << "res = |" << res << "|" << std::endl;
    return res;

}

//convert string to char * (not const) -> const_cast
static char *getCharArrayFromString(std::string s) {
    char *ch=const_cast<char *>(s.c_str());
    return ch;
}
//iterate over characters of a string to return a char * (not const) -> copy
static char *iterateChar(std::string s) {
    char *ch=new char[s.size()+1];
    for (unsigned int i=0;i<s.size();i++) {
        ch[i]=s.at(i);
    }
    ch[s.size()]='\0';
    return ch;
}

//get a Number from a char * (set precision)
template<typename T> static T getNumber(const char *val,int precision=5) {
    std::stringstream ss(val);
    ss.precision(precision);
    T number;
    ss >> number;
    return number;
}

template<typename T> static T getNumber(std::string val, int precision =5) {
    return getNumber<T>(val.c_str(), precision);
}

} // namespace string

// FILESYSTEM FUNCTIONS -- MANAGING FILES / DIRS
namespace filesystem {

    // verify the presence of backslash, and convert it to slash
    static std::string toUnixPath(std::string winpath) {
        std::string res=winpath;
        for(std::size_t i =0; i< res.size(); i++) {
            if(res[i]== '\\') res[i] = '/';
        }

        return res;
    }

    static std::string removeDouble(std::string path, char to_remove) {
            std::string res("");

            char last = ' ';
            for(std::size_t i = 0; i < path.size(); i++)
            {
                if(last == to_remove && path[i] == to_remove) {
                    continue;
                } else {
                    res += path[i];
                }

                last = res[res.size() - 1];
            }

            return res;

    }

    static std::string removeDoubleSlash(std::string path)
    {
            return removeDouble(path, '/');
    }


    static std::string remouveDoubleBackslash(std::string path)
    {
            return removeDouble(path, '\\');
    }

    static std::string getCurrentDirectory() {
        char cwd[100];
        getcwd(cwd,sizeof(cwd));
        std::string ret(cwd);
        return ret;
    }

    static std::string getHomeDirectory() {
        std::string HOME("");

#ifdef _WIN32
        std::string temp = std::string(std::getenv("HOMEDRIVE")) + std::string(std::getenv("HOMEPATH"));
        HOME = toUnixPath(temp);
#elif __linux__
       HOME = std::getenv("HOME") ? std::getenv("HOME") : ".";
#endif


        return HOME;
    }

    static std::string getEnvironnementVariable(const char *var) {
        std::string ENV = std::getenv(var) ? std::getenv(var) : ".";
        return ENV;
    }

    static std::string getEnvironnementVariable(std::string var) {return getEnvironnementVariable(var.c_str());}

    static bool isValidFilePath(const char *path) {
        return (access(path,0) !=-1);
    }

    static bool isValidFilePath(std::string path) {return isValidFilePath(path.c_str());}

    static int createDirectory(const char *path)
    {
        int nError=0;
#ifdef _WIN32
        nError=_mkdir(path);
#else
        mode_t nMode=0733;
        nError=mkdir(path,nMode);
#endif
        return nError;
    }

    static void removeFile(std::string path)
    {
        std::remove(path.c_str());
    }

    static bool removeRecursive(std::string path)
    {
        struct dirent *entry;
        DIR *dir = opendir(path.c_str());
        if(dir == NULL) return false;
        while((entry = readdir(dir)) != NULL) {
            std::string full_path = path + "/" + entry->d_name;
            removeFile(full_path);
        }


        return true;
    }


    static std::vector<std::string> listFilesInDirectory(std::string path)
    {
        struct dirent *entry;
        DIR *dir = opendir(path.c_str());
        std::vector<std::string> res;
        if(dir == NULL) return res;
        while((entry = readdir(dir)) != NULL) {
            std::string full_path = path + "/" + entry->d_name;
            res.push_back(full_path);
        }
        return res;
    }


    static void copyFile(std::string src_path, std::string dest_path) {
                std::ifstream src(src_path, std::ios::binary);
                std::ofstream dest(dest_path, std::ios::binary);
                dest << src.rdbuf();
                src.close();
                dest.close();
    }

    static bool isValidNonEmptyFile(std::string path) {
        if(! isValidFilePath(path)) return false;
        std::ifstream sz(path, std::ios::binary | std::ios::ate);
        int size = sz.tellg();
        sz.close();
        return (size <= 0) ? false : true;
    }

    static int createDirectory(std::string path) {
        return createDirectory(path.c_str());
    }

    static std::string getFilenameFromPath(std::string path) {
        std::size_t pos = path.find_last_of("/");
        if(pos == path.npos) {return path;}
        return path.substr(pos + 1);
    }

    static std::string getFolderFromPath(std::string path) {
        std::size_t pos = path.find_last_of("/");
        if(pos == path.npos) return path;
        return path.substr(0,pos);
    }


    static std::string setPathExtension(std::string path, std::string extension) {
        std::string ext = (extension.find_last_of(".") == extension.npos) ?  ("." + extension) : extension;
        std::string fpath = path;
        std::size_t ext_pos = path.find_last_of(".");
        if(ext_pos == fpath.npos) {
            fpath += ext;
        } else {
            std::string cur_ext = fpath.substr(ext_pos);
            if(cur_ext != ext) {
                        fpath = fpath.substr(0,ext_pos -1) + ext;
            }
        }
        return fpath;
    }

    static bool verifyExtension(std::string path, std::string extension) {
        std::string ext = (extension.find_last_of(".") == extension.npos) ? ("." + extension) : extension;
        std::size_t  pos = path.find_last_of(".");
        if( pos==path.npos) {
            if(extension.size()==0) return true;
            else return false;
        } else {
            std::string cur_ext = path.substr(pos);
            if(cur_ext == ext) return true;
            else return false;
        }
        return false;
    }

    static std::pair<std::string, std::string> splitNameAndExtension(std::string path) {
        std::pair<std::string, std::string> res;
        res.first = path.substr(0, path.find_last_of("."));
        res.second = path.substr(path.find_last_of("."), path.npos);
        return res;
    }

    static std::string join(std::vector<std::string> list) {
        std::string res;
        for(auto & it : list) {
#ifdef __linux__
        res += it + "/";
#elif _WIN32
        res += it + "\\";
#endif
        }

#ifdef __linux__
        res = removeDoubleSlash(res);
        if(res.back() == '/') res = res.substr(0, res.find_last_of("/"));
#elif _WIN32
        res = removeDoubleBackslash(res);
        if(res.back() == '\') res = res.substr(0, res.find_last_of("\\"));
#endif
        return res;
    }

    static int createDirectoryRecursively(std::string path)
    {

        if(MiupUtil::filesystem::isValidFilePath(MiupUtil::filesystem::getFolderFromPath(path))) {
            MiupUtil::filesystem::createDirectory(path);
            return 0;
        }

        std::size_t fpos = path.find_first_of("/");
        std::string first("");

        while(fpos != path.npos) {
            first += path.substr(0, fpos + 1);
            path = path.substr(fpos + 1, path.size());

            if(!MiupUtil::filesystem::isValidFilePath(first)) {
                MiupUtil::filesystem::createDirectory(first);
            }

            fpos = path.find_first_of("/");
        }

        MiupUtil::filesystem::createDirectory(path);

        return 0;
    }
}

namespace json {

using json = nlohmann::json;
static bool findObjects(json &data, std::list<std::string> list) {
    for(auto & it : list ) {
        if(data.find(it) == data.end()) {
            return false;
        }
    }
    return true;
}

} // json

    //GENERAL UTILITY FUNCTIONS -- TEMPLATED PRINT

    template<typename T>
    static void print(T arg) {
        std::cout << arg << "\n" << std::endl;
    }

    template<typename T, typename ... Args>
    static void print(T arg,Args...args) {
        std::cout << arg << "\t";
        print(args...);
    }


} // namespace miuputil

#endif // MIUPUTIL

