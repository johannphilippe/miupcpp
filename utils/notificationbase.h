

/*
    Copyright (C) 2019-2020 Johann Philippe

    This file is part of MIUP.

    The MIUP library is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    MIUP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with MIUP; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.
*/

#ifndef NOTIFICATIONBASE_H
#define NOTIFICATIONBASE_H

#include<iostream>
#include<fstream>
#include<functional>
#include<unordered_map>
#include"miuputil.h"

#ifdef __linux__
#include"gio/gio.h"
#elif _WIN32
#elif __APPLE__
#endif
//dans l'ordre :
//-essayer de rendre l'action locale
//-faire passer une variante
static std::unordered_map<const char *, std::pair<const char *, std::function<void()> > > action_list;

static void action (GSimpleAction *a, GVariant *variant, gpointer gp) {
    gsize size=g_variant_get_size(variant);
    const gchar *param=g_variant_get_string(variant, &size);
    std::cout << "param : " << param << std::endl;
    const gchar *aname=g_action_get_name(a);
    std::cout << "action name " << aname <<std::endl;
    g_object_unref(gp);
    g_free(gp);
}

    static GActionEntry actions[]= {
       {"action",action,"s",NULL,NULL},
    };

class Notification {
public:
    Notification(const char *name, const char *text, const char *path) :
        notif_name(std::string(name)), notif_text(std::string(text)),icon_path(std::string(path))
    {
    }
    ~Notification() {}

    void setName(const char *name) {notif_name=std::string(name);}
    void setText(const char *text) {notif_text=std::string(text);}
    void setIconPath(const char *path) {icon_path=std::string(path);}

    void pushAction(const char *name, std::function<void()> action) {
        action_list[notif_name.c_str()]=std::make_pair(name,action);
        action_name_list.push_back(name);
    }

    int start() {

#ifdef __linux__
        const std::string home=MiupUtil::filesystem::getHomeDirectory();
        const std::string desktop_path=home + "/.local/share/applications/org.gnome.miup_notif.desktop";
        const std::string desktop_extension = "org.gnome.miup_notif.desktop";

    if(g_application_id_is_valid(desktop_extension.c_str()) &&
            MiupUtil::filesystem::isValidFilePath(desktop_path.c_str()))  {
        std::cout << "valid id" << std::endl;
    }
    else {
        std::cout << "not valid id" << std::endl;
        createLinuxApplicationFile(desktop_path);
    }

    GApplication *app;
    GNotification *notification;
    GFile *file;
    GIcon *icon;

    app=g_application_new("org.gnome.miup_notif.desktop",G_APPLICATION_FLAGS_NONE);
    g_application_register(app,NULL,NULL);
    g_signal_connect(app,"activate",G_CALLBACK(activate),NULL);

    notification=g_notification_new("hello notif");
    g_notification_set_body(notification,"Today, we have pancakes and salad and fruits");

    //set icon file
    file=g_file_new_for_path("/home/johann/Images/whatsapp-logo.png");
    icon=g_file_icon_new(file);
    g_notification_set_icon(notification,G_ICON (icon));
    g_object_unref(file);

    //add action
  //  g_notification_add_button_with_target(notification,"action","app.action",G_VARIANT_CLASS_STRING,desktop_extension.c_str());



    g_action_map_add_action_entries (G_ACTION_MAP (app),actions, G_N_ELEMENTS (actions),app);


    for(std::vector<const char *>::iterator it=action_name_list.begin();it!=action_name_list.end();++it) {
            gchar *parameter=g_strdup_printf("%s:%s",notif_name.c_str(), *it);
            std::cout << parameter << std::endl;
            g_notification_add_button_with_target(notification, *it, "app.action","s" ,parameter);
    }

    //title of notif ?
    g_application_send_notification(app,"lunch is ready",notification);
    int status=g_application_run(G_APPLICATION(app),NULL,NULL);
    g_object_unref(icon);
    g_object_unref(notification);
    //g_object_unref(app);

    //g_free(app);
    //delete app;
    return status;

#elif _WIN32
#elif __APPLE__
#endif
    }

protected:

private:
    //private methods
    void createLinuxApplicationFile(std::string desktop_path) {
        const std::string desktop_file="[Desktop Entry] \nType=Application\nEncoding=UTF-8\nName=MIUP_Notification\nComment=Sample\nTerminal=false\n";
        std::ofstream ofs(desktop_path);
        ofs << desktop_file;
        ofs.close();
    }

    static void activate(GApplication *app, gpointer user_data);

    void action (GSimpleAction * /*a*/, GVariant * /*variant*/, gpointer gp) {
       std::cout << "coucou" << std::endl;
       g_object_unref(gp);
       g_free(gp);
    }

    //members
    //std::unordered_map<  const char *, std::function<void()> > action_list;

    std::string notif_name, notif_text,icon_path;
    std::vector<const char *> action_name_list;
};

void Notification::activate (GApplication * /*app*/, gpointer /*user_data*/) {
}

#endif // NOTIFICATIONBASE_H
